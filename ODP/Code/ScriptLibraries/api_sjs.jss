/**
 * Get path info without any slashes
 */
function getPathName() { 
      // setup our object by getting refs to the request and servlet objects         
      try {
    	  var externalContext = facesContext.getExternalContext();
          this.request = externalContext.getRequest(); 
          var pathInfo = this.request.getPathInfo();
          return pathInfo;
      } catch(e) { 
    	  print (e.message); 
      } 
}






