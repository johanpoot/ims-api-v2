package com.primaxis.ims.model;

import java.io.File;

/**
 * Attachment class model to manage attachments related data
 * 
 * @author BKarwal
 *
 */
public class Attachment  {
	
	private String id = null;
	private File file = null;
	private String name = null;
	private String extension = null;
	private String absolutePath = null;
	

	public Attachment(File file) {
		this.file = file;
		getAttachment();
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the extension
	 */
	public String getExtension() {
		if (name != null) {
			int i = name.lastIndexOf('.');
			int p = Math.max(name.lastIndexOf('/'), name.lastIndexOf('\\'));
			if (i > p) {
			    extension = name.substring(i+1);
			}
		}
		return extension;
	}

	/**
	 * @param extension the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	private void getAttachment() {
		if (file != null) {
			name = file.getName();
			extension = getExtension();
			absolutePath = file.getAbsolutePath();
		}
	}

	/**
	 * @return the absolutePath
	 */
	public String getAbsolutePath() {
		return absolutePath;
	}

	/**
	 * @param absolutePath the path to set
	 */
	public void setAbsolutePath(String path) {
		this.absolutePath = path;
	}
}
