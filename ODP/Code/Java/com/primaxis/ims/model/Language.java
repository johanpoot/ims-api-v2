package com.primaxis.ims.model;

import org.json.simple.JsonArray;

import com.primaxis.ims.controller.LanguageController;
import com.primaxis.ims.utils.Constants;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.ImsHomeDb;
import com.primaxis.ims.utils.Log;

import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;

public class Language {
	private ImsHomeDb imsHomeDb;
	private LanguageController languageController;
	
	public Language(LanguageController languageController, Session session) {
		try {
			imsHomeDb = new ImsHomeDb(session);
			this.languageController = languageController;
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}
	
	/**
	 * Get all languages
	 * @return
	 */
	public JsonArray getAll() {
		try {
			JsonArray languages = new JsonArray();
			View viewLanguages = imsHomeDb.getView(Constants.viewLanguageCodes);	
			if (viewLanguages != null &&  viewLanguages.getAllEntries().getCount() > 0) {
				languages = Globals.getViewData(viewLanguages);	
				return languages;			
			}
		} catch (Exception e) {
			languageController.isInValid = true;
			languageController.httpResponse = 503;
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		

		}
		return null;
	}
}
