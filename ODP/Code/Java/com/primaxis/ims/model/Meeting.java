package com.primaxis.ims.model;

import com.primaxis.ims.controller.MeetingController;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.ImsHomeDb;
import com.primaxis.ims.utils.KeywordHandler;
import com.primaxis.ims.utils.Log;

import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;

public class Meeting {
	
	//private ImsDataDb dataDb;
	private ImsHomeDb imsHomeDb;
	private MeetingController meetingController;
	private Session session;

	private String id; //six digit number
	private Document doc = null;
	private String form = "MEETING";
	private String formPrefix = form + "_";
	private String bookingDockey = null;
	private String language;
	private String languageCode;

	public Meeting(MeetingController meetingController, Session session) throws NotesException {
		this.meetingController = meetingController;
		this.session = session;
		try {
			imsHomeDb = new ImsHomeDb(session);
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}

	public void getMeeting(String id) {
		try {
			if (id != null && !id.isEmpty()) {
				doc = imsHomeDb.getMeetingDoc(id);
				if (doc != null) {
					if (doc.getItemValueString(formPrefix + "Id") != null) {
						setId(doc.getItemValueString(formPrefix + "Id"));
					}
					if (doc.getItemValueString(formPrefix + "DocKey") != null) {
						setBookingDockey(doc.getItemValueString(formPrefix + "DocKey"));
					}
					if (doc.getItemValueString(formPrefix + "Language") != null) {
						setLanguage(doc.getItemValueString(formPrefix + "Language"));
					}
					if (doc.getItemValueString(formPrefix + "LanguageCode") != null) {
						setLanguageCode(doc.getItemValueString(formPrefix + "LanguageCode"));
					}
				}
			}
		} catch (NotesException e) {
			Globals.printException(e);
		}
	}

	public String getFormPrefix() {
		return formPrefix;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String meetingId) {
		this.id = meetingId;
	}

	public Document getDoc() {
		return doc;
	}

	public void setDoc(Document doc) {
		this.doc = doc;
	}

	public String getForm() {
		return form;
	}

	/**
	 * @return the bookingDockey
	 */
	public String getBookingDockey() {
		return bookingDockey;
	}

	/**
	 * @param bookingDockey the bookingDockey to set
	 */
	public void setBookingDockey(String bookingDockey) {
		this.bookingDockey = bookingDockey;
	}

}
