package com.primaxis.ims.model;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import org.json.simple.*;

import com.primaxis.ims.utils.Constants;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.ImsHomeDb;
import com.primaxis.ims.utils.KeywordHandler;
import com.primaxis.ims.utils.Log;

import lotus.domino.Agent;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;
import lotus.domino.ViewEntryCollection;

public class User {
	private String docKey;
	private String universalId;
	private String title;
	private String firstName;
	private String lastName;
	private String fullName;
	private String email;
	private String gender;
	private String mobile;
	private String phone;
	private String pager;
	private String primaryRole;
	private String secondaryRole;
	private String defaultRole;
	private String deviceId;
	private String deviceOs;
	private String form;
	private String formPrefix;
	private String nowDateTimeFormatted;
	private String globalDateFormat;
	private String globalTimeFormat;
	private String nowDateFormatted;
	private String language;
	private String providerDocKey;
	private String providerName;
	private String accreditationLevel;
	private String lspId; //USER_LSP_ID and in booking - BOOK_Agency_Interpreter_Number
	private DateTime nowDateTime;
	private Document doc;
	private Vector providerDocKeys;
	private Vector providerNames;
	private Vector languages;
	private Vector accreditationLevels;
	private Vector languageFrom;
	private int isLspUser = 9999;
	private boolean multipleLspUsersExists = false;
	private ImsHomeDb imsHomeDb;
	private Session session;
	private KeywordHandler kwHandlerIMSDB;
	private JsonArray contactNumbers = new JsonArray();
	private StringBuilder userLog = new StringBuilder();
	private String lspInterpreterId;
	private boolean isUserUpdateEnabled = false;
	private boolean updateOnBookSaveEnable = false;
	private JsonObject config = new JsonObject();
	private String bookingDocKey = null;
	private String bookingStatus = null;
	
	public User(Session session) throws NotesException { 
		this.session = session;
		imsHomeDb = new ImsHomeDb(session);
		kwHandlerIMSDB = new KeywordHandler(imsHomeDb.getDatabase());
		globalDateFormat = kwHandlerIMSDB.getGlobalDateNotesFromFormat();
		globalTimeFormat = kwHandlerIMSDB.getGlobalTimeFormat();
		nowDateTime = kwHandlerIMSDB.dateOffset(session.createDateTime("Today"));
		nowDateFormatted = Globals.formatDate(nowDateTime, globalDateFormat);
		nowDateTimeFormatted = Globals.formatDate(nowDateTime, globalDateFormat + " " + globalTimeFormat);
	}
	
	public User(Session session, String interpreterDocKey) {
		try {
			this.session = session;
			if (interpreterDocKey != null && !interpreterDocKey.equals("")) {
				imsHomeDb = new ImsHomeDb(session);
				kwHandlerIMSDB = new KeywordHandler(imsHomeDb.getDatabase());
				getUserByDocKey(interpreterDocKey);
			}
		} catch (NotesException e) {
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}
	
	private void getUserByDocKey(String interpreterDocKey) {
		View userView = imsHomeDb.getView(Constants.VIEW_USER_BY_DOCKEY);
    	try {
    		if (userView != null) {
    			doc = userView.getDocumentByKey(interpreterDocKey);
    			if (doc != null) {
	    			isLspUser = 0;
    				getUser();
	    		}
    		}
    		userView.recycle();
		} catch (NotesException e) {
			Globals.printException(e);
		}
	}

	/**
	 * 
	 * @param docKey
	 * @param session
	 * @throws NotesException
	 */
	public void getUser() throws NotesException {
		try {
	    	if (doc != null) {
	    		setDocKey(doc.getItemValueString("DocKey"));
	    		setForm(doc.getItemValueString("Form"));
	    		setFormPrefix();
		    	setDeviceId(doc.getItemValueString(formPrefix + "deviceId"));
		    	setDeviceOs(doc.getItemValueString(formPrefix + "deviceOs"));
		    	setFirstName(doc.getItemValueString(formPrefix + "firstname"));
		    	setLastName(doc.getItemValueString(formPrefix + "lastname"));
		    	setFullName(doc.getItemValueString(formPrefix + "fullname"));
		    	setEmail(doc.getItemValueString(formPrefix + "email"));
		    	setGender(doc.getItemValueString(formPrefix + "gender"));
		    	setDefaultRole(doc.getItemValueString(formPrefix + "role_default"));
		    	setPrimaryRole(doc.getItemValueString(formPrefix + "role_primary"));
		    	setSecondaryRole(doc.getItemValueString(formPrefix + "role_secondary"));
		    	setLanguages(doc.getItemValue(formPrefix + "Languages"));
		    	setAccreditationLevels(doc.getItemValue(formPrefix + "SkillLevel"));
		    	setProviderDocKeys(doc.getItemValue(formPrefix + "Provider_DocKey"));
		    	setProviderNames(doc.getItemValue(formPrefix + "Provider_Name"));
		    	setLanguageFrom(doc.getItemValue(formPrefix + "Language_From"));
		    	setPhone(doc.getItemValueString(formPrefix + "Phone"));
		    	setMobile(doc.getItemValueString(formPrefix + "Mobile"));
		    	setPager(doc.getItemValueString(formPrefix + "Pager"));
		    	setLspId(doc.getItemValueString(formPrefix + "ID")); //USER_LSP_ID ie NAATI ID
		    	setUniversalId(doc.getUniversalID());
	    	}
		} catch (NotesException e) {
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}

	/**
	 * Find LSP User by Full Name and LSP ID(NAATI ID)
	 * @param name
	 * @return
	 */
	public int isLspUser(String name, String accreditationNumber) {
		if (name != null) {
	    	View lspUserView = imsHomeDb.getView(Constants.VIEW_USER_LSP_TA);
	    	try {
	    		if (lspUserView != null) {
	    			form = Constants.FORM_USER_LSP;
	        		setFormPrefix();	        		
	    			ViewEntryCollection all = lspUserView.getAllEntries();
	    		//	String searchString = "[" + formPrefix + "FullName]=\"" + name + "\"";
	    			String searchString = "";
	    			if (!accreditationNumber.isEmpty()) {
	    				//searchString += " AND [" + formPrefix + "ID]=\"" + accreditationNumber + "\"";
	    				searchString = "[" + formPrefix + "ID]=\"" + accreditationNumber + "\"";	    				
	    			}	    		
	    			else{
	    				searchString = "[" + formPrefix + "FullName]=\"" + name + "\"";
	    			}
	    			 System.out.println("searchString="+searchString);
	    			all.FTSearch(searchString, 0);
	    			if (!accreditationNumber.isEmpty() && all.getCount() == 0) {
	    				// if not Profiles found using accreditation number, then use fullname
	    				 System.out.println("count = 0");
	    				searchString = "[" + formPrefix + "FullName]=\"" + name + "\"";
	    				 System.out.println("searchString="+searchString);
	    				all = lspUserView.getAllEntries();
	    				all.FTSearch(searchString, 0);
	    			}
	    				    			
	    			if (all.getCount() > 0) {
		    		//	isLspUser = true;	    			
	    				if (all.getCount() == 1) {
	    					isLspUser = 1;	    
	    					 System.out.println("count = 1");
		    				doc = all.getFirstEntry().getDocument();
			    			setDocKey(doc.getItemValueString("DocKey"));
		    				getUser();
	    				} else {
	    					multipleLspUsersExists = true;	    					
	    					isLspUser = 2;
	    					 System.out.println("count = 2");
			    		//	Log.writeError("Multiple user docs found for user: " + name + " with USER_LSP_ID: " + accreditationNumber + ". Hence, user profile will not get updated. ");
			    		}
		    		}
	    			else{	    			
	    				isLspUser = 0;
	    				 System.out.println("count = 0 final");
	    			}
	    		}
	    		lspUserView.recycle();
			} catch (NotesException e) {
				Globals.printException(e);
			}
		}
		return isLspUser;
	}
	
	/**
	 * Create new user
	 * @param id
	 * @param title
	 * @param firstName
	 * @param lastName
	 * @param fullName
	 * @param gender
	 * @param email
	 * @param accreditationLevel
	 * @param lspId
	 * @param contactNumbers
	 * @param providerDocKey
	 * @param providerName
	 * @param language
	 * @param bookingDocKey
	 */
	public String add(String id, String title, String firstName, String lastName, String fullName, String gender, String email, String accreditationLevel, 
			String lspId, JsonArray contactNumbers, String providerDocKey, String providerName, String language, String bookingDocKey) {
		doc = imsHomeDb.createDocument();
		String intDocKey = "";
		try {
	    	form = Constants.FORM_USER_LSP;
    		setFormPrefix();
    		intDocKey = createDocKey(firstName, lastName);
    		addField("Form", form);
    		addField("DocKey", intDocKey);
    		addField("DocKey", createDocKey(firstName, lastName));
    		addField(formPrefix + "ID", id);
    		addField(formPrefix + "firstname", firstName);
    		addField(formPrefix + "lastname", lastName);
    		addField(formPrefix + "fullname", fullName);
    		addField(formPrefix + "gender", gender);
    		addField(formPrefix + "email", email);
    		addField(formPrefix + "Languages", language);
			if (contactNumbers.size() > 0) {
			   for (int c=0;c<contactNumbers.size();c++) {
				   JsonObject contactObj = (JsonObject) contactNumbers.get(c);
				   String type = Globals.validateString(Globals.verifyContactType((String) contactObj.get("type")));
				   String number = Globals.validateString(contactObj.get("number"));
				   if (type.equalsIgnoreCase("Mobile")) {
					   addField(formPrefix + "Mobile", number);
				   } else if(type.equalsIgnoreCase("Work")) {
					   addField(formPrefix + "Phone", number);
				   } else if(type.equalsIgnoreCase("Home")) {
					   addField(formPrefix + "Home", number);
				   } else if(type.equalsIgnoreCase("Pager")) {
					   addField(formPrefix + "Pager", number);
				   }
			   }
			}
			addField(formPrefix + "Provider_DocKey", providerDocKey);
			addField(formPrefix + "Provider_Name", providerName);
			addField(formPrefix + "SkillLevel", accreditationLevel);
			if (lspId != null && !lspId.isEmpty()) {
				addField(formPrefix + "ID", lspId);
			}
			addField("CreatedBy", "API");
			addField("DocKey_CreatedBy", "API");
			addField("CreatedDate", nowDateFormatted);

			String[] lspUserFormHeading = kwHandlerIMSDB.getKeywordValueArray(Constants.KW_USER_LSP_HEADING);
			List<String> lspUserFormHeadingList = Arrays.asList(lspUserFormHeading);
			String heading = String.join(", ", lspUserFormHeadingList);
			userLog.append(heading + " created from Booking ID: " + bookingDocKey + " for LSP: " + providerName + " - API");
			if (userLog.length() > 0) {
				Globals.LogActionToDocument(nowDateTime, doc, userLog.toString(), false, session);
			}
	    	doc.save();
	    	this.getUser();
	    	addUpdateMandatoryCompliance();
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return intDocKey;
	}
	
	/**
	 * Update user details based on configuration about which field to overwrite and/or update based on booking status
	 * Update only if one matching user found
	 * @param session
	 */
	public String update() {
		String intDocKey = "";
		try {
			
			String configJson = kwHandlerIMSDB.getKeywordValueString(Constants.KW_USER_LSP_CONFIG);
			if (configJson != null && !multipleLspUsersExists) { 
				JsonArray parsedConfig = new JsonArray();
				parsedConfig = (JsonArray) Jsoner.deserialize(configJson);
				if (!parsedConfig.isEmpty()) {
					config  = (JsonObject) parsedConfig.get(0);
					isUserUpdateEnabled  = (config.get("featureEnable") != null && config.get("featureEnable").toString().equals("1")) ? true : false;
					updateOnBookSaveEnable   = (config.get("updateOnBookSaveEnable") != null && config.get("updateOnBookSaveEnable").toString().equals("1")) ? true : false;
					JsonArray overWriteField = (JsonArray) config.get("fieldsOverWrite");
					JsonArray updateOnBookStatus = (config.get("fieldsOverWrite") != null)? (JsonArray) config.get("updateOnBookStatus"): null;
					if (isUserUpdateEnabled && updateOnBookSaveEnable && updateOnBookStatus != null && updateOnBookStatus.contains(bookingStatus)) {
						if (overWriteField.contains(formPrefix + "Gender")) {
							updateField(formPrefix + "Gender", gender, true);
						}
						if (overWriteField.contains(formPrefix + "Email")) {
							updateField(formPrefix + "Email", email, true);
						}
						if (contactNumbers.size() > 0) {
						   for (int c=0;c<contactNumbers.size();c++) {
							   JsonObject contactObj = (JsonObject) contactNumbers.get(c);
							   String type = Globals.validateString(Globals.verifyContactType((String) contactObj.get("type")));
							   String number = Globals.validateString(contactObj.get("number"));
							   if (type.equalsIgnoreCase("Mobile") && overWriteField.contains(formPrefix + "Mobile")) {
								   updateField(formPrefix + "Mobile", number, true);
							   } else if(type.equalsIgnoreCase("Work") && overWriteField.contains(formPrefix + "Phone")) {
								   updateField(formPrefix + "Phone", number, true);
							   } else if(type.equalsIgnoreCase("Home") && overWriteField.contains(formPrefix + "Home")) {
								   updateField(formPrefix + "Home", number, true);
							   } else if(type.equalsIgnoreCase("Pager") && overWriteField.contains(formPrefix + "Pager")) {
								   updateField(formPrefix + "Pager", number, true);
							   }
						   }
						}
						updateLanguageAndSkillLevels(true);
					}
				}
			}
			//Update below fields always, user coming form mobile app login
			if (deviceId != null && !deviceId.isEmpty()) {
				updateField(formPrefix + "DeviceId", deviceId, false);
				updateField(formPrefix + "DeviceOs", deviceOs, false);	
			}
			if (lspId != null && !lspId.isEmpty()) {
				updateField(formPrefix + "ID", lspId, true);
			}
			updateField("ModifiedBy", "API", false);
			updateField("ModifiedDate", nowDateFormatted, false);
			intDocKey = doc.getItemValueString("DocKey");
			String[] lspUserFormHeading = kwHandlerIMSDB.getKeywordValueArray(Constants.KW_USER_LSP_HEADING);
			List<String> lspUserFormHeadingList = Arrays.asList(lspUserFormHeading);
			String heading = String.join(", ", lspUserFormHeadingList);
			if (userLog.length() > 0) {
				userLog.append(heading);
				if (bookingDocKey != null) {
					userLog.append(" updated from Booking ID: " + bookingDocKey  + " - API. ");		
				} else {
					userLog.append(" updated - API. ");	
				}
				Globals.LogActionToDocument(nowDateTime, doc, userLog.toString(), false, session);
			}
			doc.computeWithForm(true, false);
			doc.save();
	    	addUpdateMandatoryCompliance();
		} catch (NotesException | DeserializationException e) {
			e.printStackTrace();
		}
		return intDocKey;
	}

	/**
	 * Update languages, skill levels and LSPs
	 * @param updateLog
	 */
	private void updateLanguageAndSkillLevels(boolean updateLog) {
		try {
			//Get existing language list and skill list
			if (doc.getItemValue(formPrefix + "Languages") != null && !doc.getItemValue(formPrefix + "Languages").firstElement().toString().isEmpty()) {
				languages = doc.getItemValue(formPrefix + "Languages");
			}
			if (doc.getItemValue(formPrefix + "SkillLevel") != null && !doc.getItemValue(formPrefix + "SkillLevel").firstElement().toString().isEmpty()) {
				accreditationLevels = doc.getItemValue(formPrefix + "SkillLevel");
			}
			int positionOfLanguage = -1;
			//If current language exists, get index
			if(languages != null && languages.contains(language)) {
				positionOfLanguage = languages.indexOf(language);
			}
			JsonArray fieldsUpdate = new JsonArray();
			if (config.get("fieldsUpdate") != null) {	
				fieldsUpdate = (JsonArray) config.get("fieldsUpdate");
			}

			//Add Language and Skill Level when not present
			if(fieldsUpdate.contains(formPrefix + "Languages") && positionOfLanguage == -1) {
				languages.add(language);
				accreditationLevels.add(accreditationLevel);
				updateField(formPrefix + "Languages", languages, true);
				updateField(formPrefix + "SkillLevel", accreditationLevels, true);
				
			}
			if (config.get("fieldsOverWrite") != null) {
				JsonArray fieldsOverWrite = (JsonArray) config.get("fieldsOverWrite");
				//Check & Update Skill Level when Language is present
				if(fieldsOverWrite.contains(formPrefix + "SkillLevel") && positionOfLanguage >= 0) {
					String existingSkillAtLangPos = "";
					if(accreditationLevels.size() > positionOfLanguage) {
						existingSkillAtLangPos = accreditationLevels.get(positionOfLanguage).toString();
					}
					if(!existingSkillAtLangPos.equalsIgnoreCase(accreditationLevel)) {
						if(accreditationLevels.size() == 0){
							accreditationLevels.addElement(accreditationLevel);
						} else {
							accreditationLevels.set(positionOfLanguage, accreditationLevel);
						}
						updateField(formPrefix + "SkillLevel", accreditationLevels, true); 
					}				
				}
			}
				
			if(fieldsUpdate.contains(formPrefix + "Provider_Name")) {
				providerNames = doc.getItemValue(formPrefix + "Provider_Name");
				providerDocKeys = doc.getItemValue(formPrefix + "Provider_DocKey");
				if(providerDocKeys.indexOf(providerDocKey) < 0) {
					providerNames.addElement(providerName);			
					providerDocKeys.addElement(providerDocKey);
					updateField(formPrefix + "Provider_Name", providerNames, true);
					updateField(formPrefix + "Provider_DocKey", providerDocKeys, true); 
				}		
			}	
		} catch (Exception e) {
			Globals.printException(e);
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}
	}
	
	/**
	 * This Agent is called on USER/USER_LSP document save to Update Compliance Data and User Details 
	 * to all bookings based on configurations in keyword for the User updated
	 */
	public void updateComplianceOnBookings() {
		try {
			String configJson = kwHandlerIMSDB.getKeywordValueString(Constants.KW_INTERPRETER_SELECTION_CONFIG);
			JsonArray parsedConfig = new JsonArray();
			parsedConfig = (JsonArray) Jsoner.deserialize(configJson);
			if (!parsedConfig.isEmpty()) {
				JsonObject config = (JsonObject) parsedConfig.get(0);
				if (config != null) {
					if(config.containsKey("enableCompliance") && config.get("enableCompliance").toString().equalsIgnoreCase("Y")) {
						Agent agtNotification = imsHomeDb.getDatabase().getAgent(Constants.AGENT_USER_UPDATE_BOOK);
						agtNotification.run(doc.getNoteID());
					}
				}
			}
		} catch (NotesException | DeserializationException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Add/Update mandatory Compliance
	 */
	public void addUpdateMandatoryCompliance() {
		View compView = imsHomeDb.getView(Constants.VIEW_USER_CONCAT_COMPLIANCE);
		if (compView != null) {
			String[] mandatoryCompliance = kwHandlerIMSDB.getKeywordValueArray(Constants.KW_COMPLIANCE_MANDATORY);
			for(int i=0; i<mandatoryCompliance.length; i++){
				try {
					if(compView.getDocumentByKey(docKey + "~" + mandatoryCompliance[i], true) == null) {
						Document docNew = imsHomeDb.createDocument();
						docNew.replaceItemValue("Form", Constants.FORM_USER_COMPLIANCE);
						docNew.replaceItemValue("DocKey", Globals.createDocKeyFromForm(Constants.FORM_USER_CMP, session));
						docNew.replaceItemValue("DocKey_Parent", docKey);
						docNew.replaceItemValue("USER_Compliance_Check", "Yes");
						docNew.replaceItemValue("USER_Compliance_Desc", "Auto-generated");
						docNew.replaceItemValue("USER_Compliance_Title", mandatoryCompliance[i]);
						docNew.replaceItemValue("USER_Compliance_Title_Append", "to be verified");
						DateTime yesterdayDateTime = kwHandlerIMSDB.dateOffset(session.createDateTime("Yesterday"));
						docNew.replaceItemValue("USER_Compliance_Expiry", Globals.formatDate(yesterdayDateTime, kwHandlerIMSDB.getGlobalDateNotesFromFormat()));
						docNew.replaceItemValue("USER_Compliance_Expiry_JSON", Globals.formatDate(yesterdayDateTime, "yyyyMMdd"));
						docNew.save(true, false);
					}
				} catch (NotesException e) {
					e.printStackTrace();
				}
			}	
		}
		updateComplianceOnBookings();
	}

	/**
	 * Add new field on document
	 * @param fieldName
	 * @param newVal
	 * @return
	 */
	private void addField (String fieldName, String newVal)
	{
		try {
			doc.replaceItemValue(fieldName, newVal);
		} catch (Exception e) {
			Globals.printException(e);
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}
	}
	
	/**
	 * 
	 * @param fieldName
	 * @param newVal
	 * @return
	 */
	private void updateField (String fieldName, String newVal, boolean updateLog)
	{
		try {
			if (newVal == null) {newVal = "";}
			Globals.printCode("fieldName: " +fieldName);
			Globals.printCode("newVal: " +newVal);
			Globals.printCode("updateLog: " +updateLog);
	
			if (doc.hasItem(fieldName)){
				String cVal = doc.getItemValueString (fieldName);
				Globals.printCode("cVal: " +cVal);
				if (cVal != null && !newVal.equalsIgnoreCase(cVal)){
					doc.replaceItemValue(fieldName, newVal);
					if (updateLog) {
						if (fieldName.contains("USER_LSP_")) {
							fieldName = fieldName.substring(9); //Remove USER_LSP_ prefix
						}
						userLog.append(Globals.toCamelCase(fieldName) + " updated to " + newVal + ". \n");
					}
				}
			}
			else{
				doc.replaceItemValue(fieldName, newVal);
			}
		} catch (Exception e) {
			Globals.printException(e);
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}
	}
	
	/**
	 * Update vector field
	 * @param fieldName
	 * @param newVal
	 * @param updateLog
	 */
	private void updateField(String fieldName, Vector newVal, boolean updateLog) {
		try {
			doc.replaceItemValue(fieldName, newVal);
			if (updateLog) {
				fieldName = fieldName.substring(9); //Remove USER_LSP_ prefix
				userLog.append(Globals.toCamelCase(fieldName) + " updated to " + newVal + ". \n");
			}
		} catch (Exception e) {
			Globals.printException(e);
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}
	}
	
	/**
	 * Create LSP User's DocKey
	 * @param fName
	 * @param lName
	 * @return
	 */
	private String createDocKey(String fName, String lName) {
		String key = null;
		try {
			if (kwHandlerIMSDB.getKeywordValueString(Constants.KW_USER_DOCKEY_UNIQUE).equals("1")) {
				key = "L_" + session.evaluate("@Unique").toString();
			} else {
				String prefix = "L_" + fName.toUpperCase().substring(0, 1) + lName.toUpperCase().substring(0, 1);
				//Search view if any doc key exits with same first 4 letters, if so, increment the last numbers
				View lspUserView = imsHomeDb.getView("USER_LSP_ByDocKey");
		    	DocumentCollection docCollection = lspUserView.getAllDocumentsByKey(prefix, false);
		    	int intPos = 0;
		    	if (docCollection.getCount() > 0) {
		    		Document document = docCollection.getFirstDocument(); 
		    		int i = 0;
					while (document != null) {
						i = new Integer(document.getItemValueString("DocKey").substring(document.getItemValueString("DocKey").length()-4));
						if(i > intPos) {
							intPos = i;
						}
						document = docCollection.getNextDocument(document);
					}
		    	}
		    	intPos = intPos + 1;
		    	String pattern = kwHandlerIMSDB.getKeywordValueString(Constants.KW_USER_LSP_DOCKEY_PATTERN);
				if (pattern != null) {
					key = pattern + intPos;
				}
				key = prefix + key.substring(key.length()-pattern.length());
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return key;
	}

	/**
	 * Recycle objects
	 */
	public void recycle() {
		try {
			if (imsHomeDb != null) {
				imsHomeDb.recycle();
			}
			if (session != null) {
				session.recycle();
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param id
	 */
	public void setLspId(String id) {
		this.lspId = id;
	}
	
	/**
	 * @return the docKey
	 */
	public String getDocKey() {
		return docKey;
	}

	/**
	 * @param docKey the docKey to set
	 */
	public void setDocKey(String docKey) {
		this.docKey = docKey;
	}

	/**
	 * @return the doc
	 */
	public Document getDoc() {
		return doc;
	}
	/**
	 * @param doc the doc to set
	 */
	public void setDoc(Document doc) {
		this.doc = doc;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}
	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return the primaryRole
	 */
	public String getPrimaryRole() {
		return primaryRole;
	}
	/**
	 * @param primaryRole the primaryRole to set
	 */
	public void setPrimaryRole(String primaryRole) {
		this.primaryRole = primaryRole;
	}
	/**
	 * @return the secondaryRole
	 */
	public String getSecondaryRole() {
		return secondaryRole;
	}
	/**
	 * @param secondaryRole the secondaryRole to set
	 */
	public void setSecondaryRole(String secondaryRole) {
		this.secondaryRole = secondaryRole;
	}
	/**
	 * @return the defaultRole
	 */
	public String getDefaultRole() {
		return defaultRole;
	}
	/**
	 * @param defaultRole the defaultRole to set
	 */
	public void setDefaultRole(String defaultRole) {
		this.defaultRole = defaultRole;
	}
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * @return the languages
	 */
	public Vector getLanguages() {
		return languages;
	}
	/**
	 * @param languages the languages to set
	 */
	public void setLanguages(Vector languages) {
		this.languages = languages;
	}
	/**
	 * @return the languageFrom
	 */
	public Vector getLanguageFrom() {
		return languageFrom;
	}
	/**
	 * @param languageFrom the languageFrom to set
	 */
	public void setLanguageFrom(Vector languageFrom) {
		this.languageFrom = languageFrom;
	}
	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	/**
	 * @return the deviceOs
	 */
	public String getDeviceOs() {
		return deviceOs;
	}
	/**
	 * @param deviceOs the deviceOs to set
	 */
	public void setDeviceOs(String deviceOs) {
		this.deviceOs = deviceOs;
	}
	/**
	 * @return the form
	 */
	public String getForm() {
		return form;
	}
	/**
	 * @param form the form to set
	 */
	public void setForm(String form) {
		this.form = form;
	}

	/**
	 * @param formPrefix the formPrefix to set
	 */
	public void setFormPrefix() {
		this.formPrefix = form + "_";
	}

	/**
	 * @return the formPrefix
	 */
	public String getFormPrefix() {
		return formPrefix;
	}
	/**
	 * @return the userLog
	 */
	public StringBuilder getUserLog() {
		return userLog;
	}
	/**
	 * @param userLog the userLog to set
	 */
	public void setUserLog(StringBuilder userLog) {
		this.userLog = userLog;
	}
	
	/**
	 * @return the universalId
	 */
	public String getUniversalId() {
		return universalId;
	}

	/**
	 * @param universalId the universalId to set
	 */
	public void setUniversalId(String universalId) {
		this.universalId = universalId;
	}

	/**
	 * @return the nowDateTimeFormatted
	 */
	public String getNowDateTimeFormatted() {
		return nowDateTimeFormatted;
	}

	/**
	 * @param nowDateTimeFormatted the nowDateTimeFormatted to set
	 */
	public void setNowDateTimeFormatted(String nowDateTimeFormatted) {
		this.nowDateTimeFormatted = nowDateTimeFormatted;
	}

	/**
	 * @return the nowDateTime
	 */
	public DateTime getNowDateTime() {
		return nowDateTime;
	}

	/**
	 * @param nowDateTime the nowDateTime to set
	 */
	public void setNowDateTime(DateTime nowDateTime) {
		this.nowDateTime = nowDateTime;
	}

	/**
	 * @return the lspInterpreterId
	 */
	public String getLspInterpreterId() {
		return lspInterpreterId;
	}

	/**
	 * @param lspInterpreterId the lspInterpreterId to set
	 */
	public void setLspInterpreterId(String lspInterpreterId) {
		this.lspInterpreterId = lspInterpreterId;
	}

	/**
	 * @return the isLspUser
	 */
	public int isLspUser() {
		return isLspUser;
	}

	/**
	 * @param isLspUser the isLspUser to set
	 */
	public void setLspUser(int isLspUser) {
		this.isLspUser = isLspUser;
	}

	/**
	 * @return the providerDocKeys
	 */
	public Vector getProviderDocKeys() {
		return providerDocKeys;
	}

	/**
	 * @param providerDocKeys the providerDocKeys to set
	 */
	public void setProviderDocKeys(Vector providerDocKeys) {
		this.providerDocKeys = providerDocKeys;
	}

	/**
	 * @return the providerNames
	 */
	public Vector getProviderNames() {
		return providerNames;
	}

	/**
	 * @param providerNames the providerNames to set
	 */
	public void setProviderNames(Vector providerNames) {
		this.providerNames = providerNames;
	}

	/**
	 * @return the pager
	 */
	public String getPager() {
		return pager;
	}

	/**
	 * @param pager the pager to set
	 */
	public void setPager(String pager) {
		this.pager = pager;
	}
	
	/**
	 * @return the accreditationLevels
	 */
	public Vector getAccreditationLevels() {
		return accreditationLevels;
	}
	
	/**
	 * @param accreditationLevels the accreditationLevels to set
	 */
	public void setAccreditationLevels(Vector levels) {
		this.accreditationLevels = levels;
	}

	/**
	 * 
	 * @param accreditationLevel
	 */
	public void setAccreditationLevel(String accreditationLevel) {
		this.accreditationLevel = accreditationLevel;
		
	}

	/**
	 * 
	 * @param contactNumbers
	 */
	public void setContactNumbers(JsonArray contactNumbers) {
		this.contactNumbers = contactNumbers;
	}

	/**
	 * 
	 * @param bookingDocKey
	 */
	public void setBookingDocKey(String bookingDocKey) {
		this.bookingDocKey = bookingDocKey;
	}

	/**
	 * 
	 * @param bookingStatus
	 */
	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	/**
	 * 
	 * @param key
	 */
	public void setProviderDockey(String key) {
		this.providerDocKey = key;
	}

	/**
	 * 
	 * @param name
	 */
	public void setProviderName(String name) {
		this.providerName = name;
	}

}
