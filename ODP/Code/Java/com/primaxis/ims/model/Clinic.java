package com.primaxis.ims.model;

import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsoner;

import com.primaxis.ims.controller.ClinicController;
import com.primaxis.ims.utils.Constants;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.ImsHomeDb;
import com.primaxis.ims.utils.Log;

import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;

public class Clinic {
	private ImsHomeDb imsHomeDb;
	private ClinicController clinicController;
	
	public Clinic(ClinicController clinicController, Session session) {
		try {
			imsHomeDb = new ImsHomeDb(session);
			this.clinicController = clinicController;
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}
	
	/**
	 * Get all Clinics
	 * @return
	 */
	public JsonArray getAll() {
		try {
			JsonArray clinics = new JsonArray();
			View viewClinics = imsHomeDb.getView(Constants.viewClinicCodes);	
			if (viewClinics != null &&  viewClinics.getAllEntries().getCount() > 0) {
				clinics = Globals.getViewData(viewClinics);	
				return clinics;			
			}
		} catch (Exception e) {
			clinicController.isInValid = true;
			clinicController.httpResponse = 503;
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			e.printStackTrace();
		}
		return null;
	}
}
