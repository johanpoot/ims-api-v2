package com.primaxis.ims.model;

import org.json.simple.JsonArray;

import com.primaxis.ims.controller.NotificationController;
import com.primaxis.ims.utils.Constants;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.ImsBroadcastDb;
import com.primaxis.ims.utils.ImsHomeDb;
import com.primaxis.ims.utils.Log;

import lotus.domino.Agent;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;

public class Notification {
	private String userId; // Interpreter_DocKey
	private Document userDoc;
	private String userForm;
	private String title;
	private String body;
	private String creationDate;
	private String userDeviceId;
	private String userDeviceOs;
	private ImsHomeDb imsHomeDb;
	private ImsBroadcastDb broadcastDb;
	private NotificationController notificationController;
	private String userType;
	private String form;
	private String displayStatus;
	private String bookingDocKey;
	private Agent agtNotification;
	
	public Notification(NotificationController notificationController, Session session, String interpreterDocKey) {
		try {
			imsHomeDb = new ImsHomeDb(session);
			broadcastDb = new ImsBroadcastDb(session);
			this.notificationController = notificationController;
			if (interpreterDocKey!=null && !interpreterDocKey.equals("")) {
		    	View userView = imsHomeDb.getView("USER_ByDocKey"); //Find user in DB
		    	userDoc = userView.getDocumentByKey(interpreterDocKey);
		    	if (userDoc != null) {
		    		userId = interpreterDocKey;
			    	setUserForm(userDoc.getItemValueString("Form"));
			    	setUserDeviceId(userDoc.getItemValueString("user_deviceId"));
			    	setUserDeviceOs(userDoc.getItemValueString("user_deviceOs"));
		    	}
		    }
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}
	
	public Notification(Session session) {
		try {
			imsHomeDb = new ImsHomeDb(session);
			broadcastDb = new ImsBroadcastDb(session);
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}
	
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param id the userId to set
	 */
	public void setUserId(String id) {
		this.userId = id;
	}
	/**
	 * @return the userDoc
	 */
	public Document getUserDoc() {
		return userDoc;
	}
	/**
	 * @param userDoc the userDoc to set
	 */
	public void setUserDoc(Document doc) {
		this.userDoc = doc;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}
	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}
	/**
	 * @return the creationDate
	 */
	public String getCreationDate() {
		return creationDate;
	}
	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	/**
	 * @return the userDeviceId
	 */
	public String getUserDeviceId() {
		return userDeviceId;
	}
	/**
	 * @param deviceId the userDeviceId to set
	 */
	public void setUserDeviceId(String deviceId) {
		this.userDeviceId = deviceId;
	}
	/**
	 * @return the userDeviceOs
	 */
	public String getUserDeviceOs() {
		return userDeviceOs;
	}
	/**
	 * @param deviceOs the userDeviceOs to set
	 */
	public void setUserDeviceOs(String deviceOs) {
		this.userDeviceOs = deviceOs;
	}
	/**
	 * @return the userForm
	 */
	public String getUserForm() {
		return userForm;
	}
	/**
	 * @param form the userForm to set
	 */
	public void setUserForm(String form) {
		this.userForm = form;
	}
	
	/**
	 * @return the bookingDocKey
	 */
	public String getBookingDocKey() {
		return bookingDocKey;
	}

	/**
	 * @param bookingDocKey the bookingDocKey to set
	 */
	public void setBookingDocKey(String bookingDocKey) {
		this.bookingDocKey = bookingDocKey;
	}

	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * @param userType the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

	/**
	 * @return the form
	 */
	public String getForm() {
		return form;
	}

	/**
	 * @param form the form to set
	 */
	public void setForm(String form) {
		this.form = form;
	}

	/**
	 * @return the displayStatus
	 */
	public String getDisplayStatus() {
		return displayStatus;
	}

	/**
	 * @param displayStatus the displayStatus to set
	 */
	public void setDisplayStatus(String displayStatus) {
		this.displayStatus = displayStatus;
	}

	/**
	 * Search notification for a view for given category
	 * @param searchBy
	 * @param category
	 * @return
	 */
	public JsonArray searchAllNotifications(String searchBy, String category) {
		try {
			JsonArray objNotificationss = new JsonArray();
			View viewNotifications = broadcastDb.getAllNotificationsByView(searchBy);	
			if (viewNotifications != null &&  viewNotifications.getAllEntries().getCount() > 0) {
				objNotificationss = Globals.getViewEntriesForCategory(viewNotifications, category);
			}
			return objNotificationss;
		} catch (Exception e) {
			notificationController.isInValid = true;
			notificationController.httpResponse = 503;
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		

		}
		return null;
	}
	
	/**
	 * Create an email notification document to send later through scheduled agent
	 */
	public void create() {
		try {
			if (imsHomeDb != null) {
				Document emailDoc = imsHomeDb.getDatabase().createDocument();				
				emailDoc.replaceItemValue("Form", Constants.FORM_PARAM);
				emailDoc.replaceItemValue("ImsNotifyType" , Constants.NOTIFICATION_TYPE_EMAIL);
				String ekey = bookingDocKey + "~" + displayStatus + "~" + Constants.NOTIFICATION_TYPE_EMAIL + "~" + userType + "~" + form;
				emailDoc.replaceItemValue("EmailKeys", ekey);
				emailDoc.save();
				send(emailDoc);
			} else {
				Globals.printCode("Broadcast database not found");
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}
	
	public void send(Document emailDoc) {
		try {
			if (broadcastDb != null) {
				agtNotification = broadcastDb.getDatabase().getAgent("sendNotification_RunOnServer");
				agtNotification.run(emailDoc.getNoteID());
				emailDoc.replaceItemValue("Form" , "Param_Delete");
				emailDoc.save(); 
			}
		} catch (NotesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
