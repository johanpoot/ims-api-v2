package com.primaxis.ims.model;

import com.primaxis.ims.controller.QuestionController;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.ImsHomeDb;
import com.primaxis.ims.utils.Log;
import com.primaxis.ims.utils.KeywordHandler;
import lotus.domino.View;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;
import java.util.Vector;
import org.json.simple.*;

public class Question {
	
	//private ImsDataDb dataDb;
	private ImsHomeDb imsHomeDb;
	private QuestionController questionController;
	private KeywordHandler kwHandlerIMSDB;
	private Session session;
	private String id;
	private Document doc = null;
	private String form = "PIN";
	private String formPrefix = form + "_";
	private String NoQuestions = "";


	public Question(QuestionController questionController, Session session) throws NotesException {
		this.questionController = questionController;
		this.session = session;
		try {
			imsHomeDb = new ImsHomeDb(session);
			kwHandlerIMSDB = new KeywordHandler(imsHomeDb.getDatabase());
			NoQuestions = kwHandlerIMSDB.getKeywordValueString("IMS_Connect_No_Questions");
			if (NoQuestions == ""){
				NoQuestions = "No Questions required";
			}
			
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}

	public JsonArray getQuestions(String clientid) throws NotesException  {
		JsonArray questionList = new JsonArray();
		try {
			
			JsonObject qObj = null;
			String [] qarr;		
			qarr = kwHandlerIMSDB.getIntakeQuestionKeys(clientid);
			View luQuestion = imsHomeDb.getView("luIntakeQuestionsByKey");
			Document qdoc = null;
			for(int i = 0 ; i < qarr.length ; ++i){
				qdoc = luQuestion.getDocumentByKey(qarr[i], true);
				Globals.printCode("key="+qarr[i]);
				if (qdoc!=null){
					qObj = new JsonObject();
					qObj.put("Key",qarr[i]);
					qObj.put("Order",i+1);
					qObj.put("Question",qdoc.getItemValueString("Question"));
					questionList.add(qObj);
				}
			}
			if (questionList.isEmpty()){
				qObj = new JsonObject();
				qObj.put("Key","na");
				qObj.put("Order",1);
				qObj.put("Question",NoQuestions);
				questionList.add(qObj);
			}
		} catch (NotesException e) {
			Globals.printException(e);
		}
		return questionList;
	}

	
	public Document getDoc() {
		return doc;
	}

	public void setDoc(Document doc) {
		this.doc = doc;
	}
}
