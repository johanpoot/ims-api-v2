package com.primaxis.ims.model;
import com.primaxis.ims.controller.BookingController;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.json.simple.*;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.EmbeddedObject;
import lotus.domino.NotesException;
import lotus.domino.RichTextItem;
import lotus.domino.Session;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewEntryCollection;
import lotus.domino.DocumentCollection;
import com.primaxis.ims.utils.Log;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.ImsDataDb;
import com.primaxis.ims.utils.ImsHomeDb;
import com.primaxis.ims.utils.KeywordHandler;
import com.primaxis.ims.utils.AttachmentHelper;
import com.primaxis.ims.utils.Constants;
	
/**
 * Booking class model to manage bookings related data
 * 
 * @author BKarwal
 *
 */
public class Booking  {
	
	private Document bookingDoc = null;
	private String form;
	private String formPrefix = form + "_";
	private String id;
	private String pmsId;
	private String parentId;
	private String universalId;
	private String clinic;
	private String site;
	private String siteName;
	private String location;
	private String clinicCode;
	private String costCentre;
	private String clinicAddress;
	private String clinicSuburb;
	private String clinicPostCode;
	private String clinicState;
	private DateTime appointmentStartDateTime;
	private DateTime appointmentEndDateTime;
	private String appointmentStartDateText;
	private String appointmentStartTimeText;
	private String appointmentEndTimeText;
	private int duration;
	private String actualAppointmentStartTimeText;
	private String actualAppointmentEndTimeText;
	private int actualDuration;
	private String languageCode;
	private String language;
	private String interpreterType = null;
	private String interpreterFullName = null;
	private String interpreterDocKey = null;
	private boolean isManagedByIMS = true;
	private String externalReference;
	private String patientType = null;
	private String deliveryMethod = null;
	private String communicationDetails = null;
	private String passCode = null;
	private String videoConferencePlatform = null;
	private String agencyNotes = null;
	private String interpreterNotes = null;
	private String externalLink = null;
	private String completed = "";
	private String cancelledBy;
	private String rejectedBy;
	private String status;
	private String displayStatus;
	private String genderPreferencePriority;
	private String genderPreferenceCode;
	private ImsDataDb dataDb;
	private ImsHomeDb imsHomeDb;
	private KeywordHandler kwHandlerIMSDB;
	private BookingController bookingController;
	private String jsonApiBookingDetails;
	private int countInterpreters = 0;
	private int countClinicians = 1;
	private int countPatients = 1;
	private int countContacts = 1;
	public boolean isValidInterpreterExists = false;
	private String agencyLabel;
	private String previousInterpreterName;
	private Session session;	
	private String jsonApptStartDate;
	private String jsonApptStartTime;
	
	/*Completion Details*/
	private String serviceDelivered;
	private double travelTime = 0;
	private double distanceTravelled;
	private double waitingDuration = 0;
	private double numCalls = 0;
	private double otherCharges;
	private boolean accepted = false;
	
	/*Non completion details*/
	private String nonCompletionReason;
	private String cancellationReason;
	private String cancellationNotice;
	private String cancellationTime;
	private String cancellationTimeFormatted;
	private String cancellationTimeText;
	private String cancellationDateText;
	private String cancellationCharges;
	private String cancellationRequestedBy;
	private String rejectionReason;
	private String agencyFlowComplete;
	private String agencyFlowCompleteTime;
	
	/*KPI Data*/
	private DateTime createdDateTime;
	private DateTime nowDateTime;
	private String nowDateTimeFormatted;
	private String createdDate;
	private String kpiAllocatedFirstBy = null;
	private DateTime kpiAllocatedFirstDate = null;
	private String kpiAllocatedFirstDocKey = null;
	private String kpiAllocatedLastBy = null;
	private DateTime kpiAllocatedLastDate = null;
	private String kpiAllocatedLastDocKey = null;
	private String kpiSubmittedAgencyBy = null;
	private DateTime kpiSubmittedAgencyDate = null;
	private String kpiSubmittedAgencyDocKey = null;

	private StringBuilder bookingLog = new StringBuilder();
	private JsonObject ethnicityPreference = new JsonObject();
	private JsonObject clinicFullAddress = new JsonObject();
	private JsonArray contactNumbers = new JsonArray();
	private JsonObject genderPreference = new JsonObject();
	private JsonObject address = new JsonObject();
	private JsonArray professionals = new JsonArray();
	private JsonArray patients = new JsonArray();
	private JsonArray interpreters = new JsonArray();
	private JsonArray contacts = new JsonArray();
	private JsonObject completionDetails = new JsonObject();
	private JsonArray attachments = new JsonArray();
	private boolean amendInterpreter = false;
	private String oldStatus = "";
	private String oldDisplayStatus = "";
	private String patientName = null;
	private boolean completeBooking;	
	private String payInvoice = "";
	private String chargeInvoice = "";
	private String attachmentKey = null;
	private String cbnAttachmentKey = null;
	private String notifyRequestorByEmail = null;
	private boolean isFlexible = false;
	private String flexibleTimeStart = null;
	private String flexibleTimeEnd = null;
	private String modality = null;
	private String pendingSendWebhook = Constants.NO;
	private DateTime lastModifiedDateTime;
	private String lastModifiedDateText;
	private String lastModifiedTimeText;
	private String systemComments;
	private String source = Constants.defaultSource;
	private String sourceDisplay = Constants.defaultSourceDisplay;

	/**
	 * Build Booking properties if valid booking
	 * 
	 * @param id
	 * @throws NotesException 
	 */
	public Booking(BookingController bookingController, Session session) throws NotesException {
		this.bookingController = bookingController;
		this.session = session;
		try {
			dataDb = new ImsDataDb(session);
			imsHomeDb = new ImsHomeDb(session);
			kwHandlerIMSDB = new KeywordHandler(imsHomeDb.getDatabase());
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}

	/**
	 * Build Booking properties if valid booking
	 * 
	 * @param id
	 * @throws NotesException 
	 */
	public Booking(BookingController bookingController, String id, Session session) throws NotesException {
		this.bookingController = bookingController;
		this.session = session;
		try {
			dataDb = new ImsDataDb(session);
			imsHomeDb = new ImsHomeDb(session);
			kwHandlerIMSDB = new KeywordHandler(imsHomeDb.getDatabase());
			if (id != null) {
				setBookingDoc(id);
				getBooking();
			}
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}
	
	/**
	 * Set booking notes document
	 * @param id
	 * @return
	 */
	public void setBookingDoc (String id) {
		try {
			dataDb = new ImsDataDb(session);
			bookingDoc = dataDb.getBookingDocumentByKey(id);
		} catch (Exception e) {			
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}
	
	/**
	 * Set a booking details
	 * @param id
	 * @throws NotesException
	 */
	public void getBooking() throws NotesException {
		try {		
			if (bookingDoc != null) {
				form = bookingDoc.getItemValueString("Form");
				formPrefix = form + "_";				
				id = bookingDoc.getItemValueString("DocKey");
				//Ashish - Feb 2022 - Add the External Dockey in the booking JSON
				pmsId = bookingDoc.getItemValueString(formPrefix + "PMSID");
				source = bookingDoc.getItemValueString(formPrefix + "Source");
				
				parentId = bookingDoc.getItemValueString("CBN_DocKey");
				universalId = bookingDoc.getUniversalID();				
				setAttachmentKey(bookingDoc.getItemValueString("attachKey"));
				setCbnAttachmentKey(bookingDoc.getItemValueString("CBN_attachKey"));
				//jsonApiBookingDetails = bookingDoc.getItemValueString("JSON_API_Booking_Details");
				interpreterType = bookingDoc.getItemValueString(formPrefix + "Interpreter_Type");
				interpreterDocKey = bookingDoc.getItemValueString(formPrefix + "Interpreter_DocKey");
				interpreterFullName = bookingDoc.getItemValueString(formPrefix + "Interpreter_FullName");
				status = bookingDoc.getItemValueString(formPrefix + "Status");
				displayStatus = bookingDoc.getItemValueString(formPrefix + "Status_Display");
				if (displayStatus == null || displayStatus.equals("")) { //This is for Extra Reasons
					displayStatus = status;
				}
				interpreterNotes = bookingDoc.getItemValueString(formPrefix + "InterpreterNotes");
				agencyNotes = bookingDoc.getItemValueString(formPrefix + "Comments");
				externalReference = bookingDoc.getItemValueString(formPrefix + "Agency_ID");
				externalLink = bookingDoc.getItemValueString(formPrefix + "Agency_URL");
				appointmentStartDateTime = (DateTime) bookingDoc.getItemValue(formPrefix + "Appt_Start").get(0);
				appointmentEndDateTime = (DateTime) bookingDoc.getItemValue(formPrefix + "Appt_End").get(0);
				appointmentStartDateText = bookingDoc.getItemValueString(formPrefix + "ApptStartDate_Text");
				appointmentStartTimeText = bookingDoc.getItemValueString(formPrefix + "ApptStartTime_Text");
				appointmentEndTimeText = bookingDoc.getItemValueString(formPrefix + "ApptEndTime_Text");
				duration = new Integer(bookingDoc.getItemValueInteger(formPrefix + "ApptDuration"));
				if (appointmentEndTimeText.equals("")) {
					DateTime newTemp = (DateTime) bookingDoc.getItemValue(formPrefix + "Appt_Start").get(0);
					appointmentEndTimeText = Globals.getEndTime(newTemp, duration, session);
				}	
				
				serviceDelivered = bookingDoc.getItemValueString(formPrefix + "Service_Delivered");
				actualAppointmentStartTimeText = bookingDoc.getItemValueString(formPrefix + "ApptStartTime_Actual");
				actualAppointmentEndTimeText = bookingDoc.getItemValueString(formPrefix + "ApptFinishTime_Actual");
				if (bookingDoc.getItemValueInteger(formPrefix + "ApptDuration_Actual") != 0) {
					actualDuration = bookingDoc.getItemValueInteger(formPrefix + "ApptDuration_Actual");
				}					
				if (bookingDoc.getItemValueDouble(formPrefix + "WaitingDuration") != 0) {
					waitingDuration = bookingDoc.getItemValueDouble(formPrefix + "WaitingDuration");
				}					
				if (bookingDoc.getItemValueDouble(formPrefix + "NumCalls") != 0) {
					numCalls = bookingDoc.getItemValueDouble(formPrefix + "NumCalls");
				}	
				languageCode = bookingDoc.getItemValueString(formPrefix + "Language_Code");
				language = bookingDoc.getItemValueString(formPrefix + "Language");
				modality  = bookingDoc.getItemValueString(formPrefix + "modality");
				deliveryMethod = bookingDoc.getItemValueString(formPrefix + "BookingType");
				if (bookingDoc.getItemValueString(formPrefix + "Flexible") != null && bookingDoc.getItemValueString(formPrefix + "Flexible").equalsIgnoreCase("Yes")) {
					isFlexible  = true;
				}
				flexibleTimeStart  = bookingDoc.getItemValueString(formPrefix + "Flexible_TimeStart");
				flexibleTimeEnd  = bookingDoc.getItemValueString(formPrefix + "Flexible_TimeEnd");
				patientType = bookingDoc.getItemValueString(formPrefix + "PatientType");
				communicationDetails = bookingDoc.getItemValueString(formPrefix + "CommunicationDetails");
				passCode = bookingDoc.getItemValueString(formPrefix + "Passcode");
				videoConferencePlatform = bookingDoc.getItemValueString(formPrefix + "VideoConferencePlatform");
				distanceTravelled = bookingDoc.getItemValueDouble(formPrefix + "Dist_Travelled");
				travelTime = bookingDoc.getItemValueDouble(formPrefix + "TravelTime");
				otherCharges = bookingDoc.getItemValueDouble(formPrefix + "Other");
				cancellationReason = bookingDoc.getItemValueString(formPrefix + "CancellationReason");
				site = bookingDoc.getItemValueString(formPrefix + "Site");
				siteName = bookingDoc.getItemValueString(formPrefix + "SiteName");
				location = bookingDoc.getItemValueString(formPrefix + "Location");
				clinic = bookingDoc.getItemValueString(formPrefix + "Clinic");
				costCentre = bookingDoc.getItemValueString(formPrefix + "CostCentre");
				clinicAddress = bookingDoc.getItemValueString(formPrefix + "Clinic_Address");
				clinicSuburb = bookingDoc.getItemValueString(formPrefix + "Clinic_Suburb");
				clinicPostCode = bookingDoc.getItemValueString(formPrefix + "Clinic_PostCode");
				clinicState = bookingDoc.getItemValueString(formPrefix + "Clinic_State");
				if (clinicAddress != null) {
					clinicFullAddress.put("address",clinicAddress);
				}
				if (clinicSuburb != null) {
					clinicFullAddress.put("suburb",clinicSuburb);
				}
				if (clinicPostCode != null) {
					clinicFullAddress.put("postCode",clinicPostCode);
				}
				if (clinicState != null) {
					clinicFullAddress.put("state",clinicState);
				}
				
				clinicCode = bookingDoc.getItemValueString(formPrefix + "CLN_Code");
				if (!bookingDoc.getItemValueString(formPrefix + "Completed").equals("")) {
					completed = bookingDoc.getItemValueString(formPrefix + "Completed");
					if (completed.equalsIgnoreCase("No")) {
						cancellationNotice = bookingDoc.getItemValueString(formPrefix + "CancellationNotice");
						cancellationTime = bookingDoc.getItemValueString(formPrefix + "Cancellation_Time");
						if (!cancellationTime.equals("") && cancellationTime != null) {
							setCancellationTimeFormatted();
						}
					}
				}
				nonCompletionReason = bookingDoc.getItemValueString(formPrefix + "NC_Reason");
				
				//Get count for dynamically added multiple fields, so that we can remove/update these fields later
				if (interpreterType.equalsIgnoreCase(Constants.InterpreterTypeInHouse)) { //Only one interpreter is assigned in-house currently
					interpreters = getInHouseInterpreterDetails();
				} else if (bookingDoc.getItemValueString(formPrefix + "Count_Interpreters") != null && !bookingDoc.getItemValueString(formPrefix + "Count_Interpreters").equals("")) {
					countInterpreters =  new Integer(bookingDoc.getItemValueString(formPrefix + "Count_Interpreters"));
					interpreters = getInterpreterDetails();
				}
				if (bookingDoc.getItemValueString(formPrefix + "Count_Clinicians") != null && !bookingDoc.getItemValueString(formPrefix + "Count_Clinicians").equals("")) {
					countClinicians = new Integer(bookingDoc.getItemValueString(formPrefix + "Count_Clinicians"));
				}
				if (bookingDoc.getItemValueString(formPrefix + "Count_Patients") != null && !bookingDoc.getItemValueString(formPrefix + "Count_Patients").equals("")) {
					countPatients = new Integer(bookingDoc.getItemValueString(formPrefix + "Count_Patients"));
				}
				if (bookingDoc.getItemValueString(formPrefix + "Count_Contacts") != null && !bookingDoc.getItemValueString(formPrefix + "Count_Contacts").equals("")) {
					countContacts = new Integer(bookingDoc.getItemValueString(formPrefix + "Count_Contacts"));
				}
				patients = getPatientFields();
				contacts = getContactFields();
				professionals = getProfessionalFields();
				genderPreferencePriority = bookingDoc.getItemValueString(formPrefix + "PatientGenderPreferencePriority");
				genderPreferenceCode = bookingDoc.getItemValueString(formPrefix + "PatientGenderPreference"); //Legacy field
				//If no gender pref priority set (in legacy code), set it to Must be if there is any preference existing
				if ((genderPreferencePriority.equals("") || genderPreferencePriority == null || genderPreferencePriority.equals("Select Priority")) 
						&& (genderPreferenceCode.equalsIgnoreCase("F") || genderPreferenceCode.equalsIgnoreCase("M"))) {
					genderPreferencePriority = "Must be";
				}

				if (!genderPreferencePriority.equals("") && !genderPreferencePriority.equals("N")) {
					genderPreference.put("priority", genderPreferencePriority);
					if (!genderPreferenceCode.equals("")) {
						genderPreference.put("gender", genderPreferenceCode);
					}
				}
				String ethnicityPreferencePriority = "";
				String ethnicityPreferenceCode = "";
				if (bookingDoc.getItemValueString(formPrefix + "PatientEthnicityPreferencePriority") != null) {
					ethnicityPreferencePriority = bookingDoc.getItemValueString(formPrefix + "PatientEthnicityPreferencePriority");
				}
				if (bookingDoc.getItemValueString(formPrefix + "PatientEthnicityPreferenceCode") != null) {
					ethnicityPreferenceCode = bookingDoc.getItemValueString(formPrefix + "PatientEthnicityPreferenceCode");
				}
				ethnicityPreference.put("priority", ethnicityPreferencePriority);
				ethnicityPreference.put("code", ethnicityPreferenceCode);

				//JSON computed fields
				if (bookingDoc.getItemValueString("JSON_Appt_StartDate") != null) {
					jsonApptStartDate = bookingDoc.getItemValueString("JSON_Appt_StartDate");
				}
				if (bookingDoc.getItemValueString("JSON_Appt_StartTime") != null) {
					jsonApptStartTime = bookingDoc.getItemValueString("JSON_Appt_StartTime");
				}

				//PBD attachments
				JsonArray pbdAttahments = getPbdAttachments();
				if (pbdAttahments.size()>0) {
					for (int i=0;i<pbdAttahments.size();i++) {
						JsonObject pbdAttahment = (JsonObject) pbdAttahments.get(i);
						attachments.add(pbdAttahment);
					}
				}
				//Global Attachments
				JsonArray globalAttachments = getGlobalsAttachments();
				if (globalAttachments.size()>0) {
					for (int i = 0; i<globalAttachments.size();i++) {
						JsonObject globalAttachment = (JsonObject) globalAttachments.get(i);
						attachments.add(globalAttachment);
					}
				}
				if (bookingDoc.getItemValueString(formPrefix + "accepted") != null && bookingDoc.getItemValueString(formPrefix + "accepted").equalsIgnoreCase("yes")) {
					accepted = true;
				}
				//Get KPI fields
				if (bookingDoc.hasItem("CreatedDateTime")) {
					createdDateTime = (DateTime) bookingDoc.getItemValue("CreatedDateTime").get(0);
				}
				if (bookingDoc.hasItem("KPI_AllocatedFirst_Date")) {
					kpiAllocatedFirstBy = bookingDoc.getItemValueString("KPI_AllocatedFirst_By");
					kpiAllocatedFirstDate = (DateTime) bookingDoc.getItemValue("KPI_AllocatedFirst_Date").get(0);
					kpiAllocatedFirstDocKey = bookingDoc.getItemValueString("KPI_AllocatedFirst_DocKey");
				}
				if (bookingDoc.hasItem("KPI_AllocatedLast_Date")) {
					kpiAllocatedLastBy = bookingDoc.getItemValueString("KPI_AllocatedLast_By");
					kpiAllocatedLastDate = (DateTime) bookingDoc.getItemValue("KPI_AllocatedLast_Date").get(0);
					kpiAllocatedLastDocKey = bookingDoc.getItemValueString("KPI_AllocatedLast_DocKey");
				}
				if (bookingDoc.hasItem("KPI_submitted_agency_Date")) {
					kpiSubmittedAgencyBy = bookingDoc.getItemValueString("KPI_submitted_By");
					kpiSubmittedAgencyDate = (DateTime) bookingDoc.getItemValue("KPI_submitted_agency_Date").get(0);
					kpiSubmittedAgencyDocKey = bookingDoc.getItemValueString("KPI_submitted_agency_DocKey");
				}
				payInvoice = bookingDoc.getItemValueString(formPrefix + "Pay_Invoice");
				chargeInvoice = bookingDoc.getItemValueString(formPrefix + "Charge_Invoice");
				notifyRequestorByEmail  = bookingDoc.getItemValueString(formPrefix + "NotifyRequestor");
				if (bookingDoc.hasItem("JSON_LastModified_Date_Text")) {
					lastModifiedDateText  = bookingDoc.getItemValueString("JSON_LastModified_Date_Text");
				}
				if (bookingDoc.hasItem("JSON_LastModified_Time_Text")) {
					lastModifiedTimeText  = bookingDoc.getItemValueString("JSON_LastModified_Time_Text");
				}
				if (bookingDoc.hasItem(formPrefix + "SystemComments")) {
					systemComments  = bookingDoc.getItemValueString(formPrefix + "SystemComments");
				}
				
			}
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}

	/**
	 * 
	 * @return
	 */
	public Document getBookingDoc() {
		return bookingDoc;
	}

	/**
	 * @return the form
	 */
	public String getForm() {
		return form;
	}

	/**
	 * @param form the form to set
	 */
	public void setForm(String form) {
		this.form = form;
	}

	/**
	 * @param formPrefix the formPrefix to set
	 */
	public void setFormPrefix() {
		this.formPrefix = form + "_";
	}

	/**
	 * @return the formPrefix
	 */
	public String getFormPrefix() {
		return formPrefix;
	}

	/**
	 * @return the agencyLabel
	 */
	public String getAgencyLabel() {
		return agencyLabel;
	}

	/**
	 * @param agencyLabel the agencyLabel to set
	 */
	public void setAgencyLabel(String agencyLabel) {
		this.agencyLabel = agencyLabel;
	}

	/**
	 * Set JSON field with full booking details
	 * @param json - JSON String of full booking details
	 */
	public void setJsonApiBookingDetails(String json) {
		this.jsonApiBookingDetails = json;
	}

	/**
	 * Get JSON field with full booking details
	 * @return jsonApiBookingDetails - JSON String of full booking details
	 */
	public String getJsonApiBookingDetails() {
		return jsonApiBookingDetails;
	}

	/**
	 * Get IMS ID
	 * @return
	 */
	public String getId() {
		return id;
	}

	public String getNoteId() {
		try{
			return bookingDoc.getNoteID();
		} catch (Exception e) {
		Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		Globals.printException(e);
		return null;
	}
	}
	/**
	 * Set IMS ID
	 * @param id - Booking ID
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the pmsId
	 */
	public String getPmsId() {
		return pmsId;
	}

	/**
	 * @param pmsId the pmsId to set
	 */
	public void setPmsId(String pmsId) {
		this.pmsId = pmsId;
	}

	/**
	 * Get parent booking"s id
	 * @return - id of the parent booking
	 */
	public String getParentId() {
		return parentId;
	}

	/**
	 * Set parent booking ID
	 * @param id - Booking ID
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	/**
	 * Get document's universal id
	 * @return the universalId
	 */
	public String getUniversalId() {
		return universalId;
	}

	/**
	 * @param universalId the universalId to set
	 */
	public void setUniversalId(String universalId) {
		this.universalId = universalId;
	}

	/**
	 * @return the clinic
	 */
	public String getClinic() {
		return clinic;
	}

	/**
	 * @param clinic the clinic to set
	 */
	public void setClinic(String clinic) {
		this.clinic = clinic;
	}

	/**
	 * 
	 * @return
	 */
	public String getClinicCode() {
		return clinicCode;
	}

	/**
	 * 
	 * @param clinicCode
	 */
	public void setClinicCode(String clinicCode) {
		this.clinicCode = clinicCode;
	}

	/**
	 * @return the site
	 */
	public String getSite() {
		return site;
	}

	/**
	 * @param site the site to set
	 */
	public void setSite(String site) {
		this.site = site;
	}

	/**
	 * @return the siteName
	 */
	public String getSiteName() {
		return siteName;
	}

	/**
	 * @param siteName the siteName to set
	 */
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * 
	 * @return costCentre
	 */
	public String getCostCentre() {
		return costCentre;
	}

	public void setCostCentre(String costCentre) {
		this.costCentre = costCentre;
	}

	/**
	 * @return the clinicAddress
	 */
	public String getClinicAddress() {
		return clinicAddress;
	}

	/**
	 * @param clinicAddress the clinicAddress to set
	 */
	public void setClinicAddress(String clinicAddress) {
		this.clinicAddress = clinicAddress;
	}

	/**
	 * @return the clinicSuburb
	 */
	public String getClinicSuburb() {
		return clinicSuburb;
	}

	/**
	 * @param clinicSuburb the clinicSuburb to set
	 */
	public void setClinicSuburb(String clinicSuburb) {
		this.clinicSuburb = clinicSuburb;
	}

	/**
	 * @return the clinicPostCode
	 */
	public String getClinicPostCode() {
		return clinicPostCode;
	}

	/**
	 * @param clinicPostCode the clinicPostCode to set
	 */
	public void setClinicPostCode(String clinicPostCode) {
		this.clinicPostCode = clinicPostCode;
	}

	/**
	 * @return the clinicState
	 */
	public String getClinicState() {
		return clinicState;
	}

	/**
	 * @param clinicState the clinicState to set
	 */
	public void setClinicState(String clinicState) {
		this.clinicState = clinicState;
	}

	/**
	 * @return the appointmentStartDateTime
	 */
	public DateTime getAppointmentStartDateTime() {
		return appointmentStartDateTime;
	}

	/**
	 * @param appointmentStartDateTime the appointmentStartDateTime to set
	 */
	public void setAppointmentStartDateTime(DateTime appointmentStartDateTime) {
		this.appointmentStartDateTime = appointmentStartDateTime;
	}

	/**
	 * @return the appointmentEndDateTime
	 */
	public DateTime getAppointmentEndDateTime() {
		return appointmentEndDateTime;
	}

	/**
	 * @param appointmentEndDateTime the appointmentEndDateTime to set
	 */
	public void setAppointmentEndDateTime(DateTime endDateTime) {
		this.appointmentEndDateTime = endDateTime;
	}

	/**
	 * @return the appointmentStartDateText
	 */
	public String getAppointmentStartDateText() {
		return appointmentStartDateText;
	}

	/**
	 * @param appointmentStartDateText the appointmentStartDateText to set
	 */
	public void setAppointmentStartDateText(String appointmentStartDateText) {
		this.appointmentStartDateText = appointmentStartDateText;
	}

	/**
	 * @return the appointmentStartTimeText
	 */
	public String getAppointmentStartTimeText() {
		return appointmentStartTimeText;
	}

	/**
	 * @param appointmentStartTimeText the appointmentStartTimeText to set
	 */
	public void setAppointmentStartTimeText(String appointmentStartTimeText) {
		this.appointmentStartTimeText = appointmentStartTimeText;
	}

	/**
	 * @return the appointmentEndTimeText
	 */
	public String getAppointmentEndTimeText() {
		return appointmentEndTimeText;
	}

	/**
	 * @param appointmentEndTimeText the appointmentEndTimeText to set
	 */
	public void setAppointmentEndTimeText(String appointmentEndTimeText) {
		this.appointmentEndTimeText = appointmentEndTimeText;
	}

	/**
	 * @return the interpreterType
	 */
	public String getInterpreterType() {
		return interpreterType;
	}

	/**
	 * @param interpreterType In-House or Agency?
	 */
	public void setInterpreterType(String type) {
		this.interpreterType = type;
	}

	/**
	 * @return the interpreterDocKey
	 */
	public String getInterpreterDocKey() {
		return interpreterDocKey;
	}

	/**
	 * @param interpreterDocKey ID for agency/interpreter
	 */
	public void setInterpreterDocKey(String DocKey) {
		this.interpreterDocKey = DocKey;
	}

	/**
	 * @return the interpreterFullName
	 */
	public String getInterpreterFullName() {
		return interpreterFullName;
	}

	/**
	 * @param name Interpreter Full Name or Agency Name
	 */
	public void setInterpreterFullName(String name) {
		this.interpreterFullName = name;
	}

	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * @return the actualAppointmentStartTimeText
	 */
	public String getActualAppointmentStartTimeText() {
		return actualAppointmentStartTimeText;
	}

	/**
	 * @param actualAppointmentStartTimeText the actualAppointmentStartTimeText to set
	 */
	public void setActualAppointmentStartTimeText(String actualAppointmentStartTimeText) {
		this.actualAppointmentStartTimeText = actualAppointmentStartTimeText;
	}

	/**
	 * @return the actualAppointmentEndTimeText
	 */
	public String getActualAppointmentEndTimeText() {
		return actualAppointmentEndTimeText;
	}

	/**
	 * @param actualAappointmentEndTimeText the actualAappointmentEndTimeText to set
	 */
	public void setActualAppointmentEndTimeText(String actualAppointmentEndTimeText) {
		this.actualAppointmentEndTimeText = actualAppointmentEndTimeText;
	}

	/**
	 * @return the actualDuration
	 */
	public double getActualDuration() {
		return actualDuration;
	}

	/**
	 * @param actualApptDuration the actualDuration to set
	 */
	public void setActualDuration(int actualApptDuration) {
		this.actualDuration = actualApptDuration;
	}

	/**
	 * @return the languageCode
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * @param languageCode the languageCode to set
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the isManagedByIMS
	 */
	public boolean isManagedByIMS() {
		return isManagedByIMS;
	}

	/**
	 * @param isManagedByIMS the isManagedByIMS to set
	 */
	public void setManagedByIMS(boolean isManagedByIMS) {
		this.isManagedByIMS = isManagedByIMS;
	}

	/**
	 * @return the externalReference
	 */
	public String getExternalReference() {
		return externalReference;
	}

	/**
	 * @param externalReference the externalReference to set
	 */
	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}

	/**
	 * @return the patientType
	 */
	public String getPatientType() {
		return patientType;
	}

	/**
	 * 
	 * @param patientType
	 */
	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	/**
	 * @return the patientName
	 */
	public String getPatientName() {
		return patientName;
	}

	/**
	 * @param patientName the patientName to set
	 */
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	/**
	 * @return the deliveryMethod
	 */
	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	/**
	 * @param deliveryMethod the deliveryMethod to set
	 */
	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	/**
	 * @return the communicationDetails
	 */
	public String getCommunicationDetails() {
		return communicationDetails;
	}

	/**
	 * @param communicationDetails - the communicationDetails to set
	 */
	public void setCommunicationDetails(String communicationDetails) {
		this.communicationDetails = communicationDetails;
	}

	/**
	 * @return the videoConferencePlatform
	 */
	public String getVideoConferencePlatform() {
		return videoConferencePlatform;
	}

	/**
	 * @param videoConferencePlatform - the videoConferencePlatform to set
	 */
	public void setVideoConferencePlatform(String videoConferencePlatform) {
		this.videoConferencePlatform = videoConferencePlatform;
	}

	/**
	 * @return the agencyNotes
	 */
	public String getAgencyNotes() {
		return agencyNotes;
	}

	/**
	 * @param agencyNotes the requestorComments to set
	 */
	public void setAgencyNotes(String agencyNotes) {
		this.agencyNotes = agencyNotes;
	}

	/**
	 * @return the agencyNotes
	 */
	public String getInterpreterNotes() {
		return interpreterNotes;
	}

	/**
	 * @param agencyNotes the requestorComments to set
	 */
	public void setInterpreterNotes(String interpreterNotes) {
		this.interpreterNotes = interpreterNotes;
	}

	/**
	 * @return the externalLink
	 */
	public String getExternalLink() {
		return externalLink;
	}

	/**
	 * @param externalLink the externalLink to set
	 */
	public void setExternalLink(String externalLink) {
		this.externalLink = externalLink;
	}

	/**
	 * @return the cancelledBy
	 */
	public String getCancelledBy() {
		return cancelledBy;
	}

	/**
	 * @param cancelledBy the cancelledBy to set
	 */
	public void setCancelledBy(String cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	/**
	 * @return the cancellationReason
	 */
	public String getCancellationReason() {
		return cancellationReason;
	}

	/**
	 * @param cancellationReason the cancellationReason to set
	 * Same field(Cancellation reason) is used for Completion/Non-completion notes
	 */
	public void setCancellationReason(String reason) {
		this.cancellationReason = reason;
	}

	/**
	 * @return the nonCompletionReason
	 */
	public String getNonCompletionReason() {
		return nonCompletionReason;
	}

	/**
	 * @param nonCompletionReason the nonCompletionReason to set
	 */
	public void setNonCompletionReason(String nonCompletionReason) {
		this.nonCompletionReason = nonCompletionReason;
	}
	
	/**
	 * @return the rejectionReason
	 */
	public String getRejectionReason() {
		return rejectionReason;
	}

	/**
	 * @param rejectionReason the rejectionReason to set
	 */
	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}

	/**
	 * @return the rejectedBy
	 */
	public String getRejectedBy() {
		return rejectedBy;
	}

	/**
	 * @param rejectedBy the rejectedBy to set
	 */
	public void setRejectedBy(String rejectedBy) {
		this.rejectedBy = rejectedBy;
	}

	/**
	 * @return the cancellationRequestedBy
	 */
	public String getCancellationRequestedBy() {
		return cancellationRequestedBy;
	}

	/**
	 * @param cancellationRequestedBy the cancellationRequestedBy to set
	 */
	public void setCancellationRequestedBy(String cancellationRequestedBy) {
		this.cancellationRequestedBy = cancellationRequestedBy;
	}

	/**
	 * @return the cancellationNotice
	 */
	public String getCancellationNotice() {
		return cancellationNotice;
	}

	/**
	 * @param cancellationNotice the cancellationNotice to set
	 */
	public void setCancellationNotice(String cancellationNotice) {
		this.cancellationNotice = cancellationNotice;
	}
	
	/**
	 * @return the cancellationTime
	 */
	public String getCancellationTime() {
		return cancellationTime;
	}

	/**
	 * @param cancellationTime the cancellationTime to set
	 */
	public void setCancellationTime(String cancellationTime) {
		this.cancellationTime = cancellationTime;
	}
	
	/**
	 * @return the cancellationTimeText
	 */
	public String getCancellationTimeText() {
		return cancellationTimeText;
	}

	/**
	 * @param cancellationTimeText the cancellationTimeText to set
	 */
	public void setCancellationTimeText(String cancellationTimeText) {
		this.cancellationTimeText = cancellationTimeText;
	}

	
	/**
	 * @return the cancellationDateText
	 */
	public String getCancellationDateText() {
		return cancellationDateText;
	}

	/**
	 * @param cancellationDateText the cancellationDateText to set
	 */
	public void setCancellationDateText(String cancellationDateText) {
		this.cancellationDateText = cancellationDateText;
	}
	
	/**
	 * @return the cancellation time in format yyyyMMdd HH:mm
	 */
	public String getCancellationTimeFormatted() {
		return cancellationTimeFormatted;
	}

	/**
	 * @param cancellationTimeFormatted the formatted cancellation time to set
	 */
	public void setCancellationTimeFormatted() {
		DateTime cancellationDateTimeToFormat = null;
		try {
			cancellationDateTimeToFormat = Globals.getDateTimeFromString(cancellationTime, session);
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());				
			Globals.printException(e);
		}
		cancellationTimeFormatted = Globals.formatDate(cancellationDateTimeToFormat, "yyyyMMdd HH:mm");
	}
	
	/**
	 * @return the cancellationCharges
	 */
	public String getCancellationCharges() {
		return cancellationCharges;
	}

	/**
	 * @param cancellationCharges the cancellationCharges to set
	 */
	public void setCancellationCharges(String cancellationCharges) {
		this.cancellationCharges = cancellationCharges;
	}
	
	/**
	 * @return the rejectionReason
	 */
	public String getAgencyFlowComplete() {
		return agencyFlowComplete;
	}

	/**
	 * @param agencyFlowComplete the agencyFlowComplete to set
	 */
	public void setAgencyFlowComplete(String agencyFlowComplete) {
		this.agencyFlowComplete = agencyFlowComplete;
	}

	/**
	 * @return the agencyFlowCompleteTime
	 */
	public String getAgencyFlowCompleteTime() {
		return agencyFlowCompleteTime;
	}

	/**
	 * @param agencyFlowCompleteTime the agencyFlowCompleteTime to set
	 */
	public void setAgencyFlowCompleteTime(String agencyFlowCompleteTime) {
		this.agencyFlowCompleteTime = agencyFlowCompleteTime;
	}

	/**
	 * @return the completed
	 */
	public String getCompleted() {
		return completed;
	}

	/**
	 * @param completed the completed to set
	 */
	public void setCompleted(String completed) {
		this.completed = completed;
	}

	/**
	 * @return the ethnicityPreference
	 */
	public JsonObject getEthnicityPreference() {
		return ethnicityPreference;
	}

	/**
	 * @param ethnicityPreference the ethnicityPreference to set
	 */
	public void setEthnicityPreference(JsonObject ethnicityPreference) {
		this.ethnicityPreference = ethnicityPreference;
	}

	/**
	 * @return the contactNumbers
	 */
	public JsonArray getContactNumbers() {
		return contactNumbers;
	}

	/**
	 * @param contactNumbers the contactNumbers to set
	 */
	public void setContactNumbers(JsonArray contactNumbers) {
		this.contactNumbers = contactNumbers;
	}

	/**
	 * @return the genderPreference
	 */
	public JsonObject getGenderPreference() {
		return genderPreference;
	}

	/**
	 * @param genderPreference the genderPreference to set
	 */
	public void setGenderPreference(JsonObject genderPreference) {
		this.genderPreference = genderPreference;
	}

	/**
	 * @return the address
	 */
	public JsonObject getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(JsonObject address) {
		this.address = address;
	}

	/**
	 * @return the professionals
	 */
	public JsonArray getProfessionals() {
		return professionals;
	}

	/**
	 * @param professionals the professionals to set
	 */
	public void setProfessionals(JsonArray professionals) {
		this.professionals = professionals;
	}

	/**
	 * @return the patients
	 */
	public JsonArray getPatients() {
		return patients;
	}

	/**
	 * @param patients the patients to set
	 */
	public void setPatients(JsonArray patients) {
		this.patients = patients;
	}

	/**
	 * @return the interpreters
	 */
	public JsonArray getInterpreters() {
		return interpreters;
	}

	/**
	 * @param interpreters the interpreters to set
	 */
	public void setInterpreters(JsonArray interpreters) {
		this.interpreters = interpreters;
	}

	/**
	 * @return the contacts
	 */
	public JsonArray getContacts() {
		return contacts;
	}

	/**
	 * @param contacts the contacts to set
	 */
	public void setContacts(JsonArray contacts) {
		this.contacts = contacts;
	}

	/**
	 * @return the dataDB
	 */
	public ImsDataDb getDataDb() {
		return dataDb;
	}

	/**
	 * @param dataDB the dataDB to set
	 */
	public void setDataDb(ImsDataDb dataDb) {
		this.dataDb = dataDb;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @param displayStatus the displayStatus to set
	 */
	public void setDisplayStatus(String displayStatus) {
		this.displayStatus = displayStatus;
	}
	
	/**
	 * @return the displayStatus
	 */
	public String getDisplayStatus() {
		return displayStatus;
	}
	
	/**
	 * @return the genderPreferencePriority
	 */
	public String getGenderPreferencePriority() {
		return genderPreferencePriority;
	}

	/**
	 * @param genderPreferencePriority the genderPreferencePriority to set
	 */
	public void setGenderPreferencePriority(String genderPreferencePriority) {
		this.genderPreferencePriority = genderPreferencePriority;
	}

	/**
	 * @return the genderPreferenceCode
	 */
	public String getGenderPreferenceCode() {
		return genderPreferenceCode;
	}

	/**
	 * @param genderPreferenceCode the genderPreferenceCode to set
	 */
	public void setGenderPreferenceCode(String genderPreferenceCode) {
		this.genderPreferenceCode = genderPreferenceCode;
	}

	public double getTravelTime() {
		return travelTime;
	}

	public void setTravelTime(double time) {
		this.travelTime = time;
	}

	public double getDistanceTravelled() {
		return distanceTravelled;
	}

	public void setDistanceTravelled(double distance) {
		this.distanceTravelled = distance;
	}

	public double getOtherCharges() {
		return otherCharges;
	}

	public void setOtherCharges(double otherCharges) {
		this.otherCharges = otherCharges;
	}

	/**
	 * @return the createdDateTime
	 */
	public DateTime getCreatedDateTime() {
		return createdDateTime;
	}

	/**
	 * @param createdDateTime2 the createdDateTime to set
	 */
	public void setCreatedDateTime(DateTime createdDateTime2) {
		this.createdDateTime = createdDateTime2;
	}

	/**
	 * @return the kpiAllocatedFirstBy
	 */
	public String getKpiAllocatedFirstBy() {
		return kpiAllocatedFirstBy;
	}

	/**
	 * @param kpiAllocatedFirstBy the kpiAllocatedFirstBy to set
	 */
	public void setKpiAllocatedFirstBy(String kpiAllocatedFirstBy) {
		this.kpiAllocatedFirstBy = kpiAllocatedFirstBy;
	}

	/**
	 * @return the kpiAllocatedFirstDate
	 */
	public DateTime getKpiAllocatedFirstDate() {
		return kpiAllocatedFirstDate;
	}

	/**
	 * @param kpiAllocatedFirstDate the kpiAllocatedFirstDate to set
	 */
	public void setKpiAllocatedFirstDate(DateTime kpiAllocatedFirstDate) {
		this.kpiAllocatedFirstDate = kpiAllocatedFirstDate;
	}

	/**
	 * @return the kpiAllocatedFirstDocKey
	 */
	public String getKpiAllocatedFirstDocKey() {
		return kpiAllocatedFirstDocKey;
	}

	/**
	 * @param kpiAllocatedFirstDocKey the kpiAllocatedFirstDocKey to set
	 */
	public void setKpiAllocatedFirstDocKey(String kpiAllocatedFirstDocKey) {
		this.kpiAllocatedFirstDocKey = kpiAllocatedFirstDocKey;
	}

	/**
	 * @return the kpiAllocatedLastBy
	 */
	public String getKpiAllocatedLastBy() {
		return kpiAllocatedLastBy;
	}

	/**
	 * @param kpiAllocatedLastBy the kpiAllocatedLastBy to set
	 */
	public void setKpiAllocatedLastBy(String kpiAllocatedLastBy) {
		this.kpiAllocatedLastBy = kpiAllocatedLastBy;
	}

	/**
	 * @return the kpiAllocatedLastDate
	 */
	public DateTime getKpiAllocatedLastDate() {
		return kpiAllocatedLastDate;
	}

	/**
	 * @param kpiAllocatedLastDate the kpiAllocatedLastDate to set
	 */
	public void setKpiAllocatedLastDate(DateTime kpiAllocatedLastDate) {
		this.kpiAllocatedLastDate = kpiAllocatedLastDate;
	}

	/**
	 * @return the kpiAllocatedLastDocKey
	 */
	public String getKpiAllocatedLastDocKey() {
		return kpiAllocatedLastDocKey;
	}

	/**
	 * @param kpiAllocatedLastDocKey the kpiAllocatedLastDocKey to set
	 */
	public void setKpiAllocatedLastDocKey(String kpiAllocatedLastDocKey) {
		this.kpiAllocatedLastDocKey = kpiAllocatedLastDocKey;
	}

	/**
	 * @return the kpiSubmittedAgencyBy
	 */
	public String getKpiSubmittedAgencyBy() {
		return kpiSubmittedAgencyBy;
	}

	/**
	 * @param kpiSubmittedAgencyBy the kpiSubmittedAgencyBy to set
	 */
	public void setKpiSubmittedAgencyBy(String kpiSubmittedAgencyBy) {
		this.kpiSubmittedAgencyBy = kpiSubmittedAgencyBy;
	}

	/**
	 * @return the kpiSubmittedAgencyDate
	 */
	public DateTime getKpiSubmittedAgencyDate() {
		return kpiSubmittedAgencyDate;
	}

	/**
	 * @param kpiSubmittedAgencyDate the kpiSubmittedAgencyDate to set
	 */
	public void setKpiSubmittedAgencyDate(DateTime kpiSubmittedAgencyDate) {
		this.kpiSubmittedAgencyDate = kpiSubmittedAgencyDate;
	}

	/**
	 * @return the kpiSubmittedAgencyDocKey
	 */
	public String getKpiSubmittedAgencyDocKey() {
		return kpiSubmittedAgencyDocKey;
	}

	/**
	 * @param kpiSubmittedAgencyDocKey the kpiSubmittedAgencyDocKey to set
	 */
	public void setKpiSubmittedAgencyDocKey(String kpiSubmittedAgencyDocKey) {
		this.kpiSubmittedAgencyDocKey = kpiSubmittedAgencyDocKey;
	}

	/**
	 * @return the createdDate
	 */
	public String getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the nowDateTime
	 */
	public DateTime getNowDateTime() {
		return nowDateTime;
	}

	/**
	 * @param nowDateTime the nowDateTime to set
	 */
	public void setNowDateTime(DateTime nowDateTime) {
		this.nowDateTime = nowDateTime;
	}

	/**
	 * @return the nowDateTimeFormatted
	 */
	public String getNowDateTimeFormatted() {
		return nowDateTimeFormatted;
	}

	/**
	 * @param nowDateTimeFormatted the nowDateTimeFormatted to set
	 */
	public void setNowDateTimeFormatted(String nowDateTimeFormatted) {
		this.nowDateTimeFormatted = nowDateTimeFormatted;
	}

	/**
	 * 
	 */
	public void createBooking() {
		try {
			bookingDoc = dataDb.getDatabase().createDocument();
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
	}
	
	/**
	 * Saves the booking information 
	 */
	public void save() {
		try {
			bookingDoc.replaceItemValue("Form", form);
			if (isManagedByIMS) { //TODO - Currently not required - default is yes
				bookingDoc.replaceItemValue (formPrefix + "isManaged_By_IMS", "true");
			} else {
				bookingDoc.replaceItemValue (formPrefix + "isManaged_By_IMS", "false");
			}
			bookingDoc.replaceItemValue(formPrefix + "Interpreter_DocKey", interpreterDocKey);
			bookingDoc.replaceItemValue(formPrefix + "Interpreter_FullName", interpreterFullName);
			bookingDoc.replaceItemValue(formPrefix + "Interpreter_Type", interpreterType);
			bookingDoc.replaceItemValue(formPrefix + "Source", source );
			bookingDoc.replaceItemValue(formPrefix + "Source_Display", interpreterFullName);
			bookingDoc.replaceItemValue(formPrefix + "Appt_Start", appointmentStartDateTime);
			bookingDoc.replaceItemValue(formPrefix + "Appt_End", appointmentEndDateTime);
			bookingDoc.replaceItemValue(formPrefix + "ApptStartDate_Text", appointmentStartDateText);
			bookingDoc.replaceItemValue(formPrefix + "ApptStartTime_Text", appointmentStartTimeText);
			bookingDoc.replaceItemValue(formPrefix + "ApptDuration", duration);
			bookingDoc.replaceItemValue(formPrefix + "Language_Code", languageCode);
			bookingDoc.replaceItemValue(formPrefix + "Language", language);
			bookingDoc.replaceItemValue(formPrefix + "Agency_ID", externalReference);
			bookingDoc.replaceItemValue(formPrefix + "Agency_URL", externalLink);
			bookingDoc.replaceItemValue(formPrefix + "BookingType", deliveryMethod);
			bookingDoc.replaceItemValue(formPrefix + "CommunicationDetails", communicationDetails);
			bookingDoc.replaceItemValue(formPrefix + "VideoConferencePlatform", videoConferencePlatform);
			if (isFlexible) bookingDoc.replaceItemValue(formPrefix + "Flexible", Constants.YES);
			bookingDoc.replaceItemValue(formPrefix + "Flexible_TimeStart", flexibleTimeStart);
			bookingDoc.replaceItemValue(formPrefix + "Flexible_TimeEnd", flexibleTimeEnd);
			bookingDoc.replaceItemValue(formPrefix + "modality", modality);
			bookingDoc.replaceItemValue(formPrefix + "PatientType", patientType);
			bookingDoc.replaceItemValue(formPrefix + "Comments", agencyNotes); 
			bookingDoc.replaceItemValue(formPrefix + "InterpreterNotes", interpreterNotes);
			bookingDoc.replaceItemValue(formPrefix + "SystemComments", systemComments);
			bookingDoc.replaceItemValue(formPrefix + "Site", site);
			bookingDoc.replaceItemValue(formPrefix + "SiteName", siteName);
			bookingDoc.replaceItemValue(formPrefix + "Location", location);
			bookingDoc.replaceItemValue(formPrefix + "Clinic", clinic);
			bookingDoc.replaceItemValue(formPrefix + "CostCentre", costCentre);
			bookingDoc.replaceItemValue(formPrefix + "Clinic_Address", clinicAddress);
			bookingDoc.replaceItemValue(formPrefix + "Clinic_Suburb", clinicSuburb);
			bookingDoc.replaceItemValue(formPrefix + "Clinic_PostCode", clinicPostCode);
			bookingDoc.replaceItemValue(formPrefix + "Clinic_State", clinicState);
			bookingDoc.replaceItemValue(formPrefix + "CLN_Code", clinicCode);
			bookingDoc.replaceItemValue(formPrefix + "Agency_Last_Updated", nowDateTimeFormatted);
			
			//Ashish - Feb 2022 - Add the external Booking ID reference (pmsId)
			if(pmsId != null){ bookingDoc.replaceItemValue(formPrefix + "PMSID", pmsId); }
			
			bookingDoc.replaceItemValue(formPrefix + "isCreatedByIMSAPI", "Y");
			bookingDoc.replaceItemValue(formPrefix + "WaitingDuration", waitingDuration);
			bookingDoc.replaceItemValue(formPrefix + "NumCalls", numCalls);
			bookingDoc.replaceItemValue("CreatedDateTime", createdDateTime);
			bookingDoc.replaceItemValue("DocKey_CreatedBy", interpreterDocKey);
			bookingDoc.replaceItemValue("CreatedDate", createdDate);
			bookingDoc.replaceItemValue("CreatedBy", interpreterFullName);
			if (kpiAllocatedFirstBy != null && !kpiAllocatedFirstBy.equals("")) {
				bookingDoc.replaceItemValue("KPI_AllocatedFirst_By", kpiAllocatedFirstBy);
			}
			if (kpiAllocatedFirstDate != null && !kpiAllocatedFirstDate.equals("")) {
				bookingDoc.replaceItemValue("KPI_AllocatedFirst_Date", kpiAllocatedFirstDate);
			}
			if (kpiAllocatedFirstDocKey != null && !kpiAllocatedFirstDocKey.equals("")) {
				bookingDoc.replaceItemValue("KPI_AllocatedFirst_DocKey", kpiAllocatedFirstDocKey);
			}
			if (kpiAllocatedLastBy != null && !kpiAllocatedLastBy.equals("")) {
				bookingDoc.replaceItemValue("KPI_AllocatedLast_By", kpiAllocatedLastBy);
			}
			if (kpiAllocatedLastDate != null && !kpiAllocatedLastDate.equals("")) {
				bookingDoc.replaceItemValue("KPI_AllocatedLast_Date", kpiAllocatedLastDate);
			}
			
			if (kpiAllocatedLastDocKey != null && !kpiAllocatedLastDocKey.equals("")) {
				bookingDoc.replaceItemValue("KPI_AllocatedLast_DocKey", kpiAllocatedLastDocKey);
			}
			if (kpiSubmittedAgencyBy != null && !kpiSubmittedAgencyBy.equals("")) {
				bookingDoc.replaceItemValue("KPI_submitted_agency_By", kpiSubmittedAgencyBy);
			}
			if (kpiSubmittedAgencyDate != null && !kpiSubmittedAgencyDate.equals("")) {
				bookingDoc.replaceItemValue("KPI_submitted_agency_Date", kpiSubmittedAgencyDate);
			}
			if (kpiSubmittedAgencyDocKey != null && !kpiSubmittedAgencyDocKey.equals("")) {
				bookingDoc.replaceItemValue("KPI_submitted_agency_DocKey", kpiSubmittedAgencyDocKey);
			}
			setGenderPreference();
			addProfessionalFields(getProfessionals());
			addPatientFields(getPatients());
			addContactFields(getContacts());	
			addInterpreterFields(getInterpreters());	
			setEthnicityPreferenceFields();
			bookingDoc.replaceItemValue(formPrefix + "Status", status);
			displayStatus = bookingController.getKWBookingStatus(status); 
			if (displayStatus == null || displayStatus.equals("")) { //This is for Extra Reasons
				displayStatus = status;
			}
			bookingDoc.replaceItemValue(formPrefix + "Status_Display", displayStatus);
			if (!bookingController.isInValid() || bookingController.isLSPAllowedToCreateIgnoreValid()) {
				
				bookingLog.append("Created document from IMS API by " + agencyLabel + ": " + bookingController.getClientName()); 
				Globals.LogActionToDocument(nowDateTime, bookingDoc, bookingLog.toString(), false, session);
				bookingLog = new StringBuilder();
				if (bookingController.isInValid()) {
					bookingLog.append("****Booking encountered below errors****");
					Globals.LogActionToDocument(nowDateTime, bookingDoc, bookingLog.toString(), false, session);
					ArrayList<Integer> messageCode = bookingController.messageCode;
					Map<Integer, String> invalidDataString = bookingController.invalidDataString;
					if (messageCode.size() > 0) {
						for (int code : messageCode) {
							bookingLog = new StringBuilder();
							bookingLog.append(Globals.getMessageFromCode(code, session, kwHandlerIMSDB) + " ");
							if (invalidDataString.get(code) != null) {
								bookingLog.append(" - " + invalidDataString.get(code) + ". ");
							}
							Globals.LogActionToDocument(nowDateTime, bookingDoc, bookingLog.toString(), false, session);	
							
						}
					}
					if (invalidDataString.size() > 0) {
						bookingLog = new StringBuilder();
						bookingLog.append("**************************************");
						Globals.LogActionToDocument(nowDateTime, bookingDoc, bookingLog.toString(), false, session);	
					}
					status = Constants.bookingStatusDataBookingError;
					bookingDoc.replaceItemValue(formPrefix + "Status", status);
					displayStatus = bookingController.getKWBookingStatus(status);
					bookingDoc.replaceItemValue(formPrefix + "Status_Display", displayStatus);
				}
				bookingDoc.computeWithForm(true, false);
				bookingDoc.save();
				id = bookingDoc.getItemValueString("DocKey");
				//Trigger email document creation
				if (oldDisplayStatus != null && !oldDisplayStatus.equalsIgnoreCase(displayStatus)) {
					notifyByEmail(displayStatus);
				}
			}

		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
	}

	/**
	 * Checks if the field value is different and replaces it if it is
	 * returns true if we replaced it, false otherwise.
	 * @param fieldName
	 * @param value
	 * @return
	 */
	public void addFieldOnBooking (String fieldName, String fieldValue) throws NotesException {
		try {
			if (fieldName != null && fieldValue != null && !fieldName.equals("") && !fieldValue.equals("")) {
				bookingDoc.replaceItemValue(fieldName.trim(), fieldValue);
			}
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);	
		}
	}

	/**
	 * Removes field on a booking
	 * @param fieldName
	 * @return
	 */
	public void removeFieldOnBooking (String fieldName) throws NotesException {
		try {
			if (fieldName != null && !fieldName.equals("")) {
				bookingDoc.removeItem(fieldName.trim());
			}
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
	}
	
	/**
	 * Get field from a booking
	 * @param type
	 * @param fieldName
	 * @return
	 * @throws NotesException
	 */
	public String getFieldFromBooking (String type, String fieldName) throws NotesException {
		try {
			if (fieldName != null && !fieldName.equals("")) {
				if (type == null || type.equals("")) {
					type = "String";
				}
				if (type.equalsIgnoreCase("String")) {
					if (!bookingDoc.getItemValueString(fieldName.trim()).equals("") && bookingDoc.getItemValueString(fieldName.trim()) != null) {
						return bookingDoc.getItemValueString(fieldName.trim());
					}
				}
			}
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
		return "";
	}

	/**
	 * Add professional/clinician details 
	 * @param professionals
	 */
	public void addProfessionalFields(JsonArray professionals) {
		try {
			if (professionals.size() > 0) {
				addFieldOnBooking(formPrefix + "Count_Clinicians", Integer.toString(professionals.size()));
				//We can have multiple professionals/clinician
				for(int i=0;i<professionals.size();i++) {
				   JsonObject dataObj = (JsonObject) professionals.get(i);
				   String ClinicianTitle = null;
				   String ClinicianFirstName = null;
				   String ClinicianLastName = null;
				   String ClinicianName = null;
				   String ClinicianStartTime = null;
				   String ClinicianEndTime = null;
				   String ClinicianEmail = null;

				   String title = "";
				   if (dataObj.get("title") != null) {
					   title = Globals.validateString(dataObj.get("title"));
				   }
				   String firstName = "";
				   if (dataObj.get("firstName") != null) {
					   firstName = Globals.validateString(dataObj.get("firstName"));
				   }
				   String lastName = "";
				   if (dataObj.get("lastName") != null) {
					   lastName = Globals.validateString(dataObj.get("lastName"));
				   }
				   //Default to appt start and end time 
				   String startTime = appointmentStartTimeText; //TODO more features in future to check if start and end time falls between appointment start and end time.
				   String endTime = appointmentEndTimeText;
				   if (dataObj.get("startTime") != null) {
					   startTime = Globals.validateString(dataObj.get("startTime"));
				   }
				   if (dataObj.get("endTime") != null) {
					   endTime = Globals.validateString(dataObj.get("endTime"));
				   }
				   String email = null;
				   if (dataObj.get("email") != null && !dataObj.get("email").toString().isEmpty()) {
					   if (Globals.isValidEmail((String) dataObj.get("email"))) {
						   email = Globals.validateValue("email", dataObj.get("email"));
					   } else {
						   bookingController.messageCode.add(3018);
						   bookingController.isInValid = true;
					   }
				   }
				   if (firstName.equals("")) {
					   bookingController.messageCode.add(3019);
					   bookingController.isInValid = true;
				   }
				   if (i == 0) { //Keep legacy fields as well
					   ClinicianTitle = formPrefix + "ClinicianTitle";
					   ClinicianFirstName = formPrefix + "ClinicianFirstName";
					   ClinicianLastName = formPrefix + "ClinicianLastName";
					   ClinicianName = formPrefix + "Clinician";
					   ClinicianStartTime = formPrefix + "ClinicianStartTime";
					   ClinicianEndTime = formPrefix + "ClinicianEndTime";
					   ClinicianEmail = formPrefix + "ClinicianEmail";
				   } else {
					   ClinicianTitle = formPrefix + "Clinician_" + i + "_Title";
					   ClinicianName = formPrefix + "Clinician_" + i;
					   ClinicianFirstName = formPrefix + "Clinician_" + i + "_FirstName";
					   ClinicianLastName = formPrefix + "Clinician_" + i + "_LastName";
					   ClinicianStartTime = formPrefix + "Clinician_" + i + "_StartTime";
					   ClinicianEndTime = formPrefix + "Clinician_" + i + "_EndTime";
					   ClinicianEmail = formPrefix + "Clinician_" + i + "_Email";
				   }
				   addFieldOnBooking(ClinicianTitle, title);
				   addFieldOnBooking(ClinicianFirstName, firstName);
				   addFieldOnBooking(ClinicianLastName, lastName);
				   addFieldOnBooking(ClinicianName, ((title.equals("") ? "" : title + " ") + firstName + " " + lastName).trim()); 
				   addFieldOnBooking(ClinicianStartTime, startTime);
				   addFieldOnBooking(ClinicianEndTime, endTime);
				   addFieldOnBooking(ClinicianEmail, email);
				   if (dataObj.get("contactNumbers") != null) {
					   setContactNumbers(Globals.validateJsonArray(dataObj.get("contactNumbers")));
					   addFieldOnBooking(formPrefix + "Count_Clinician_"+ i +"_ContactNumbers", Integer.toString(contactNumbers.size()));
					   //We can have multiple contact numbers for a professional
					   if (contactNumbers.size() > 0) {
						   for(int c=0;c<contactNumbers.size();c++) {
							   JsonObject contactObj = (JsonObject) contactNumbers.get(c);
							   String ClinicianContactType;
							   String type = Globals.validateString(Globals.verifyContactType((String) contactObj.get("type")));
							   String number = Globals.validateString(contactObj.get("number"));
							   if (i == 0  && type.equalsIgnoreCase("Mobile")) { //Keep legacy field
								   ClinicianContactType = formPrefix + "ClinicianContact"; //For the first clinician, we need keep the legacy field for the existing clinician contact
							   } else {
								   ClinicianContactType = formPrefix + "Clinician_" + i + "_Contact_" + type; //Ex: Clinician_0_Contact_0_Mobile
							   }
							   addFieldOnBooking(ClinicianContactType , number); 
						   } 
					   }
				   }
				}
			}
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
			return;
		}
	}
	
	/**
	 * Add patient details
	 * 
	 * @param patients
	 * @param bookingController 
	 * @throws NotesException 
	 */
	public void addPatientFields(JsonArray patients) throws NotesException {
		try {
			if (patients.size() > 0) {
				addFieldOnBooking(formPrefix + "Count_Patients", Integer.toString(patients.size()));
				//We can have multiple patients
				for(int i=0;i<patients.size();i++) {
				   JsonObject dataObj = (JsonObject) patients.get(i);
				   String PatientUR;
				   String PatientTitle;
				   String PatientFirstName;
				   String PatientLastName;
				   String PatientFullName;
				   String PatientDOB;
				   String PatientEmail;
				   String PatientStartTime;
				   String PatientEndTime;
				   String PatientGender;

				   String urNumber = "";
				   if (dataObj.get("urNumber") != null) {
					   urNumber = Globals.validateString(dataObj.get("urNumber"));
				   }
				   String dob = "";
				   if (dataObj.get("dob") != null) {
					   dob = Globals.validateString(dataObj.get("dob"));
				   }
				   String title = "";
				   if (dataObj.get("title") != null) {
					   title = Globals.validateString(dataObj.get("title"));
				   }
				   String firstName = "";
				   if (dataObj.get("firstName") != null) {
					   firstName = Globals.validateString(dataObj.get("firstName"));
				   }
				   String lastName = "";
				   if (dataObj.get("lastName") != null) {
					   lastName = Globals.validateString(dataObj.get("lastName"));
				   }
				   //Default to appt start and end time 
				   String startTime = appointmentStartTimeText; //TODO more features in future to check if start and end time falls between appointment start and end time.
				   String endTime = appointmentEndTimeText;
					
				   if (dataObj.get("startTime") != null) {
					   startTime = Globals.validateString(dataObj.get("startTime"));
				   }
				   if (dataObj.get("endTime") != null) {
					   endTime = Globals.validateString(dataObj.get("endTime"));
				   }
				   String email = null;
				   if (dataObj.get("email") != null && !dataObj.get("email").toString().isEmpty()) {
					   if (Globals.isValidEmail((String) dataObj.get("email"))) {
						   email = Globals.validateValue("email", dataObj.get("email"));
					   } else {
						   bookingController.messageCode.add(4030);
						   bookingController.isInValid = true;
					   }
				   }
				   /* if (urNumber == null || urNumber.equals("")) {
				   		bookingController.messageCode.add(4008);
	  					bookingController.isInValid = true;
				   }*/
				   if (firstName.equals("") || lastName.equals("")) {
					   bookingController.messageCode.add(3021);
					   bookingController.isInValid = true;
				   }
				   //Check if same URNumber for same clinic, appointment start date/time exists
				   if (!urNumber.equals("") && appointmentStartDateText != null && clinic != null) {
					   if (isDuplicateBooking(urNumber, appointmentStartDateText, appointmentStartTimeText, clinic)) {
						   bookingController.messageCode.add(3022);
						   bookingController.isInValid = true;
					   }
				   }
				   String gender = null;
				   if (dataObj.get("gender") != null) {
					   gender = Globals.verifyGenderFormat((String) dataObj.get("gender").toString());
				   }
				   if (i == 0) { //Keep legacy fields for first patient
					   PatientUR = formPrefix + "PatientUR";
					   PatientTitle = formPrefix + "PatientTitle";
					   PatientFirstName = formPrefix + "PatientFirstName";
					   PatientLastName = formPrefix + "PatientLastName";
					   PatientFullName = formPrefix + "PatientFullName";
					   PatientDOB = formPrefix + "Patient_DOB_Text";
					   PatientGender = formPrefix + "PatientGender";
					   PatientStartTime = formPrefix + "PatientStartTime";
					   PatientEndTime = formPrefix + "PatientEndTime";
					   PatientEmail = formPrefix + "PatientEmail";
				   } else {
					   PatientUR = formPrefix + "Patient_" + i + "_UR";
					   PatientTitle = formPrefix + "Patient_" + i + "_Title";
					   PatientFirstName = formPrefix + "Patient_" + i + "_FirstName";
					   PatientLastName = formPrefix + "Patient_" + i + "_LastName";
					   PatientFullName = formPrefix + "Patient_" + i + "_FullName";
					   PatientDOB = formPrefix + "Patient_" + i + "_DOB_Text";
					   PatientGender = formPrefix + "Patient_" + i + "_Gender";
					   PatientStartTime = formPrefix + "Patient_" + i + "_StartTime";
					   PatientEndTime = formPrefix + "Patient_" + i + "_EndTime";
					   PatientEmail = formPrefix + "Patient_" + i + "_Email";	
				   }
				   addFieldOnBooking(PatientUR, urNumber); 
				   addFieldOnBooking(PatientTitle, title); 
				   addFieldOnBooking(PatientFirstName, firstName);
				   addFieldOnBooking(PatientLastName, lastName); 
				   addFieldOnBooking(PatientFullName, (title.equals("") ? "" : title + " ") + firstName + " " + lastName); 
				   if (dob != null && !dob.equals("")) {
					   String validDob = Globals.getJavaFormattedDate(dob, "yyyyMMdd", kwHandlerIMSDB.getGlobalDateNotesFromFormat());
						  if (validDob == null || dob.length() != 8) {
						   bookingController.isInValid = true;
						   bookingController.messageCode.add(3020);	
					   } else {
						   addFieldOnBooking(PatientDOB, validDob);
					   }
				   }
				   addFieldOnBooking(PatientGender, gender);
				   addFieldOnBooking(PatientStartTime, startTime);
				   addFieldOnBooking(PatientEndTime, endTime);
				   addFieldOnBooking(PatientEmail, email);
				   
				   if (dataObj.get("contactNumbers") != null) {
					   contactNumbers =  Globals.validateJsonArray(dataObj.get("contactNumbers"));
					   addFieldOnBooking(formPrefix + "Count_Patient_" + i + "_ContactNumbers", Integer.toString(contactNumbers.size()));
					   if (contactNumbers.size() > 0) {
						   //We can have multiple contact numbers for a patient
						   for (int c=0;c<contactNumbers.size();c++) {
							   JsonObject contactObj = (JsonObject) contactNumbers.get(c);
							   String type = Globals.validateString(Globals.verifyContactType((String) contactObj.get("type")));
							   String number = "";
							  
							   String PatientContactType = null;
							   if (i == 0) { //Add to legacy fields as well, still cannot add MCode as this is not received in Create API
								   if (type.equalsIgnoreCase("Mobile")) { 
									   if (contactObj.get("number") != null) {
										   number = Globals.validateIntString(contactObj.get("number")); //For patient mobile, keep it numbers
									   }
									   PatientContactType = formPrefix + "Patient_Mobile";
								   }
								   if (type.equalsIgnoreCase("Home")) { 
									   if (contactObj.get("number") != null) {
										   number = Globals.validateString(contactObj.get("number"));
									   }
									   PatientContactType = formPrefix + "PatientPhone";
								   }
								   if (type.equalsIgnoreCase("Work")) { 
									   if (contactObj.get("number") != null) {
										   number = Globals.validateString(contactObj.get("number"));
									   }
									   PatientContactType = formPrefix + "Patient_Work";
								   }
							   } else {
								   PatientContactType = formPrefix + "Patient_" + i + "_Contact_" + type; //In this way, we can split to get which patient and which contact and whats the type. Example:BOOK_Patient_0_ContactType_0_Home/BOOK_Patient_0_ContactType_0_Mobile
							   }
							   addFieldOnBooking(PatientContactType , number);
						   }
					   }
				   }
				   if (dataObj.get("address") != null) {
					   if (Globals.isValidJsonObject(dataObj.get("address"))) {
							setAddress(Globals.validateJsonObject(dataObj.get("address")));
							setPatientAddress(i);
					   } else {
						   bookingController.isInValid = true;
						   bookingController.messageCode.add(4005);		
					   }
				   }
				   if (getPatientType() != null) {
					   if (getPatientType().equalsIgnoreCase(Constants.homeVisit)) {
						   if (address.size() == 0) {
							   bookingController.messageCode.add(3026);
							   bookingController.isInValid = true;
							}
					   }
				   }
				   
				}
			}	
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
			return;
		}
	}
	
	/**
	 * Add contact details 
	 * @param contacts
	 */
	public void addContactFields(JsonArray contacts) {
		try {
			if (contacts.size() > 0) {
				addFieldOnBooking(formPrefix + "Count_Contacts", Integer.toString(contacts.size()));
				//We can have multiple contacts
				for(int i=0;i<contacts.size();i++) {
				   JsonObject dataObj = (JsonObject) contacts.get(i);
				   String ContactTitle;
				   String ContactFirstName;
				   String ContactLastName;
				   String ContactFullName;
				   String ContactEmail;
				   
				   String title = Globals.validateString(dataObj.get("title"));
				   String firstName = Globals.validateString(dataObj.get("firstName"));
				   String lastName = Globals.validateString(dataObj.get("lastName"));
				   String email = null;
				   String workNumber = "";
				   Boolean overrideFromClinic = false;
				   if (kwHandlerIMSDB.getKeywordValueString(Constants.KW_OVERRIDE_REQU_DETAILS_FROM_CLINIC).equals("1") && bookingController.isLspAllowedToAccessAll){
				   		JsonObject clinicDetails = kwHandlerIMSDB.getClinicDetails(clinicCode);
				   		overrideFromClinic = true;
				   		email = clinicDetails.get("clinicContactEmail") != null ? (String) clinicDetails.get("clinicContactEmail") : "";
				   		workNumber = clinicDetails.get("clinicContactPhone") != null ? (String) clinicDetails.get("clinicContactPhone") : "";
				   		
				   }
				   if (firstName.equals("")) {
					   bookingController.messageCode.add(3036);
					   bookingController.isInValid = true;
				   }
				   if (!overrideFromClinic && dataObj.get("email") != null && !dataObj.get("email").toString().isEmpty()) {
					   if (Globals.isValidEmail((String) dataObj.get("email"))) {
						   email = Globals.validateValue("email", dataObj.get("email"));
					   } else {
						   bookingController.messageCode.add(3023);
						   bookingController.isInValid = true;
					   }
				   }
				   if (i == 0) { //Keep legacy fields as well
					   ContactTitle = formPrefix + "RequestedByTitle";
					   ContactFirstName = formPrefix + "RequestedByFirstName";
					   ContactLastName = formPrefix + "RequestedByLastName";
					   ContactFullName = formPrefix + "RequestedBy";
					   ContactEmail = formPrefix + "RequestedByEmail";
				   } else {
					   ContactTitle = formPrefix + "RequestedBy_" + i + "_Title";
					   ContactFirstName = formPrefix + "RequestedBy_" + i + "_FirstName";
					   ContactLastName = formPrefix + "RequestedBy_" + i + "_LastName";
					   ContactFullName = formPrefix + "RequestedBy_" + i;
					   ContactEmail = formPrefix + "RequestedBy_" + i + "_Email"; //RequestedBy_0_Email	  
				   }
				   addFieldOnBooking(ContactTitle, title); 
				   addFieldOnBooking(ContactFirstName, firstName); 
				   addFieldOnBooking(ContactLastName, lastName); 
				   addFieldOnBooking(ContactFullName, (title.equals("") ? "" : title + " ") + firstName + " " + lastName); 
				   addFieldOnBooking(ContactEmail, email);
				   if (dataObj.get("contactNumbers") != null) {
					   contactNumbers =  Globals.validateJsonArray(dataObj.get("contactNumbers"));
					   addFieldOnBooking(formPrefix + "Count_Contact_" + i + "_ContactNumbers", Integer.toString(contactNumbers.size()));
					   if (contactNumbers.size() > 0) {
						   //We can have multiple contact numbers for a Contact
						   for (int c=0;c<contactNumbers.size();c++) {
							   JsonObject contactObj = (JsonObject) contactNumbers.get(c);
							   String ContactContactType = null;
							   String type = Globals.validateString(Globals.verifyContactType((String) contactObj.get("type")));
							   String number = Globals.validateString(contactObj.get("number"));
							   if (i == 0 && type.equalsIgnoreCase("Work")) { //For first contact, save as Work, we need to keep legacy field as well as new for API
								   ContactContactType = formPrefix + "RequestedByContact";
							   } else {
								   ContactContactType = formPrefix + "RequestedBy_" + i + "_Contact_" + type; //RequestedBy_0_ContactType_0_Mobile
							   }
							   if (type.equalsIgnoreCase("Work") && overrideFromClinic){
								   number = workNumber;
							   }
							   if (!(overrideFromClinic && type.equalsIgnoreCase("Work"))){
								   addFieldOnBooking(ContactContactType , number);
							   }
						   }
					   }
					   if (overrideFromClinic){
						   addFieldOnBooking(formPrefix + "RequestedByContact" , workNumber);
					   }
				   }
				}
			}
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
			return;
		}
	}
	
	/**
	 * Add interpreter details 
	 * @param interpreters
	 */
	public void addInterpreterFields(JsonArray interpreters) {
		try {
			String tempDocKey = "";
			if (interpreters.size() > 0) {
				addFieldOnBooking(formPrefix + "Count_Interpreters", Integer.toString(interpreters.size()));
				//We can have multiple interpreters
				for(int i=0;i<interpreters.size();i++) {
				   JsonObject dataObj = (JsonObject) interpreters.get(i);
				   String InterpreterId;
				   String InterpreterDocKey;	
				   String Interpreter;
				   String InterpreterTitle;
				   String InterpreterFirstName;
				   String InterpreterLastName;
				   String AccreditationNumber;
				   String AccreditationLevel;
				   String InterpreterGender;
				   String InterpreterEmail;
				   String lspInterpreterId = Globals.validateString(dataObj.get("id"));
				   String title = Globals.validateString(dataObj.get("title"));
				   String firstName = Globals.validateString(dataObj.get("firstName"));
				   String lastName = Globals.validateString(dataObj.get("lastName"));
				   String email = null;
				   if (firstName.equals("") && lastName.equals("")) {
					   bookingController.messageCode.add(3016);
					   bookingController.isInValid = true;
				   }
				   if (dataObj.get("email") != null && !dataObj.get("email").toString().isEmpty()) {
					   if (Globals.isValidEmail((String) dataObj.get("email"))) {
						   email = Globals.validateValue("email", dataObj.get("email"));
					   } else {
						   bookingController.messageCode.add(3024);
						   bookingController.isInValid = true;
					   }		
				   }
				   String gender = "";
				   if (dataObj.get("gender") != null) {
					   gender = Globals.verifyGenderFormat((String) dataObj.get("gender").toString());
				   }
				   if (getGenderPreferencePriority() != null && getGenderPreferencePriority().equalsIgnoreCase("Must be") 
						   && !getGenderPreferenceCode().equalsIgnoreCase(gender)
						   && !getGenderPreferenceCode().equalsIgnoreCase("N")) {
						if (getGenderPreferenceCode().equalsIgnoreCase("M")) {
							   bookingController.messageCode.add(4010);
						}
						if (getGenderPreferenceCode().equalsIgnoreCase("F")) {
							   bookingController.messageCode.add(4015);					   
						}
						if (getGenderPreferenceCode().equalsIgnoreCase("U")) {
							   bookingController.messageCode.add(4020);
						}
						if (getGenderPreferenceCode().equalsIgnoreCase("I")) {
							   bookingController.messageCode.add(4029);
						}
						bookingController.isInValid = true;
				   }
				   String accreditationLevel = "";
				   String accreditationNumber = "";
				   if (dataObj.get("accreditationLevel") != null) {
					   accreditationLevel = Globals.validateString(dataObj.get("accreditationLevel"));
					   //Validate Accreditation Level against Keyword
					   boolean isValidLevel = bookingController.validateKWAccreditationLevel(accreditationLevel);
					   if (!isValidLevel) {
							bookingController.messageCode.add(3008);
							bookingController.isInValid = true;
					   }
				   }
				   if (dataObj.get("accreditationNumber") != null) {
					   accreditationNumber = Globals.validateString(dataObj.get("accreditationNumber"));
				   }
				   if (i == 0) { //Keep legacy fields as well
					   InterpreterId = formPrefix + "Agency_Interpreter_Id";
					   Interpreter = formPrefix + "Agency_Interpreter";
					   InterpreterTitle = formPrefix + "Agency_Interpreter_Title";
					   InterpreterFirstName = formPrefix + "Agency_Interpreter_FirstName";
					   InterpreterLastName = formPrefix + "Agency_Interpreter_LastName";
					   InterpreterEmail = formPrefix + "Agency_Interpreter_Email";
					   InterpreterGender = formPrefix + "Agency_Interpreter_Gender";
					   AccreditationLevel = formPrefix + "Agency_Interpreter_Level";
					   AccreditationNumber = formPrefix + "Agency_Interpreter_Number";
					   InterpreterDocKey = formPrefix + "Agency_Interpreter_DocKey";
				   } else {
					   InterpreterId = formPrefix + "Agency_Interpreter_" + i + "_Id";
					   Interpreter = formPrefix + "Agency_Interpreter_" + i;
					   InterpreterTitle = formPrefix + "Agency_Interpreter_" + i + "_Title";
					   InterpreterFirstName = formPrefix + "Agency_Interpreter_" + i + "_FirstName";
					   InterpreterLastName = formPrefix + "Agency_Interpreter_" + i + "_LastName";
					   InterpreterEmail = formPrefix + "Agency_Interpreter_" + i + "_Email";
					   InterpreterGender = formPrefix + "Agency_Interpreter_" + i + "_Gender";
					   AccreditationLevel = formPrefix + "Agency_Interpreter_" + i + "_Acc_Level";
					   AccreditationNumber = formPrefix + "Agency_Interpreter_" + i + "_Acc_Number";
					   InterpreterDocKey = formPrefix + "Agency_Interpreter_" + i + "_DocKey";
				   }
				   addFieldOnBooking(InterpreterId, lspInterpreterId); 
				   addFieldOnBooking(InterpreterTitle, title);
				   addFieldOnBooking(InterpreterFirstName, firstName);
				   addFieldOnBooking(InterpreterLastName, lastName);
				   String interpreterName = (title.equals("") ? "" : (title + " ")) + firstName + " " + lastName;
				   addFieldOnBooking(Interpreter, interpreterName); 
				   addFieldOnBooking(InterpreterGender, gender);
				   addFieldOnBooking(InterpreterEmail, email);
				   addFieldOnBooking(AccreditationLevel, accreditationLevel);
				   addFieldOnBooking(AccreditationNumber, accreditationNumber);
				   if (interpreterName != null && interpreterName != "") {
					   bookingLog.append("Interpreter Name updated to " + "\"" + interpreterName + "\". ");
				   }
				   if (dataObj.get("contactNumbers") != null) {
					   contactNumbers =  Globals.validateJsonArray(dataObj.get("contactNumbers"));
					   addFieldOnBooking(formPrefix + "Count_Interpreter_" + i + "_ContactNumbers", Integer.toString(contactNumbers.size()));
					   if (contactNumbers.size() > 0) {
						   //We can have multiple contact numbers for a Contact
						   for (int c=0;c<contactNumbers.size();c++) {
							   JsonObject contactObj = (JsonObject) contactNumbers.get(c);
							   String InterpreterContactType;
							   String type = Globals.validateString(Globals.verifyContactType((String) contactObj.get("type")));
							   String number = Globals.validateString(contactObj.get("number"));
							   if (i == 0  && type.equalsIgnoreCase("Mobile")) {
								   InterpreterContactType = formPrefix + "Agency_Interpreter_Contact"; //Keep legacy field
							   } else {
								   InterpreterContactType = formPrefix + "Agency_Interpreter_" + i + "_Contact_" + type;
							   }
							   addFieldOnBooking(InterpreterContactType , number); 
						   }
					   }
				   }
				 //Update status to Booked if agency and interpreter details added <=moved here incase error occurs in lspuser handling and leaves booking in submitted status
				   if (interpreterDocKey != null && interpreterFullName != null && interpreterType != null) {
					   status = Constants.bookingStatusBooked; //set status to booked
				   }
				   //add/update LSP user
				   if (!bookingController.isMobileApp() && !bookingController.isInValid) {
					   User user = new User(session);
					   int intLspUsers = user.isLspUser(interpreterName, accreditationNumber);
					   // If only one user profile is found - then update the user
					   if (intLspUsers==1) {
						   user.setLspId(accreditationNumber);
						   user.setLspInterpreterId(lspInterpreterId);
						   user.setTitle(title);
						   user.setFirstName(firstName);
						   user.setLastName(lastName);
						   user.setFullName(interpreterName);
						   user.setEmail(email);
						   user.setGender(gender);
						   user.setProviderDockey(interpreterDocKey);
						   user.setProviderName(interpreterFullName);
						   user.setAccreditationLevel(accreditationLevel);
						   user.setContactNumbers(contactNumbers);
						   user.setLanguage(language);
						   user.setBookingDocKey(id);
						   user.setBookingStatus(status);
						   tempDocKey = user.update();						    
						   updateFieldOnBooking(InterpreterDocKey, tempDocKey);						   
					   }
					   else if(intLspUsers==0){
						// If no user profiles found, then create a new profile					   
						   tempDocKey = user.add(lspInterpreterId, title, firstName, lastName, interpreterName, gender, email, accreditationLevel, accreditationNumber,
										   contactNumbers, interpreterDocKey, interpreterFullName, language, id); 
						   addFieldOnBooking(InterpreterDocKey, tempDocKey); 
						   }
					  else if(intLspUsers==2){
						// If multiple users found, then do nothing		
							Log.writeError("Multiple user docs found for user: " + interpreterName + " with USER_LSP_ID: " + accreditationNumber + ". Hence, user profile will not get updated. ");
					  		}   
					
				   }
				}
			}
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
			return;
		}
	}

	/**
	 * Remove interpreter fields/details
	 */
	public void removeInterpreterFields() {
		try {
			//We can have multiple interpreters
			//Get count of interpreters for this booking and remove each field
			for(int i=0;i<countInterpreters;i++) {
				String InterpreterId;
				String Interpreter;
				String InterpreterTitle;
				String InterpreterFirstName;
				String InterpreterLastName;
				String AccreditationNumber;
				String AccreditationLevel;
				String InterpreterGender;
				String InterpreterEmail;
				String AgencyInterpreterDocKey;
			   			 
			   if (i == 0) { //Remove legacy fields as well
				   InterpreterId = formPrefix + "Agency_Interpreter_Id";
				   InterpreterTitle = formPrefix + "Agency_Interpreter_Title";
				   InterpreterFirstName = formPrefix + "Agency_Interpreter_FirstName";
				   InterpreterLastName = formPrefix + "Agency_Interpreter_LastName";
				   Interpreter = formPrefix + "Agency_Interpreter";
				   InterpreterEmail = formPrefix + "Agency_Interpreter_Email";
				   InterpreterGender = formPrefix + "Agency_Interpreter_Gender";
				   AccreditationLevel = formPrefix + "Agency_Interpreter_Level";
				   AccreditationNumber = formPrefix + "Agency_Interpreter_Number";
				   AgencyInterpreterDocKey = formPrefix + "Agency_Interpreter_DocKey";
			   } else {
				   InterpreterId = formPrefix + "Agency_Interpreter_" + i + "_Id";
				   Interpreter = formPrefix + "Agency_Interpreter_" + i;
				   InterpreterTitle = formPrefix + "Agency_Interpreter_" + i + "_Title";
				   InterpreterFirstName = formPrefix + "Agency_Interpreter_" + i + "_FirstName";
				   InterpreterLastName = formPrefix + "Agency_Interpreter_" + i + "_LastName";
				   InterpreterEmail = formPrefix + "Agency_Interpreter_" + i + "_Email";
				   InterpreterGender = formPrefix + "Agency_Interpreter_" + i + "_Gender";
				   AccreditationLevel = formPrefix + "Agency_Interpreter_" + i + "_Acc_Level";
				   AccreditationNumber = formPrefix + "Agency_Interpreter_" + i + "_Acc_Number";
				   AgencyInterpreterDocKey = formPrefix + "Agency_Interpreter_" + i + "_Agency_Interpreter_DocKey";
			   }
			   previousInterpreterName = getFieldFromBooking("String", Interpreter);
			   if (previousInterpreterName != "" && previousInterpreterName != null) {
				   amendInterpreter = true;
				   bookingLog.append("Interpreter Name: \"" + previousInterpreterName + "\" is removed. ");
			   }
			   removeFieldOnBooking(InterpreterId);
			   removeFieldOnBooking(InterpreterTitle); 
			   removeFieldOnBooking(InterpreterFirstName); 
			   removeFieldOnBooking(InterpreterLastName); 
			   removeFieldOnBooking(Interpreter); 
			   removeFieldOnBooking(InterpreterGender);
			   removeFieldOnBooking(InterpreterEmail);
			   removeFieldOnBooking(AccreditationLevel);
			   removeFieldOnBooking(AccreditationNumber);
			   removeFieldOnBooking(AgencyInterpreterDocKey);
			   
			   //We can have multiple contact numbers for an interpreter
			   int countInterpretersContactNumbers = 1;
				if (bookingDoc.getItemValueString(formPrefix + "Count_Interpreter_" + i + "_ContactNumbers") != null 
						&& !bookingDoc.getItemValueString(formPrefix + "Count_Interpreter_" + i + "_ContactNumbers").equals("")) {
					countInterpretersContactNumbers = new Integer(bookingDoc.getItemValueString(formPrefix + "Count_Interpreter_" + i + "_ContactNumbers"));
				}
			   if (countInterpretersContactNumbers > 0) {
				   for (int c=0;c<countInterpretersContactNumbers;c++) {
					   String InterpreterContactType;
					   String InterpreterContactTypeWork;
					   String InterpreterContactTypeHome;
					   String InterpreterContactTypeMobile;
					   if (i == 0 && c == 0) {
						   InterpreterContactType = formPrefix + "Agency_Interpreter_Contact"; //Remove the legacy field
						   removeFieldOnBooking(InterpreterContactType); 
					   }
					   InterpreterContactTypeWork = formPrefix + "Agency_Interpreter_" + i + "_Contact_Work";
					   removeFieldOnBooking(InterpreterContactTypeWork); 
					   InterpreterContactTypeHome = formPrefix + "Agency_Interpreter_" + i + "_Contact_Home";
					   removeFieldOnBooking(InterpreterContactTypeHome); 
					   InterpreterContactTypeMobile = formPrefix + "Agency_Interpreter_" + i + "_Contact_Mobile";
					   removeFieldOnBooking(InterpreterContactTypeMobile); 
				   }
					//Remove count field as well;
					removeFieldOnBooking(formPrefix + "Count_Interpreter_" + i + "_ContactNumbers");
			   }
			}
			//Remove count field as well;
			removeFieldOnBooking(formPrefix + "Count_Interpreters");
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
			return;
		}
	}

	/**
	 * Get interpreter details for multiple interpreters
	 * Check if mandatory info exists
	 * @return
	 */
	public JsonArray getInterpreterDetails() {
		interpreters = new JsonArray();
		try {
			//We can have multiple interpreters
			//Get count of interpreters for this booking and remove each field
			for(int i=0;i<countInterpreters;i++) {
				JsonObject interpreter = new JsonObject();
				String InterpreterId;
				String Interpreter;
				String InterpreterFirstName;
				String InterpreterLastName;
				String AccreditationNumber;
				String AccreditationLevel;
				String InterpreterGender;
				String InterpreterEmail;
			   			 
			   if (i == 0) { //Get legacy fields as well
				   InterpreterId = formPrefix + "Agency_Interpreter_Id";
				   Interpreter = formPrefix + "Agency_Interpreter";
				   InterpreterFirstName = formPrefix + "Agency_Interpreter_FirstName";
				   InterpreterLastName = formPrefix + "Agency_Interpreter_LastName";
				   InterpreterEmail = formPrefix + "Agency_Interpreter_Email";
				   InterpreterGender = formPrefix + "Agency_Interpreter_Gender";
				   AccreditationLevel = formPrefix + "Agency_Interpreter_Level";
				   AccreditationNumber = formPrefix + "Agency_Interpreter_Number";
			   } else {
				   InterpreterId = formPrefix + "Agency_Interpreter_" + i + "_Id";
				   Interpreter = formPrefix + "Agency_Interpreter_" + i;
				   InterpreterFirstName = formPrefix + "Agency_Interpreter_" + i + "_FirstName";
				   InterpreterLastName = formPrefix + "Agency_Interpreter_" + i + "_LastName";
				   InterpreterEmail = formPrefix + "Agency_Interpreter_" + i + "_Email";
				   InterpreterGender = formPrefix + "Agency_Interpreter_" + i + "_Gender";
				   AccreditationLevel = formPrefix + "Agency_Interpreter_" + i + "_Acc_Level";
				   AccreditationNumber = formPrefix + "Agency_Interpreter_" + i + "_Acc_Number";
			   }
			   if (InterpreterId != null && !getFieldFromBooking("String", InterpreterId).equals("")) {
				   interpreter.put("id", getFieldFromBooking("String", InterpreterId)); 
			   }
			   if (InterpreterFirstName != null && !getFieldFromBooking("String", InterpreterFirstName).equals("")) {
				   interpreter.put("firstName", getFieldFromBooking("String", InterpreterFirstName)); 
			   }
			   if (InterpreterLastName != null && !getFieldFromBooking("String", InterpreterLastName).equals("")) {
				   interpreter.put("lastName", getFieldFromBooking("String", InterpreterLastName)); 
			   }
			   if (Interpreter != null && !getFieldFromBooking("String", Interpreter).equals("")) {
				   interpreter.put("fullName", getFieldFromBooking("String", Interpreter)); 
			   }
			   if (InterpreterGender != null && !getFieldFromBooking("String", InterpreterGender).equals("")) {
				   interpreter.put("gender", getFieldFromBooking("String", InterpreterGender)); 
			   }
			   if (InterpreterEmail != null && !getFieldFromBooking("String", InterpreterEmail).equals("")) {
				   interpreter.put("email", getFieldFromBooking("String", InterpreterEmail)); 
			   }
			   if (AccreditationLevel != null && !getFieldFromBooking("String", AccreditationLevel).equals("")) {
				   interpreter.put("accreditationLevel", getFieldFromBooking("String", AccreditationLevel)); 
			   }
			   if (AccreditationNumber != null && !getFieldFromBooking("String", AccreditationNumber).equals("")) {
				   interpreter.put("accreditationNumber", getFieldFromBooking("String", AccreditationNumber));
			   }
			   if (!Interpreter.equals("") && !AccreditationLevel.equals("") && !AccreditationNumber.equals("")) {
				   isValidInterpreterExists = true;
			   }
			   int countInterpretersContactNumbers = 1;
			   if (countInterpretersContactNumbers>0) {
				   JsonArray contactObj = new JsonArray();
				   //We can have multiple contact numbers for a Contact
				   for (int c=0;c<countInterpretersContactNumbers;c++) {
					   JsonObject contactObjMob = new JsonObject();
					   JsonObject contactObjHome = new JsonObject();
					   JsonObject contactObjWork = new JsonObject();
					   String InterpreterContactTypeWork = null;
					   String InterpreterContactTypeHome = null;
					   String InterpreterContactTypeMobile;
					   String numberMobile = "";
					   String numberHome = "";
					   String numberWork = "";
					   if (i == 0 && c == 0) {
						   InterpreterContactTypeMobile = formPrefix + "Agency_Interpreter_Contact"; //Get the legacy field
					   } else {
						   InterpreterContactTypeWork = formPrefix + "Agency_Interpreter_" + i + "_Contact_Work";
						   InterpreterContactTypeHome = formPrefix + "Agency_Interpreter_" + i + "_Contact_Home";
						   InterpreterContactTypeMobile = formPrefix + "Agency_Interpreter_" + i + "_Contact_Mobile";
					   }
					   if (InterpreterContactTypeWork != null && !getFieldFromBooking("String", InterpreterContactTypeWork).equals("")) {
						   numberWork= getFieldFromBooking("String", InterpreterContactTypeWork);
					   }
					   if (InterpreterContactTypeHome != null && !getFieldFromBooking("String", InterpreterContactTypeHome).equals("")) {
						   numberHome = getFieldFromBooking("String", InterpreterContactTypeHome);
					   }
					   if (InterpreterContactTypeMobile != null && !getFieldFromBooking("String", InterpreterContactTypeMobile).equals("")) {
						   numberMobile = getFieldFromBooking("String", InterpreterContactTypeMobile);
					   }
					   if (numberMobile != null) {
						   contactObjMob.put("type", "Mobile");
						   contactObjMob.put("number", numberMobile);
					   }
					   if (numberHome != null) {
						   contactObjHome.put("type", "Home");
						   contactObjHome.put("number", numberHome);
					   }
					   if (numberWork != null) {
						   contactObjWork.put("type", "Work");
						   contactObjWork.put("number", numberWork);
					   }
					   if (contactObjMob != null) {
						   contactObj.add(contactObjMob);
					   }
					   if (contactObjHome != null) {
						   contactObj.add(contactObjHome);
					   }
					   if (contactObjWork != null) {
						   contactObj.add(contactObjWork);
					   }
				   }
				   if (contactObj != null) {
					   interpreter.put("contactNumbers", contactObj);
				   }
			   }
			   interpreters.add(interpreter);
			}
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
		return interpreters;
	}
	
	/**
	 * Set address for patient if it is a Home Visit
	 * @param i 
	 * 
	 * @param address
	 * @throws NotesException
	 */
	public void setPatientAddress(int i) throws NotesException 
	{
		try {
			StringBuilder PatientAddress = new StringBuilder();
			String line1 = null;
			String line2 = "";
			String suburb = null;
			String stateCode = null;
			String postCode = null;
			if (address.size() > 0) {
				if (address.get("line1") != null) {
					line1 = Globals.validateString(address.get("line1"));
					PatientAddress.append(line1);
				}
				if (address.get("line2") != null) {
					line2 = Globals.validateString(address.get("line2"));
					PatientAddress.append("\n" + line2);
				}
				if (address.get("address") != null) {
					line1 = Globals.validateString(address.get("address"));
					PatientAddress.append("\n" + line1);
				}
				if (address.get("suburb") != null) {
					suburb = Globals.validateString(address.get("suburb"));
					if (i == 0) {//Keep legacy field as well
						addFieldOnBooking(formPrefix + "PatientSuburb" , suburb);
					} else {
						addFieldOnBooking(formPrefix + "Patient_" + i + "_Suburb" , suburb);
					}
				}

				if (address.get("state") != null) {
					stateCode = Globals.validateString(address.get("state"));
				}
				if (address.get("stateCode") != null) {
					stateCode = Globals.validateString(address.get("stateCode"));
				}
				if (stateCode != null && !stateCode.equals("")) {
					String[] stateList = bookingController.getKWStateList("States");
					if (stateList != null) {
						for (String code : stateList) {
							if (code.equalsIgnoreCase(stateCode)) {
								if (i == 0) {//Keep legacy field as well
									addFieldOnBooking(formPrefix + "PatientState" , code);
								} else {
									addFieldOnBooking(formPrefix + "Patient_" + i + "_State" , code);
								}
								break;
							}
						}
					}
				}
				if (address.get("postCode") != null) {
					postCode = Globals.validateString(address.get("postCode"));
					if (i == 0) {//Keep legacy field as well
						addFieldOnBooking(formPrefix + "PatientPostCode" , postCode);
					} else {
						addFieldOnBooking(formPrefix + "Patient_" + i + "_PostCode" , postCode);
					}
				}
				if (getPatientType().equalsIgnoreCase(Constants.homeVisit)) {
					if (line1 == "" && suburb == "" && stateCode == "" && postCode == "") {
					   bookingController.messageCode.add(3025);
					   bookingController.isInValid = true;
					}
				}
				if (i == 0) { //Keep legacy field as well
					addFieldOnBooking(formPrefix + "PatientAddress" , PatientAddress.toString());
				} else {
					addFieldOnBooking(formPrefix + "Patient_" + i + "_Address" , PatientAddress.toString());
				}
			}
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
	}
	
	/**
	 * Set Ethinicity preference
	 * @throws NotesException 
	 */
	public void setEthnicityPreferenceFields() throws NotesException {
		if (ethnicityPreference != null && ethnicityPreference.size() > 0) {
			if (ethnicityPreference.get("priority") != null && Globals.isValidPriority(ethnicityPreference.get("priority").toString())) {
				if (ethnicityPreference.get("code") != null) {
					String priority = Globals.validateString(ethnicityPreference.get("priority"));
					String code = Globals.validateString(ethnicityPreference.get("code"));
					String ethnicity;
					ethnicity = bookingController.getKWEthnicityFromCode(code);
					if (ethnicity != null) {
						addFieldOnBooking(formPrefix + "PatientEthnicityPreferencePriority", priority);
						addFieldOnBooking(formPrefix + "PatientEthnicityPreferenceCode", code);
					} else {
						bookingController.invalidDataString.put(3032, code);
						bookingController.messageCode.add(3032);
						bookingController.isInValid = true;
					}
				}
			} else {
				bookingController.invalidDataString.put(3030, ethnicityPreference.get("priority").toString());
				bookingController.messageCode.add(3030);
				bookingController.isInValid = true;
			}
		}
	}
	
	/**
	 * Set gender preference
	 * @throws NotesException 
	 */
	public void setGenderPreference() throws NotesException {
		if (genderPreference != null && genderPreference.size() > 0) {
			if (genderPreference.get("priority") != null && Globals.isValidPriority(genderPreference.get("priority").toString())) {
				String priority = Globals.validateString(genderPreference.get("priority"));
				String code = Globals.verifyGenderPrefFormat(genderPreference.get("gender").toString());
				setGenderPreferencePriority(priority);
				setGenderPreferenceCode(code);
				if (code != null) {
					addFieldOnBooking(formPrefix + "PatientGenderPreferencePriority", priority); //update UI in IMS - TODO in new version
					addFieldOnBooking(formPrefix + "PatientGenderPreference", code); //Need to keep the legacy field for existing bookings
				} else {
					bookingController.invalidDataString.put(3031, code);
					bookingController.messageCode.add(3031);
					bookingController.isInValid = true;
				}
			} else {
				bookingController.invalidDataString.put(3030, genderPreference.get("priority").toString());
				bookingController.messageCode.add(3030);
				bookingController.isInValid = true;
			}
		}
	}

	/**
	 * Check if there is any other booking for same patient at same time
	 * @param urNumber
	 * @param appointmentStartDateText
	 * @param appointmentStartTimeText
	 * @param Clinic
	 * @return
	 * @throws NotesException
	 */
	public boolean isDuplicateBooking(String urNumber, String appointmentStartDateText, String appointmentStartTimeText, String Clinic) throws NotesException {
		try {
			DocumentCollection dc = (DocumentCollection) dataDb.getDatabase().getAllDocuments();
			dc.FTSearch("[" + formPrefix + "PatientUR]=\"" + urNumber + "\" AND " +
					"[" + formPrefix + "ApptStartDate_Text]=\"" + appointmentStartDateText + "\" AND " +
					"[" + formPrefix + "ApptStartTime_Text]= \"" + appointmentStartTimeText + "\" AND " +
					"[" + formPrefix + "Clinic]=\"" + Clinic + "\" AND ( " +
					"[" + formPrefix + "Status]=\"draft\" OR " +
					"[" + formPrefix + "Status]=\"requested\" OR " +
					"[" + formPrefix + "Status]=\"booked\" OR " +
					"[" + formPrefix + "Status]=\"submitted_agency\" OR " +
					"[" + formPrefix + "Status]=\"updated\")", 0);
			int matches = dc.getCount();
			
			if (matches > 0) {
		    	  return true;
			}
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
		return false;
	}
	
	/**
	 * Retrieve bookings for given Agency since last modified till today
	 * @param interpreterDocKey
	 * @param modifiedSince
	 * @param todayDate
	 * @return
	 * @throws NotesException
	 */
	public JsonArray searchBookings(String interpreterDocKey, String modifiedSince,  String todayDate) throws NotesException {
		try {
			View viewAllBookingsByDate = dataDb.getLspBookingsByModifiedDateView();
			JsonArray arrStartEndPosition = new JsonArray();
			//Uses JSON modified date which gets updated only if few fields are updated
			arrStartEndPosition = Globals.View_GetEntryPositions(viewAllBookingsByDate, interpreterDocKey+"~"+modifiedSince, interpreterDocKey+"~"+todayDate);
			JsonArray objBookings = null;
			if(!arrStartEndPosition.isEmpty()){
				objBookings = dataDb.getViewColumnValueByPositions(viewAllBookingsByDate, (Integer) arrStartEndPosition.get(0), (Integer) arrStartEndPosition.get(1));
			}	
			return objBookings;
		} catch (Exception e) {
			bookingController.isInValid = true;
			bookingController.httpResponse = 503;
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);			

		}
		return null;
	}
	
	/**
	 * Search all bookings for given various scenarios of search
	 * 
	 * @param searchBy
	 * @param searchByValue
	 * @param operator
	 * @param todayDate
	 * @return
	 * @throws NotesException
	 */
	public JsonArray searchAllBookings(String searchBy, String searchByValue, String operator, String todayDate, Boolean isEntryPositionDependent, Boolean isFutureOnly) throws NotesException {
		try {
			JsonArray objBookings = new JsonArray();
			View viewBookings = dataDb.getAllBookingsByView(searchBy);	
			if (isFutureOnly) {
				viewBookings = dataDb.getFutureBookingsByView(searchBy);
			}
			if (viewBookings != null &&  viewBookings.getAllEntries().getCount() > 0) {
				if (isEntryPositionDependent) { //Get all bookings based on start and end position in view
					String startKey = null;
					String endKey = null;
					Document firstEntryDoc = viewBookings.getFirstDocument();
					Document lastEntryDoc = viewBookings.getLastDocument();
					String firstEntryDocApptStartDate = firstEntryDoc.getItemValueString("View_Appt_StartDate");
					String lastEntryDocApptStartDate = lastEntryDoc.getItemValueString("View_Appt_StartDate");
					if (searchBy != null && searchBy.equalsIgnoreCase("ModifiedDate")) {
						startKey = searchByValue;
						endKey = todayDate;
					} else if (operator.equals("<")) {
						startKey = firstEntryDocApptStartDate;
						String previousDate = Globals.getNextOrPrevDate(false, "yyyyMMdd", searchByValue);
						endKey = previousDate;					
					} else if (operator.equals(">")) {
						String nextDate = Globals.getNextOrPrevDate(true, "yyyyMMdd", searchByValue);
						startKey = nextDate;
						endKey = lastEntryDocApptStartDate;
					} else if (operator.equals("<=")) {
						startKey = firstEntryDocApptStartDate;
						endKey = searchByValue;
					} else if (operator.equals(">=")) {
						startKey = searchByValue;
						endKey = lastEntryDocApptStartDate;
					} else if (operator.equals("=")) {
						startKey = searchByValue;
						endKey = searchByValue;
					}
					JsonArray arrStartEndPosition = new JsonArray();
					arrStartEndPosition = Globals.View_GetEntryPositions(viewBookings, startKey, endKey);
					if(!arrStartEndPosition.isEmpty()) {
						objBookings = dataDb.getViewColumnValueByPositions(viewBookings, (Integer) arrStartEndPosition.get(0), (Integer) arrStartEndPosition.get(1));
					}
				} else { //get all entries based on categorized column from a view
					objBookings = Globals.getViewEntriesForCategory(viewBookings, searchByValue);
				}
				if (searchBy == null) { //return all bookings from default view
					ViewEntryCollection allEntries = viewBookings.getAllEntries();
					ViewEntry entry = allEntries.getFirstEntry();
					int pos = (entry.getColumnValues().size() - 1);
					while (entry != null) {
						objBookings.add(entry.getColumnValues().elementAt(pos).toString());
						ViewEntry tmpentry = allEntries.getNextEntry(entry);
						entry.recycle();
						entry = tmpentry;
					}
					allEntries.recycle();
				}
				viewBookings.clear();
			}
			return objBookings;
		} catch (Exception e) {
			bookingController.isInValid = true;
			bookingController.httpResponse = 503;
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);			

		}
		return null;
	}
	
	/**
	 * Get details of a booking
	 * @return - JsonObject of booking details
	 */
	public JsonObject getBookingDetails() {
		try {
			JsonObject dataJson = new JsonObject();
			if (id != null) { dataJson.put("imsId", id); }
			//Ashish - Feb 2022 - Add External booking docKey as well			
			if (pmsId != null) { dataJson.put("pmsId", pmsId); }
			if (parentId != null) { dataJson.put("parentImsId", parentId); }
			if (jsonApptStartDate != null) { dataJson.put("appointmentDate", jsonApptStartDate); }//2021-01-21
			if (jsonApptStartTime != null) { dataJson.put("appointmentTime", jsonApptStartTime); }//HH:mm
			if (duration != 0) { 
				dataJson.put("durationInMinutes", duration); 
				if (bookingController.isMobileApp()) {
					dataJson.put("duration", bookingController.minutesToHours(duration)); 
				} else {
					dataJson.put("duration", duration); 
				}
			}
			if (language != null) { dataJson.put("language", language); }
			if (languageCode != null) { dataJson.put("languageCode", languageCode); } 
			if (externalReference != null) { dataJson.put("externalReference", externalReference); } 
			if (externalLink != null) { dataJson.put("externalLink", externalLink); }
			if (deliveryMethod != null) { dataJson.put("deliveryMethod", deliveryMethod); } 
			dataJson.put("isFlexible", isFlexible);

			//Set flexible time start and end to 24 hour format to send in JSON
			if (flexibleTimeStart != null && !flexibleTimeStart.isEmpty()) {
				flexibleTimeStart = Globals.getJavaFormattedDate(flexibleTimeStart, bookingController.getGlobalTimeFormat(), Constants.timeFormat24Hour);
			}
			if (flexibleTimeEnd != null && !flexibleTimeEnd.isEmpty()) {
				flexibleTimeEnd = Globals.getJavaFormattedDate(flexibleTimeEnd, bookingController.getGlobalTimeFormat(), Constants.timeFormat24Hour);
			}
			if (flexibleTimeStart != null) { dataJson.put("flexibleTimeStart", flexibleTimeStart); } 
			if (flexibleTimeEnd != null) { dataJson.put("flexibleTimeEnd", flexibleTimeEnd); } 
			if (deliveryMethod != null) { dataJson.put("deliveryMethod", deliveryMethod); } 
			if (modality != null) { dataJson.put("modality", modality); } 
			if (videoConferencePlatform != null) { dataJson.put("videoConferencePlatform", videoConferencePlatform); } 
			if (communicationDetails != null) {
				dataJson.put("communicationDetails", communicationDetails + ((passCode!=null && !passCode.isEmpty())? " #" + passCode : ""));
			}
			if (patientType != null) { dataJson.put("patientType", patientType); }
			if (clinicCode != null) { dataJson.put("clinicCode", clinicCode); }
			if (costCentre != null) { dataJson.put("costCentre", costCentre); }
			if (site != null) { dataJson.put("facility", site); }
			if (location != null) { dataJson.put("service", location); }
			if (clinic != null) { dataJson.put("clinic", clinic); }
			if (clinicFullAddress != null) { dataJson.put("clinicAddress", clinicFullAddress); }
			if (isFlexible) {
				agencyNotes = agencyNotes + ". Flexible Time Range: " + flexibleTimeStart + "-" + flexibleTimeEnd;
			}
			if (agencyNotes != null) { dataJson.put("notesForLSP", agencyNotes); }
			if (interpreterNotes != null) { dataJson.put("notesForInterpreter", interpreterNotes); }
			if (genderPreference != null) { dataJson.put("genderPreference", genderPreference); }
			if (ethnicityPreference != null) { dataJson.put("ethnicityPreference", ethnicityPreference); }
			if (professionals != null) { dataJson.put("professionalDetails", professionals); }
			if (patients != null) { dataJson.put("patientDetails", patients); }
			if (interpreters != null) { dataJson.put("interpreterDetails", interpreters); }
			if (contacts != null) { dataJson.put("contactDetails", contacts); }
			if (cancellationTimeFormatted != null) { dataJson.put("cancellationDateTime", cancellationTimeFormatted); }
			JsonArray finalAttachments = new JsonArray();
			if (attachments != null && attachments.size()>0) { //Get name of attachment only
				for (int p=0;p<attachments.size();p++) {
					JsonObject attachment = new JsonObject();
					JsonObject attachmentobj = (JsonObject) attachments.get(p);
					attachment.put("name", attachmentobj.get("name").toString());
					finalAttachments.add(attachment);
				}
			} 
			dataJson.put("attachments", finalAttachments);
			if (clinicFullAddress != null) { 
				dataJson.put("serviceAddress", clinicFullAddress); 
			}
			if (isPatientAddressFieldsVisible() && getPatientAddressFields(0).size() > 0) {
				dataJson.put("serviceAddress", getPatientAddressFields(0));
			}
			//Post Booking details
			if (serviceDelivered.equalsIgnoreCase("Yes")) {
				completionDetails.put("serviceDelivered", true);
				String actualAppointmentStartTimeFormatted = Globals.getJavaFormattedDate(appointmentStartDateText + " " + actualAppointmentStartTimeText, bookingController.getGlobalDateFormat() + " " + bookingController.getGlobalTimeFormat(), Constants.timeFormat24Hour);
				String actualAppointmentEndTimeFormatted = Globals.getJavaFormattedDate(appointmentStartDateText + " " + actualAppointmentEndTimeText, bookingController.getGlobalDateFormat() + " " + bookingController.getGlobalTimeFormat(), Constants.timeFormat24Hour);
				completionDetails.put("actualStartTime", actualAppointmentStartTimeFormatted);
				completionDetails.put("actualEndTime", actualAppointmentEndTimeFormatted);
				completionDetails.put("actualDuration", actualDuration);
			}
			if (serviceDelivered.equalsIgnoreCase("No")) {
				completionDetails.put("serviceDelivered", false);
				completionDetails.put("reason", nonCompletionReason);
			}
			completionDetails.put("travelTime", travelTime);
			completionDetails.put("distanceTravelled", distanceTravelled);
			completionDetails.put("otherCharges", otherCharges);
			completionDetails.put("completionNotes", cancellationReason);
			completionDetails.put("waitingDuration", numCalls); //TODO - Currently app has waitingDuration field//To replace in app to numCalls when we have code
			dataJson.put("completionDetails", completionDetails);
			if (!bookingController.isLspAllowedToAccessAll && (status.equalsIgnoreCase(Constants.bookingStatusAmended) 
					|| status.toString().equalsIgnoreCase(Constants.bookingStatusCancellationRequested))) {
				displayStatus = Constants.bookingStatusRescheduling;
			}
			if (bookingController.isMobileApp()) {
				String[] fieldsToReplace = kwHandlerIMSDB.getKeywordValueArray(Constants.KW_DYNAMIC_FIELD_VALUE_REPLACE);
				//Below code is to replace one field's value with another field's value or update to - in returned JSON field. 
				if (fieldsToReplace != null && !fieldsToReplace.equals("-")) {
					for (String fields : fieldsToReplace) {
						String[] arrFields = fields.split("\\|");
						if (Globals.indexInBound(arrFields, 0) && Globals.indexInBound(arrFields, 1)) {
							String field2 = arrFields[1];
							if (field2.equals("-")) { //If field2 is -, update value to - to hide the value
								dataJson.put(arrFields[0], "-");
							} else {
								Field field = this.getClass().getDeclaredField(field2);
								if (field != null) {
									Object objectValue = field.get(this);	
									if (objectValue != null) {
										dataJson.put(arrFields[0], objectValue.toString());
									}
								}
							}
						}
					}
				}
				if (status.equalsIgnoreCase(Constants.bookingStatusReallocated)) { //return status for reallocated with specific display status using KW
					displayStatus = bookingController.getKeywordValue(Constants.KW_STATUS_REALLOCATED_INTERPRETER);
				} else if (status.equalsIgnoreCase(Constants.bookingStatusAmended)) { //return status booked for amended 
					displayStatus = Constants.bookingStatusBookedDisplay;
				}
				if (displayStatus.equalsIgnoreCase(Constants.bookingStatusPendingDisplay)) {
					accepted = false;
				} 
				dataJson.put("accepted", accepted);
				dataJson.put("allowCompletion", isCompletionAllowed());
				//Allow status override
				String overrideStatus = kwHandlerIMSDB.getKeywordValueString(Constants.KW_MOBILE_BOOKEDSTATUSOVERRIDE);
				if (displayStatus.equalsIgnoreCase(Constants.bookingStatusBooked) && overrideStatus != null && !overrideStatus.equals("-")) {
					displayStatus = overrideStatus;
				}
			}
			if (displayStatus != null) { dataJson.put("status", displayStatus); }
			//Display systemComments for PMS bookings
			if (systemComments != null && bookingController.isLspAllowedToAccessAll()) { dataJson.put("systemComments", systemComments); }
			return dataJson;
		} catch (Exception e) {
			bookingController.isInValid = true;
			bookingController.httpResponse = 503; 
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);			
		}
		return null;
	}
	
	/**
	 * Check visibility of patient's address fields
	 * @return
	 */
	public boolean isPatientAddressFieldsVisible() {
		boolean bShow = false;
		if (kwHandlerIMSDB.getKeywordValueString(Constants.KW_ALLOW_HIDE_PATIENT_ADDRESS_FIELDS).equalsIgnoreCase(Constants.NO)) {
			bShow = true;
		}
		String[] arrList = kwHandlerIMSDB.getKeywordValueArray(Constants.KW_LIST_PATIENT_ADDRESS_SHOW);
		if (arrList != null) {								
			for (String st : arrList) {
				if(st.equalsIgnoreCase(patientType)) {
					bShow = true;
					break;
				}
			}
		}
		return bShow;
	}

	/**
	 * Allow booking completion only for given status and not invoiced
	 * @return
	 */
	public boolean isCompletionAllowed() {
		boolean bAllwed = false;
		String[] arrAllowedStatusToComplete = kwHandlerIMSDB.getKeywordValueArray(Constants.KW_MOBILE_ALLOWEDSTATUSTOCOMPLETE);
		if (arrAllowedStatusToComplete != null) {								
			for (String st : arrAllowedStatusToComplete) {
				if(st.equalsIgnoreCase(displayStatus)) {
					bAllwed = true;
					break;
				}
			}
		}	
		if (isInvoiced()) {
			bAllwed = false;
		}
		return bAllwed;
	}

	/**
	 * Update partially an existing booking for actions
	 * 
	 * @param bookingAction 
	 * 
	 */
	public void partialUpdate(String action) {
		try {
			bookingLog = new StringBuilder();
			displayStatus = bookingController.getKWBookingStatus(status);
			if (displayStatus == null || displayStatus.equals("")) { //This is for Extra Reasons
				displayStatus = status;
			}
			if (completed != null && completed.equalsIgnoreCase(Constants.NO) && action.equalsIgnoreCase(Constants.ACTION_COMPLETED)) { //Update to Service Not Delivered Display status only.
				displayStatus = bookingController.getKWBookingStatus(Constants.bookingStatusNotDelivered);
			}
			if (interpreterType.equalsIgnoreCase(Constants.InterpreterTypeAgency)) {
				updateFieldOnBooking(formPrefix + "Agency_Last_Updated", nowDateTimeFormatted);
				updateFieldOnBooking(formPrefix + "Agency_ID", externalReference);
				updateFieldOnBooking(formPrefix + "Agency_URL", externalLink);
			}
			updateFieldOnBooking(formPrefix + "Status", status);
			updateFieldOnBooking(formPrefix + "Status_Display", displayStatus);
			
			//No action required for accepted yet except adding a booking log entry
			if (action.equalsIgnoreCase(Constants.ACTION_ACCEPTED)) {
				updateFieldOnBooking(formPrefix + "accepted", "Yes");
			}
			if (action.equalsIgnoreCase(Constants.ACTION_RECEIVED)) {
				if (action.equalsIgnoreCase(Constants.ACTION_RECEIVED) && (status.equalsIgnoreCase(Constants.bookingStatusCancelled) || (completed != null && completed.equalsIgnoreCase(Constants.NO)))) {
					bookingLog.append("Booking cancellation received - by ");
				}
				//Update KPI fields
				updateFieldOnBooking("KPI_submitted_agency_By", kpiSubmittedAgencyBy);
				updateFieldOnBooking("KPI_submitted_agency_Date", kpiSubmittedAgencyDate);
				updateFieldOnBooking("KPI_submitted_agency_DocKey", kpiSubmittedAgencyDocKey);
			}
			if (action.equalsIgnoreCase(Constants.ACTION_ALLOCATED)) {
				removeInterpreterFields(); //Clear previous interpreter fields
				addInterpreterFields(interpreters);	
				//Update KPI fields
				updateFieldOnBooking("KPI_AllocatedFirst_By", kpiAllocatedFirstBy);
				updateFieldOnBooking("KPI_AllocatedFirst_Date", kpiAllocatedFirstDate);
				updateFieldOnBooking("KPI_AllocatedFirst_DocKey", kpiAllocatedFirstDocKey);
				updateFieldOnBooking("KPI_AllocatedLast_By", kpiAllocatedLastBy);
				updateFieldOnBooking("KPI_AllocatedLast_Date", kpiAllocatedLastDate);
				updateFieldOnBooking("KPI_AllocatedLast_DocKey", kpiAllocatedLastDocKey);
			}
			if (action.equalsIgnoreCase(Constants.ACTION_UNALLOCATED)) {
				removeInterpreterFields();
			}
			if (action.equalsIgnoreCase(Constants.ACTION_REJECTED)) {
				removeInterpreterFields(); //Clear previous interpreter fields
				updateRejectionDetails();
				if (rejectionReason != null) {
					bookingLog.append("Rejection reason: " + rejectionReason  +". ");
				}
			}
			if (action.equalsIgnoreCase(Constants.ACTION_CANCELLED)) {
				removeInterpreterFields(); //Clear previous interpreter fields
				updateCancellationDetails();
				if (pendingSendWebhook.equalsIgnoreCase(Constants.YES)){
					updateFieldOnBooking(formPrefix+"PendingSendWebhook", pendingSendWebhook);
				}
			}
			if (action.equalsIgnoreCase(Constants.ACTION_COMPLETED)) {
				updateCompletionDetails();
			}
			if (!bookingController.isInValid) {
				//Update last modified if booking is actioned by PMS
				if (bookingController.isLspAllowedToAccessAll()) {
					updateFieldOnBooking(formPrefix+"LastModified_DateTime", nowDateTime);
					updateFieldOnBooking("JSON_LastModified_Date_Text", Globals.formatTime (nowDateTime, "yyyy-MM-dd"));
					updateFieldOnBooking("JSON_LastModified_Time_Text", Globals.formatTime (nowDateTime, "HH:mm"));
				}
				if (oldDisplayStatus != null && !oldDisplayStatus.equalsIgnoreCase(displayStatus)) {
					bookingLog.append("Status changed to '" + displayStatus + "'. ");
				}
				if(bookingController.isLspAllowedToAccessAll()) {
					bookingLog.append("Booking '" + action.toUpperCase() + "' - by " + agencyLabel + ": " + bookingController.getConsumerName());
				} else if (interpreterType.equalsIgnoreCase(Constants.InterpreterTypeInHouse)) {
					bookingLog.append("Booking '" + action.toUpperCase() + "' - by Interpreter: " + interpreterFullName);
				} else {
					if (action.equalsIgnoreCase(Constants.ACTION_RECEIVED) && (status.equalsIgnoreCase(Constants.bookingStatusCancelled) || completed != null && completed.equalsIgnoreCase(Constants.NO))) {
						bookingLog.append(agencyLabel + ": " + bookingController.getClientName());
					} else {
						bookingLog.append("Booking '" + action.toUpperCase() + "' - by " + agencyLabel + ": " + bookingController.getClientName());
					}
				}
				Globals.LogActionToDocument(nowDateTime, bookingDoc, bookingLog.toString(), false, session);
				bookingDoc.computeWithForm(true, false);
				bookingDoc.save();
				id = bookingDoc.getItemValueString("DocKey");
				//If a booking status is changed, trigger email document creation
				if (action.equalsIgnoreCase(Constants.ACTION_ACCEPTED)) {
					notifyByEmail(Globals.toCamelCase(action));
				} else if (oldDisplayStatus != null && !oldDisplayStatus.equalsIgnoreCase(displayStatus)) {
					notifyByEmail(action);
				}
			}

		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
	}
	
	private void updateAgencyCompletion() {
		//CBN also has prefix BOOK
		updateFieldOnBooking("BOOK_Agency_FlowComplete", agencyFlowComplete ); 
		updateFieldOnBooking("BOOK_Agency_FlowComplete_Status", displayStatus); 
		updateFieldOnBooking("BOOK_Agency_FlowComplete_Time", agencyFlowCompleteTime);
	}
	
	public void updateCancellationDetails() {
		try {
			updateFieldOnBooking(formPrefix + "Completed", completed);
			updateFieldOnBooking(formPrefix + "NC_Reason", Constants.bookingStatusCancelled);
			cancellationNotice = calculateCancellationNotice();
			if (cancellationNotice != null) {
				calculateCancellationCharges();
				updateFieldOnBooking(formPrefix + "CancellationNotice", cancellationNotice);
			}
			updateFieldOnBooking(formPrefix + "CancellationReason", cancellationReason);
			updateFieldOnBooking(formPrefix + "Cancellation_Time", cancellationTime);
			updateFieldOnBooking(formPrefix + "CancelDate_Text", cancellationDateText);
			updateFieldOnBooking(formPrefix + "CancelTime_Text", cancellationTimeText);
			updateFieldOnBooking(formPrefix + "CancelledBy", cancelledBy);
			updateFieldOnBooking(formPrefix + "CancellationRequestedBy", cancellationRequestedBy);
			updateAgencyCompletion();
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
	}
	
	
	
	/**
	 * Remove the Book_Completed and Cancellation details from the booking
	 * Required when a booking is set to New or Submitted Status
	 */
	public void removeCancellationDetails(){		
		try{
			
			updateFieldOnBooking(formPrefix + "Completed", "");
			updateFieldOnBooking(formPrefix + "NC_Reason", "");			
			updateFieldOnBooking(formPrefix + "CancellationNotice", "");
			updateFieldOnBooking(formPrefix + "CancellationReason", "");
			updateFieldOnBooking(formPrefix + "Cancellation_Time", "");
			updateFieldOnBooking(formPrefix + "CancelDate_Text", "");
			updateFieldOnBooking(formPrefix + "CancelTime_Text", "");
			updateFieldOnBooking(formPrefix + "CancelledBy", "");
			updateFieldOnBooking(formPrefix + "CancellationRequestedBy", "");

			updateFieldOnBooking("BOOK_Agency_FlowComplete", agencyFlowComplete ); 
			updateFieldOnBooking("BOOK_Agency_FlowComplete_Status", displayStatus); 
			updateFieldOnBooking("BOOK_Agency_FlowComplete_Time", agencyFlowCompleteTime);			
			
		}catch(Exception e){			
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
			
		}		
	}
	
	/**
	 * Update completion fields
	 * Here we won't update field completed to Yes or No as coordinator need to confirm booking before sending booking to full completion
	 */
	public void updateCompletionDetails() {
		try {
			StringBuilder strLog = new StringBuilder();
			if (completed != null) {
				if (strLog.length()>0) {
					strLog.append(", ");
				}
				updateFieldOnBooking(formPrefix + "Service_Delivered", completed);
				strLog.append("Service delivered: '" + completed + "'");
			}
			if (completed != null && completed.equalsIgnoreCase(Constants.YES)) {
				if (completeBooking) { // Yes based on incoming serviceDelivered if allowed to save
					updateFieldOnBooking(formPrefix + "Completed", Constants.YES);
				}
				if (actualAppointmentStartTimeText != null) {
					if (strLog.length()>0) {
						strLog.append(", ");
					}
					strLog.append("Actual Start Time: '" + actualAppointmentStartTimeText + "'");
				} else {
					actualAppointmentStartTimeText = appointmentStartTimeText;
					actualAppointmentEndTimeText = appointmentEndTimeText;
				}
				if (actualDuration != 0 ) {
					if (strLog.length()>0) {
						strLog.append(", ");
					}
					strLog.append("Actual Duration (in minutes): '" + actualDuration + "'");
				} else {
					actualDuration = duration;
				}
				updateFieldOnBooking(formPrefix + "ApptStartTime_Actual", actualAppointmentStartTimeText);
				updateFieldOnBooking(formPrefix + "ApptFinishTime_Actual", actualAppointmentEndTimeText);
				updateFieldOnBooking(formPrefix + "ApptDuration_Actual", actualDuration);
				updateFieldOnBooking("Service_Completed", Constants.YES);
			} else {
				updateFieldOnBooking("Service_Completed", Constants.NO);
				if (completeBooking) { // Yes/No based on incoming serviceDelivered
					updateFieldOnBooking(formPrefix + "Completed", Constants.NO);
				}
				if (nonCompletionReason != null) {
					if (strLog.length()>0) {
						strLog.append(", ");
					}
					strLog.append("Reason: '" + nonCompletionReason + "'");
					updateFieldOnBooking(formPrefix + "NC_Reason", nonCompletionReason);
				}
			}
			if (cancellationReason != null) {
				updateFieldOnBooking(formPrefix + "CancellationReason", cancellationReason);
				if (strLog.length()>0) {
					strLog.append(", ");
				}
				strLog.append("Completion Notes: '" + cancellationReason + "'");
			}
			if (distanceTravelled != 0) {
				updateFieldOnBooking(formPrefix + "Dist_Travelled", distanceTravelled);
				if (strLog.length()>0) {
					strLog.append(", ");
				}
				strLog.append("Distance Travelled: '" + distanceTravelled + "'");
			}
			if (travelTime != 0) {
				updateFieldOnBooking(formPrefix + "TravelTime", travelTime);
				if (strLog.length()>0) {
					strLog.append(", ");
				}
				strLog.append("Travel Time: '" + travelTime + "'");
			}
			if (otherCharges != 0) {
				updateFieldOnBooking(formPrefix + "Other", otherCharges);
				if (strLog.length()>0) {
					strLog.append(", ");
				}
				strLog.append("Other Charges: '" + otherCharges + "'");
			}
			if (numCalls != 0 ) {
				updateFieldOnBooking(formPrefix + "NumCalls", numCalls);
				if (strLog.length()>0) {
					strLog.append(", ");
				}
				strLog.append("Preparation Time/Wait Time (in minutes): '" + numCalls + "'");
			}
			if (strLog != null) {
				bookingLog.append("Booking updated with these details: " + strLog.toString() + ". ");
			}
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
	}
	
	/**
	 * TODO in future once invoice recon is completed, can reuse code or run existing agent to find the charges
	 */
	private String calculateCancellationCharges() {
		return null;
	}

	
	/**
	 * Update rejection fields
	 * This is NOT a booking's end of flow
	 */
	public void updateRejectionDetails() throws NotesException {
		if (interpreterType.equalsIgnoreCase(Constants.InterpreterTypeAgency)) {
			//updateFieldOnBooking(formPrefix + "Completed", "No");
			//updateFieldOnBooking(formPrefix + "NC_Reason", Constants.bookingStatusRejected);
			updateFieldOnBooking(formPrefix + "RejectedBy", rejectedBy);
			//updateFieldOnBooking(formPrefix + "CancellationReason", rejectionReason);
			updateFieldOnBooking(formPrefix + "Agency_FlowComplete", "Yes");
			updateFieldOnBooking(formPrefix + "Agency_FlowComplete_Status", displayStatus);
			updateFieldOnBooking(formPrefix + "Agency_FlowComplete_Time", nowDateTime.getDateOnly() + " " + Globals.formatTime (nowDateTime, "hh:mm:ss"));
		} else {
			removeFieldOnBooking(formPrefix + "Interpreter_Type");
			removeFieldOnBooking(formPrefix + "Interpreter_DocKey");
			removeFieldOnBooking(formPrefix + "Interpreter_FullName");
		}
		updateFieldOnBooking(formPrefix + "accepted", "No");
		//Update legacy fields for CBN
		if (isCombinedBooking()) {
			if (interpreterType.equalsIgnoreCase(Constants.InterpreterTypeAgency)) {
				updateFieldOnBooking("BOOK_Agency_FlowComplete", "Yes"); 
				updateFieldOnBooking("BOOK_Agency_FlowComplete_Status", displayStatus); 
				updateFieldOnBooking("BOOK_Agency_FlowComplete_Time", nowDateTime.getDateOnly());
			}
		}
	}
	
	/**
	 * Calculate cancellation notice period from a Lotus Script agent
	 * 
	 * @return
	 * @throws NotesException 
	 */
	public String calculateCancellationNotice() throws NotesException {
		String cancellationNotice = null;
		try {
			int hours = 0;
			Document tempDoc = imsHomeDb.getDatabase().getProfileDocument("TimeDifference", universalId);
			if(appointmentStartDateTime != null) {
				bookingDoc.getFirstItem(formPrefix + "Appt_Start").copyItemToDocument(tempDoc);
			}
			tempDoc.save(true, false);
			String strID = tempDoc.getNoteID();
			tempDoc.recycle();
			imsHomeDb.getDatabase().getAgent("agtTimeDiff").run(strID);
			tempDoc = imsHomeDb.getDatabase().getDocumentByID(strID);
			if(tempDoc == null) {
				Log.writeError ("ERROR: Cannot Locate Time Difference document");		
			}
			hours = (int) tempDoc.getItemValueDouble("Hours");
			if (hours > 24){
				cancellationNotice = "24 hrs prior";
			} else {
				cancellationNotice = "Same day";
			}
		} catch(NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
		return cancellationNotice;
	}

	/**
	 * Checks if the field value is different and replaces it if it is
	 * 	returns true if we replaced it, false otherwise.
	 * @param fieldName - Field name to be updated
	 * @param newVal - New string value of the field to set
	 * @return - Boolean - True if field is updated 
	 */
	private boolean updateFieldOnBooking (String fieldName, String newVal)
	{
		try {
			String cVal = bookingDoc.getItemValueString (fieldName);
			if (newVal == null) {newVal = "";}
			if (!newVal.equalsIgnoreCase(cVal)){
				bookingDoc.replaceItemValue(fieldName, newVal);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
			return false;
		}
	}
	
	/**
	 * Checks if the field value is different and replaces it if it is
	 * 	returns true if we replaced it, false otherwise.
	 * @param fieldName - Field name to be updated
	 * @param newVal - New int value of the field to set
	 * @return - Boolean - True if field is updated 
	 */
	private boolean updateFieldOnBooking (String fieldName, int newVal)
	{
		int cVal = 0;
		try {
			if (!bookingDoc.getItemValueString(fieldName).equals("")) {
				cVal = new Integer(bookingDoc.getItemValueString(fieldName));
			}
			if (newVal == 0) {
				bookingDoc.removeItem(fieldName);
				return true;
			} else if (newVal != 0 && newVal != cVal) {
				bookingDoc.replaceItemValue(fieldName, newVal);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
			return false;
		}
	}
	
	/**
	 * 
	 * @param fieldName
	 * @param newVal
	 * @return
	 */
	private boolean updateFieldOnBooking (String fieldName, double newVal)
	{
		int cVal = 0;
		try {
			if (!bookingDoc.getItemValueString(fieldName).equals("")) {
				cVal = new Integer(bookingDoc.getItemValueString(fieldName));
			}
			if (newVal == 0) {
				bookingDoc.removeItem(fieldName);
				return true;
			} else if (newVal != 0 && newVal != cVal) {
				bookingDoc.replaceItemValue(fieldName, newVal);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
			return false;
		}
	}
	
	/**
	 * 
	 * @param fieldName
	 * @param newVal
	 * @return
	 */
	private boolean updateFieldOnBooking (String fieldName, DateTime newVal)
	{
		try {
			String cVal = bookingDoc.getItemValueString (fieldName);
			if (newVal != null){
				bookingDoc.replaceItemValue(fieldName, newVal);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
			return false;
		}
	}
	
	/**
	 * Updates an existing booking
	 * @throws SecurityException 
	 * @throws NoSuchFieldException 
	 * 
	 */
	public void update(JsonObject objValues, boolean bUpdateStatus) throws NotesException, NoSuchFieldException, SecurityException {
		try {			
			bookingDoc.replaceItemValue(formPrefix + "Appt_Start", appointmentStartDateTime);
			bookingDoc.replaceItemValue(formPrefix + "ApptStartDate_Text", appointmentStartDateText);
			bookingDoc.replaceItemValue(formPrefix + "ApptStartTime_Text", appointmentStartTimeText);
			bookingDoc.replaceItemValue(formPrefix + "ApptDuration", duration);
			bookingDoc.replaceItemValue(formPrefix + "Agency_ID", externalReference);
			bookingDoc.replaceItemValue(formPrefix + "Agency_URL", externalLink);
			bookingDoc.replaceItemValue(formPrefix + "BookingType", deliveryMethod);
			bookingDoc.replaceItemValue(formPrefix + "CommunicationDetails", communicationDetails);
			bookingDoc.replaceItemValue(formPrefix + "VideoConferencePlatform", videoConferencePlatform);
			if (bookingController.isLspAllowedToAccessAll) {
				bookingDoc.replaceItemValue(formPrefix + "PatientType", patientType);
			}
			
			//WH Change - 15/12/2022 - Update Clinic Details in UpdateBooking API call
			if(clinicCode != null){
				bookingDoc.replaceItemValue(formPrefix + "CLN_Code", clinicCode);
				bookingDoc.replaceItemValue(formPrefix + "Clinic", clinic);
				bookingDoc.replaceItemValue(formPrefix + "Clinic_Address", clinicAddress);
				bookingDoc.replaceItemValue(formPrefix + "Clinic_Suburb", clinicSuburb);
				bookingDoc.replaceItemValue(formPrefix + "Clinic_PostCode", clinicPostCode);
				bookingDoc.replaceItemValue(formPrefix + "Clinic_State", clinicState);			
				bookingDoc.replaceItemValue(formPrefix + "Site", site);
				bookingDoc.replaceItemValue(formPrefix + "SiteName", siteName);
				bookingDoc.replaceItemValue(formPrefix + "Location", location);
			}
			
			bookingDoc.replaceItemValue(formPrefix + "CostCentre", costCentre);
			bookingDoc.replaceItemValue(formPrefix + "Comments", agencyNotes);			
			bookingDoc.replaceItemValue(formPrefix + "Language", language);
			bookingDoc.replaceItemValue(formPrefix + "Language_Code", languageCode);
			bookingDoc.replaceItemValue(formPrefix + "SystemComments", systemComments);
			
			if (address != null) {
				   if (Globals.isValidJsonObject(address)) {
						setAddress(Globals.validateJsonObject(address));
						setPatientAddress(0);
				   } else {
					   bookingController.isInValid = true;
					   bookingController.messageCode.add(4005);		
				   }
		   }
		   if (bookingController.isLspAllowedToAccessAll && patientType != null && patientType.equalsIgnoreCase(Constants.homeVisit) && address.size() == 0) {
			   bookingController.messageCode.add(3026);
			   bookingController.isInValid = true;
		   }
			setGenderPreference();
			if (!bookingController.isInValid) {
				if (objValues.size() > 0) {
					Iterator<String> itr = objValues.keySet().iterator();
					while (itr.hasNext()) {
						String fieldName = itr.next();
						if (fieldName != null) {
							JsonObject values = (JsonObject) objValues.get(fieldName);
							if (values != null) {
								String oldValue = null;
								String newValue = null;
								if (values.get("Old") != null) {
									oldValue = values.get("Old").toString();
								}
								if (values.get("New") != null) {
									newValue = values.get("New").toString();
								}
								if (oldValue != null && newValue  != null && !oldValue.equalsIgnoreCase(newValue)) {
									String changed = "";
									if (oldValue.equals("")) {
										changed = " updated to \"";
									} else {
										changed = " changed from \"" + oldValue + "\" to \"";
									}
									if(bookingController.isLspAllowedToAccessAll()){
										Globals.LogActionToDocument(nowDateTime, bookingDoc, fieldName + changed + newValue.toString() + "\" by " + agencyLabel + ": " + bookingController.getConsumerName(), false, session);
									}else{
										Globals.LogActionToDocument(nowDateTime, bookingDoc, fieldName + changed + newValue.toString() + "\" by " + agencyLabel + ": " + bookingController.getClientName(), false, session);
									}
									bookingDoc.save();
									//If a booking status is changed, trigger email document creation
									if (oldDisplayStatus != null && !oldDisplayStatus.equalsIgnoreCase(displayStatus)) {
										notifyByEmail(displayStatus);
									}
								}
							}
						}
					}
				}
				
				//WH - in update booking call, if the status is updated then update in the booking doc
				if(bUpdateStatus){
					bookingDoc.replaceItemValue(formPrefix + "Status", status);
					displayStatus = bookingController.getKWBookingStatus(status); 
					if (displayStatus == null || displayStatus.equals("")) { //This is for Extra Reasons
						displayStatus = status;
					}
					bookingDoc.replaceItemValue(formPrefix + "Status_Display", displayStatus);
				}
				
				if (pendingSendWebhook.equalsIgnoreCase(Constants.YES)){
					updateFieldOnBooking(formPrefix+"PendingSendWebhook", pendingSendWebhook);
				}
				bookingDoc.computeWithForm(true, false);
				//Update last modified if booking gets updated by PMS
				if (bookingController.isLspAllowedToAccessAll()) {
					updateFieldOnBooking(formPrefix+"LastModified_DateTime", nowDateTime);
					updateFieldOnBooking("JSON_LastModified_Date_Text", Globals.formatTime (nowDateTime, "yyyy-MM-dd"));
					updateFieldOnBooking("JSON_LastModified_Time_Text", Globals.formatTime (nowDateTime, "HH:mm"));
				}
				bookingDoc.save();
				id = bookingDoc.getItemValueString("DocKey");
				//If a booking status is changed, trigger email document creation
				if (oldDisplayStatus != null && !oldDisplayStatus.equalsIgnoreCase(displayStatus)) {
					notifyByEmail(displayStatus);
				}
				
				//dataDb.getDatabase().updateFTIndex(true);
			}

		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
			Globals.printException(e);
		}
	}
	
	/**
	 * Check if booking is cancelled via finding booking completion
	 * If status is cancelled
	 * @return
	 */
	public boolean isBookingCancelled() {
		if (completed != null && completed.equalsIgnoreCase(Constants.NO) && completed.equalsIgnoreCase(Constants.bookingStatusCancelled)) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if a booking is a child booking of the combined booking
	 * @return - Boolean - True if it is a child booking
	 */
	public boolean isChildBooking() {
		if (!parentId.equals("")) {
			return true;
		}
		return false;
	}
	/**
	 * Checks if a booking is a combined booking
	 * @return - Boolean - True if it is a combined booking
	 */
	public boolean isCombinedBooking() {
		if (form.equalsIgnoreCase(Constants.formCBN)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Get child bookings if booking is parent
	 * @return - DocumentCollection of all child bookings
	 */
	public DocumentCollection getChildBookings() {
		DocumentCollection bookings;
		try {
			View allCombinedBookings = dataDb.getAllCombinedBookingsView();
			bookings = (DocumentCollection) allCombinedBookings.getAllDocumentsByKey(id);
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
			Globals.printException(e);
			return null;
		}
		return bookings;
	}
	
	public JsonArray getPatientFields() {
		JsonArray patientsData = new JsonArray();
		try {
			if (countPatients > 0) {
				//We can have multiple patients, get all
				for(int i=0;i<countPatients;i++) {
					JsonObject dataObj = new JsonObject();
					String PatientUR;
					String PatientTitle;
					String PatientFirstName;
					String PatientLastName;
					String PatientDOB;
				   	String PatientEmail;
				   	String PatientStartTime;
				   	String PatientEndTime;
				   	String PatientGender;
				   
				   	if (i == 0) { //Get data from legacy fields if available
				   		PatientUR = formPrefix + "PatientUR";
				   		PatientTitle = formPrefix + "PatientTitle";
				   		PatientFirstName = formPrefix + "PatientFirstName";
				   		PatientLastName = formPrefix + "PatientLastName";
				   		PatientDOB = formPrefix + "Patient_DOB_Text";
				   		PatientGender = formPrefix + "PatientGender";
				   		PatientStartTime = formPrefix + "PatientStartTime";
				   		PatientEndTime = formPrefix + "PatientEndTime";
				   		PatientEmail = formPrefix + "PatientEmail";
				   	} else {
				   		PatientUR = formPrefix + "Patient_" + i + "_UR";
				   		PatientTitle = formPrefix + "Patient_" + i + "_Title";
				   		PatientFirstName = formPrefix + "Patient_" + i + "_FirstName";
				   		PatientLastName = formPrefix + "Patient_" + i + "_LastName";
				   		PatientDOB = formPrefix + "Patient_" + i + "_DOB_Text";
				   		PatientGender = formPrefix + "Patient_" + i + "_Gender";
				   		PatientStartTime = formPrefix + "Patient_" + i + "_StartTime";
				   		PatientEndTime = formPrefix + "Patient_" + i + "_EndTime";
				   		PatientEmail = formPrefix + "Patient_" + i + "_Email";	
				   	}
	
				   	String urNumber = getFieldFromBooking("String", PatientUR);
				   	String title = getFieldFromBooking("String", PatientTitle); 
				   	String firstName = getFieldFromBooking("String", PatientFirstName);
				   	String lastName = getFieldFromBooking("String", PatientLastName); 
				   	String dob = getFieldFromBooking("String", PatientDOB);
				   	String gender = getFieldFromBooking("String", PatientGender);
				   	String startTime = getFieldFromBooking("String", PatientStartTime);
				   	String endTime = getFieldFromBooking("String", PatientEndTime);
				   	String email = getFieldFromBooking("String", PatientEmail);
				   	int countPatientsContactNumbers = 1;
				   	if (startTime == null || startTime.equals("")) {
				   		startTime = appointmentStartTimeText;
				   	}
				   	if (endTime == null || endTime.equals("")) {
				   		endTime = appointmentEndTimeText;
				   	}
				   	if (!getFieldFromBooking("String", formPrefix + "Count_Patient_" + i + "_ContactNumbers").equals("")) {
				   		countPatientsContactNumbers = new Integer(getFieldFromBooking("String", formPrefix + "Count_Patient_" + i + "_ContactNumbers"));
				   	}
				   	if (urNumber != null) {
				   		dataObj.put("urNumber", urNumber);
				   	}
					if (title != null) {
				   		dataObj.put("title", title);
				   	}
					if (firstName != null) {
				   		dataObj.put("firstName", firstName);
				   	}
					if (lastName != null) {
				   		dataObj.put("lastName", lastName);
				   	}
					//Set patientName as a single field currently
					setPatientName(firstName + " " + lastName);
					if (dob != null && !dob.isEmpty()) {
						dob = Globals.getJavaFormattedDate(dob, kwHandlerIMSDB.getGlobalDateNotesFromFormat(), "yyyy-MM-dd"); //return date in format yyyy-MM-dd
				   		dataObj.put("dob", dob);
				   	}
					if (gender != null) {
				   		dataObj.put("gender", gender);
				   	}
					if (startTime != null) {
				   		dataObj.put("startTime", startTime);
				   	}
					if (endTime != null) {
				   		dataObj.put("endTime", endTime);
				   	}
					if (email != null) {
				   		dataObj.put("email", email);
				   	}
				   
				   	if (countPatientsContactNumbers > 0) {
				   		JsonArray contactObj = new JsonArray();
				   		//We can have multiple contact numbers for a patient
				   		for (int c=0;c<countPatientsContactNumbers;c++) {
				   			JsonObject contactObjMob = new JsonObject();
				   			JsonObject contactObjHome = new JsonObject();
				   			JsonObject contactObjWork = new JsonObject();
			   				String PatientContactTypeMobile = null;
			   				String PatientContactTypeHome = null;
			   				String PatientContactTypeWork = null;
			   				String numberMobile = "";
			   				String numberHome = "";
			   				String numberWork = "";
				   			if (i == 0) { //Get legacy fields
				   				String PatientContactTypeLegacyMobileCode = formPrefix + "Patient_MCode"; //BOOK_Patient_MCode
				   				PatientContactTypeMobile = formPrefix + "Patient_Mobile"; //BOOK_Patient_Mobile
				   				PatientContactTypeWork = formPrefix + "Patient_Work"; //BOOK_Patient_Work
				   				PatientContactTypeHome = formPrefix + "PatientPhone"; //BOOK_PatientPhone - legacy
					   			String PatientContactTypeLegacyMobileCodeN = getFieldFromBooking("String", PatientContactTypeLegacyMobileCode);
					   			if (!getFieldFromBooking("String", PatientContactTypeMobile).equals("")){
					   				numberMobile = PatientContactTypeLegacyMobileCodeN + getFieldFromBooking("String", PatientContactTypeMobile);
					   			}
				   			} else {
					   			PatientContactTypeHome = formPrefix + "Patient_" + i + "_Contact_Home"; //BOOK_Patient_1_ContactType_0_Home					   			
					   			PatientContactTypeMobile = formPrefix + "Patient_" + i + "_Contact_Mobile"; //BOOK_Patient_1_ContactType_0_Mobile
					   			PatientContactTypeWork = formPrefix + "Patient_" + i + "_Contact_Work"; //BOOK_Patient_1_ContactType_0_Work
					   			numberMobile = getFieldFromBooking("String", PatientContactTypeMobile);
				   		
				   			}
				   			numberHome = getFieldFromBooking("String", PatientContactTypeHome);
				   			numberWork = getFieldFromBooking("String", PatientContactTypeWork);

				   			if (numberMobile != null) {
				   				contactObjMob.put("type", "Mobile");
				   				contactObjMob.put("number", numberMobile);
				   			}
				   			if (numberHome != null) {
				   				contactObjHome.put("type", "Home");
				   				contactObjHome.put("number", numberHome);
				   			}
				   			if (numberWork != null) {
				   				contactObjWork.put("type", "Work");
				   				contactObjWork.put("number", numberWork);
				   			}
				   			if (contactObjMob != null) {
				   				contactObj.add(contactObjMob);
				   			}
				   			if (contactObjHome != null) {
				   				contactObj.add(contactObjHome);
				   			}
				   			if (contactObjWork != null) {
				   				contactObj.add(contactObjWork);
				   			}
				   		}
			   			if (contactObj != null) {
			   				dataObj.put("contactNumbers", contactObj);
			   			}
				   	}
				   	if (getPatientAddressFields(i).size()>0) {
				   		dataObj.put("address", getPatientAddressFields(i));
				   	}
				   	if (dataObj.size()>0) {
				   		patientsData.add(dataObj);
				   	}
				}
			} 
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
		return patientsData;
	}
		
	/**
	 * Get Patient's address fields
	 * 
	 * @param i - Patient count for number of patient
	 * @return jsonAddress - JSON Object for address
	 * @throws NotesException
	 */
	public JsonObject getPatientAddressFields(int i) throws NotesException {
		JsonObject jsonAddress = new JsonObject();
		try {
			String address = "";
			String suburb = "";
			String state = "";
			String postCode = "";
			if (i == 0) { //Get legacy fields for first contact
				address = getFieldFromBooking("String", formPrefix + "PatientAddress");
				suburb = getFieldFromBooking("String", formPrefix + "PatientSuburb");
				state = getFieldFromBooking("String", formPrefix + "PatientState");
				postCode = getFieldFromBooking("String", formPrefix + "PatientPostCode");
			} else {
				address = getFieldFromBooking("String", formPrefix + "Patient_" + i + "_Address");
				suburb = getFieldFromBooking("String", formPrefix + "Patient_" + i + "_Suburb");
				state = getFieldFromBooking("String", formPrefix + "Patient_" + i + "_State");
				postCode = getFieldFromBooking("String", formPrefix + "Patient_" + i + "_PostCode");
			}
			if (address != null && !address.isEmpty()) {
				jsonAddress.put("address", address);
			}
			if (suburb != null && !suburb.isEmpty()) {
				jsonAddress.put("suburb", suburb);
			}
			if (state != null && !state.isEmpty()) {
				jsonAddress.put("state", state);
			}
			if (postCode != null && !postCode.isEmpty()) {
				jsonAddress.put("postCode", postCode);
			}
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
		return jsonAddress;
	}
	
	public JsonArray getContactFields() {
		JsonArray contactsData = new JsonArray();
		JsonObject globalContactDetails = bookingController.getGlobalContactDetails();
		try {
			if (countContacts > 0) {
				//We can have multiple patients, get all
				for(int i=0;i<countContacts;i++) {
					JsonObject dataObj = new JsonObject();
					String RequestedByTitle = null;
					String RequestedByFullName = null;
					String RequestedByFirstName = null;
					String RequestedByLastName = null;
				   	String RequestedByEmail = null;
				   	String title = null;
				   	String fullName = null;
				   	String firstName = null;
				   	String lastName = null;
				   	String email = null;
				   	if (i == 0) { //Get data from legacy fields if available
				   		RequestedByFullName = formPrefix + "RequestedBy";
				   		RequestedByEmail = formPrefix + "RequestedByEmail";
				   	} else {
				   		RequestedByTitle = formPrefix + "RequestedBy_" + i + "_Title";
				   		RequestedByFirstName = formPrefix + "RequestedBy_" + i + "_FirstName";
				   		RequestedByLastName = formPrefix + "RequestedBy_" + i + "_LastName";
				   		RequestedByEmail = formPrefix + "RequestedBy_" + i + "_Email";	
				   	}
				   	if (RequestedByTitle != null && !RequestedByTitle.equals("")) {				   
				   		title = getFieldFromBooking("String", RequestedByTitle);
				   	}
				   	if (RequestedByFullName != null && !RequestedByFullName.equals("")) {				   
				   		fullName = getFieldFromBooking("String", RequestedByFullName); 
				   	}
				   	if (RequestedByFirstName != null && !RequestedByFirstName.equals("")) {				   
				   		firstName = getFieldFromBooking("String", RequestedByFirstName); 
				   	}
				   	if (RequestedByLastName != null && !RequestedByLastName.equals("")) {	
					   	lastName = getFieldFromBooking("String", RequestedByLastName);
				   	}
				   	if (RequestedByEmail != null && !RequestedByEmail.equals("")) {	
					   	email = getFieldFromBooking("String", RequestedByEmail);
				   	}
				   	int countContactContactNumbers = 1;
				   	if (!getFieldFromBooking("String", formPrefix + "Count_Contact_" + i + "_ContactNumbers").equals("")) {
				   		countContactContactNumbers = new Integer(getFieldFromBooking("String", formPrefix + "Count_Contact_" + i + "_ContactNumbers"));
				   	}
					if (title != null) {
				   		dataObj.put("title", title);
				   	}
					if (fullName != null) {
				   		dataObj.put("fullName", fullName);
				   	}
					if (firstName != null) {
				   		dataObj.put("firstName", firstName);
				   	}
					if (lastName != null) {
				   		dataObj.put("lastName", lastName);
				   	}
					
					if (email != null) {
				   		dataObj.put("email", email);
				   	}
					//Overwrite first requester details with global contact details if allowed
					if (globalContactDetails.size() > 0 && i==0) {
						if (kwHandlerIMSDB.getKeywordValueString(Constants.KW_API_USE_REQU_NAME) == null
								|| kwHandlerIMSDB.getKeywordValueString(Constants.KW_API_USE_REQU_NAME).equalsIgnoreCase(Constants.NO)) { //Overwrite name only if requester name is empty
							dataObj.put("fullName", globalContactDetails.get("fullName"));
							dataObj.put("firstName", globalContactDetails.get("firstName"));
							dataObj.put("lastName", globalContactDetails.get("lastName"));
						}
						dataObj.put("email", globalContactDetails.get("email"));
					}
				   	if (countContactContactNumbers > 0) {
				   		JsonArray contactObj = new JsonArray();
				   		//We can have multiple contact numbers for a contact
				   		for (int c=0;c<countContactContactNumbers;c++) {
				   			JsonObject contactObjMob = new JsonObject();
				   			JsonObject contactObjHome = new JsonObject();
				   			JsonObject contactObjWork = new JsonObject();
			   				String cContactTypeMobile = null;
			   				String cContactTypeHome = null;
			   				String cContactTypeWork = null;
			   				String numberMobile = null;
			   				String numberHome = null;
			   				String numberWork = null;
				   			if (i == 0) { //Get legacy contact field
				   				cContactTypeWork = formPrefix + "RequestedByContact"; // Saving it as Work
				   			} else {					   			
				   				cContactTypeWork = formPrefix + "RequestedBy_" + i + "_Contact_Mobile";				   		
				   			}
				   			cContactTypeMobile = formPrefix + "RequestedBy_" + i + "_Contact_Mobile";
			   				cContactTypeHome = formPrefix + "RequestedBy_" + i + "_Contact_Home";
				   			numberMobile = getFieldFromBooking("String", cContactTypeMobile);
				   			numberHome = getFieldFromBooking("String", cContactTypeHome);
				   			numberWork = getFieldFromBooking("String", cContactTypeWork);
				   			//Overwrite first requester details with global contact details if allowed
							if (globalContactDetails.size() > 0 && i==0) {
								numberMobile = (String) globalContactDetails.get("mobilePhone");
								numberWork= (String) globalContactDetails.get("workPhone");
							}
				   			if (numberMobile != null) {
				   				contactObjMob.put("type", "Mobile");
				   				contactObjMob.put("number", numberMobile);
				   			}
				   			if (numberHome != null) {
				   				contactObjHome.put("type", "Home");
				   				contactObjHome.put("number", numberHome);
				   			}
				   			if (numberWork != null) {
				   				contactObjWork.put("type", "Work");
				   				contactObjWork.put("number", numberWork);
				   			}
				   			if (contactObjMob != null) {
				   				contactObj.add(contactObjMob);
				   			}
				   			if (contactObjHome != null) {
				   				contactObj.add(contactObjHome);
				   			}
				   			if (contactObjWork != null) {
				   				contactObj.add(contactObjWork);
				   			}
				   			
				   		}
				   		if (contactObj != null) {
				   			dataObj.put("contactNumbers", contactObj);
				   		}
				   	}
				   	if (dataObj.size()>0) {
				   		contactsData.add(dataObj);
				   	}
				}
			} 
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
		return contactsData;
	}
	
	public JsonArray getProfessionalFields() {
		JsonArray professionalData = new JsonArray();
		try {
			if (countClinicians > 0) {
				//We can have multiple patients, get all
				for(int i=0;i<countClinicians;i++) {
					JsonObject dataObj = new JsonObject();
					String cTitle;
					String cFirstName;
					String cLastName;
					String cFullName;
				   	String cEmail;
					String cStartTime;
				   	String cEndTime;
				   
				   	if (i == 0) { //Get data from legacy fields if available
				   		cTitle = formPrefix + "ClinicianTitle";
				   		cFirstName = formPrefix + "ClinicianFirstName";
				   		cLastName = formPrefix + "ClinicianLastName";
				   		cFullName = formPrefix + "Clinician";
				   		cEmail = formPrefix + "ClinicianEmail";
				   		cStartTime = formPrefix + "ClinicianStartTime";
				   		cEndTime = formPrefix + "ClinicianEndTime";
				   	} else {
				   		cTitle = formPrefix + "Clinician_" + i + "_Title";
				   		cFirstName = formPrefix + "Clinician_" + i + "_FirstName";
				   		cLastName = formPrefix + "Clinician_" + i + "_LastName";
				   		cFullName = formPrefix + "Clinician_" + i;
				   		cEmail = formPrefix + "Clinician_" + i + "_Email";	
				   		cStartTime = formPrefix + "Clinician_" + i + "_StartTime";
				   		cEndTime = formPrefix + "Clinician_" + i + "_EndTime";
				   	}
	
				   	String title = getFieldFromBooking("String", cTitle); 
				   	String firstName = getFieldFromBooking("String", cFirstName); 
				   	String lastName = getFieldFromBooking("String", cLastName); 
				   	String name = getFieldFromBooking("String", cFullName); 
				   	String email = getFieldFromBooking("String", cEmail);
				   	String startTime = getFieldFromBooking("String", cStartTime);
				   	String endTime = getFieldFromBooking("String", cEndTime);
				   	
				   	int countCliniciansContactNumbers = 1;
				   	if (!getFieldFromBooking("String", formPrefix + "Count_Clinician_" + i + "_ContactNumbers").equals("")) {
				   		countCliniciansContactNumbers = new Integer(getFieldFromBooking("String", formPrefix + "Count_Clinician_" + i + "_ContactNumbers"));
				   	}
					if (title != null) {
				   		dataObj.put("title", title);
				   	}
					if (firstName != null) {
				   		dataObj.put("firstName", firstName);
				   	}
					if (lastName != null) {
				   		dataObj.put("lastName", lastName);
				   	}
					if (name != null) {
				   		dataObj.put("fullName", title != "" ? title + " " + name : "" + name);
				   	}
					if (email != null) {
				   		dataObj.put("email", email);
				   	}

				   	if (startTime == null || startTime.equals("")) {
				   		startTime = appointmentStartTimeText;
				   	}
				   	if (endTime == null || endTime.equals("")) {
				   		endTime = appointmentEndTimeText;
				   	}
					if (startTime != null) {
				   		dataObj.put("startTime", startTime);
				   	}
					if (endTime != null) {
				   		dataObj.put("endTime", endTime);
				   	}
				   
				   	if (countCliniciansContactNumbers > 0) {
				   		JsonArray contactObj = new JsonArray();
				   		//We can have multiple contact numbers for a Clinician
				   		for (int c=0;c<countCliniciansContactNumbers;c++) {
				   			JsonObject contactObjMob = new JsonObject();
				   			JsonObject contactObjHome = new JsonObject();
				   			JsonObject contactObjWork = new JsonObject();
			   				
			   				String ClinicianContactTypeHome = formPrefix + "Clinician_" + i + "_Contact_Home";					   			
			   				String ClinicianContactTypeMobile = formPrefix + "Clinician_" + i + "_Contact_Mobile";
			   				String ClinicianContactTypeWork = formPrefix + "Clinician_" + i + "_Contact_Work";
			   				if (i == 0) {
			   					ClinicianContactTypeMobile = formPrefix + "ClinicianContact";
			   				}
			   				String numberMobile = getFieldFromBooking("String", ClinicianContactTypeMobile);
			   				String numberHome = getFieldFromBooking("String", ClinicianContactTypeHome);
			   				String numberWork = getFieldFromBooking("String", ClinicianContactTypeWork);

				   			if (numberMobile != null) {
				   				contactObjMob.put("type", "Mobile");
				   				contactObjMob.put("number", numberMobile);
				   			}
				   			if (numberHome != null) {
				   				contactObjHome.put("type", "Home");
				   				contactObjHome.put("number", numberHome);
				   			}
				   			if (numberWork != null) {
				   				contactObjWork.put("type", "Work");
				   				contactObjWork.put("number", numberWork);
				   			}
				   			if (contactObjMob != null) {
				   				contactObj.add(contactObjMob);
				   			}
				   			if (contactObjHome != null) {
				   				contactObj.add(contactObjHome);
				   			}
				   			if (contactObjWork != null) {
				   				contactObj.add(contactObjWork);
				   			}
				   		}
			   			if (contactObj != null) {
			   				dataObj.put("contactNumbers", contactObj);
			   			}
				   	}
				   	if (dataObj.size()>0) {
				   		professionalData.add(dataObj);
				   	}
				}
			} 
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
		return professionalData;
	}
	
	/**
	 * Check if external reference is unique except current booking
	 * @param externalReference
	 * @param bookingId
	 * @return
	 */
	public boolean isExternalIdExist(String externalReference, String bookingId) {
		try {
			View luView = dataDb.getAllBookingsByExternalIdView();
			Vector<String> luKeys = new Vector<String> ();
			luKeys.add(externalReference);
			Document booking = luView.getDocumentByKey(luKeys, true);
			luView.recycle();
			if (booking!=null && !booking.getItemValueString("DocKey").equalsIgnoreCase(bookingId)) {
				return true;
			}
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
		return false;
	}
	
	
	
	/**
	 * Check if PMS ID is unique except current booking
	 * @param strPMSID
	 * @param strBookingID
	 * @return
	 */
	public boolean isPMSIDExists(String strPMSID, String strBookingID){
		try {			
			View luView = dataDb.getDatabase().getView("vwAllDocsByPMSID");
			Vector<String> luKeys = new Vector<String> ();
			luKeys.add(strPMSID);
			Document docBook = luView.getDocumentByKey(luKeys, true);
			luView.recycle();
			if (docBook!=null && !docBook.getItemValueString("DocKey").equalsIgnoreCase(strBookingID)) {
				return true;
			}
		}catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
		return false;
	}
	
	/**
	 * Get all IMS IDs for a PMS ID
	 * @param strPMSID
	 * @return
	 */
	public ArrayList<String> getAllExistingIMSIdsForPMSId(String strPMSID) {
	    ArrayList<String> objBookings = new ArrayList<String>();
		try {		
			View luView = dataDb.getDatabase().getView("vwAllDocsByPMSID");
			Vector<String> luKeys = new Vector<String> ();
			luKeys.add(strPMSID);
			ViewEntryCollection allEntries = luView.getAllEntries();
			ViewEntry entry = allEntries.getFirstEntry();
			while (entry != null) {
				Vector v = entry.getColumnValues();
		        for (int i=0; i<v.size(); i++) {
		          if (i==1 && v.elementAt(i) != null && !v.elementAt(i).toString().isEmpty()) {
		            System.out.println("\t" + v.elementAt(i)); 
		            objBookings.add(v.elementAt(i).toString());
		            }
		        }
				ViewEntry tmpentry = allEntries.getNextEntry(entry);
				entry.recycle();
				entry = tmpentry;
			}
			allEntries.recycle();
		}catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
		return objBookings;
	}
	

	/**
	 * Check if booking document is being edited by IMS user and is currently locked for changes
	 * @return
	 */
	public boolean isLocked() {
		try {
			View luView = imsHomeDb.getView("luLock");
			Document lockDocument = luView.getDocumentByKey(universalId);
			luView.recycle();
			if (lockDocument != null) {
				DateTime lockExpiryDateTime = (DateTime) lockDocument.getItemValue("lock_expire").get(0);
				if (nowDateTime.timeDifferenceDouble(lockExpiryDateTime) > 0) {
					return false;
				}
				return true;
			}
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			Globals.printException(e);
		}
		return false;
	}
	
	/**
	 * Get details of in-house interpreter for a booking
	 * @return JsonArray
	 */
	public JsonArray getInHouseInterpreterDetails() {
		interpreters = new JsonArray();
		try {
			//User user = new User(session, interpreterDocKey);//TODO - To get full details, access User because booking does not contain full information on in-house interpreter
			int i = 0;
			JsonObject interpreter = new JsonObject();
			String InterpreterId = formPrefix + "Interpreter_DocKey";
			String Interpreter = formPrefix + "Interpreter_FullName";
			//String InterpreterFirstName = formPrefix + "Interpreter_FirstName";
			//String InterpreterLastName = formPrefix + "Interpreter_LastName";
			//String InterpreterEmail = formPrefix + "Interpreter_Email";
			//String InterpreterGender = formPrefix + "Interpreter_Gender";
			//String AccreditationLevel = formPrefix + "Interpreter_Level";
			//String AccreditationNumber = formPrefix + "Interpreter_Number";
		   
		   if (InterpreterId != null) {
			   interpreter.put("id", getFieldFromBooking("String", InterpreterId)); 
		   }
		   if (Interpreter != null) {
			   interpreter.put("fullName", getFieldFromBooking("String", Interpreter)); 
		   }
		   /*if (InterpreterFirstName != null) {
			   interpreter.put("firstName", getFieldFromBooking("String", InterpreterFirstName)); 
		   }
		   if (InterpreterLastName != null) {
			   interpreter.put("lastName", getFieldFromBooking("String", InterpreterLastName)); 
		   }
		   if (InterpreterGender != null && !getFieldFromBooking("String", InterpreterGender).equals("")) {
			   interpreter.put("gender", getFieldFromBooking("String", InterpreterGender)); 
		   }
		   if (InterpreterEmail != null && !getFieldFromBooking("String", InterpreterEmail).equals("")) {
			   interpreter.put("email", getFieldFromBooking("String", InterpreterEmail)); 
		   }
		   if (AccreditationLevel != null && !getFieldFromBooking("String", AccreditationLevel).equals("")) {
			   interpreter.put("accreditationLevel", getFieldFromBooking("String", AccreditationLevel)); 
		   }
		   if (AccreditationNumber != null && !getFieldFromBooking("String", AccreditationNumber).equals("")) {
			   interpreter.put("accreditationNumber", getFieldFromBooking("String", AccreditationNumber));
		   }
		   if (!Interpreter.equals("") && !AccreditationLevel.equals("") && !AccreditationNumber.equals("")) {
			   isValidInterpreterExists = true;
		   }*/
		   int countInterpretersContactNumbers = 1;
		   if (countInterpretersContactNumbers>0) {
			   JsonArray contactNumbers = new JsonArray();
			   //We can have multiple contact numbers for a Contact
			   for (int c=0;c<countInterpretersContactNumbers;c++) {
				   JsonObject contactNumber = new JsonObject();
				   String InterpreterContactTypeWork = null;
				   String InterpreterContactTypeHome = null;
				   String InterpreterContactTypeMobile;
				   if (i == 0 && c == 0) {
					   InterpreterContactTypeMobile = formPrefix + "Interpreter_Contact"; //Get the legacy field
				   } else {
					   InterpreterContactTypeWork = formPrefix + "Interpreter_" + i + "_Contact_Work";
					   InterpreterContactTypeHome = formPrefix + "Interpreter_" + i + "_Contact_Home";
					   InterpreterContactTypeMobile = formPrefix + "Interpreter_" + i + "_Contact_Mobile";
				   }
				   if (InterpreterContactTypeWork != null && !getFieldFromBooking("String", InterpreterContactTypeWork).equals("")) {
					   contactNumber.put("Work", getFieldFromBooking("String", InterpreterContactTypeWork)); 
				   }
				   if (InterpreterContactTypeHome != null && !getFieldFromBooking("String", InterpreterContactTypeHome).equals("")) {
					   contactNumber.put("Home", getFieldFromBooking("String", InterpreterContactTypeHome)); 
				   }
				   if (InterpreterContactTypeMobile != null && !getFieldFromBooking("String", InterpreterContactTypeMobile).equals("")) {
					   contactNumber.put("Mobile", getFieldFromBooking("String", InterpreterContactTypeMobile)); 
				   }
				   if (contactNumber.size()>0) {
					   contactNumbers.add(contactNumber);
				   }
			   }
			   if (contactNumbers.size()>0) {
				   interpreter.put("contactNumbers", contactNumbers); 
			   }
		   }
		   interpreters.add(interpreter);
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
			Globals.printException(e);
		}
		return interpreters;
	}

	/**
	 * Get post booking details attachments array with file names
	 * @return 
	 */
	public JsonArray getPbdAttachments() {
		JsonArray pbdAttachments = new JsonArray();
		JsonObject fileData = new JsonObject();
		try {
			RichTextItem rtItem = (RichTextItem) bookingDoc.getFirstItem("BOOK_Receipts");
			if (rtItem != null) {
				Vector v = rtItem.getEmbeddedObjects();
		        Enumeration enumeration = Collections.enumeration(v);
		        while (enumeration.hasMoreElements()) {
		        	EmbeddedObject eo = (EmbeddedObject) enumeration.nextElement();
			  		AttachmentHelper attachmentHelper = new AttachmentHelper(session);
					fileData = attachmentHelper.getAttachmentDetails(eo);
					pbdAttachments.add(fileData);
		        }
				rtItem.recycle();
			}
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
			e.printStackTrace();
		}
		return pbdAttachments;
	}
    
	/**
	 * Get global/non-global allAttachments array with file names
	 * @return 
	 */
	public JsonArray getGlobalsAttachments() {
		JsonArray allAttachments = new JsonArray();
		JsonObject fileData = new JsonObject();
		DocumentCollection attachmentsCollection = null;
		try {
			if (attachmentKey != null && !attachmentKey.isEmpty()) {
				DocumentCollection att = getAttachmentDocuments(attachmentKey);
				if (att != null && att.getCount() > 0) {
					attachmentsCollection = att;
				}
			}
			if (cbnAttachmentKey != null && !cbnAttachmentKey.isEmpty()) {
				DocumentCollection att = getAttachmentDocuments(cbnAttachmentKey);
				if (att != null && attachmentsCollection != null && att.getCount() > 0 && attachmentsCollection.getCount() > 0) {
					attachmentsCollection.merge(att);
				} else if (att != null && att.getCount() > 0) {
					attachmentsCollection = att;
				}
				
			}
			if (attachmentsCollection != null && attachmentsCollection.getCount() > 0) {
				Document attachmentDocument = attachmentsCollection.getFirstDocument();
				while (attachmentDocument != null) {
					if (attachmentDocument.hasItem("ATT_Attachment")) {
						RichTextItem rtItem = (RichTextItem) attachmentDocument.getFirstItem("ATT_Attachment");
						if (rtItem != null) {
							Vector v = rtItem.getEmbeddedObjects();
					        Enumeration enumeration = Collections.enumeration(v);
					        while (enumeration.hasMoreElements()) {
					        	EmbeddedObject eo = (EmbeddedObject) enumeration.nextElement();
						  		AttachmentHelper attachmentHelper = new AttachmentHelper(session);
								fileData = attachmentHelper.getAttachmentDetails(eo);
								eo.recycle();
								allAttachments.add(fileData);
					        }
						}
						rtItem.recycle();
					}
					attachmentDocument = attachmentsCollection.getNextDocument(attachmentDocument);
				}            
				if (attachmentDocument != null) {
					attachmentDocument.recycle();
				}
				if (attachmentsCollection != null) {
					attachmentsCollection.recycle();
				}
			}
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			e.printStackTrace();
		}
		return allAttachments;
	}

	/**
	 * Get attachment documents
	 * @param key
	 * @return
	 */
	private DocumentCollection getAttachmentDocuments(String key) {
		DocumentCollection collection = null;
		try {
			collection = imsHomeDb.getViewDocumentsByKey(Constants.VIEW_ATTACHMENTS_BY_PARENT_DOCKEY, key);
		} catch (Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
			Globals.printException(e);
		}
		return collection;
	}

	/**
	 * User profile setting for sending email
	 * @param docUser
	 * @param strType
	 * @return
	 */
	private static boolean isProfileDisabled(Document docUser , String type){
		boolean bIsDisabled = false;
		try{
			String strProfileDisabled = docUser.getItemValueString(type + "_DisableEmail");
			if(strProfileDisabled.toUpperCase().equals("YES")){
				bIsDisabled = true;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return bIsDisabled;
	}
	
	/**
	 * Send email notifications to users
	 */
	public void notifyByEmail(String action){
		try {
			Notification notification = new Notification(session);
			if(bookingDoc != null) {
				boolean notifyRequestor = true;
				notification.setDisplayStatus(action);
				notification.setBookingDocKey(id);
				if(kwHandlerIMSDB.getKeywordValueString("Notification_Enabled").equals("1")) {	
					if(kwHandlerIMSDB.getKeywordValueString("Notification_Enabled_REQ").equals("1")) {
						if(notifyRequestorByEmail != null && notifyRequestorByEmail.equalsIgnoreCase("Yes")) {
							if(kwHandlerIMSDB.getKeywordValueString("AllowMultiBookings").equalsIgnoreCase("Yes") && displayStatus != Constants.bookingStatusNew) {
								if(!notifyByEmailChildBooking(action)){ // if we're meant to send the CBN email
									notifyRequestor = false;
								}
							}
							//lookup requester by email
							Document docUser = imsHomeDb.getView("REQU_ByEmail").getDocumentByKey(bookingDoc.getItemValueString(formPrefix + "RequestedByEmail"), true);
							//Check if current Requester's notification is enabled / disabled
							if (notifyRequestor) {
								if (docUser != null) {
									if(!isProfileDisabled(docUser, Constants.FORM_REQUESTOR)){
										notification.setUserType(Constants.NOTIFICATION_USER_TYPE_REQUESTOR);
										notification.setForm(Constants.FORM_REQUESTOR);
										notification.create();
									}
								} else { //requester not found so send email anyway
									notification.setUserType(Constants.NOTIFICATION_USER_TYPE_REQUESTOR);
									notification.setForm(Constants.FORM_REQUESTOR);
									notification.create();
								}
							}		
						}
					}
					if(kwHandlerIMSDB.getKeywordValueString("Notification_Enabled_CRD").equals("1")) {
						notification.setUserType(Constants.NOTIFICATION_USER_TYPE_COORDINATOR);
						notification.setForm(Constants.formUser);
						notification.create();
					}
				}
			}
		} catch(Exception e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			e.printStackTrace();
		}
	}

	private boolean notifyByEmailChildBooking(String action) {
		try {
			// If AllowMultiBookings not enabled, send email 
			if(!kwHandlerIMSDB.getKeywordValueString("AllowMultiBookings").equalsIgnoreCase("Yes")){return true;}
			// IF AllowMultiBookings enabled and the current booking is part of Combined booking	
			String strCBNDocKey = parentId;
			if(kwHandlerIMSDB.getKeywordValueString("AllowMultiBookings").equalsIgnoreCase("Yes") && (strCBNDocKey != null && !strCBNDocKey.isEmpty())){
				// Check if other child bookings are in the same status as current booking
				// Use CBN_DocKey to get CBN booking handle
				Document docCBN = bookingDoc.getParentDatabase().getView("vwAllByDocKey").getDocumentByKey(strCBNDocKey, true);
				if (docCBN != null){
					// Then use CBN_BOOK_DocKeys to get handle of all child bookings
					lotus.domino.DocumentCollection bookDC = bookingDoc.getParentDatabase().getView("GRID_CBN_BOOK").getAllDocumentsByKey(docCBN.getItemValueString("DocKey"), true);
					if(bookDC.getCount() != 0) {
						Document childDocument = bookDC.getFirstDocument();
						String currentBookingStatus = "";
						if(displayStatus == "Cancelled") {
							currentBookingStatus = action;
						} else {
							currentBookingStatus = displayStatus;
						}
						boolean bChildSame = true;
						// loop through all child bookings 
						while (childDocument != null) {
							String childStatus = childDocument.getItemValueString(Constants.formBook + "_Status_Display");
							// Ignore if current booking
							// If there"s any bookings left in list (could have only been one booking) then
							if(action.equals("Cancelled") && childDocument.getItemValueString(Constants.formBook + "_Completed").equalsIgnoreCase(Constants.NO)) {
								childStatus = "Cancelled";
							}
							// If status is same as current status for all bookings (ignoring current booking)
							// SHOULD CHECK IF IT'S BOOKED/COMPLETED/CANCELLED????
							if(!currentBookingStatus.equals(childStatus)) {
								// set flag as false - this means not all bookings are booked/cancelled etc, so don't send an email yet
								if (!childStatus.equals(Constants.bookingStatusCancelledDisplay) && childDocument.getItemValueString(Constants.formBook + "_Completed").equalsIgnoreCase(Constants.NO)) {
									//ignore - cancelled child docs should be ignored if we aren't doing a cancellation
								} else {
									bChildSame = false;
								}
								
							}
							childDocument = bookDC.getNextDocument(childDocument);
						}					
						if(!bChildSame) {
							return false;
						}
					}			
				}		
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	/**
	 * 
	 * @param attachment
	 */
	public void addPbdAttachment(Attachment attachment) {
		RichTextItem rtItem = null;
		try {
			if (!bookingDoc.hasItem("BOOK_Receipts")) { //Attachment for booking receipts
				rtItem = bookingDoc.createRichTextItem("BOOK_Receipts");
			} else {
				rtItem = (RichTextItem) bookingDoc.getFirstItem("BOOK_Receipts");
			}
			rtItem.embedObject(EmbeddedObject.EMBED_ATTACHMENT, null, attachment.getAbsolutePath(), attachment.getName() + "." + attachment.getExtension());
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			bookingController.httpResponse = 503;
			bookingController.messageCode.add(1000);
			bookingController.isInValid = true;
			e.printStackTrace();
		}
	}

	/**
	 * Set to save completed
	 * @param b
	 */
	public void saveCompleted(boolean b) {
		completeBooking = b;
	}
	
	/**
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public JsonObject getAttachmentEncodedDetails(String fileName) throws IOException {
		JsonObject fileData = new JsonObject();
			try {
			if (attachments.size() > 0) {
		    	AttachmentHelper attachmentHelper = new AttachmentHelper(session);
				for(int i=0; i<attachments.size();i++) {
					JsonObject file = (JsonObject) attachments.get(i);
					String filePath = file.get("path").toString();
					if (file.get("name") != null && file.get("name").toString().equalsIgnoreCase(fileName)) {
						Path path = new File(filePath).toPath();  
						String encoded = attachmentHelper.encoder(filePath);
						String mimeType = Files.probeContentType(path);
						fileData.put("mimeType", mimeType);
						fileData.put("name", file.get("name"));
						fileData.put("encoded", encoded);
				    }
				}
			}
		} catch (IOException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			e.printStackTrace();
		}
		return fileData;
	}

	/**
	 * 
	 * @param oldStatus
	 */
	public void setOldStatus(String oldStatus) {
		this.oldStatus= oldStatus;
	}

	/**
	 * 
	 * @param oldDisplayStatus
	 */
	public void setOldDisplayStatus(String oldDisplayStatus) {
		this.oldDisplayStatus= oldDisplayStatus;
	}

	/**
	 * Set attachments array with file names
	 * @param attachments
	 */
	public void setPbdAttachments(JsonArray attachments) {
		this.attachments = attachments;
	}

	/**
	 * @return the waitingDuration
	 */
	public double getWaitingDuration() {
		return waitingDuration;
	}

	/**
	 * @param time the waitingDuration to set
	 */
	public void setWaitingDuration(double time) {
		this.waitingDuration = time;
	}

	/**
	 * @return the numCalls
	 */
	public double getNumCalls() {
		return numCalls;
	}

	/**
	 * @param time the numCalls to set
	 */
	public void setNumCalls(double time) {
		this.numCalls = time;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getPayInvoice() {
		return payInvoice;
	}

	/**
	 * 
	 * @param payInvoice
	 */
	public void setPayInvoice(String payInvoice) {
		this.payInvoice = payInvoice;
	}

	/**
	 * 
	 * @return
	 */
	public String getChargeInvoice() {
		return chargeInvoice;
	}

	/**
	 * 
	 * @param chargeInvoice
	 */
	public void setChargeInvoice(String chargeInvoice) {
		this.chargeInvoice = chargeInvoice;
	}
	
	/**
	 * Is Invoiced
	 * @return
	 */
	public boolean isInvoiced() {
		if (payInvoice.isEmpty() && chargeInvoice.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Get attachment's key
	 * @return
	 */
	public String getAttachmentKey() {
		return attachmentKey;
	}

	/**
	 * 
	 * @param attachmentKey
	 */
	public void setAttachmentKey(String attachmentKey) {
		this.attachmentKey = attachmentKey;
	}

	/**
	 * 
	 * @return
	 */
	public String getCbnAttachmentKey() {
		return cbnAttachmentKey;
	}

	/**
	 * 
	 * @param cbnAttachmentKey
	 */
	public void setCbnAttachmentKey(String cbnAttachmentKey) {
		this.cbnAttachmentKey = cbnAttachmentKey;
	}

	public boolean isFlexible() {
		return isFlexible;
	}

	public void setFlexible(boolean isFlexible) {
		this.isFlexible = isFlexible;
	}

	public String getFlexibleTimeStart() {
		return flexibleTimeStart;
	}

	public void setFlexibleTimeStart(String flexibleTimeStart) {
		this.flexibleTimeStart = flexibleTimeStart;
	}

	public String getFlexibleTimeEnd() {
		return flexibleTimeEnd;
	}

	public void setFlexibleTimeEnd(String flexibleTimeEnd) {
		this.flexibleTimeEnd = flexibleTimeEnd;
	}

	public String getModality() {
		return modality;
	}

	public void setModality(String modality) {
		this.modality = modality;
	}

	public void recycle() {
		this.recycle();
	}

	public String getPendingSendWebhook() {
		return pendingSendWebhook;
	}

	public void setPendingSendWebhook(String pendingSendWebhook) {
		this.pendingSendWebhook = pendingSendWebhook;
	}

	public DateTime getLastModifiedDateTime() {
		return lastModifiedDateTime;
	}

	public void setLastModifiedDateTime(DateTime lastModifiedDateTime) {
		this.lastModifiedDateTime = lastModifiedDateTime;
	}

	public String getLastModifiedDateText() {
		return lastModifiedDateText;
	}

	public void setLastModifiedDateText(String lastModifiedDateText) {
		this.lastModifiedDateText = lastModifiedDateText;
	}

	public String getLastModifiedTimeText() {
		return lastModifiedTimeText;
	}

	public void setLastModifiedTimeText(String lastModifiedTimeText) {
		this.lastModifiedTimeText = lastModifiedTimeText;
	}

	public String getSystemComments() {
		return systemComments;
	}

	public void setSystemComments(String systemComments) {
		this.systemComments = systemComments;
	}

	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceDisplay() {
		return sourceDisplay ;
	}
	
	public void setSourceDisplay(String sourceDisplay) {
		this.sourceDisplay = sourceDisplay;
	}
}