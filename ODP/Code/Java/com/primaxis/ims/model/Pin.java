package com.primaxis.ims.model;

import com.primaxis.ims.controller.PinController;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.ImsHomeDb;
import com.primaxis.ims.utils.KeywordHandler;
import com.primaxis.ims.utils.Log;

import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;

public class Pin {
	
	//private ImsDataDb dataDb;
	private ImsHomeDb imsHomeDb;
	private PinController pinController;
	private Session session;
	private KeywordHandler kwHandlerIMSDB;

	private String id;
	private Document doc = null;
	private String form = "PIN";
	private String formPrefix = form + "_";
	private String formSource = "";

	public Pin(PinController pinController, Session session) throws NotesException {
		this.pinController = pinController;
		this.session = session;
		try {
			imsHomeDb = new ImsHomeDb(session);
			kwHandlerIMSDB = new KeywordHandler(imsHomeDb.getDatabase());
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}

	public void getPin(String id) {
		try {
			if (id != null && !id.isEmpty()) {
				doc = imsHomeDb.getPinDoc(id);
				if (doc != null) {
					if (doc.getItemValueString(formPrefix + "Id") != null) {
						id = doc.getItemValueString(formPrefix + "Id");
					}
				}
			}
		} catch (NotesException e) {
			Globals.printException(e);
		}
	}

	public boolean verifyPin(String clientid, String PIN) {
		try {
			Globals.printCode("clientid="+clientid + " PIN="+PIN);
			if (clientid != null && !clientid.isEmpty()) {
				doc = kwHandlerIMSDB.getPINDoc(clientid, PIN);
				if (doc != null) {
					return true;
				}
			}
		} catch (NotesException e) {
			Globals.printException(e);
		}
		return false;
	}

	
	public String getId() {
		return id;
	}

	public void setId(String clientPin) {
		this.id = clientPin;
	}

	public Document getDoc() {
		return doc;
	}

	public void setDoc(Document doc) {
		this.doc = doc;
	}

	public String getForm() {
		return form;
	}

	public String getFormPrefix() {
		return formPrefix;
	}

}
