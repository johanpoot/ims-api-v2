package com.primaxis.ims.utils;

import lotus.domino.Base;
import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.Document;

/**
 * Log - static object to log  messages, currently just to the console
 * includes a 'debug' flag to turn the logging on and off 
 * 
 * @author	Baljit Karwal
 * @since	01 May 2019
 */
public class Log {
	private static boolean debug;
	private static Database logDB;
	private ImsHomeDb ImsHomeDb;
	private Document logDoc;
	private Document errorLog;
	
	//turn the debug flag on or off
	public static void setDebug (boolean debug){
		Log.debug = debug;
	}
	//write the error no matter what
	public static void writeError (String logLine){
		System.out.println (logLine);
	}
	
	//if we're debugging then write to the console
	public static void writeLine (String logLine){
		if (debug == true) {
			System.out.println(logLine);
		}
	}
	
	//Setup the logDB if it doesn't already exist
	public Log (Session session){
		try {
			//use a keyword handler for the current DB to get the keyword for IMS database and IMS Server
			KeywordHandler kwHandler = new KeywordHandler(session.getCurrentDatabase());
			//Session session = Globals.getSession();
			String server = session.getCurrentDatabase().getServer();
			String remoteServer = kwHandler.getKeywordValueString("DB_FilePath_IMS_Server");
			if (!remoteServer.equals("-")) {
				server = remoteServer;
			}
			ImsHomeDb = new ImsHomeDb(session);
			KeywordHandler kwHandlerIMSDB = new KeywordHandler(ImsHomeDb.getDatabase());
			if (!remoteServer.equals("-")) {
				server = remoteServer;
			}
			String dbPath = kwHandlerIMSDB.getKeywordValueString("DB_FilePath_IMS_REST_API_Log");
			logDB = session.getDatabase(server, dbPath);
			String debugFlag = kwHandlerIMSDB.getKeywordValueString ("IMS_Rest_API_Debug");
			if (debugFlag.equalsIgnoreCase("YES")) {
				Log.debug = true;
			} else {
				Log.debug = false;
			}
		} catch (NotesException e) {
			writeLine("Log error message: " + e.getMessage());
			Globals.printException(e);
		}
	}
	
	/**
	 * update the log document with the request JSON
	 * @param requestJSON
	 * @throws NotesException
	 */
	public void updateLogField (String field, String value)
	{
		try {
			if (field != null && value != null) {
				logDoc.replaceItemValue (field, value);
			}
			if (debug) {
				//always save if debug is turned on
				logDoc.save();
			}
		} catch (NotesException e) {
			Log.writeError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void logAPIError(String errorJSON, String docKey, String errorMessage, String interpreterDocKey) {
		try {
			errorLog = logDB.createDocument();
			errorLog.replaceItemValue("Form", "API_Error_Log");
			if (logDoc != null) {
				//if we get an error always save the log
				logDoc.save();
				errorLog.replaceItemValue ("API_Log_Document", logDoc.getUniversalID());
			}
			errorLog.replaceItemValue("DocKey", docKey);
			errorLog.replaceItemValue("UserId", interpreterDocKey);
			errorLog.replaceItemValue("API_Error_Message", errorMessage);
			errorLog.replaceItemValue("API_Error_JSON", errorJSON);
			errorLog.save();
			recycleObject (errorLog);
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}
	
	public void logAPIError (String errorJSON, String docKey, String errorMessage)
	{
		try {
			errorLog = logDB.createDocument();
			errorLog.replaceItemValue("Form", "API_Error_Log");
			if (logDoc != null) {
				//if we get an error always save the log
				logDoc.save();
				errorLog.replaceItemValue ("API_Log_Document", logDoc.getUniversalID());
			}
			errorLog.replaceItemValue("DocKey", docKey);
			errorLog.replaceItemValue("API_Error_Message", errorMessage);
			errorLog.replaceItemValue("API_Error_JSON", errorJSON);
			errorLog.save();
			recycleObject (errorLog);
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update the log document with the request JSON
	 * @param requestJSON
	 * @throws NotesException
	 */
	public void addRequestToLog (String requestJSON)
	{
		try {
			logDoc.replaceItemValue ("API_Request", requestJSON);
			if (debug) {
				//always save if debug is turned on
				logDoc.save();
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update the log document with the response JSON
	 * @param responseJSON
	 * @throws NotesException
	 */
	public void addResponseToLog(String responseJSON) {
		try {
			byte[] stringBytes = responseJSON.getBytes(java.nio.charset.StandardCharsets.UTF_8);
			if (stringBytes.length <= 32 * 1024) {
				logDoc.replaceItemValue ("API_Response", responseJSON);
			} else {
				logDoc.replaceItemValue ("API_Response", "Responce is too big to add to log");
			}
			if (debug) {
				//always save if debug is turned on
				logDoc.save();
			}
		} catch (NotesException e) {
			Globals.printException(e);
		}
	}

	/**
	 * Create a new API log document, logging the request.
	 * @param requestJSON
	 * @param method
	 * @param api
	 * @param Who
	 */
	public void createAPILog (String method, String api)
	{
		try {
			//if we already have one then we have to recycle it
			//recycleObject (logDoc);
			logDoc = logDB.createDocument();
			logDoc.replaceItemValue("Form", "IMS_API_Request_Log");
			logDoc.replaceItemValue("API_REQUEST_URL", api);
			logDoc.replaceItemValue("API_REQUEST_METHOD", method);
			if (debug) {
				//always save if debug is turned on
				logDoc.save();
			}
			
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			e.printStackTrace();
		}
	}
	
	
	public void recycle () {
		recycleObject (errorLog);
		recycleObject (logDoc);
		recycleObject (logDB);
	}
	
	/**
	 * Check if the object is not null and call it's recycle if you can
	 * @param notesObject
	 */
	private void recycleObject (Base notesObject){
		if (notesObject != null)
		{
			try {
				notesObject.recycle();
			} catch (NotesException e) {
				//don't need to do anything
			}
		}
	}
	
	public void saveAPILog ()
	{
		try{
			logDoc.save();
		}catch (Exception e){
			//Nothing
		}
	}
	
}
