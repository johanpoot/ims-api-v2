package com.primaxis.ims.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;

import org.json.simple.JsonObject;

import com.ibm.xsp.http.UploadedFile;

import lotus.domino.Document;
import lotus.domino.EmbeddedObject;
import lotus.domino.NotesException;
import lotus.domino.Session;


public class AttachmentHelper {
	
	private static final String TEMP_DIR = "C:\\Temp";
	private static Session session;
	private File file = null;
	private String name = null;
	private String extension = null;
	private String path = null;

	public AttachmentHelper(Session session2) {
		session = session2;
	}
	
	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Get file extension
	 * @param fileName
	 * @return
	 */
	public String getExtension() {
		String extension = "";
		int i = name.lastIndexOf('.');
		int p = Math.max(name.lastIndexOf('/'), name.lastIndexOf('\\'));
		if (i > p) {
		    extension = name.substring(i+1);
		}
		return extension;
	}

	/**
	 * @param extension the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * Generates image from base 64 string and return path of temp file
	 * @param fileString
	 * @param fileName
	 * @return
	 */
	public static String getPathFromBase64String(String fileString, String fileName) {
	    byte[] fileBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(fileString);
	    String path = null;
		try {
			BufferedImage image = ImageIO.read(new ByteArrayInputStream(fileBytes));
			if (image != null) {
			    // write the image to a file
			    File outputfile = new File(getTempDirFromIni(), fileName + ".png");
			    ImageIO.write(image, "png", outputfile);
			    path = outputfile.getPath();
			} else {
				Globals.printCode("Invalid Image");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	    return path;
	}
	
	private static String getTempDirFromIni() {
        String tempDir = TEMP_DIR;
        try {
        	if (session.getEnvironmentString("NOTES_TEMPDIR", true) != null) {
                tempDir = session.getEnvironmentString("NOTES_TEMPDIR", true);
        	}
        } catch (NotesException e) {
            e.printStackTrace();
        }
    	return tempDir;
	}

	/**
	 * Delete temporary file
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static String deleteTempFile (String fileName) throws IOException {
		File tempFile = null;
		try {
			tempFile = new File(getTempDirFromIni(), new File(fileName).getName());
			if (tempFile.exists()) {
				tempFile.delete();
			}
		} catch (Exception ignored) {}
		return "";
	}
	
	/**
	 * Encode file in base64 string
	 * @param filePath
	 * @return
	 */
	public static String encoder(String filePath) {
        String base64File = "";
        File file = new File(filePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            // Reading a file from file system
            byte fileData[] = new byte[(int) file.length()];
            imageInFile.read(fileData);
            base64File = Base64.getEncoder().encodeToString(fileData);
        } catch (FileNotFoundException e) {
            System.out.println("File not found" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the file " + ioe);
        }
        return base64File;
    }

	/**
	 * Return file details JSON Object with name/type/base64 string
	 * @param fileObj - Custom file object with required details
	 * @return
	 */
	public JsonObject getAttachmentDetails(EmbeddedObject eo) {
		JsonObject fileObj = new JsonObject();
		try {
			if (eo.getType() == EmbeddedObject.EMBED_ATTACHMENT) {
			    String thisPath = getTempDirFromIni() + "/" +eo.getSource();
			    eo.extractFile(thisPath);
			    name = eo.getName();
			    extension = getExtension();
			    fileObj.put("name", name);
			    fileObj.put("path", thisPath);
			}
		} catch (NotesException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return fileObj;
	}
	
	public File getFile(String path) {
		File file = null;
		try {
	    	file = new java.io.File(path);
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return file;
	}
	
	/**
	 * Get attachment base URL for a notes document
	 * @param doc
	 * @return
	 */
	public String getAttachmentBaseUrl(Document doc) {
		String url = null;
		String attachmentUrl = null;
		try {
			url = doc.getParentDatabase().getHttpURL();
			int idx = url.indexOf("?OpenDatabase");
		    if(idx != -1) { url = url.substring(0, idx);} //remove ?OpenDatabase 
			attachmentUrl = url + "/" + doc.getUniversalID() + "/$FILE/";
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return attachmentUrl;
	}
	
	/**
	 * Create a file on temporary path
	 * @param file
	 */
	public File createFile(Object file, String fileName) throws IOException {
		File correctedFile = null;
		if (file != null) {
		    UploadedFile myFile = (UploadedFile) file;
		    File tempFile = myFile.getServerFile();
			if (tempFile != null && tempFile.exists()) {
		    	name = myFile.getClientFileName();
		    	extension = getExtension();
                correctedFile = new java.io.File(getTempDirFromIni() + java.io.File.separator + fileName + "." + extension);
		        tempFile.renameTo(correctedFile);
		    }
		}
    	return correctedFile;
	}

	/**
	 * 
	 * @param file
	 */
	public void deleteTempFile(File file) {
		File tempFile = null;
		try {
			tempFile = new File(getTempDirFromIni(), file.getName());
			if (tempFile.exists()) {
				tempFile.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
