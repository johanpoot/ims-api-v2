package com.primaxis.ims.utils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Vector;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.NotesException;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.Session;


/**
 * Handles the IMS Home database
 * lets you get keywords from the database
 * 
 * @author Baljit Karwal
 *
 */
public class ImsHomeDb {
	//must push session into this STATIC variable from the NOTES AGENT so it's available to the class when needed
	private Database db;
	private KeywordHandler kwHandler;
	
	public ImsHomeDb (Session session) throws NotesException
	{
		//use a keyword handler for the current DB to get the keyword for IMS database
		kwHandler = new KeywordHandler(session.getCurrentDatabase());
		String server = session.getCurrentDatabase().getServer();
		String remoteServer = kwHandler.getKeywordValueString("DB_FilePath_IMS_Server");
		if (!remoteServer.equals("-")) {
			server = remoteServer;
		}
		String dbPath = kwHandler.getKeywordValueString("DB_FilePath_IMS");
		db = session.getDatabase(server, dbPath);
	}
	
	public String getDatabaseTitle ()
	{
		try {
			return db.getTitle();
		} catch (NotesException e) {
			return "";
		}
	}
	
	//get the database
	public Database getDatabase() {
		return db;
	}
	
	/**
	 * Get View
	 * @param luViewName - View name
	 * @return View
	 */
	public View getView (String luViewName) {
		try {
			View luView = db.getView(luViewName);
			return luView;			
		} catch (NotesException e) {
			Globals.printException(e);
			return null;
		}
	}
	
	/**
	 * 
	 * @param languageServiceProvider
	 * @return
	 */
	public boolean isLspEnabledToAccessApi(String languageServiceProvider) {
		try {
			if (languageServiceProvider != null) {
				View luView = getView("API_LSP");
				Document clientDoc = luView.getDocumentByKey(languageServiceProvider);
				luView.recycle();
				if (clientDoc != null) {
					String isEnabled = clientDoc.getItemValueString("AGN_Enable_API");
					if (isEnabled.equalsIgnoreCase("Yes")) {
						return true;
					}
				}
			}
		} catch (NotesException e) {
			Globals.printException(e);
		}
		return false;
	}
	
	public boolean IsBase64(String base64String) {
	    try {
	    	byte[] credDecoded = Base64.getDecoder().decode(base64String);
	        return true;
	    } catch( Exception e) {
	        return false;
	    }
	}
	
	/**
	 * Validate user credentials for "Basic base64credentials" and return user document
	 * @param authorization
	 * @return
	 */
	public Document getValidUserDocByBase64(String authorization) {
		Document userDoc = null;
		try {
			String username = null;
			String password = null;
			if (authorization != null && authorization.toLowerCase().startsWith("basic")) {
			    String base64Credentials = authorization.substring("Basic".length()).trim();
			    if (!base64Credentials.isEmpty() && IsBase64(base64Credentials)) {
				    byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
				    String credentials = new String(credDecoded, StandardCharsets.UTF_8);
				    final String[] values = credentials.split(":", 2); //Credentials = username:password
				    if (Globals.indexInBound(values, 0)) {
				    	username =  values[0];
				    }
				    if (Globals.indexInBound(values, 1)) {
				    	password =  values[1];
				    }
				    if (username!=null && !username.equals("") && password != null && !password.equals("")) {
				    	View userView = getView("USER_ByDocKey"); //Find user in DB by username
	                    userDoc = userView.getDocumentByKey(username);
				    	if (userDoc == null) {
					    	View userViewByEmail = getView("USER_ByEmail"); //Find user in DB by email if username not found(user login by email)
				    		userDoc = userViewByEmail.getDocumentByKey(username);
				    	}
	                    if (userDoc != null && userDoc.getItemValueString("USER_Password").equals(password)) { //Validate incoming user
	                		return userDoc;
	                    }
				    }
			    }
			}
		} catch (NotesException e) {
			Globals.printException(e);
		}
		return null;
	}
		
	/**
	 * Find user document in DB by user id or email
	 * @param username
	 * @return
	 */
	public Document getValidUserDocByUsername(String username) {
		Document userDoc = null;
		try {
			if (username != null) {
				View userView = getView("USER_ByDocKey"); //Find user in DB by username
	            userDoc = userView.getDocumentByKey(username);
		    	if (userDoc == null) {
			    	View userViewByEmail = getView("USER_ByEmail"); //Find user in DB by email if username not found(user login by email)
		    		userDoc = userViewByEmail.getDocumentByKey(username);
		    	}
			}
		} catch (NotesException e) {
			Globals.printException(e);
		}
		return userDoc;
	}
		
	/**
	 * Recycle Objects
	 */
	public void recycle () {
		kwHandler.recycle();
		if (db != null) {
			try {
				db.recycle();
			} catch (NotesException e) {
				//don't need to do anything
			}
		}
	}

	/**
	 * Check if language name exists in IMS DB and returns it
	 * 
	 * @param languageName
	 * @return languageName
	 * @throws NotesException
	 */
	public String checkLanguageName(String languageName) throws NotesException {
		try {
			if (languageName != null) {
				View luView = getView("LANG_ByName");
				Document langDoc = luView.getDocumentByKey(languageName);
				luView.recycle();
				if (langDoc != null) {
					String Language = langDoc.getItemValueString("Language");
					if (Language != null && Language.equalsIgnoreCase(languageName)) {
						return Language;
					}
				}
			}
		} catch (Exception e) {
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			return null;
		}
		return null;
	}


	/**
	 * Retrieve language name from language code or alternate language code
	 * 
	 * @param code
	 * @param bSearchAltCode
	 * @return
	 * @throws NotesException
	 */
	public String getLanguageFromCode(String code, boolean bSearchAltCode) throws NotesException {
		String Language = null;
		ViewEntry entry = null;
		Document docLanguage;
		View luView;
		try {
			if (code != null) {
				if(bSearchAltCode) {
					luView = db.getView("LANG_byAlternateCode");
				} else {
					luView = db.getView("LANG_byCode");
				}
				if (luView != null) {
					entry = luView.getEntryByKey(code, true);
					if(entry != null) {
						docLanguage = entry.getDocument();
						Language = docLanguage.getItemValueString("Language");
					}
				}
				if(entry != null) {
					entry.recycle();
				}
				luView.recycle();
			}
		} catch (Exception e) {
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			return null;
		}
		return Language;
	}

	/**
	 * Create document
	 * @return
	 */
	public Document createDocument() {
		Document doc = null;
		try {
			doc = db.createDocument();
		} catch (NotesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return doc;
	}
	
	/**
	 * 
	 * @param luViewName
	 * @param key
	 * @return
	 */
	public DocumentCollection getViewDocumentsByKey(String luViewName, String key) {
		DocumentCollection collection = null;
		Vector<String> luKeys = new Vector<String> ();
		try {
			View luView = db.getView(luViewName);
			luKeys.add(key);
			collection = luView.getAllDocumentsByKey(luKeys, true);			
		} catch (NotesException e) {
			Globals.printException(e);
		}
		return collection;
	}

	/**
	 * Get pin document
	 * @param id
	 * @return
	 */
	public Document getPinDoc(String key) {
		Document pinDoc = null;
		try {
			if (key != null) {
				View luView = getView("PIN_ById");
				pinDoc = luView.getDocumentByKey(key, true);
				luView.recycle();
			}
		} catch (NotesException e) {
			Globals.printException(e);
		}
		return pinDoc;
	}


	public Document getMeetingDoc(String key) {
		Document meetingDoc = null;
		try {
			if (key != null) {
				View luView = getView("MEETING_ById");
				meetingDoc = luView.getDocumentByKey(key, true);
				luView.recycle();
			}
		} catch (NotesException e) {
			Globals.printException(e);
		}
		return meetingDoc;
	}
}
