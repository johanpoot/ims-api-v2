package com.primaxis.ims.utils;

import java.util.Vector;

import org.json.simple.*;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewNavigator;

/**
 * Handles the IMS Data database
 * lets you get keywords or documents from the 
 * database
 * 
 * @author Baljit Karwal
 *
 */
public class ImsDataDb {
	//must push session into this STATIC variable from the NOTES AGENT so it's available to the class when needed
	private ImsHomeDb ImsHomeDb;
	private Database db;
	
	public ImsDataDb (Session session) throws NotesException {
		//use a keyword handler for the current DB to get the keyword for IMS database and IMS Server
		KeywordHandler kwHandler = new KeywordHandler(session.getCurrentDatabase());
		//Session session = Globals.getSession();
		String server = session.getCurrentDatabase().getServer();
		String remoteServer = kwHandler.getKeywordValueString("DB_FilePath_IMS_Server");
		if (!remoteServer.equals("-")) {
			server = remoteServer;
		}
		ImsHomeDb = new ImsHomeDb(session);
		KeywordHandler kwHandlerIMSDB = new KeywordHandler(ImsHomeDb.getDatabase());
		String dbPath = kwHandlerIMSDB.getKeywordValueString("DB_FilePath_BOOK");
		db = session.getDatabase(server, dbPath);
	}
	
	public String getDatabaseTitle ()
	{
		try {
			return db.getTitle();
		} catch (NotesException e) {
			return "";
		}
	}
	
	//get the database
	public Database getDatabase() {
		return db;
	}
	
	/**
	 * Get all bookings by date view
	 * @return - View with all bookings by date
	 */
	public View getLspBookingsByModifiedDateView ()
	{
		String luViewName;
		try {
			luViewName = "vwLspDocsByModifiedDateForImsApi";			
			View luView = db.getView(luViewName);
			return luView;			
		} catch (NotesException e) {
			Globals.printException(e);
			return null;
		}
	}
	
	/**
	 * Get all combined bookings view
	 * @return - View with all combined bookings
	 */
	public View getAllCombinedBookingsView ()
	{
		String luViewName;
		try {
			luViewName = "GRID_CBN_BOOK";			
			View luView = db.getView(luViewName);
			return luView;			
		} catch (NotesException e) {
			Globals.printException(e);
			return null;
		}
	}
	
	/**
	 * Get booking by DocKey
	 * @param documentKey
	 * @return
	 */
	public Document getBookingDocumentByKey (String documentKey)
	{
		String luViewName;
		String luAllDocByPmsID;
		Vector<String> luKeys = new Vector<String> ();
		try {
			luViewName = "vwAllByDocKey";		
			luAllDocByPmsID = "vwAllDocsByPMSID";
			View luView = db.getView(luViewName);
			View luVwByPMSID = db.getView(luAllDocByPmsID);
			luKeys.add(documentKey);
			Document booking = luView.getDocumentByKey(luKeys, true);
			if(booking == null){
				booking = luVwByPMSID.getDocumentByKey(luKeys,true);
			}
			return booking;			
		} catch (NotesException e) {
			Globals.printException(e);
			return null;
		}
	}
	
	public JsonArray getViewColumnValueByPositions(View objView, Integer posStart, Integer posEnd) throws NotesException {
		ViewNavigator nav = null;
		JsonArray objBookings  = new JsonArray();
		try {
			objView.setAutoUpdate(false);
			nav = objView.createViewNav();
			nav.setBufferMaxEntries(5000); //Max docs search 
			ViewEntry entry = nav.getCurrent();
			int pos = (entry.getColumnValues().size() - 1);
			nav.gotoPos(Integer.toString(posStart), '.');
			entry = nav.getCurrent();
			for (int i = 0; i <= (posEnd - posStart); i++) {
				objBookings.add(entry.getColumnValues().elementAt(pos).toString());
				ViewEntry tmpentry = nav.getNext(entry);
				entry.recycle();
				entry = tmpentry;
			}
			objView.setAutoUpdate(true);
		} catch (NotesException e) {
			Globals.printException(e);
		}
		return objBookings;
	}
	
	/**
	 * Get view for all bookings by LSP external id
	 * @return View
	 */
	public View getAllBookingsByExternalIdView() {
		String luViewName;
		try {
			luViewName = "vwBookingsLSPExtRefID";			
			View luView = db.getView(luViewName);
			return luView;
		} catch (NotesException e) {
			Globals.printException(e);
			return null;
		}
	}
	
	/**
	 * Get all bookings for given view name or return all bookings
	 * 
	 * @param searchBy
	 * @return
	 */
	public View getAllBookingsByView (String searchBy)
	{
		String luViewName;
		if (searchBy == null || searchBy.equals("")) {
			luViewName = "vwAllDocs";
		} else {
			luViewName = "vwAllDocsBy" + searchBy;
		}
		try {
			View luView = db.getView(luViewName);
			return luView;			
		} catch (NotesException e) {
			Globals.printCode("Unable to find view: " + luViewName + " for IMS API");
			Globals.printException(e);
			return null;
		}
	}
	
	/**
	 * Get future bookings for given view name or return all future bookings
	 * 
	 * @param searchBy
	 * @return
	 */
	public View getFutureBookingsByView (String searchBy)
	{
		String luViewName;
		if (searchBy == null || searchBy.equals("")) {
			luViewName = "vwFutureDocs";
		} else {
			luViewName = "vwFutureDocsBy" + searchBy;
		}
		try {
			View luView = db.getView(luViewName);
			return luView;			
		} catch (NotesException e) {
			Globals.printCode("Unable to find view: " + luViewName + " for IMS API");
			Globals.printException(e);
			return null;
		}
	}

	/**
	 * Get View by name
	 * @param viewName
	 * @return
	 */
	public View getView(String viewName) {
		try {
			View luView = db.getView(viewName);
			return luView;			
		} catch (NotesException e) {
			Globals.printCode("Unable to find view: " + viewName + " for IMS API");
			Globals.printException(e);
			return null;
		}
	}
	
}

