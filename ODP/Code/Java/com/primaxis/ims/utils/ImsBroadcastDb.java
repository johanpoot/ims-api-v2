package com.primaxis.ims.utils;

import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.View;
import lotus.domino.Session;


/**
 * Handles the IMS Broadcast database
 * 
 * @author Baljit Karwal
 *
 */
public class ImsBroadcastDb {
	private Database db;
	private KeywordHandler kwHandler;
	private ImsHomeDb ImsHomeDb;
	
	public ImsBroadcastDb (Session session) throws NotesException
	{
		//use a keyword handler for the current DB to get the keyword for current database
		kwHandler = new KeywordHandler(session.getCurrentDatabase());
		String server = session.getCurrentDatabase().getServer();
		String remoteServer = kwHandler.getKeywordValueString("DB_FilePath_IMS_Server");
		if (!remoteServer.equals("-")) {
			server = remoteServer;
		}
		ImsHomeDb = new ImsHomeDb(session);
		KeywordHandler kwHandlerIMSDB = new KeywordHandler(ImsHomeDb.getDatabase());
		String dbPath = kwHandlerIMSDB.getKeywordValueString("DB_FilePath_Broadcast");
		db = session.getDatabase(server, dbPath);
	}
	
	public String getDatabaseTitle ()
	{
		try {
			return db.getTitle();
		} catch (NotesException e) {
			return "";
		}
	}
	
	//get the database
	public Database getDatabase() {
		return db;
	}
	
	/**
	 * Get View
	 * @param luViewName - View name
	 * @return View
	 */
	public View getView (String luViewName) {
		try {
			View luView = db.getView(luViewName);
			return luView;			
		} catch (NotesException e) {
			Globals.printException(e);
			return null;
		}
	}
		
	/**
	 * Recycle Objects
	 */
	public void recycle () {
		kwHandler.recycle();
		if (db != null) {
			try {
				db.recycle();
			} catch (NotesException e) {
				//don't need to do anything
			}
		}
	}
	
	/**
	 * 
	 * @param searchBy
	 * @return
	 */
	public View getAllNotificationsByView(String searchBy) {
		String luViewName;
		if (searchBy == null || searchBy.equals("")) {
			luViewName = "vwAllNotifications";
		} else {
			luViewName = "vwAllNotificationsBy" + searchBy;
		}
		try {
			View luView = db.getView(luViewName);
			return luView;			
		} catch (NotesException e) {
			Globals.printCode("Unable to find view: " + luViewName + " for IMS API");
			Globals.printException(e);
			return null;
		}
	}
}
