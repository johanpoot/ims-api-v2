package com.primaxis.ims.utils;
import java.util.Vector;
import org.json.simple.*;

import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.Log;

import lotus.domino.Base;
import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;
import lotus.domino.Agent;
import lotus.domino.ViewEntry;

/**
 * Keyword Handler
 * This class handles getting the keywords from a database,
 * by default it uses the KWD_byName view but this can be over written if needed
 * 
 * @author Baljit Karwal
 * @version 1.0
 */
public class KeywordHandler {
	private Database db;
	private View vwKeywords;
	private View vwMappingIMS;
	private View vwMappingAgency;

	public KeywordHandler (Session session) throws NotesException
	{
		this.db = session.getCurrentDatabase();
		vwKeywords = db.getView("KWD_byName");
	}

	public KeywordHandler (Database db) throws NotesException
	{
		this.db = db;
		vwKeywords = db.getView("KWD_byName");
	}

	/**
	 * getKeyword from current DB
	 * returns the keyword specified
	 * @param kwdName
	 * @return
	 */
	public Document getKeyword (String kwdName){
		try {
			Document keyDoc = getKWView().getDocumentByKey(kwdName);
			return keyDoc;
		} catch (NotesException e) {
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			return null;
		}
	}

	/**
	 * Get multiple lines text keyword values as array
	 * 
	 * @param kwdName
	 * @return
	 */
	public String[] getKeywordValueArray (String kwdName)
	{
		Document keyDoc;
		Vector kwVal;
		String [] val;
		keyDoc = getKeyword (kwdName);
		if (!(keyDoc == null)){
			try {
				kwVal = keyDoc.getItemValue("KWMultiText");
				keyDoc.recycle();
				val = new String [kwVal.size()];
				kwVal.toArray(val);
				return val;
			} catch (NotesException e) {
				Globals.printException(e);
				Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
				return null;
			}
		}else {
			return null;
		}
	}
	
	/**
	 * Return the string value of the keyword specified
	 * @param kwdName
	 * @return
	 */
	public String getKeywordValueString (String kwdName){
		Document keyDoc;
		String val;
		keyDoc = getKeyword (kwdName);
		if (keyDoc != null) {
			try {
				val = keyDoc.getItemValueString("KWText");
				keyDoc.recycle();
				return val;
			} catch (NotesException e) {
				Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
				Globals.printException(e);
				return "";
			}
		} else {
			return "";
		}
	}

	/**
	 * Get multiple lines text keyword values as vector
	 * 
	 * @param kwdName
	 * @return
	 */
	public Vector getKeywordList (String kwdName)
	{
		Document keyDoc;
		Vector kwValues = new Vector();  
		keyDoc = getKeyword (kwdName);
		if (!(keyDoc == null)) {
			try {
				kwValues = keyDoc.getItemValue("KWMultiText");
		        keyDoc.recycle();
			} catch (NotesException e) {
				Globals.printException(e);
				Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
			}				
		}
		return kwValues;
	}

	private View getKWView (){
		try {
		if (vwKeywords == null) {vwKeywords = db.getView("KWD_byName");}
		} catch (Exception e){
			Globals.printException(e);
			//nothing it will drop out
		}
		return vwKeywords;		 
	}

	/**
	 * Validate Clinic Code and get clinic details
	 * @param key
	 * @return
	 * @throws NotesException
	 */
	public JsonObject getClinicDetails(String key) throws NotesException
	{
		JsonObject clinicDetails = new JsonObject();
		String alternateCode = null;
		try {
			View luView = db.getView("clinic-codes");
			Document cDoc = luView.getDocumentByKey(key, true);
			if (cDoc == null) {
				luView = db.getView("luCLN_AltCode");
				cDoc = luView.getDocumentByKey(key, true);
				if (cDoc != null) {
					Vector altCodes = cDoc.getItemValue("CLN_AltCode");
					if (altCodes != null) {
						String [] altCodesList = new String [altCodes.size()];
						altCodes.toArray(altCodesList);
						if (altCodesList.length > 0) {
							for (int i=0; i < altCodesList.length; i++) {
								String existingAltCode = altCodesList[i];
								if (key.equals(existingAltCode)) {
									alternateCode = existingAltCode;
								}
							}
						}
					}
				}
			}
			if (cDoc != null && (cDoc.getItemValueString("CLN_Code").equals(key) || (alternateCode != null && alternateCode.equals(key)))) {
				clinicDetails.put("clinicCode", cDoc.getItemValueString("CLN_Code"));
				if (cDoc.getItemValueString("CLN_Site_Code") != null && !cDoc.getItemValueString("CLN_Site_Code").equals("")) {
					clinicDetails.put("siteCode", cDoc.getItemValueString("CLN_Site_Code"));
				}
				if (cDoc.getItemValueString("CLN_Location_Name") != null && !cDoc.getItemValueString("CLN_Location_Name").equals("")) {
					clinicDetails.put("locationName", cDoc.getItemValueString("CLN_Location_Name"));
				}
				if (cDoc.getItemValueString("CLN_Clinic_Name") != null && !cDoc.getItemValueString("CLN_Clinic_Name").equals("")) {
					clinicDetails.put("clinicName", cDoc.getItemValueString("CLN_Clinic_Name"));
				}
				if (cDoc.getItemValueString("CLN_Clinic_Address") != null && !cDoc.getItemValueString("CLN_Clinic_Address").equals("")) {
					clinicDetails.put("clinicAddress", cDoc.getItemValueString("CLN_Clinic_Address"));
				}
				if (cDoc.getItemValueString("CLN_Clinic_Suburb") != null && !cDoc.getItemValueString("CLN_Clinic_Suburb").equals("")) {
					clinicDetails.put("clinicSuburb", cDoc.getItemValueString("CLN_Clinic_Suburb"));
				}
				if (cDoc.getItemValueString("CLN_Clinic_PostCode") != null && !cDoc.getItemValueString("CLN_Clinic_PostCode").equals("")) {
					clinicDetails.put("clinicPostCode", cDoc.getItemValueString("CLN_Clinic_PostCode"));
				}
				if (cDoc.getItemValueString("CLN_Clinic_State") != null && !cDoc.getItemValueString("CLN_Clinic_State").equals("")) {
					clinicDetails.put("clinicState", cDoc.getItemValueString("CLN_Clinic_State"));
				}
				if (cDoc.getItemValueString("CLN_Clinic_ContactName") != null && !cDoc.getItemValueString("CLN_Clinic_ContactName").equals("")) {
					clinicDetails.put("clinicContactName", cDoc.getItemValueString("CLN_Clinic_ContactName"));
				}
				
				if (cDoc.getItemValueString("CLN_Clinic_ContactPhone") != null && !cDoc.getItemValueString("CLN_Clinic_ContactPhone").equals("")) {
					clinicDetails.put("clinicContactPhone", cDoc.getItemValueString("CLN_Clinic_ContactPhone"));
				}
				
				if (cDoc.getItemValueString("CLN_Clinic_ContactEmail") != null && !cDoc.getItemValueString("CLN_Clinic_ContactEmail").equals("")) {
					clinicDetails.put("clinicContactEmail", cDoc.getItemValueString("CLN_Clinic_ContactEmail"));
				}
				
				if (cDoc.getItemValueString("CLN_Clinic_Video") != null && !cDoc.getItemValueString("CLN_Clinic_Video").equals("")) {
					clinicDetails.put("clinicVideoURL", cDoc.getItemValueString("CLN_Clinic_Video"));
				}
			}

		} catch (Exception e) {
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}
		return clinicDetails;
	}
	
	/**
	 * Get clinic code from clinic details
	 * @param site
	 * @param siteName
	 * @param location
	 * @param clinic
	 * @return
	 * @throws NotesException
	 */
	public String getClinicCode(String siteName, String location, String clinic) throws NotesException
	{
		String clinicCode = null;
		try {
			View luView = db.getView("lkpIMSClinicCodes");
			Vector<String> luKeys = new Vector<String> ();
			luKeys.add(siteName+"~"+location+"~"+clinic);
			Document cDoc = luView.getDocumentByKey(luKeys, true);
			if (cDoc != null && !cDoc.getItemValueString("CLN_Code").equals("")) {
				clinicCode = cDoc.getItemValueString("CLN_Code");
			}

		} catch (Exception e) {
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}
		return clinicCode;
	}
	
	/**
	 * Return pin doc based on client id / pin code sent, this will be a clinic or requ record that contains the pin code

	 * @return
	 */
	public Document getPINDoc(String clientid,String PIN) throws NotesException{
		Document pindoc = null;
		Globals.printCode("getPINDoc");
		try {
			View luView = db.getView("luKeyByClientID"); //Now look up authcode, could be on site or LOC
			ViewEntry ve = luView.getEntryByKey(clientid, true);
			if (ve!=null){
				Globals.printCode("Client id found: " + clientid);
				Vector v = ve.getColumnValues();
				String key = v.elementAt(1).toString();
				luView = db.getView("luByCode"); //Finally lookup pin attached to the above site/loc
				pindoc = luView.getDocumentByKey(key+PIN, true);
				if (pindoc!=null){
					Globals.printCode("pin id verified: " + clientid);
				}
				else
				{
					Globals.printCode("NOT FOUND pin id: " + clientid);
				}
			}
			else
			{
				Globals.printCode("NOT FOUND Client id: " + clientid);
			}
		} catch (Exception e) {
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}	
		return pindoc;
	}
	
	/**
	 * Returns array of question dockeys which is located on site, loc, clinic or requ where the clientid is defined
	 * @return
	 */
	public String[] getIntakeQuestionKeys(String clientid) throws NotesException{
		String[] quest = null;
		Vector <String> vec = null;
		try {
			View luView = db.getView("luKeyByClientID"); //Now look up authcode, could be on site or LOC
			Document qdoc = luView.getDocumentByKey(clientid, true);
			
			if (qdoc!=null){
				vec  = qdoc.getItemValue("QuestionDocKey");
				quest = new String [vec.size()];
				vec.toArray(quest);
			}
			else
			{
				quest = new String[1];
				quest[0]="No Questions";
			}
		} catch (Exception e) {
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}	
		return quest;
	}
	/**
	 * Validate Cost Centre against clinic code
	 * @param clinicCode
	 * @param costCentre
	 * @return
	 */
	public boolean isValidCostCentre(String siteName, String location, String clinic, String costCentre) {
		boolean isValidClinicCode = false;
		try {
			View luView = db.getView("lkpIMSClinicCodes");
			Vector<String> luKeys = new Vector<String> ();
			if (siteName != null && location != null && clinic != null) {
				luKeys.add(siteName+"~"+location+"~"+clinic);
				Document cDoc = luView.getDocumentByKey(luKeys, true);
				Vector costCentres = cDoc.getItemValue("CLN_CostCentre");
				if (costCentres != null) {
					String [] costCentresList = new String [costCentres.size()];
					costCentres.toArray(costCentresList);
					if (costCentresList.length > 0) {
						for (int i=0; i < costCentresList.length; i++) {
							String existingCostCentre = costCentresList[i];
							if (costCentre.equals(existingCostCentre)) {
								isValidClinicCode = true;
							}
						}
					}
				}
				cDoc.recycle();
			}
		} catch (Exception e) {
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}
		return isValidClinicCode;
	}

	/**
	 * Fetch the site name from the site keyword (if there is one)
	 * @param site
	 * @return
	 */
	public String getSiteName (String PREFIX, String site)
	{
		String [] siteList;
		String [] vals;
		siteList = getKeywordValueArray (PREFIX + "_Site");
		for (int i=0; i<siteList.length; i++)
		{
			if (siteList[i].contains("|"))
			{
				vals = siteList[i].split("\\|");
				if (vals[1].trim().equalsIgnoreCase(site))
				{
					return (vals[0].trim());
				}	
			}
		}
		
		//if we get here we didn't find a match - or there were no aliases so we just return the incomming val
		return (site);		
	}

	public void updateKeywordValue (String kwdName, String value){
		try {
			Document keyDoc;
			keyDoc = getKeyword (kwdName);
			keyDoc.replaceItemValue("KWText", value);
			keyDoc.save(true);
			keyDoc.recycle();
			
		} catch (NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
			Globals.printException(e);
		}
	}

	/**
	 * Retrieve state from states
	 * 
	 * @param State
	 * @return
	 * @throws NotesException
	 */
	public String getStateFromStates(String State) throws NotesException{		
		String[] fieldList = getKeywordValueArray("states");
		try {
			if (State != null) {
				for (int i=0; i<fieldList.length; i++)
				{
					String state = fieldList[i];
					if (state.equalsIgnoreCase(State)) {
						return state;
					}
				}
			}
		} catch (Exception e) {
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			return null;
		}
		return null;
	}
	
	public String getEthnicityFromCode(String Code) throws NotesException{		
		String[] fieldList = getKeywordValueArray("EthnicityList");
		String Ethnicity;
		String EthnicityCode;
		try {
			if (fieldList.length > 0) {
				for (int i=0; i<fieldList.length; i++)
				{
					String[] vals = fieldList[i].split("\\|");
					Ethnicity = vals[0].trim();
					EthnicityCode = vals[1].trim();
					if (EthnicityCode.equalsIgnoreCase(Code)) {
						return Ethnicity;
					}
				}
			}
		} catch (Exception e) {
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			return null;
		}
		return null;
	}
	
	/**
	 * 
	 * @param Level
	 */
	public boolean validateAccreditationLevel(String Level) {
		if (Level != null) {
			try {
				 String[] levels = getKeywordValueArray("SkillLevel");
				   if (levels != null && levels.length > 0) {
						for (int a=0; a<levels.length;a++ ) {
							String levelValue = levels[a];
							if (Level.equalsIgnoreCase(levelValue)) {
								return true;
							}
						}
					}
			} catch (Exception e) {
				Globals.printException(e);
				Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			}
		}
		return false;
	}
	
	/**
	 * Validate the non-completion reason against the configured list
	 * @param Reason
	 * @param isMobileApp 
	 * @return
	 */
	public boolean validateNonCompletionReason(String Reason, boolean isMobileApp) {
		if (Reason != null) {
			try {
				Vector reasons = getKeywordList(Constants.KW_IMS_LSP_NC_REASONS);
				if (isMobileApp) {
					reasons = getKeywordList(Constants.KW_IMS_INTERPRETER_NC_REASONS);
				}
				if (reasons != null && reasons.size() > 0) {
					for (int a=0; a<reasons.size();a++ ) {
						String reasonValue = reasons.elementAt(a).toString();
						if (isMobileApp) {
							String[] arrReason = reasonValue.split("\\|");
							if (Globals.indexInBound(arrReason, 0) && Globals.indexInBound(arrReason, 1)) {
								reasonValue = arrReason[1].trim();
							}
							if (!Globals.indexInBound(arrReason, 1)) { 
								reasonValue = arrReason[0].trim();
							}
						}
						if (Reason.equalsIgnoreCase(reasonValue)) {
							return true;
						}
					}
				}
			} catch (Exception e) {
				Globals.printException(e);
				Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			}
		}
		return false;
	}
	
	/**
	 * Get language service provider (agency) name by client id
	 * @param ClientId
	 * @return
	 */
	public String getLSPByClientId(String ClientId) {
		String languageServiceProvider = null;
		try {
			View vwLSP = db.getView("API_LSP");
			Document keyDoc = vwLSP.getDocumentByKey(ClientId);
			if (keyDoc != null) {
				languageServiceProvider = keyDoc.getItemValueString("AGN_Name");
				keyDoc.recycle();
			}
			vwLSP.recycle();
			return languageServiceProvider;
		} catch (NotesException e) {
			Globals.printException(e);
		}
		return null;
	}
	
	/**
	 * Get language service provider (agency) name by consumer name
	 * @param consumerName
	 * @return
	 */
	public String getLSPByConsumerName(String consumerName) {
		String languageServiceProvider = null;
		try {
			View vwLSP = db.getView("API_LSP");
			Document keyDoc = vwLSP.getDocumentByKey(consumerName);
			if (keyDoc != null) {
				languageServiceProvider = keyDoc.getItemValueString("AGN_Name");
				keyDoc.recycle();
			}
			vwLSP.recycle();
			return languageServiceProvider;
		} catch (NotesException e) {
			Globals.printException(e);
		}
		return null;
	}
	
	public boolean updateLSPToken(String ClientId, String token) {
		try {
			View vwLSP = db.getView("API_LSP");
			Document keyDoc = vwLSP.getDocumentByKey(ClientId, true);
			keyDoc.replaceItemValue("AGN_API_token", token);
			keyDoc.save(true);
			keyDoc.recycle();
		} catch (NotesException e) {
			Globals.printException(e);
		}
		return false;
	}
	
	/**
	 * Get API Token for agency/provider
	 * @return
	 */
	public String getAgencyToken(String agency){
		Document keyDoc;
		View luView;
		try {
			luView = db.getView("AGN_NameList");
			if (agency != null && !agency.equals("")) {
				keyDoc = luView.getDocumentByKey (agency, true);
				if (keyDoc != null){
					String val = keyDoc.getItemValueString("AGN_API_token");
					keyDoc.recycle();
					return val;
				}
			}
			luView.recycle();
		} catch (NotesException e) {
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}
		return null;
	}
	
	/**
	 * Get DocKey for agency/provider
	 * @return
	 */
	public String getAgencyDocKey (String agency){
		Document keyDoc;
		View luView;
		String val = "";
			try {
				luView = db.getView("AGN_NameList");
				if (!agency.equals("")) {
					keyDoc = luView.getDocumentByKey (agency, true);
					if (keyDoc != null){
						val = keyDoc.getItemValueString("DocKey");
						keyDoc.recycle();
					}
				}
				luView.recycle();
				return val;
			} catch (NotesException e) {
				Globals.printException(e);
				Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
				return "";
			}
	}

	
	/**
	 * Get API Token for agency/provider
	 * @return
	 */
	public Boolean isAgencyAPIEnabled(String agency){
		Document keyDoc;
		View luView;
		try {
			luView = db.getView("AGN_NameList");
			if (agency != null && !agency.equals("")) {
				keyDoc = luView.getDocumentByKey (agency, true);
				if (keyDoc != null){
					String val = keyDoc.getItemValueString("AGN_Enable_API");
					keyDoc.recycle();
					return val.equalsIgnoreCase("yes");
				}
			}
			luView.recycle();
		} catch (NotesException e) {
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}
		return false;
	}
	/**
	 * Clean up any Notes objects we have
	 */
	public void recycle ()
	{
		recycleObject (vwKeywords);
		recycleObject (vwMappingIMS);
		recycleObject (vwMappingAgency);
		recycleObject (db);
	}
	
	/**
	 * Check if the object is not null and call it's recycle if you can
	 * @param notesObject
	 */
	private void recycleObject (Base notesObject) {
		if (notesObject != null)
		{
			try {
				notesObject.recycle();
				notesObject = null;
			} catch (NotesException e) {
				//don't need to do anything
			}
		}
	}
	
	/**
	 * Get user details by DocKey
	 * Add more details as required
	 * 
	 * @param key
	 * @return
	 * @throws NotesException
	 */
	public JsonObject getUserDetails(String key) throws NotesException
	{
		JsonObject userDetails = new JsonObject();
		try {
			View luView = db.getView("USER_ByDocKey");
			Document uDoc = luView.getDocumentByKey(key, true);
			if (uDoc != null) {
				if (uDoc.getItemValueString("User_FirstName") != null && !uDoc.getItemValueString("User_FirstName").equals("")) {
					userDetails.put("firstName", uDoc.getItemValueString("User_FirstName"));
				}
				if (uDoc.getItemValueString("User_LastName") != null && !uDoc.getItemValueString("User_LastName").equals("")) {
					userDetails.put("lastName", uDoc.getItemValueString("User_LastName"));
				}
				if (uDoc.getItemValueString("User_FullName") != null && !uDoc.getItemValueString("User_FullName").equals("")) {
					userDetails.put("fullName", uDoc.getItemValueString("User_FullName"));
				}
				if (uDoc.getItemValueString("User_Email") != null && !uDoc.getItemValueString("User_Email").equals("")) {
					userDetails.put("email", uDoc.getItemValueString("User_Email"));
				}
				if (uDoc.getItemValueString("User_Phone") != null && !uDoc.getItemValueString("User_Phone").equals("")) {
					userDetails.put("mobilePhone", uDoc.getItemValueString("User_Phone"));//Send USER_Phone for Metro North instead of Mobile as ONCALL is using Mobile Type to fetch User phone value.
				}
				if (uDoc.getItemValueString("User_Mobile") != null && !uDoc.getItemValueString("User_Mobile").equals("")) {
					userDetails.put("workPhone", uDoc.getItemValueString("User_Mobile"));
				}
			}

		} catch (Exception e) {
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}
		return userDetails;
	}
	
	/**
	 * Check if consumer is allowed to access all data
	 * 
	 * @param consumerName
	 * @return
	 */
	public boolean isConsumerAllowedToAccessAll(String consumerName) {
		if (consumerName != null) {
			try {
				Vector allowedToAccessAll = getKeywordList(Constants.KW_ALLOWED_CONSUMER_ACCESS_ALL);
				if (allowedToAccessAll != null && allowedToAccessAll.size() > 0) {
					for (int a=0;a<allowedToAccessAll.size();a++) {
						String allowedLsp = allowedToAccessAll.elementAt(a).toString();
						if (consumerName.equalsIgnoreCase(allowedLsp)) {
							return true;
						}
					}
				}
			} catch (Exception e) {
				Globals.printException(e);
				Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			}
		}
		return false;
	}
	
	/**
	 * Check if consumer is allowed to create booking ignoring validations
	 * 
	 * @param consumerName
	 * @return
	 */
	public boolean isConsumerAllowedToCreateIgnoreValid(String consumerName) {
		if (consumerName != null) {
			try {
				Vector allowedToAccessAll = getKeywordList(Constants.KW_ALLOWED_CONSUMER_CREATE_IGNORE_VALID);
				if (allowedToAccessAll != null && allowedToAccessAll.size() > 0) {
					for (int a=0;a<allowedToAccessAll.size();a++) {
						String allowedLsp = allowedToAccessAll.elementAt(a).toString();
						if (consumerName.equalsIgnoreCase(allowedLsp)) {
							return true;
						}
					}
				}
			} catch (Exception e) {
				Globals.printException(e);
				Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			}
		}
		return false;
	}

	/**
	 * Get Timezone offset and adjust date time
	 * @return
	 */
	public DateTime dateOffset(DateTime dt) {
		try {
			dt.setNow();
			String offset = getKeywordValueString("TimezoneOffset");		
			if (offset == "" || offset == null) {
				return dt;
			} else {
				int offsetTime = new Integer(offset);
				dt.adjustHour(offsetTime, true);
				return dt;
			}
		}
		catch(NotesException e) {
			return dt;
		}
	}

	/**
	 * Date format to display
	 * @return
	 */
	public String getGlobalDateNotesFromFormat() {
		if (getKeywordValueString("Global_DateNotes_From") != null) {
			return getKeywordValueString("Global_DateNotes_From");
		}
		return  "dd/MM/yyyy";		
	}

	/**
	 * Time format to display
	 * @return
	 */
	public String getGlobalTimeFormat() {
		
		if (getKeywordValueArray("Global_TimeFormat") != null) {
			if (getKeywordValueArray("Global_TimeFormat")[0].contains("A")) {
				return getKeywordValueArray("Global_TimeFormat")[0].toLowerCase();
			} else {
				return getKeywordValueArray("Global_TimeFormat")[0];
			}
		}
		return  "hh:mm a";		
	}

	/**
	 * Date format to display on log
	 * @return
	 */
	public String getLogDateTimeFormat() {
		if (getKeywordValueString("LogDateTimeFormat") != null) {
			return getKeywordValueString("LogDateTimeFormat");
		}
		return  "dd/MM/yyyy hh:mm a";		
	}

	public Boolean isSkipCancelRequested() {
		if (getKeywordValueString("BOOK_ByPass_CancelReq") != null) {
			return getKeywordValueString("BOOK_ByPass_CancelReq").equalsIgnoreCase("1");
		}
		return false;	
	}
}