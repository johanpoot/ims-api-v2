package com.primaxis.ims.utils;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.json.simple.*;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.EmbeddedObject;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;
import lotus.domino.ViewColumn;
import lotus.domino.ViewEntry;
import lotus.domino.ViewEntryCollection;
import lotus.domino.ViewNavigator;

/**
 * Global static objects/functions we need access too
 * @author Baljit Karwal
 * @since Jan 2019
 */
public class Globals {
	
	public static int maxStringLength = 255;

    public static void MaxMemory() {
        Runtime rt = Runtime.getRuntime();
        long totalMem = rt.totalMemory();
        long maxMem = rt.maxMemory();
        long freeMem = rt.freeMemory();
        double megs = 1048576.0;

        System.out.println ("Total Memory: " + totalMem + " (" + (totalMem/megs) + " MiB)");
        System.out.println ("Max Memory:   " + maxMem + " (" + (maxMem/megs) + " MiB)");
        System.out.println ("Free Memory:  " + freeMem + " (" + (freeMem/megs) + " MiB)");
    }
	/**
	 * return the formatted date
	 * @param date - Notes DateTime
	 * @param toformat - To format string
	 * @return
	 */
	public static String formatDate (DateTime dateTime, String toformat) {
		try {
			Date javaDate = dateTime.toJavaDate();
			Format dateFormatter = new SimpleDateFormat(toformat);
			return dateFormatter.format(javaDate);
		} catch (NotesException e) {
			Log.writeLine ("CLASS: Globals *** " + e.getMessage());		
			return null;
		}
	}
	
	/**
	 * Convert date string to Java Date
	 * @param date - Date String
	 * @param format - from date format
	 * @return
	 */
	public static Date stringToJavaDate(String date, String format) throws ParseException {
		try {
			Date javaDate = new SimpleDateFormat(format).parse(date);
			return javaDate;
		} catch (ParseException e) {
			Log.writeLine ("CLASS: Globals *** " + e.getMessage());		
			return null;
		}
	}
	
	/**
	 * 
	 * @param date Java Date String
	 * @param fromFormat From date format string
	 * @param toFormat To date format string
	 * @return
	 */
	public static String getJavaFormattedDate(String date, String fromFormat, String toFormat) throws ParseException {
		try {
			Date javaDate = new SimpleDateFormat(fromFormat).parse(date);
			Format dateFormatter = new SimpleDateFormat(toFormat);
			String formattedDate = dateFormatter.format(javaDate);
			return formattedDate;
			
		} catch (ParseException e) {
			Globals.printException(e);
			return null;
		}
	}

	public static String getFormattedNotesDateTimeCurrent (DateTime dtNow, String toFormat) {
		try {
			Date javaDate = dtNow.toJavaDate();
			Format dateFormatter = new SimpleDateFormat(toFormat);
			String formattedDate = dateFormatter.format(javaDate);
			return formattedDate;
		} catch (NotesException e) {
			Globals.printException(e);
			return null;
		}
	}
	
	/**
	 * Retrieve previous or next date
	 * 
	 * @param incrementDate - True to increase and False to decrease date
	 * @param dateFormat - Current date format for given date string
	 * @param date - Date String from which previous or next date to be created
	 * @return
	 */
	public static String getNextOrPrevDate(boolean incrementDate, String dateFormat, String date) throws ParseException {
		try {
			SimpleDateFormat format = new SimpleDateFormat(dateFormat);
			Date dt = null;
			dt = format.parse(date);
			org.joda.time.DateTime givenDate = new org.joda.time.DateTime(dt);
			org.joda.time.DateTime adjustedDate;
			if (incrementDate) {
				adjustedDate = givenDate.plusDays(1);
			} else {
				adjustedDate = givenDate.minusDays(1);
			}
			DecimalFormat df = new DecimalFormat("00"); //Format date and month to have 2 digits
			String adjustedDateString = adjustedDate.getYear() + "" + df.format(adjustedDate.getMonthOfYear()) + "" + df.format(adjustedDate.getDayOfMonth());
			return adjustedDateString;
		} catch (ParseException e) {
			Globals.printException(e);
			return null;
		}
	}

	/**
	 * Retrieve previous or next year's date
	 * 
	 * @param incrementDate - True to increase and False to decrease date
	 * @param dateFormat - Current date format for given date string
	 * @param date - Date String from which previous or next year to be created
	 * @return
	 */
	public static String getNextOrPrevYearDate(boolean incrementYear, String dateFormat, String date) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		Date dt = null;
		try {
			dt = format.parse(date);
			org.joda.time.DateTime givenDate = new org.joda.time.DateTime(dt);
			org.joda.time.DateTime adjustedYear;
			if (incrementYear) {
				adjustedYear = givenDate.plusYears(1);
			} else {
				adjustedYear = givenDate.minusYears(1);
			}
			DecimalFormat df = new DecimalFormat("00"); //Format date and month to have 2 digits
			String adjustedDate = adjustedYear.getYear() + "" + df.format(adjustedYear.getMonthOfYear()) + "" + df.format(adjustedYear.getDayOfMonth());
			return adjustedDate;
			
		} catch (ParseException e) {
			Globals.printException(e);
			return null;
		}
	}
	
	
	/**
	 *
	 * @param date - format should be 01/29/2020 09:18:00 AM ZE10
	 * @return
	 * @throws NotesException
	 */
	public static DateTime getDateTimeFromString(String date, Session session) throws NotesException {
		try {
			if (date != null) {
				DateTime formattedDate = session.createDateTime(date);
				return formattedDate;
			}
		} catch (NotesException e) {
			Globals.printException(e);
			return null;
		}
		return null;
	}
	
	public static String formatPhoneforOncall (String phone)
	{
		return phone.replaceAll("\\s","");
	}

	/**
	 * Return the formatted time	
	 * @param time
	 * @param formatStr
	 * @return
	 */
	public static String formatTime (DateTime time, String formatStr)
	{
		Format dateFormatter = new SimpleDateFormat (formatStr);
		try {
			return dateFormatter.format(time.toJavaDate());
		} catch (NotesException e) {
			return "";
		}
	}

	/**
	 * Return the required gender string
	 * @param sGender
	 * @return
	 */
	public static String GenderFormat (String sGender){
		if (sGender.equalsIgnoreCase("F")) {return "F";}
		if (sGender.equalsIgnoreCase("M")) {return "M";}
		if (sGender.equalsIgnoreCase("I")) {return "I";}
		if (sGender.equalsIgnoreCase("U")) {return "U";}
		return "U";
	}
	
	/**
	 * 
	 * @param dateTime
	 * @param duration
	 * @return
	 */
	public static DateTime getEndDateTime (DateTime dateAndTime, int duration)
	{
		try {
			dateAndTime.adjustMinute(duration);
		} catch (NotesException e) {
			Globals.printException(e);
		}
		return dateAndTime;
	}
	
	/**
	 * Get end time from DateTime and duration
	 * 
	 * @param dateTime
	 * @param duration
	 * @return
	 */
	public static String getEndTime (DateTime dateTime, int duration, Session session)
	{
		String endTime;
		try {
			dateTime.adjustMinute(duration);
			ImsHomeDb ImsHomeDb = new ImsHomeDb(session);
			KeywordHandler kwHandlerIMSDB = new KeywordHandler(ImsHomeDb.getDatabase());
			endTime = formatTime(dateTime, kwHandlerIMSDB.getGlobalTimeFormat());
		} catch (NotesException e) {
			return "";
		}
		return endTime;
	}
	
	public static String getMonthString (int month){
		String monthStr;
		
		switch (month) {
		case 1:  monthStr = "January";       break;
		case 2:  monthStr = "February";      break;
		case 3:  monthStr = "March";         break;
		case 4:  monthStr = "April";         break;
		case 5:  monthStr = "May";           break;
		case 6:  monthStr = "June";          break;
		case 7:  monthStr = "July";          break;
		case 8:  monthStr = "August";        break;
		case 9:  monthStr = "September";     break;
		case 10: monthStr = "October";       break;
		case 11: monthStr = "November";      break;
		case 12: monthStr = "December";      break;
		default: monthStr = "Invalid month"; break;
		}
		return monthStr;
	}
	
	/**
	 * Log to the BOOK_LOG field
	 * @param doc
	 * @param logString
	 * @param appendUserName
	 */
	public static void LogActionToDocument (DateTime nowDateTime, Document doc, String logString, boolean appendUserName, Session session)
	{
		try {
			String pre =  doc.getItemValueString("form");
			Vector<String> logValue = doc.getItemValue(pre+"_Log");
			//get the date time formatted as we want it
			ImsHomeDb ImsHomeDb = new ImsHomeDb(session);
			KeywordHandler kwHandlerIMSDB = new KeywordHandler(ImsHomeDb.getDatabase());
			String stringVal = formatDate(nowDateTime, kwHandlerIMSDB.getLogDateTimeFormat());
			//add the logString
			stringVal += " - " + logString;
			//if we need to add the user name add it
			if (appendUserName)
			{
				stringVal += " - " + session.getCommonUserName();
			}
			//add it to the log
			logValue.addElement(stringVal);
			//replace the field with the new value
			doc.replaceItemValue(pre+"_Log", logValue);
		}catch (Exception e)
		{
			Log.writeLine("Error updating Log on booking document: " + logString + "\n" + e.getMessage());
			Globals.printException(e);
		}
	}

	/**
	 * Ignore case
	 * @param jobj
	 * @param key
	 * @return
	 */
	public static String getIgnoreCase(JsonObject jobj, String key) {
	    Iterator<String> iter = jobj.keySet().iterator();
	    while (iter.hasNext()) {
	        String key1 = iter.next();
	        if (jobj.get(key1) != null && key1.equalsIgnoreCase(key)) {
	        	return validateString(jobj.get(key1));
	        }
	    }
	    return null;

	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isValidLength(String value) {
		if (value.length() <= maxStringLength) {
			return true;
		}
		return false;
	}
	
	/**
	 * Validate String
	 * @param name
	 * @return
	 */
	public boolean validateName (String name){
		if (!name.equals("")) {
	        String regx = "^[\\p{L}\\s.�\\-,]+$";
	        Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
	        Matcher matcher = pattern.matcher(name);
	        return matcher.matches();
		}
		return false;
    }
	
	/**
	 * Validate Email
	 * @param email
	 * @return
	 */
	public static boolean isValidEmail (String email){
		if (!email.equals("")) {
	        String regx = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
	        Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
	        Matcher matcher = pattern.matcher(email);
	        return matcher.matches();
		}
		return false;
    }
	
	/**
	 * Validate URL
	 * @param stringURL
	 * @return
	 * @throws MalformedURLException 
	 * @throws URISyntaxException 
	 */
	public static boolean isValidURL (String stringURL) throws MalformedURLException, URISyntaxException{
		try {
			URL url = new URL(trimString(stringURL));
			url.toURI();
			{
				return true;
			}
		} catch (MalformedURLException e) {
			return false;
		} catch ( URISyntaxException u) {
			return false;
		}
		
    }
	
	/**
	 * Trim and convert object to string
	 * @param object
	 * @return
	 */
	public static String trimString(Object object) {
		String value = null;
		if(object != null) {
			value = ((String) object.toString());
			if (value != "" && value != null) {
				value = value.trim();
				String scriptTag = "<script>(.*)</script>";
	            Pattern pattern = Pattern.compile(scriptTag);
	            Matcher matcher = pattern.matcher(value);
	            if (matcher.find()) {
	                value = value.replace(matcher.group(1), "");
	            }
				value = value.replaceAll("<[^>]*>", "");
			}
		}
		return value;
	}
	
	/**
	 * Trim string
	 * @param string
	 * @return
	 */
	public static String trimString(String string) {
		String value = null;
		if(string != null) {
			value = string;
			if (value != "" && value != null) {
				value = value.trim();
				String scriptTag = "<script>(.*)</script>";
	            Pattern pattern = Pattern.compile(scriptTag);
	            Matcher matcher = pattern.matcher(value);
	            if (matcher.find()) {
	                value = value.replace(matcher.group(1), "");
	            }
				value = value.replaceAll("<[^>]*>", "");
			}
		}
		return value;
	}
	
	/**
	 * Validate string
	 * @param object
	 * @return
	 */
	public static boolean isValidString (Object object){
		String value = "";
		if(object != null) {
			value = trimString(object);
			if (!value.equals("")) {
				return true;
			}
		}
		return false;  
    }
	
	/**
	 * Validate String for combination of alphabets, integers and special characters
	 * 
	 * @param name
	 * @return
	 */
	public static boolean isValidAlphabetString (Object object){
		String value = "";
		if(object != null) {
			value = trimString(object);
			if (!value.equals("")) {
				String regx = "^[a-zA-Z]+$";
				Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
				Matcher matcher = pattern.matcher(value);
				if (matcher.matches()) {
					return true;
				}
			}
		}
		return false;
       
    }

	/**
	 * Validate combination of alphabets and integer string
	 * 
	 * @param object
	 * @return
	 */
	public static boolean isValidAlphaIntString (Object object){
		String value = "";
		if(object != null) {
			value = trimString(object);
			if (!value.equals("")) {
				String regx = "^[a-zA-Z0-9]+$";
				Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
				Matcher matcher = pattern.matcher(value);
				if (!matcher.matches()) {
					Log.writeLine("Error in validating combination of alphabets and integers: " + object.toString());
					return false;
				}
			}
		}
		return true;
       
    }

	/**
	 * Validate float string
	 * 
	 * @param object
	 * @return
	 */
	public static boolean isValidFloatString (Object object){
		String value = "";
		if(object != null) {
			value = trimString(object);
			if (!value.equals("")) {
				String regx = "^[0-9]*\\.?[0-9]*$";
				Pattern pattern = Pattern.compile(regx);
				Matcher matcher = pattern.matcher(value);
				if (!matcher.matches()) {		
					Log.writeLine("Error in validating numbers and decimal string: " + object.toString());
					return false;
				}
			}
		}
		return true;
    }

	/**
	 * Validate float/Integer string
	 * 
	 * @param object
	 * @return
	 */
	public static boolean isValidFloatIntString (Object object){
		String value = "";
		if(object != null) {
			value = trimString(object);
			if (!value.equals("")) {
				String regx = "[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?";
				Pattern pattern = Pattern.compile(regx);
				Matcher matcher = pattern.matcher(value);
				if (!matcher.matches()) {		
					Log.writeLine("Error in validating numbers and decimal string: " + object.toString());
					return false;
				}
			}
		}
		return true;
    }
	
	/**
	 * Validate Float/integer for integers only
	 * 
	 * @param object
	 * @return
	 */
	public static String validateFloatIntString (Object object){
		String value = "";
		try {
			value = trimString(object);
			if (!value.equals("") && value != null) {
				String regx = "[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?";
				Pattern pattern = Pattern.compile(regx);
				Matcher matcher = pattern.matcher(value);
				if (!matcher.matches()) {
					value = ""; //not valid, empty the value
				}
			}
		} catch (Exception e) {
			Log.writeLine ("Error in validating float integer string: " + object.toString());		
		}
		return value;
       
    }
	
	public static int validateStringToInt (Object object){
		int value = 0;
		try {
			String val = trimString(object);
			if (val != null && val != "") {
				String regxInt = "^[0-9]*$";
				Pattern patternInt = Pattern.compile(regxInt);
				Matcher matcherInt = patternInt.matcher(val);
				if (!matcherInt.matches()) {
					value = 0;
				} else {
					value = new Integer(val);
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			Log.writeLine ("Error in validating float or integer string: " + object.toString());		
		}
		return value;
       
    }
	
	public static double validateStringToDouble (Object object){
		double value = 0;
		try {
			String val = trimString(object);
			if (val != null && val != "") {
				String regxFloat = "[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?";
				Pattern patternFloat = Pattern.compile(regxFloat);
				Matcher matcherFloat = patternFloat.matcher(val);
				if (!matcherFloat.matches()) {
					value = 0;
				} else {
					value = Double.parseDouble(val);
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			Log.writeLine ("Error in validating float or integer string: " + object.toString());		
		}
		return value;
       
    }

	/**
	 * Validate integer string
	 * 
	 * @param object
	 * @return
	 */
	public static boolean isValidIntString (Object object){
		String value = "";
		if(object != null) {
			value = trimString(object);
			if (!value.equals("")) {
				String regx = "^[0-9]*$";
				Pattern pattern = Pattern.compile(regx);
				Matcher matcher = pattern.matcher(value);
				if (!matcher.matches()) {		
					return false;
				}
			}
		}
		return true;
    }
	
	/**
	 * 
	 * @param object
	 * @return
	 */
	public static boolean isValidJsonArray(Object object) {
		try {
			Jsoner.deserialize(object.toString(), new JsonObject());
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * 
	 * @param object
	 * @return
	 */
	public static boolean isValidJsonObject(Object object) {
		try {
			Jsoner.deserialize(object.toString(), new JsonObject());
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * 
	 * @param object
	 * @return
	 */
	public static String validateAlphaIntString (Object object){
		String value = "";
		try {
			value = trimString(object);
			if (value != "" && value != null) {
				String regx = "^[a-zA-Z0-9]+$";
				Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
				Matcher matcher = pattern.matcher(value);
				if (!matcher.matches()) {
					value = ""; //not valid, empty the value
				}
			}
		} catch (Exception e) {
			Log.writeLine ("Error in validating String: " + object.toString());		
			Globals.printException(e);
		}
		return value;
       
    }
	
	/**
	 * Validate String for integers only
	 * 
	 * @param name
	 * @return
	 */
	public static String validateIntString (Object object){
		String value = "";
		try {
			value = trimString(object);
			if (!value.equals("") && value != null) {
				String regx = "^[0-9]*$";
				Pattern pattern = Pattern.compile(regx);
				Matcher matcher = pattern.matcher(value);
				if (!matcher.matches()) {
					value = ""; //not valid, empty the value
				}
			}
		} catch (Exception e) {
			Log.writeLine ("Error in validating String: " + object.toString());		
		}
		return value;
       
    }
	
	/**
	 * Validate Integer
	 * @param object
	 * @return
	 */
	public static int validateIntValue(Object object) {
		int value = 0;
		try {
			if (!object.equals("") && object != null) {
				value = new Integer (trimString(object));
			}
		} catch (Exception e) {
			Log.writeLine ("Error in validating integer value: " + object.toString());		
		}
		return value;
	}
	
	/**
	 * Clean and Validate String values
	 * @param type
	 * @param object
	 * @return
	 */
	public static String validateValue(String type, Object object) {
		String value = "";				
		try {
			if (object != null ) {
				value = trimString(object);			
				String s = trimString(value);
				value = (s.length() > maxStringLength) ? s.substring(0, maxStringLength) : s;
			}
		} catch (Exception e) {
			Log.writeLine ("Error in validating String: " + object.toString());		
		}
		return value;
	}
	
	/**
	 * Clean and Validate String values for max length
	 * @param type
	 * @param object
	 * @return
	 */
	public static String validateString(Object object) {
		String value = "";				
		try {
			if (object != null ) {
				String s = trimString(object);				
				value = (s.length() > maxStringLength) ? s.substring(0, maxStringLength) : s;
			}
		} catch (Exception e) {
			Log.writeLine ("Error in validating String: " + object.toString() + " - Class: " + e.getClass() + " with exception: " + e.getMessage());		
		}
		return value;
	}
	
	/**
	 * Validate text string without max length
	 * @param object
	 * @return
	 */
	public static String validateTextString(Object object) {
		String value = "";				
		try {
			if (object != null ) {
				value = trimString(object);				
			}
		} catch (Exception e) {
			Log.writeLine ("Error in validating String: " + object.toString() + " - Class: " + e.getClass() + " with exception: " + e.getMessage());		
		}
		return value;
	}
	
	/**
	 * /**
	 * Clean and Validate String values
	 * @param object
	 * @param defaultValue
	 * @return
	 */
	public static String validateWithDefaultValue(Object object, String defaultValue) {
		String value = defaultValue;
		try {
			if (object != "" && object != null) {
				String s = trimString((String) object);				
				value = (s.length() > maxStringLength) ? s.substring(0, maxStringLength) : s;
			}
			
		} catch (Exception e) {
			Log.writeLine("Error in validating value: " + object.toString());
		}
		return value;
	}
	
	/**
	 * Clean and Validate Boolean values
	 * @param object
	 * @param defaultValue
	 * @return
	 */
	public static Boolean validateValue(Object object, Boolean defaultValue) {
		Boolean value = defaultValue;
		if (object != "" && object != null) {
			value = ((Boolean) object).booleanValue();
		} else {
			Log.writeLine("Error in validating boolean: " + object.toString());
		}
		return value;
	}
	
	/**
	 * Clean and Validate JsonArray values
	 * @param isJSONArray
	 * @param object
	 * @return
	 */
	public static JsonArray validateJsonArray(Object object) {
		JsonArray value = new JsonArray();
		if (object != "" && object != null) {
			value = ((JsonArray) object);
		} else {
			Log.writeLine("Error in validating JsonArray: " + object.toString());
		}
		return value;
	}
	
	/**
	 * Clean and Validate JsonObject values
	 * @param object
	 * @return
	 */
	public static JsonObject validateJsonObject(Object object) {
		JsonObject value = new JsonObject();
		if (!object.equals("") && object != null) {
			value = ((JsonObject) object);
		}
		return value;
	}
	
	/**
	 * Create a random unique ID for each API request
	 * @return
	 */
	public static String requestID() {
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        return randomUUIDString;
	}
	
	/**
	 * Create unique name based on date/time
	 * @return
	 */
	public static String createUniqueName() {
		String filename = "";
		SimpleDateFormat dateFormatter = new SimpleDateFormat("HHmmssSSS");
	    Date date = new Date();
	    filename = dateFormatter.format(date);
	    return filename;
	}

	public static String validateGenderValue(Object object) {
		String value = "";
		if (object != "" && object != null) {
			value = ((String) object);
		} else {
			Log.writeLine("Error in validating JsonObject: " + object.toString());
		}
		return value;
	}
	
	/**
	 * Get HTTP response message for HTTP Code
	 * 
	 * @param hTTPResponse
	 * @return
	 */
	public static String getHTTPResponseMsg(int code) {
		
		switch (code) {
			case 200:
				return Constants.HTTP_OK;
			case 201:
				return Constants.HTTP_CREATED;
			case 202:
				return Constants.HTTP_ACCEPTED;
			case 204:
				return Constants.HTTP_NOCONTENT;
			case 301:
				return Constants.HTTP_MOVEDPERMANENTLY;
			case 400:
				return Constants.HTTP_BADREQUEST;
			case 401:
				return Constants.HTTP_UNAUTHORIZED;
			case 403:
				return Constants.HTTP_FORBIDDEN;
			case 404:
				return Constants.HTTP_NOTFOUND;
			case 405:
				return Constants.HTTP_METHODNOTALLOWED;
			case 406:
				return Constants.HTTP_NOTACCEPTABLE;
			case 409:
				return Constants.HTTP_CONFLICT;
			case 422:
				return Constants.HTTP_UNPROCESSABLEENTITY;
			case 500:
				return Constants.HTTP_INTERNALSERVERERROR;
			case 503:
				return Constants.HTTP_SERVICEUNAVAILABLE;
		}
		return null;
	}
	
	/**
	 * Get error message for error code
	 * 
	 * @param code
	 * @return
	 * @throws NotesException 
	 */
	public static String getMessageFromCode(int code, Session session, KeywordHandler kwHandlerIMSDB) throws NotesException {
		String message = null;
		switch (code) {
			case 1000:
				message = Constants.errorApiIsUnavailable;
				break;
			case 1001:
				message = Constants.errorUnableToGenerateJwt;
				break;
			case 1002:
				message = Constants.errorInValidClientId;
				break;
			case 1003:
				message = Constants.errorInValidClientSecret;
				break;
			case 1004:
				message = Constants.errorInValidGrantType;
				break;
			case 1005:
				message = Constants.errorInValidToken;
				break;
			case 1006:
				message = Constants.errorTokenExpired;
				break;
			case 1007:
				message = Constants.errorInvalidPin;
				break;
			case 3001:
				message = Constants.errorInValidBookingId;
				break;
			case 3002:
				message = Constants.errorInValidExternalUrl;
				break;
			case 3003:
				message = Constants.errorInValidAppointmentDateTimeFormat;
				break;
			case 3004:
				String hoursAllowed = kwHandlerIMSDB.getKeywordValueString("hoursAllowedToCreateBookingInPast");
				message = Constants.errorInValidAppointmentDateInPast;
				message = message.replace("72", hoursAllowed);
				break;
			case 3005:
				message = Constants.errorInValidClinicCode;
				break;
			case 3006:
				message = Constants.errorInValidCostcentre;
				break;
			case 3007:
				message = Constants.errorInValidLanguageCode;
				break;
			case 3008:
				String[] allowedSkills = kwHandlerIMSDB.getKeywordValueArray("SkillLevel");
				List<String> allowedSkillsList = Arrays.asList(allowedSkills);
				String str = String.join(", ", allowedSkillsList);
				message = Constants.errorInValidAccrLevel;
				message += str;
				break;
			case 3009:
				message = Constants.errorInValidBookingStateReceived;
				break;
			case 3010:
				message = Constants.errorInValidBookingStateAllocated;
				break;
			case 3011:
				message = Constants.errorInValidBookingStateUnAllocated;
				break;
			case 3012:
				message = Constants.errorInValidBookingStateCancelled;
				break;
			case 3013:
				message = Constants.errorInValidBookingStateRejected;
				break;
			case 3014:
				message = Constants.errorInValidBookingStateForPast;
				break;
			case 3015:
				message = Constants.errorInvalidModifiedDateFormat;
				break;
			case 3016:
				message = Constants.errorMissingInterpreterDetails;
				break;
			case 3017:
				message = Constants.errorInvalidAction;
				break;
			case 3018:
				message = Constants.errorInValidEmailProf;
				break;
			case 3019:
				message = Constants.errorInValidFnameProf;
				break;
			case 3020:
				message = Constants.errorInValidPatientDateOfBirthFormat;
				break;
			case 3021:
				message = Constants.errorInValidFnameLnamePatient;
				break;
			case 3022:
				message = Constants.errorInValidBooking;
				break;
			case 3023:
				message = Constants.errorInValidEmailContact;
				break;
			case 3024:
				message = Constants.errorInValidEmailInter;
				break;
			case 3025:
				message = Constants.errorInValidAddressFormat;
				break;
			case 3026:
				message = Constants.errorInValidAddress;
				break;
			case 3027:
				message = Constants.errorInValidMandatory;
				break;
			case 3028:
				message = Constants.errorUnableToAssignAgencyID;
				break;
			case 3029:
				message = Constants.errorInvalidDuration;
				break;
			case 3030:
				message = Constants.errorInvalidPriority;
				break;
			case 3031:
				message = Constants.errorInValidGenderPrefCode;
				break;
			case 3032:
				message = Constants.errorInvalidEthnicityCode;
				break;
			case 3033:
				message = Constants.errorUnableToCancelCombinedBooking;
				break;
			case 3034:
				message = Constants.errorIrrelevantBookingId;
				break;
			case 3035:
				message = Constants.errorInvalidExternalRef;
				break;
			case 3036:
				message = Constants.errorInValidFnameContact;
				break;
			case 3037:
				message = Constants.errorBookingLocked;
				break;
			case 3038:
				message = Constants.errorInValidBookingStateCancelled;
				break;
			case 3039:
				message = Constants.errorInValidBookingStateUpdated;
				break;
			case 3040:
				message = Constants.errorUnableToUpdateCombinedBooking;
				break;
			case 3041:
				String[] arrAllowedStatusToComplete = kwHandlerIMSDB.getKeywordValueArray(Constants.KW_MOBILE_ALLOWEDSTATUSTOCOMPLETE);
				List<String> allowedStatusToComplete = Arrays.asList(arrAllowedStatusToComplete);
				String status = String.join(", ", allowedStatusToComplete);
				message = Constants.errorInValidBookingStateCompleted;
				message = message.replace("<status>", status + " and not invoiced");
				break;
			case 3042:
				message = Constants.errorInValidBookingStateForFuture;
				break;
			case 3043:
				message = Constants.errorInValidAppointmentTimeFormat;
				break;
			case 3044:
				message = Constants.errorInValidNonCompletionReason;
				break;
			case 3045:
				String hoursAllowedToAction = kwHandlerIMSDB.getKeywordValueString("hoursAllowedToActionBooking");
				message = Constants.errorInValidBookingStateForAction;
				message = message.replace("24", hoursAllowedToAction);
				break;
			case 3046:
				message = Constants.errorInvalidActionToAccessAll;
				break;
			case 3047:
				message = Constants.errorInvalidActionToUserAccess;
				break;
			case 3048:
				message = Constants.errorInValidBookingStateAccepted;
				break;
			case 3049:
				message = Constants.errorInvalidWaitingDuration;
				break;
			case 3050:
				String hoursAllowedPast_UpdateBook = kwHandlerIMSDB.getKeywordValueString("hoursAllowedToUpdateBookingInPast");
				message = Constants.errorInValidAppointmentDateInPastUpdate;
				message = message.replace("0", hoursAllowedPast_UpdateBook);
				break;		
			case 3051:	
				message = Constants.errorInvalidLanguageChange;
				break;
			//Below error codes to be reviewed while working on patient end-point and remove duplicates			
			case 4000:
				message = Constants.errorInValidURL;
				break;
			case 4001:
				message = Constants.errorInValidAuthenticationScheme;
				break;
			case 4002:
				message = Constants.errorWrongCredentials;
				break;
			case 4003:
				message = Constants.errorInValidContentType;
				break;
			case 4004:
				message = Constants.errorEmptyJsonRequest;
				break;
			case 4005:
				message = Constants.errorInValidJsonRequest;
				break;
			case 4006:
				message = Constants.errorMissingID;
				break;
			case 4007:
				message = Constants.errorMissingPatientFirstName;
				break;
			case 4008:
				message = Constants.errorMissingPatientUrNumber;
				break;
			case 4009:
				message = Constants.errorMissingLanguageCode;
				break;
			case 4010:
				message = Constants.errorInValidInterpreterGenderPrefMale;
				break;
			case 4011:
				message = Constants.errorInValidAppointmentDateTimeFormat;
				break;
			case 4012:
				message = Constants.errorInValidID;
				break;
			case 4013:
				message = Constants.errorInvalidFirstName;
				break;
			case 4014:
				message = Constants.errorInvalidURNumber;
				break;
			case 4015:
				message = Constants.errorInValidInterpreterGenderPrefFemale;
				break;
			case 4016:
				message = Constants.errorInValidFacility;
				break;
			case 4017:
				message = Constants.errorInValidLastName;
				break;
			case 4018:
				message = Constants.errorInValidSecondName;
				break;
			case 4019:
				message = Constants.errorInValidTitle;
				break;
			case 4020:
				message = Constants.errorInValidInterpreterGenderPrefUnknown;
				break;
			case 4021:
				message = Constants.errorInValidGender;
				break;
			case 4022:
				message = Constants.errorInValidLanguage;
				break;
			case 4023:
				message = Constants.errorInValidLength;
				break;
			case 4024:
				message = Constants.errorInValidPhoneNumber;
				break;
			case 4025:
				message = Constants.errorInValidGenderPrefCode;
				break;
			case 4026:
				message = Constants.errorInValidGenderPrefPriority;
				break;
			case 4027:
				message = Constants.errorInValidStateCode;
				break;
			case 4028:
				message = Constants.errorInValidPostCode;
				break;
			case 4029:
				message = Constants.errorInValidInterpreterGenderPrefindeterminate;
				break;
			case 4030:
				message = Constants.errorInValidEmailPatient;
				break;
			case 5001:
				message = Constants.errorInValidUserId;
				break;
			case 5002:
				String pmsValue = "PMS ID";
				String labels = kwHandlerIMSDB.getKeywordValueString(Constants.KW_DYNAMIC_LABELS);
				if (labels != null) {
					JsonArray labelsJson = Jsoner.deserialize(labels, new JsonArray());
					for (int l=0; l<labelsJson.size();l++) {
						JsonObject label = (JsonObject) labelsJson.get(l);
						if (label.get("key").equals("PMS ID")) { //Get label for BOOK_Source
							pmsValue = label.get("value").toString();
						}
					}
				}
				message = Constants.errorInValidPmsID;
				message = message.replace("<pmsid>", pmsValue);
				break;
			case 5003:
				pmsValue = "PMS ID";
				labels = kwHandlerIMSDB.getKeywordValueString(Constants.KW_DYNAMIC_LABELS);
				if (labels != null) {
					JsonArray labelsJson = Jsoner.deserialize(labels, new JsonArray());
					for (int l=0; l<labelsJson.size();l++) {
						JsonObject label = (JsonObject) labelsJson.get(l);
						if (label.get("key").equals("PMS ID")) { //Get label for BOOK_Source
							pmsValue = label.get("value").toString();
						}
					}
				}
				message = Constants.errorInValidImsID;
				message = message.replace("<pmsid>", pmsValue);
				break;
		}
		return message;
	}

	/**
	 * Validate is JSON is valid 
	 * 
	 * @param jsonString
	 * @return
	 * @throws NotesException
	 */
	public static boolean isValidJson(String jsonString) throws Exception {
		try {
			Jsoner.deserialize(jsonString, new JsonObject());
			return true;
		} catch (Exception e) {
			try {
				Jsoner.deserialize(jsonString, new JsonArray());
				return true;
			} catch (Exception n) {
				Globals.printException(n);
			}
			Globals.printException(e);
		}
		return false;
	}
	
	/**
	 * APIs- isBookingAPIEnabled, isPatientAPIEnabled, isBookingCompletionAPIEnabled
	 * 
	 * @param api
	 * @return
	 */
	public static boolean isAPIEnabled(String api, Session session, KeywordHandler kwHandlerIMSDB) {
		try {
			if (api != null) {
				String isTrue = kwHandlerIMSDB.getKeywordValueString(api);
				if (isTrue != null && isTrue.equalsIgnoreCase("yes")) {
					return true;
				}
			}
		} catch (Exception e) {
			Log.writeLine("Unable to find KW: " + api);
		}
		return false;
	}

	/**
	 * Verify and return valid contact type
	 * @param contactType
	 * @return
	 */
	public static String verifyContactType(String contactType) {
		String type = Constants.defaultContactType;
		contactType = validateString(contactType);
		if (contactType.equalsIgnoreCase("Mobile") 
				|| contactType.equalsIgnoreCase("Home")
				|| contactType.equalsIgnoreCase("Work")
				|| contactType.equalsIgnoreCase("Pager")) {
			type = formatContactType(contactType);
		}
		return type;
	}

	/**
	 * Return the clean contact type
	 * @param gender
	 * @return
	 */
	public static String formatContactType (String type){
		if (type.equalsIgnoreCase("Mobile")) {return "Mobile";}
		if (type.equalsIgnoreCase("Home")) {return "Home";}
		if (type.equalsIgnoreCase("Work")) {return "Work";}
		if (type.equalsIgnoreCase("Pager")) {return "Pager";}
		return Constants.defaultContactType;
	}

	/**
	 * Return the clean gender code
	 * @param gender
	 * @return
	 */
	public static String GenderCodeFormat (String gender){
		if (gender.equalsIgnoreCase("F")) {return "F";}
		if (gender.equalsIgnoreCase("M")) {return "M";}
		if (gender.equalsIgnoreCase("I")) {return "I";}
		if (gender.equalsIgnoreCase("U")) {return "U";}
		return Constants.defaultPatientGender;
	}
	
	/**
	 * Verify and return valid gender
	 * Default to U
	 * @param gender
	 * @return
	 */
	public static String verifyGenderFormat(String gender) {
		String retGender = Constants.defaultPatientGender;
		gender = validateString(gender);
		if (gender!= null && (gender.equalsIgnoreCase("F") 
				|| gender.equalsIgnoreCase("M")
				|| gender.equalsIgnoreCase("I")
				|| gender.equalsIgnoreCase("U"))) {
			retGender = GenderCodeFormat(gender);
		}
		return retGender;
	}

	/**
	 * Return the clean gender preference code
	 * @param gender
	 * @return
	 */
	public static String GenderPrefCodeFormat (String gender){
		if (gender.equalsIgnoreCase("F")) {return "F";}
		if (gender.equalsIgnoreCase("M")) {return "M";}
		if (gender.equalsIgnoreCase("NP")) {return "NP";}
		return "NP";
	}
	
	/**
	 * Verify and return valid gender preference for patient
	 * Default to NP
	 * @param gender - Gender preference - F, M, NP
	 * @return
	 */
	public static String verifyGenderPrefFormat(String gender) {
		String retGender = Constants.defaultGenderPref;
		gender = validateString(gender);
		if (gender!= null && (gender.equalsIgnoreCase("F") 
				|| gender.equalsIgnoreCase("M")
				|| gender.equalsIgnoreCase("NP"))) {
			retGender = GenderPrefCodeFormat(gender);
		}
		return retGender;
	}

	/**
	 * Check format/case and return the exact priority
	 * 
	 * @param priority
	 * @return
	 */
	public static boolean isValidPriority(String priority) {
		try {
			if (priority.equalsIgnoreCase("Must be") 
					|| priority.equalsIgnoreCase("Must not be")
					|| priority.equalsIgnoreCase("Prefer to be")
					|| priority.equalsIgnoreCase("Prefer not to be")) {
				return true;
			}
		} catch (Exception e) {
			Log.writeLine("Invalid priority");
		}
		return false;
	}
	
	/**
	 * Verify and return patient type
	 * 
	 * @param patientType - Type of patient - Inpatient, Outpatient, Home Visit, ED, Group
	 * @return
	 */
	public static String verifyPatientType(String patientType) {
		String retPatientType = null;
		patientType = validateString(patientType);
		if (patientType!= null && (patientType.equalsIgnoreCase("Inpatient") 
				|| patientType.equalsIgnoreCase("Outpatient")
				|| patientType.equalsIgnoreCase(Constants.homeVisit)
				|| patientType.equalsIgnoreCase("ED")
				|| patientType.equalsIgnoreCase("Group"))) {
			retPatientType = formatPatientType(patientType);
		}
		return retPatientType;
	}
	
	/**
	 * Check format/case and return the exact patient type
	 * 
	 * @param patientType - Type of patient - Inpatient, Outpatient, Home Visit, ED, Group
	 * @return
	 */
	public static String formatPatientType (String patientType){
		if (patientType.equalsIgnoreCase("Inpatient")) {return "Inpatient";}
		if (patientType.equalsIgnoreCase("Outpatient")) {return "Outpatient";}
		if (patientType.equalsIgnoreCase("Home Visit")) {return "Home Visit";}
		if (patientType.equalsIgnoreCase("ED")) {return "ED";}
		if (patientType.equalsIgnoreCase("Group")) {return "Group";}
		return null;
	}
	
	/**
	 * 
	 * @param sDeliveryMethod
	 * @return
	 */
	public static String verifyDeliveryMethod(String sDeliveryMethod) {
		String retDeliveryMethod = null;
		sDeliveryMethod = validateString(sDeliveryMethod);
		if (sDeliveryMethod != null && (sDeliveryMethod.equalsIgnoreCase("Onsite") 
				|| sDeliveryMethod.equalsIgnoreCase("Telephone")
				|| sDeliveryMethod.equalsIgnoreCase("Video-enabled"))) {
			retDeliveryMethod = formatDeliveryMethod(sDeliveryMethod);
		}
		return retDeliveryMethod;
	}
	
	/**
	 * 
	 * @param sDeliveryMethod
	 * @return
	 */
	public static String formatDeliveryMethod (String sDeliveryMethod){
		if (sDeliveryMethod.equalsIgnoreCase("Onsite")) {return "Onsite";}
		if (sDeliveryMethod.equalsIgnoreCase("Telephone")) {return "Telephone";}
		if (sDeliveryMethod.equalsIgnoreCase("Video-enabled")) {return "Video-enabled";}
		return null;
	}
	
	/**
	 * Finds the start and end positions for given start key and end key range in a view
	 *  Start and End key is combination of Agency DocKey and a Date
	 *  
	 * @param viewAllDocsByDate - View to search for all bookings
	 * @param startKey - View Start Position
	 * @param endKey - View End Position
	 * @return
	 * @throws NumberFormatException
	 * @throws NotesException
	 */
	public static JsonArray View_GetEntryPositions(View view, String startKey, String endKey) throws Exception, NumberFormatException, NotesException {
		JsonArray ret = new JsonArray();
		if (view != null && startKey != null && endKey != null) {
			//Start Date is Greater than End Date
			if(View_GetEntryPosition_Continue(startKey, endKey) == false){return ret;}
			int posStart = 0;
			boolean isFirstPosition = true; 
			int iCount = 0;
			while (posStart == 0) {		
				posStart = View_GetEntryPosition_GetPosition(view, startKey, isFirstPosition);
				if(posStart == 0){
					startKey = View_GetEntryPosition_AdjustSearchString(startKey, isFirstPosition);	
					//Start Date is Greater than End Date
					if(View_GetEntryPosition_Continue(startKey, endKey) == false){
						return ret;
					}
				}
				iCount++;
				if(iCount > Constants.maxNumberOfBookingsToBeReturned){return ret;}
			}
			int posEnd = 0; 
			boolean isFirstPosition1 = false;
			while (posEnd == 0){		
				posEnd = View_GetEntryPosition_GetPosition(view, endKey, isFirstPosition1);
				if(posEnd == 0){
					endKey = View_GetEntryPosition_AdjustSearchString(endKey, isFirstPosition1);
					//Start Date is Greater than End Date
					if(View_GetEntryPosition_Continue(startKey, endKey) == false){
						return null;
					}			
				}
			}
			ret.add(posStart);
			ret.add(posEnd);
		}
		return ret;	
	}
	
	/**
	 * Getting a position of searched key from a view
	 * 
	 * @param viewAllDocsByDate
	 * @param strSearchString
	 * @param isFirstPosition
	 * @return
	 * @throws NumberFormatException
	 * @throws NotesException
	 */
	private static int View_GetEntryPosition_GetPosition(View view, String strSearchString, boolean isFirstPosition) throws NumberFormatException, NotesException{
		ViewEntryCollection allEntriesCollection = view.getAllEntriesByKey(strSearchString, true);
		int intPosition = allEntriesCollection.getCount();	
		if(intPosition == 0){return 0;}
		if(isFirstPosition){intPosition = 1;}
		int t1 = new Integer(allEntriesCollection.getNthEntry(intPosition).getPosition('.'));
		//printCode("~~~~~~~ Position Found for " + (isFirstPosition ? "StartDate" : "EndDate" + "~~~~~~~~~~~~intPosition~~~~~~~"+intPosition+"~~~~~~~~t1~~~~~~~"+t1));
		return t1;
	}
	
	/**
	 * Check if end key is greater or not
	 * 
	 * @param startKey
	 * @param endKey
	 * @return
	 * @throws NotesException 
	 */
	private static boolean View_GetEntryPosition_Continue(String startKey, String endKey) throws NotesException{
		int intStart = 0;
		int intEnd = 0;
		if(startKey.indexOf("~")>=0) {
			intStart = new Integer(startKey.split("~")[1]);
			intEnd = new Integer(endKey.split("~")[1]);	
		} else {
			intStart = new Integer(startKey); 
			intEnd = new Integer(endKey);
		}
		return intStart <= intEnd;
	}
	
	/**
	 * Adjusts search string to build next search string from Agency Doc Key and Date String
	 * 
	 * @param strSearchString - Agency Doc Key and Date String
	 * @param isFirstPosition - Is Start date?
	 * @return String - Adjusted Agency Doc Key and Next Date String
	 * @throws NotesException
	 */
	private static String View_GetEntryPosition_AdjustSearchString(String strSearchString, boolean isFirstPosition) throws Exception {
		String strApp = "";
		String strReturn = "";
		String strDtString = strSearchString;	
		if(strSearchString.indexOf("~")>=0){
			strDtString = strSearchString.split("~")[1]; 
			strApp = strSearchString.split("~")[0] + "~";	
		}
		String dateToProcess;
		boolean incrementDate = false;
		if(isFirstPosition) {
			incrementDate = true;
		} else {
			incrementDate = false;
		}
		dateToProcess = getNextOrPrevDate(incrementDate, "yyyyMMdd", strDtString);
		strReturn = strApp + dateToProcess;	
		//printCode("Adjusted " + (isFirstPosition ? "StartDate" : "EndDate") + " ~~~~~ " + strSearchString + " to " + strReturn);	
		return strReturn;
	}
	
	/**
	 * For developer use only to java print
	 * 
	 * @param lineToPrint
	 */
	public static void printCode(String lineToPrint) {
		System.out.print(lineToPrint);
		return;
	}

	/**
	 * For developer use only to print exceptions in console
	 * @param e
	 */
	public static void printException(Exception e) {
		e.printStackTrace();
	}

	/**
	 * Check if JSON date object is valid
	 * 
	 * @param dateObject
	 * @param dateFormat
	 * @return
	 */
	public static boolean isValidDate(Object dateObject, String dateFormat) {
		try {
			String value = "";
			if(dateObject != null) {
				value = trimString(dateObject);
				if (!value.equals("")) {
					SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
					dateFormatter.setLenient(false);
					dateFormatter.parse(value);
					return true;
				}
			}
			return true;  
		} catch (Exception e) {
			Globals.printException(e);
			return false;  
		}
	}
	
	/**
	 * Return valid date from JSON date object
	 * 
	 * @param dateObject
	 * @param dateFormat
	 * @return
	 */
	public static String validatedDate(Object dateObject, String dateFormat) {
		try {
			String value = "";
			if(dateObject != null) {
				value = trimString(dateObject);
				if (!value.equals("")) {
					SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
					sdf.setLenient(false);
					sdf.parse(value);
					return value;
				}
			}
			return null;  
		} catch (Exception e) {
			Globals.printException(e);
			return null;  
		}
	}
	
	/**
	 * To check if array index exists in array
	 * 
	 * @param data
	 * @param index
	 * @return
	 */
	public static boolean indexInBound(String[] data, int index){
	    return data != null && index >= 0 && index < data.length;
	}

	/**
	 * Check if passed DateTime is a future date
	 * @param dateTime
	 * @return
	 */
	public static boolean isFutureDate(DateTime nowDateTime, DateTime dateTime, Session session) {
		try {
			if (nowDateTime.timeDifference(dateTime) <= 0) {
				return true;
			}
		} catch (NotesException e) {
			printException(e);
		}
		return false;
	}
	
	/**
	 * Check if date is within allowed date to action a booking
	 * @param dateTime
	 * @param session
	 * @return
	 */
	public static boolean isAllowedToAction(DateTime nowDateTime, DateTime dateTime, Session session, KeywordHandler kwHandlerIMSDB) {
		try {
			int hoursInFuture = new Integer(kwHandlerIMSDB.getKeywordValueString("hoursAllowedToActionBooking"));
			DateTime allowedActionDate = session.createDateTime(dateTime.toJavaDate()); //Make sure to convert to java date otherwise Notes will change the original datetime variable value as well to adjusted hourn
			allowedActionDate.adjustHour(hoursInFuture);
			if (nowDateTime.timeDifference(allowedActionDate) <= 0) {
				allowedActionDate.adjustHour(-hoursInFuture); 
				return true;
			}
		} catch (NotesException e) {
			printException(e);
		}
		return false;
	}

	/**
	 * Convert incoming format yyyyMMdd HH:mm to Notes DateTime
	 * 
	 * @param date - yyyyMMdd
	 * @param time - HH:mm
	 * @return
	 * @throws NotesException
	 */
	public static DateTime getDateTime (String date, String time, Session session) throws NotesException
	{
		String day;
		String year;
		String monthStr;
		DateTime dateTime = null;
		try {
			if (date.length() != 8) {
				return null;
			}
			year = date.substring(0,4);
			monthStr = date.substring(4,6);
			day = date.substring(6,8);
			ImsHomeDb ImsHomeDb = new ImsHomeDb(session);
			KeywordHandler kwHandlerIMSDB = new KeywordHandler(ImsHomeDb.getDatabase());
			String Global_DateNotes_From = kwHandlerIMSDB.getGlobalDateNotesFromFormat();
			String dateSeparator = " ";
			if (Global_DateNotes_From.contains("/")) {
				dateSeparator = "/";
			} else if (Global_DateNotes_From.contains("-")) {
				dateSeparator = "-";
			}
			int indexOfdd = Global_DateNotes_From.indexOf("dd");
			int indexOfMM = Global_DateNotes_From.indexOf("MM");
			int indexOfyyyy = Global_DateNotes_From.indexOf("yyyy");
			String dateFormat = day + dateSeparator + monthStr + dateSeparator + year + " " + time; //default is dd/MM/yyyy HH:mm
			if (indexOfdd == 3 && indexOfMM == 0 && indexOfyyyy == 6) { //MM/dd/yyyy
				dateFormat = monthStr + dateSeparator + day + dateSeparator + year + " " + time;
			}
			dateTime = session.createDateTime(dateFormat);
			if (dateTime != null) {
				return dateTime;
			}
		} catch (NotesException e) {
			printException(e);	
		}
		return null;
	}
	
	
	public static String getLSPByConsumerName(String consumerName, Session session, KeywordHandler kwHandlerIMSDB) {
		String languageServiceProvider = null;
		try {
			if (consumerName != null) {
				languageServiceProvider = kwHandlerIMSDB.getLSPByConsumerName(consumerName);
			}
		} catch (Exception e) {
			Log.writeLine("Unable to find Consumer Name: " + consumerName);
		}
		return languageServiceProvider;
	}
	
	/**
	 * Get mapped query parameters
	 * @param query
	 * @return
	 */
	public static Map<String, Object> getQueryMap(String query) { 
	    Map<String, Object> map = new HashMap<String, Object>();
		if (query != null && !query.equals("")) {
		    String[] params = query.split("&");  
		    for (String param : params) {  
		    	 String[] arrPath = param.split("=");
		    	 String name = null;
		    	 String value = null;
		    	 if (arrPath != null) {
			    	 if (Globals.indexInBound(arrPath, 0)) {
			    		 name = arrPath[0];
			    	 }
			    	 if (Globals.indexInBound(arrPath, 1)) {
			    		 value = arrPath[1];
			    	 }  
		    	 }
	    		 map.put(name, value); 
		    }  
		}
	    return map;  
	}
	
	/**
	 * Get view column names and data entries in JSON
	 * @param view
	 * @return
	 */
	public static JsonArray getViewData(View view) {
		JsonArray rowData = new JsonArray();
		try {
			ViewEntryCollection allEntries = view.getAllEntries();
			if (allEntries.getCount() > 0) {
				ViewEntry entry = allEntries.getFirstEntry();
				while(entry != null) {
					Vector columns = view.getColumns();
					if (columns.size() != 0) {
						JsonObject colData = new JsonObject();
						for (int i=0; i < columns.size(); i++) {
							ViewColumn column = (ViewColumn)columns.elementAt(i);
							String itemName = column.getItemName();
							String entryString = entry.getColumnValues().elementAt(i).toString();
							if (entryString.startsWith("[")) {
								if (entryString.equals("[]")) {
									JsonArray entryArr = new JsonArray();
									colData.put(itemName, entryArr);
								} else {
									String entryArr [] = entryString.replace("[", "").replace ("]", "").split (",");
									if (entryArr.length > 0) {
										colData.put(itemName, Arrays.asList(entryArr));
									}
								}
							} else {
								colData.put(itemName, entryString);
							}
			          	}
						rowData.add(colData);
					}
					entry = allEntries.getNextEntry(entry);
				}
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return rowData;
	}
	
	/**
	 * 
	 * Get array of document entries from a categorized view
	 * 
	 * @param view - View
	 * @param category - Category name
	 * @return
	 */
	public static JsonArray getViewEntriesForCategory(View view, String category) {
		JsonArray objEntries = new JsonArray();
		try {
			ViewNavigator vn = view.createViewNavFromCategory(category);
			if (vn.getCount() > 0) {
				ViewEntry entry = vn.getCurrent();
				int pos = (entry.getColumnValues().size() - 1);
				while (entry != null) {
					objEntries.add(entry.getColumnValues().elementAt(pos).toString());
					ViewEntry tmpentry = vn.getNext(entry);
					entry.recycle();
					entry = tmpentry;
				}
				vn.recycle();
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return objEntries;
	}
	
	/**
	 * Compare array values to sort the array for given key
	 * @param jsonArr
	 * @param keyName
	 * @param sortOrderAsc
	 * @return
	 */
	public static JsonArray getSortedList(JsonArray jsonArr, final String keyName, final boolean sortOrderAsc) {
	    List<JsonObject> jsonValues = new ArrayList<JsonObject>();
	    JsonArray sortedJsonArray = new JsonArray();
	    for (int i = 0; i < jsonArr.size(); i++) {
	    	JsonObject jsonObj = Jsoner.deserialize(jsonArr.get(i).toString(), new JsonObject());
	        jsonValues.add(jsonObj);
	    }
	    Collections.sort( jsonValues, new Comparator<JsonObject>() {
	        @Override
	        public int compare(JsonObject a, JsonObject b) {
	            String valA = new String();
	            String valB = new String();
	            try {
	                valA = (String) a.get(keyName);
	                valB = (String) b.get(keyName);
	            } 
	            catch (Exception e) {
	    			Log.writeLine("Unable to compare json array due to exception: " + e.getMessage());
	            }
	            if (sortOrderAsc) {
	            	return valA.compareTo(valB); //ascending
	            } else {
	            	return -valA.compareTo(valB); //descending
	            }	           
	        }
	    });
	    for (int i = 0; i < jsonArr.size(); i++) {
	        (sortedJsonArray).add(jsonValues.get(i));
	    }
	   return sortedJsonArray;
	}
	
	/**
	 * double to int typecasting
	 * @param value
	 */
	public static int doubleToInt(double value) {
        int IntValue = (int) value;
        return IntValue;
    }

	/** 
	 * calculates the distance between two locations in kilometer 
	 */
	public static double getDistance(double lat1, double lng1, double lat2, double lng2) {
	    double earthRadius = 6371; // in miles, change to 3958.75 for MILES output
	    double dLat = Math.toRadians(lat2-lat1);
	    double dLng = Math.toRadians(lng2-lng1);
	    double sindLat = Math.sin(dLat / 2);
	    double sindLng = Math.sin(dLng / 2);
	    double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
	        * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;
	    return dist; // output distance, in kilometer
	}
	
	/**
	 * Convert string to camel case
	 * @param str
	 * @return
	 */
	public static String toCamelCase(String str) {
		String[] wordList = str.toLowerCase().split("_");
		String finalStr = "";
		for (String word : wordList) {
			finalStr += capitalize(word);
		}
		return finalStr;
	}
	
	/**
	 * Convert string to upper case
	 * @param line
	 * @return
	 */
	public static String capitalize(String line) {
		return Character.toUpperCase(line.charAt(0)) + line.substring(1);
	}
	
	/**
	 * Create unique doc key for given form
	 * @param form
	 * @param session
	 * @return
	 */
	public static String createDocKeyFromForm(String form, Session session) {
		String docKey = form + "-";
		String name;
		try {
			name = session.getCommonUserName();
			if (form == "BOOK") { docKey=""; } 
			String str4;
			Vector serverName = session.evaluate("@Unique");
			String unique = serverName.get(0).toString();
			if(name.indexOf(" ") >= 0) {
				if(name.lastIndexOf(" ")<name.length()-2)
					str4 = name.charAt(0)
						+ name.substring(name.lastIndexOf(" ") + 1, 2)
						+ name.charAt(name.length()-1);
				else
					str4 = name.charAt(0)
						+ name.substring(name.lastIndexOf(" ") + 1);
			} else {
				if(name.length() > 3)
					str4 = name.substring(0, 3)
						+ name.charAt(name.length()-1);
				else
					str4 = name; 
			}
			if(str4.length() < 4)
				str4 = str4 + "ZZZZ".substring(0, 4-str4.length());
				
			docKey = docKey + str4.toUpperCase() + "-";
			docKey = docKey + unique.substring(unique.indexOf("-") + 1);
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return docKey; 
	}
	
	/**
	 * Get request parameters as case-insensitive key
	 * @param paramsMap
	 * @param parameter
	 * @return
	 */
	public static String getRequestParameter(Map<String, Object> paramsMap, String parameter) {
		try {
		    for (Entry<String, Object> entry : paramsMap.entrySet()) {
		        if(entry.getKey().equalsIgnoreCase(parameter)) {
		        	String[] value = (String[]) entry.getValue();
		        	if (value[0] != null && !value[0].isEmpty()) 
		        		return value[0].trim();
		        }
		    }
		} catch (Exception e) {
			Globals.printException(e);
		}
	    return null;
	}
	
	/**
	 * Get request parameters correct name to avoid case-sensitivity
	 * @param paramNames
	 * @param parameter
	 * @return
	 */
	public static String getRequestParameterNames(Enumeration<String> paramNames, String parameter) {
		try {
			while (paramNames.hasMoreElements()) {
	        	String param = paramNames.nextElement();
	        	if (param != null && param.equalsIgnoreCase(parameter)) {
	        		return param;
	        	}
	        }
		} catch (Exception e) {
			Globals.printException(e);
		}
		return null;
	}
}
