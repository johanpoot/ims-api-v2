package com.primaxis.ims.utils;

import java.util.ArrayList;

/**
 * Class to define all constants
 * @author BKarwal
 * @since 01 March 2019
 *
 */
public class Constants {

	public static final String YES = "Yes";
	public static final String NO = "No";
	public static final Boolean TRUE = true;
	public static final Boolean FALSE = false;
	//HTTP methods
	public static String HTTP_POST = "POST";
	public static String HTTP_GET = "GET";
	public static String HTTP_PUT = "PUT";
	public static String HTTP_PATCH = "PATCH";
	
	public static ArrayList<String> allowedHttpMethods = new ArrayList<String>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		{ 
			add(Constants.HTTP_POST); 
			add(Constants.HTTP_GET); 
			add(Constants.HTTP_PUT); //Ashish - 20/03/2022 - for WH Allow Booking updates through Put methods also 
			add(Constants.HTTP_PATCH); 
		}
	};
	
	//Allowed actions for PATCH request
	public static String ACTION_ACCEPTED = "accepted";
	public static String ACTION_UNALLOCATED = "unallocated";
	public static String ACTION_ALLOCATED = "allocated";
	public static String ACTION_RECEIVED = "received";
	public static String ACTION_REJECTED = "rejected";
	public static String ACTION_CANCELLED = "cancelled";
	public static String ACTION_COMPLETED = "completed";
	
	public static ArrayList<String> allowedActions = new ArrayList<String>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{ 
			add(Constants.ACTION_RECEIVED);
			add(Constants.ACTION_ALLOCATED);
			add(Constants.ACTION_UNALLOCATED);
			add(Constants.ACTION_CANCELLED);
			add(Constants.ACTION_REJECTED);
			add(Constants.ACTION_COMPLETED);
		}
	};
	
	public static ArrayList<String> ALLOWED_ACTION_TO_ACCESS_ALL = new ArrayList<String>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		{ 
			add(Constants.ACTION_CANCELLED); 
			add(Constants.ACTION_COMPLETED);
		}
	};
	
	public static ArrayList<String> ALLOWED_ACTION_TO_USER_ACCESS = new ArrayList<String>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		{ 
			add(Constants.ACTION_ACCEPTED);
			add(Constants.ACTION_REJECTED); 
			add(Constants.ACTION_COMPLETED);
		}
	};
	public static String serviceNounAuth = "auth";
	public static String serviceNounBookings = "bookings";
	public static String serviceNounPatients = "patients";
	public static String serviceNounUsers = "users";
	public static String serviceNounNotifications = "notifications";
	public static String serviceNounLanguages = "languages";
	public static String serviceNounClinics = "clinics";
	public static String serviceNounBookingLabels = "booking-labels";
	public static String serviceNounCheckin = "checkin";
	public static String serviceNounCheckinStatus = "checkin-status";
	public static String serviceNounCheckout = "checkout";
	//IMS Connect URLs
	public static String serviceNounPins = "pins";
	public static String serviceNounMeetings = "meetings";
	public static String serviceNounQuestions = "questions";
	public static String serviceNounCDR = "cdr";
	///////////////////////////
	public static String allowedGrantType = "client_credentials";
    public static String authenticationScheme = "Bearer";	
    public static String contentTypeJson = "application/json";
	public static String contentTypeForm = "application/x-www-form-urlencoded";
	public static String contentTypeFormMultipart = "multipart/form-data";

	public static String errorInValidURL = "Invalid URL";
	public static String errorEmptyJsonRequest = "Invalid JSON. Request should not be empty";
	public static String errorWrongCredentials = "Invalid username and/or password";
	public static String errorInValidContentType = "Invalid Content Type. Only application/json or multipart/form-data is accepted";
	public static String errorInternalServer = "Internal server error";
	
	public static String defaultPrefix = "BOOK";
	public static final String formBook = "BOOK";
	public static final String formPatient = "Patient";
	public static final String formUser = "USER";
	public static final String FORM_REQUESTOR = "REQU";
	public static final String FORM_PARAM = "PARAM";
	public static final String FORM_PARAM_DELETE = "Param_Delete";
	public static final String FORM_USER_LSP = "USER_LSP";
	public static final String FORM_USER_COMPLIANCE = "USER_Compliance";
	public static final String FORM_USER_CMP = "USER_CMP";
	public static final String formCDR = "CDR";
	
	public static String homeVisit = "Home Visit";
	public static String who = "IMS";
	public static String keywordNameWho = "Who";
	public static String keywordNameClientCredentials = "clientCredentials";
	
	public static String HTTP_OK = "OK";
	public static String HTTP_CREATED = "Created";
	public static String HTTP_ACCEPTED = "Accepted";
	public static String HTTP_NOCONTENT = "No content";
	public static String HTTP_MOVEDPERMANENTLY = "Moved permanently";
	public static String HTTP_BADREQUEST = "Bad request";
	public static String HTTP_UNAUTHORIZED = "Unauthorized";
	public static String HTTP_FORBIDDEN = "Forbidden";
	public static String HTTP_NOTFOUND = "Not found";
	public static String HTTP_METHODNOTALLOWED = "Method not allowed. Only POST is allowed";
	public static String HTTP_NOTACCEPTABLE = "Not acceptable";
	public static String HTTP_INTERNALSERVERERROR = "Internal server error";
	public static String HTTP_REQUESTTIMEOUT = "Request timeout";
	public static String HTTP_SERVICEUNAVAILABLE = "Service unavailable";
	public static String HTTP_UNPROCESSABLEENTITY = "Unprocessable Entity";
	public static String HTTP_CONFLICT = "Conflict";
	//Auth endpoint related errors
	public static String errorApiIsUnavailable = "API is unavailable. Please try again later.";
	public static String errorUnableToGenerateJwt = "Unable to generate JWT";
	public static String errorInValidClientId = "Invalid client id";
	public static String errorInValidGrantType = "Invalid grant type";
	public static String errorInValidClientSecret = "Invalid client secret";
	public static String errorInValidAuthenticationScheme = "Invalid authentication scheme";
	public static String errorInValidToken = "Invalid token";
	public static String errorTokenExpired = "Token is expired";
	public static String errorInvalidPin = "Invalid client_pin";
	
	//Booking endpoint related errors
	public static String errorInValidBookingId = "Invalid booking id";
	public static String errorInValidExternalUrl = "Invalid external URL.";
	public static String errorInValidAddress = "Address is required for Home Visit.";
	public static String errorInValidMandatory = "Missing mandatory booking data. This should include patient and contact details as well.";
	public static String errorInvalidModifiedDateFormat = "Invalid modified date format. Date must be a valid date with format YYYYMMDD. Only numbers are allowed.";	
	public static String errorInValidAppointmentDateTimeFormat = "Invalid appointment date or time. Date must be a valid date with format YYYYMMDD. Only numbers are allowed. Time format should be HH:mm.";
	public static String errorInValidAppointmentTimeFormat = "Invalid time format. Time format should be HH:mm.";
	public static String errorInValidAppointmentDateInPast = "Appointment date cannot exceed 72 hours in the past.";
	public static String errorInValidAppointmentDateInPastUpdate = "Appointment date cannot exceed 0 hours in the past to update the booking.";
	public static String errorInValidPatientDateOfBirthFormat = "Invalid patient's date of birth format. Date must be a valid date with format YYYYMMDD. Only numbers are allowed.";
	public static String errorInValidClinicCode = "Invalid clinic code.";
	public static String errorInValidCostcentre = "Invalid cost centre.";
	public static String errorInValidLanguageCode = "Invalid language code.";
	public static String errorInValidLanguageMatch = "Invalid language.";
	public static String errorInValidEmailProf = "Invalid Email of professional.";
	public static String errorInValidFnameProf = "Professional first name is required.";
	public static String errorInValidFnameLnamePatient = "Patient's first name and last name is required.";
	public static String errorInValidBooking = "There is an existing booking for this patient within the booking date-time you have entered.";
	public static String errorInValidFnameContact = "Contact's first name is required.";
	public static String errorInValidEmailContact = "Invalid Email of contact.";
	public static String errorInValidEmailInter = "Invalid Email of interpreter.";
	public static String errorInValidAccrLevel = "Invalid Accreditation Level. Only these levels are allowed: ";
	public static String errorInValidAddressFormat = "Address Line 1, Suburb, State Code, Post Code are required for " + homeVisit + ".";
	public static String errorInValidJsonRequest = "Invalid JSON request.";
	public static String errorInvalidExternalRef = "External reference is mandatory and should be not be already existing.";
	public static String errorInValidInterpreterGenderPrefMale = "Interpreter gender must be a male.";
	public static String errorInValidInterpreterGenderPrefFemale = "Interpreter gender must be a female.";
	public static String errorInValidInterpreterGenderPrefUnknown = "Interpreter gender must be unknown.";
	public static String errorInValidInterpreterGenderPrefindeterminate = "Interpreter gender must be indeterminate.";
	public static String errorInValidNonCompletionReason = "Invalid non-completion reason. Use one of the reasons from the non-completion reasons list provided by Primaxis.";
	public static String errorInValidUserId = "Invalid user id.";
	public static String errorInvalidLanguageChange = "Language update not allowed. Bookings with status 'New' and 'Rejected by Agency' are allowed only.";
	
	//Ashish - Feb 2022 - Duplication Check for PMS ID
	public static String errorInValidPmsID = "<pmsid> for the booking is mandatory and should not be already existing.";
	public static String errorInValidImsID = "This <pmsid> is used for these existing bookings: ";

	public static String prefixNok = "NOK";
	public static String keywordClientName = "ClientName";
	public static ArrayList<String> allowedURLEndPoints = new ArrayList<String>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		{
			add(serviceNounBookings); 
			add(serviceNounUsers); 
			add(serviceNounNotifications);
			add(serviceNounLanguages); 
			add(serviceNounClinics); 
			add(serviceNounBookingLabels);
			add(serviceNounCheckin);
			add(serviceNounCheckout);
			add(serviceNounCheckinStatus);
			add(serviceNounPins);
			add(serviceNounMeetings);
			add(serviceNounQuestions);
			add(serviceNounCDR);
		}
	};
	
	//Patient form related errors
	public static String errorMissingID = "ID is mandatory.";
	public static String errorMissingPatientFirstName = "Patient first name is mandatory.";
	public static String errorMissingPatientUrNumber = "Patient UR Number is mandatory.";
	public static String errorMissingLanguageCode = "Language code is mandatory.";
	public static String errorInValidID = "Invalid ID. Only alphabets, integers and special characters are allowed.";
	public static String errorInvalidFirstName = "Invalid first name. Only alphabets, integers and special characters are allowed.";
	public static String errorInvalidURNumber = "Invalid UR number. Only alphabets and integers are allowed.";
	public static String errorInValidFacility = "Invalid facility. Only alphabets and integers are allowed.";
	public static String errorInValidLastName = "Invalid last name. Only alphabets, integers and special characters are allowed.";
	public static String errorInValidSecondName = "Invalid second name. Only alphabets, integers and special characters are allowed.";
	public static String errorInValidTitle = "Invalid title. Only alphabets, integers and special characters are allowed.";
	public static String errorInValidGender = "Invalid gender. Must be one of: M, F, I, U.";
	public static String errorInValidLanguage = "Invalid language. Only alphabets, integers and special characters are allowed.";
	public static String errorInValidLength = "Invalid length of string. Max length is 255 for id, facility, title, name, language, relationship.";
	public static String errorInValidPhoneNumber = "Invalid phone number. String of numbers is allowed.";	
	public static String errorInValidEmailPatient = "Invalid Email of patient.";
	public static String errorInValidGenderPrefCode = "Invalid gender preference code. Must be one of: M, F, NP.";
	public static String errorInValidGenderPrefPriority = "Invalid gender preference priority. Must be one of: Must be, Must not be, Prefer to be, Prefer not to be.";
	public static String errorInValidStateCode = "Invalid state code. Must be one of: ACT, NSW, NT, QLD, SA, TAS, VIC, WA.";
	public static String errorInValidPostCode = "Invalid post code. Only numbers are allowed.";

	//Booking Statuses
	public static String bookingStatusNew = "requested";
	public static String bookingStatusAmended = "updated";
	public static String bookingStatusSubmitted = "submitted_agency";
	public static String bookingStatusBooked = "booked";
	public static String bookingStatusCancelled = "cancelled";
	public static String bookingStatusCancellationRequested = "cancellation_req";
	public static String bookingStatusUnmet = "unmet";
	public static String bookingStatusRejected = "unmet_pending";
	public static String bookingStatusWaitingAcknowledgment = "agency_waiting";
	public static String bookingStatusDefault = bookingStatusSubmitted;
	public static String bookingStatusReallocated = "reallocated";
	public static String bookingStatusRescheduling = "Rescheduling";
	public static String bookingStatusServiceDelivered = "service_delivered";
	public static String bookingStatusNotDelivered = "Not_Delivered";
	public static String bookingStatusPending = "allocatedInterpreter";
	public static String bookingStatusCompleted = "completed";
	public static String bookingStatusDataBookingError = "api_create_error";
	
	//Display Status	
	public static String bookingStatusCancelledDisplay = "Cancelled";
	public static String bookingStatusAmendedDisplay = "Amended";
	public static String bookingStatusBookedDisplay = "Booked";
	public static String bookingStatusPendingDisplay = "Pending";
	
	/**
	 * Booking statuses allowed from get booking details end point for agency
	 * These statuses should be live/active status only
	 */
	public static ArrayList<String> allowedStatusForLspToGetBooking = new ArrayList<String>() {
		private static final long serialVersionUID = 1L;
		{ 
			add(Constants.bookingStatusWaitingAcknowledgment);
			add(Constants.bookingStatusSubmitted);
			add(Constants.bookingStatusBooked);
			add(Constants.bookingStatusAmended);
		}
	};
	
	public static String defaultGenderPref = "NP";
	public static String defaultPatientGender = "U";
	public static String defaultContactType = "Mobile";
	public static String errorUnableToAssignAgencyID = "Unable to assign provider's ID. Contact <a href='mailto:ims.support@primaxis.com;subject:API request error: unable to assign agency DocKey'>Primaxis</a>";
	public static String InterpreterTypeAgency = "Agency";
	public static String InterpreterTypeInHouse = "In-House";
	public static String errorInvalidWaitingDuration = "Invalid waiting duration format. Must be a valid duration in minutes.";
	public static String errorInvalidDuration = "Invalid duration format. Must be a valid duration in minutes.";
	public static String errorInvalidPriority = "Invalid priority for gender or ethnicity preference details. Valid values are: Must be, Must not be, Prefer to be, Prefer not to be.";
	public static String errorInvalidEthnicityCode = "Invalid ethnicity.";
	public static String keywordNameSharedClientSecret = "sharedClientSecret";
	public static int maxNumberOfBookingsToBeReturned = 5000;
	public static int defaultNumberOfBookingsToBeReturned = 1000;
	public static int maxNumberOfNotificationsToBeReturned = 5000;
	public static int defaultNumberOfNotificationsToBeReturned = 1000;
	public static String defaultSearchSortBy = "Ascending";
	public static String formCBN = "CBN";
	public static String errorIrrelevantBookingId = "This booking is no longer required.";
	public static String defaultSource = "IMS";
	public static String defaultSourceDisplay = "IMS";
	public static String sourcePMS = "PMS";
	public static String sourcePMSDisplay = "PMS";
	
	static String commaseparatedlistAllowedActions = allowedActions.toString().replace("[", "").replace("]", "").replace(" ", "");
	static String commaseparatedlistAllowedActionsAccessAll = ALLOWED_ACTION_TO_ACCESS_ALL.toString().replace("[", "").replace("]", "").replace(" ", "");
	static String commaseparatedlistAllowedActionsUser = ALLOWED_ACTION_TO_USER_ACCESS.toString().replace("[", "").replace("]", "").replace(" ", "");
	
	public static String errorInvalidAction = "Invalid action. Only " + commaseparatedlistAllowedActions + " and completed actions are allowed.";
	public static String errorInvalidActionToAccessAll = "Invalid action. Only " + commaseparatedlistAllowedActionsAccessAll + " actions are allowed.";
	public static String errorInvalidActionToUserAccess = "Invalid action. Only " + commaseparatedlistAllowedActionsUser + " actions are allowed.";
	
	//Action end-point related errors
	public static String errorInValidBookingStateAccepted = "Status update not allowed. Bookings with status 'Booked' are allowed only."; 
	public static String errorInValidBookingStateReceived = "Status update not allowed. Bookings with status 'Waiting for acknowledgement' and 'Cancelled' are allowed only."; 
	public static String errorInValidBookingStateAllocated = "Booking is in a state that cannot be allocated. Bookings with status 'Submitted to Agency' and 'Booked' are allowed only."; 
	public static String errorInValidBookingStateUnAllocated = "Booking is in a state that cannot be unallocated. Bookings with status 'Submitted to Agency' and 'Booked' are allowed only."; 
	public static String errorInValidBookingStateForPast = "Updates are not allowed on past bookings.";
	
	//ASHISH - March 2022 - WH
	//Allow cancellation on all the statuses except completed (even of new, booked, amended and other status. Changed the msg related to it.)
	//public static String errorInValidBookingStateCancelled = "Booking is in a state that cannot be cancelled. Bookings with status 'Waiting for acknowledgement', 'Submitted to Agency' and 'Booked' are allowed only.";
	public static String errorInValidBookingStateCancelled = "Booking is in a state that cannot be cancelled. A completed, service-delivered or already cancelled booking cannot be cancelled.";
	public static String errorInValidBookingStateUpdated = "Booking is in a state that cannot be updated. Bookings with status 'Waiting for acknowledgement', 'Submitted to Agency' and 'Booked' are allowed only.";
	public static String errorInValidBookingStateRejected = "Booking is in a state that cannot be rejected. Bookings with status 'Waiting for acknowledgement', 'Submitted to Agency' and 'Booked' are allowed only.";
	public static String errorMissingInterpreterDetails = "Missing mandatory interpreter details. First name or last name are required.";
	public static String errorUnableToCancelCombinedBooking = "Combined bookings cannot be cancelled.";
	public static String errorUnableToUpdateCombinedBooking = "Combined bookings cannot be updated.";
	public static String errorBookingLocked = "Booking is being updated. Please try again later.";
	public static String errorInValidBookingStateCompleted = "Booking is in a state that cannot be completed. Bookings with status <status> are allowed only.";
	public static String errorInValidBookingStateForFuture = "Completion is not allowed on future bookings.";
	public static String errorInValidBookingStateForAction = "Updates are not allowed 24 hours after the start date time of the booking.";

	public static final String regex24Hour = "(?:[0-1][0-9]|2[0-4]):[0-5]\\d";
	
	//Date and time formats
	public static String timeFormat24Hour = "HH:mm";
	
	
	//Keyword names
	public static final String KW_ALLOWED_CONSUMER_ACCESS_ALL = "AllowedLSPsToAccessAll";
	public static final String KW_ALLOWED_CONSUMER_CREATE_IGNORE_VALID = "AllowedLSPsToCreateIgnoreValid";
	public static final String KW_IMS_CORE_NC_REASONS = "IMS_Core_NC_Reasons_List";
	public static final String KW_IMS_REQUESTOR_NC_REASONS = "IMS_Requestor_Core_NC_Reasons_List";
	public static final String KW_IMS_INTERPRETER_NC_REASONS = "IMS_NC_Reasons_List_Interpreter";
	public static final String KW_IMS_LSP_NC_REASONS = "AllowedNonCompletionReasons";
	public static final String KW_STATUS_REALLOCATED_INTERPRETER = "BOOK_Status_reallocated_Api_Interpreter";
	public static final String KW_DYNAMIC_LABELS = "BOOK_Dynamic_FieldLabels_API";	
	public static final String KW_USER_DOCKEY_UNIQUE = "USER_Dockey_Unique";	
	public static final String KW_USER_LSP_DOCKEY_PATTERN = "USER_LSP_DocKey_Pattern";
	public static final String KW_USER_LSP_HEADING = "USER_LSP_Heading";
	public static final String KW_USER_LSP_CONFIG = "LSP_USER_Config";
	public static final String KW_OVERRIDE_REQU_DETAILS_FROM_CLINIC = "OverrideREQUDetailsFromClinic";
	//Ashish - March 2022 - Keyword to configure all the allowed status of IMS booking for cancellation call - WH
	public static final String KW_ALLOWED_UPDATE_STATUS = "API_AllowUpdate_Status";
	public static final String KW_ONDEMAND_CONFIG = "OnDemand_Config";
	public static final String KW_ALLOWED_UPDATE_CANCEL_BOOKING = "AllowUpdateOnCancelledBooking";
	public static final String KW_ALLOW_MULTI_BOOKINGS = "AllowMultiBookings";
	public static final String KW_ALLOW_HIDE_PATIENT_ADDRESS_FIELDS = "AllowHidePatientAddressFields";
	public static final String KW_LIST_PATIENT_ADDRESS_SHOW = "ListPatientAddressShow";
	public static final String KW_DYNAMIC_FIELD_VALUE_REPLACE = "BOOK_Dynamic_Field_Replace_Value";
	public static final String KW_INTERPRETER_SELECTION_CONFIG = "Interpreter_Selection_Config";
	public static final String KW_COMPLIANCE_MANDATORY = "Compliance_Mandatory";
	public static final String KW_ALLOW_INTERPRETER_SERVICE_DELIVERED = "AllowInterpreterServiceDelivered";
	public static final String KW_MOBILE_ALLOWEDSTATUSTOCOMPLETE = "Mobile_AllowedStatusToComplete";
	public static final String KW_MOBILE_BOOKEDSTATUSOVERRIDE = "Mobile_BookedStatusOverride";
	public static final String KW_GLOBAL_CONTACT_DOCKEY = "OnCall_GlobalContactDocKey";
	public static final String KW_ALLOW_GLOBAL_CONTACT = "AllowGlobalContact";
	public static final String KW_API_USE_REQU_NAME = "USE_API_REQU_NAME";
	public static final String KW_WEBHOOKS_CONFIG = "Webhooks_Config";
	public static final String KW_API_SKIP_AMEND_STATUS = "API_SkipAmendStatus";
	
	//Agents
	public static final String AGENT_RESOURCE_CALENDAR_JSON = "agtResourceCalendarJSON";
	public static final String AGENT_USER_UPDATE_BOOK = "agtUSER_UpdateBOOK_OnSave";
	public static final String AGENT_SEND_WEBHOOK = "agt_Send_Webhook_Data";

	//Views
	public static String viewLanguageCodes = "language-codes";
	public static String viewClinicCodes = "clinic-codes";
	public static final String VIEW_USER_LSP_FULLNAME = "USER_LSP_NameList";
	public static final String VIEW_USER_LSP_TA = "USER_LSP_TA";
	public static final String VIEW_USER_BY_DOCKEY = "USER_ByDocKey";
	public static final String VIEW_USER_CONCAT_COMPLIANCE = "luUserConcatCompliance";
	public static final String VIEW_ATTACHMENTS_BY_PARENT_DOCKEY = "ATT_ByParentDocKey";
	
	//Notification Type
	public static final String NOTIFICATION_TYPE_EMAIL = "EML";
	public static final String NOTIFICATION_USER_TYPE_INTERPRETER = "INT";
	public static final String NOTIFICATION_USER_TYPE_COORDINATOR = "CRD";
	public static final String NOTIFICATION_USER_TYPE_REQUESTOR= "REQ";
}
