package com.primaxis.ims.utils;

import java.util.Vector;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;
import lotus.domino.DocumentCollection;

/**
 * Handles the IMS Patient Data database
 * lets you get keywords or documents from the 
 * database
 * 
 * @author Baljit Karwal
 *@since 12 April 2019
 */
public class ImsPatientDataDb {

	//must push session into this STATIC variable from the NOTES AGENT so it's available to the class when needed
	private ImsHomeDb ImsHomeDb;
	private Database db;
	private KeywordHandler kwHandler;
	private View luView;
	
	public ImsPatientDataDb (Session session) throws NotesException
	{
		//use a keyword handler for the current DB to get the keyword for IMS database and IMS Server
		KeywordHandler kwHandler = new KeywordHandler(session.getCurrentDatabase());
		String server = session.getCurrentDatabase().getServer();
		String remoteServer = kwHandler.getKeywordValueString("DB_FilePath_IMS_Server");
		if (!remoteServer.equals("-")) {
			server = remoteServer;
		}
		ImsHomeDb = new ImsHomeDb(session);
		KeywordHandler kwHandlerIMSDB = new KeywordHandler(ImsHomeDb.getDatabase());
		if (!remoteServer.equals("-")) {
			server = remoteServer;
		}
		String dbPath = kwHandlerIMSDB.getKeywordValueString("DB_FilePath_Patient");
		db = session.getDatabase(server, dbPath);
	}
	
	public String getDatabaseTitle ()
	{
		try {
			return db.getTitle();
		} catch (NotesException e) {
			return "";
		}
	}
	
	//get the database
	public Database getDatabase() {
		return db;
	}
	
	/**
	 * Retrieve existing documents for patient
	 * 
	 * @param patientID
	 * @return
	 */
	@SuppressWarnings({ "unchecked"})
	public Document getPatientDocumentByID (String patientID)
	{
		String luViewName;
		Vector luKeys = new Vector();
		try {
			luViewName = "vwAllByID";			
			View luView = db.getView(luViewName);
			if (luView == null){
				return null;
			}
			luKeys.add(patientID);
			DocumentCollection dc = luView.getAllDocumentsByKey(luKeys, true);
			Document patientDoc = dc.getFirstDocument();			
			luView.recycle();
			return patientDoc;			
		} catch (NotesException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/*public void recycle ()
	{
		kwHandler.recycle();
		if (db != null)
		{
			try {
				db.recycle();
			} catch (NotesException e) {
				//don't need to do anything
			}
		}
		if (luView != null)
		{
			try {
				luView.recycle();
			} catch (NotesException e) {
				//don't need to do anything
			}
		}
	}*/
}
