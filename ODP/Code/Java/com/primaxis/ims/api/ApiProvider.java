package com.primaxis.ims.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.*;
import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;

import com.primaxis.ims.utils.Log;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.ImsHomeDb;
import com.primaxis.ims.utils.KeywordHandler;
import com.primaxis.ims.model.User;
import com.primaxis.ims.utils.Constants;

/**
 * Base Abstract Class for REST Web Service Provider
 * 
 * @author	BKarwal
 * @since	21 December 2018
 */
public abstract class ApiProvider {
	
	public int httpResponse = 200;
	public boolean success = true;
    public boolean error = false;
    public String errorMessage;
    public int errorCode;
    public boolean isInValid = false;
    public ArrayList<Integer> messageCode = new ArrayList<Integer>();
    public Map<Integer, String> invalidDataString = new HashMap<Integer, String>();
	protected Log Log;
	protected KeywordHandler kwHandler;
   	protected Database db;
	protected ImsHomeDb ImsHomeDb;
	protected KeywordHandler kwHandlerIMSDB;
	protected HttpServletResponse response;
	protected JsonObject requestJson;
	protected JsonObject responseJson;
    protected String responseJsonString;
    protected JsonObject dataJson = new JsonObject();
    protected JsonArray messageArray = new JsonArray();
    protected String who = Constants.who;
    protected String requestId;
    protected String reqServiceID;
    protected Session session;
    public String interpreterDocKey;
    public String interpreterFullName;
    protected String languageServiceProvider;
	public boolean isLspAllowedToAccessAll = false;
	public boolean isLSPAllowedToCreateIgnoreValid = false;
	private boolean isMobileApp = false;
	protected boolean isUserAllowed = false;
	protected Map<String, Object> params;
	protected String consumerName;
	protected String globalDateFormat;
	protected String globalTimeFormat;
	protected String nowDateTimeFormatted;
	protected DateTime nowDateTime;
	private String userId = null;
	
	
	protected ApiProvider(Database db, JsonObject jsonObjRequest, Log apiLog, HttpServletResponse HttpServletResponse, Session session, String languageServiceProvider, String consumerName, Map<String, Object> params) throws NotesException, IOException
	{	
		try { 
			this.db = db;
			response = HttpServletResponse;
			requestJson = jsonObjRequest;
			kwHandler = new KeywordHandler (db);
			ImsHomeDb = new ImsHomeDb (session);
			kwHandlerIMSDB = new KeywordHandler(ImsHomeDb.getDatabase());
			this.Log = apiLog;
			this.session = session;
			this.params =  params;
			this.consumerName = consumerName;
			this.globalDateFormat = kwHandlerIMSDB.getGlobalDateNotesFromFormat();
			this.globalTimeFormat = kwHandlerIMSDB.getGlobalTimeFormat();
			nowDateTime = kwHandlerIMSDB.dateOffset(session.createDateTime("Today"));
			nowDateTimeFormatted = Globals.formatDate(nowDateTime, globalDateFormat + " " + globalTimeFormat);
			isLspAllowedToAccessAll = kwHandlerIMSDB.isConsumerAllowedToAccessAll(consumerName);
			isLSPAllowedToCreateIgnoreValid = kwHandlerIMSDB.isConsumerAllowedToCreateIgnoreValid(consumerName);
			if (params.get("userId") != null) {
				Document userDoc = ImsHomeDb.getValidUserDocByUsername(((String) params.get("userId")).trim());
				userId = userDoc.getItemValueString("DocKey"); //update userId to DocKey as user can login by id or email
				if (userDoc != null) {
					isUserAllowed = true;
				}
			}
			this.languageServiceProvider = languageServiceProvider;
			if (isUserAllowed && userId != null) { //If not an LSP but userId
				interpreterDocKey = userId;
				User user = new User(session, interpreterDocKey);
				interpreterFullName =  user.getFullName();
				setMobileApp(true);
			} else if (languageServiceProvider != null && !isLspAllowedToAccessAll && !isLSPAllowedToCreateIgnoreValid) { 
				interpreterFullName = languageServiceProvider;
				interpreterDocKey = kwHandlerIMSDB.getAgencyDocKey(languageServiceProvider);
			} 
		} catch (Exception e) {
			this.Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
			return;
		}
	}
	
	abstract public String generateResponse() throws NotesException;

	//Clean up all our Notes Objects
	public void recycle () {
		if (Log != null){
			Log.recycle();
		}
		if (kwHandlerIMSDB != null) {
			kwHandlerIMSDB.recycle();
		}
		if (kwHandler != null) {
			kwHandler.recycle();
		}
		if (db != null) {
			try {
				db.recycle();
			} catch (NotesException e) {
				//don't need to do anything
			}
		}
	}

	/**
	 * @return the isMobileApp
	 */
	public boolean isMobileApp() {
		return isMobileApp;
	}

	/**
	 * @param isMobileApp the isMobileApp to set
	 */
	public void setMobileApp(boolean isMobileApp) {
		this.isMobileApp = isMobileApp;
	}
}
