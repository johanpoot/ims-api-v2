package com.primaxis.ims.api;

import com.primaxis.ims.controller.BookingController;
import com.primaxis.ims.controller.ClinicController;
import com.primaxis.ims.controller.LanguageController;
import com.primaxis.ims.controller.MeetingController;
import com.primaxis.ims.controller.NotificationController;
import com.primaxis.ims.controller.PinController;
import com.primaxis.ims.controller.UserController;
import com.primaxis.ims.controller.CDRController;
import com.primaxis.ims.controller.QuestionController;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import lotus.domino.*;
import com.primaxis.ims.utils.Log;
import com.primaxis.ims.utils.Constants;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.ImsHomeDb;
import com.primaxis.ims.utils.KeywordHandler;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.*;

import com.ibm.domino.services.ServiceException;
import com.ibm.domino.services.rest.RestServiceEngine;
import com.ibm.xsp.extlib.component.rest.CustomService;
import com.ibm.xsp.extlib.component.rest.CustomServiceBean;
import com.ibm.xsp.extlib.util.ExtLibUtil;

/**
 * API by Primaxis (IMS) to provide options to other LSP/agencies/clients to send/receive data to/from IMS
 * Includes API related to Bookings and Patient - Patient API here is not in use.
 * Updated - 10 June 2020 - Disabled/commented code related to auth endpoint as it will be handled by API gateway - WebMethods API
 * @author BKarwal
 * @since 21 December 2018
 *
 */
public class Api extends CustomServiceBean {
	private String username;
	private String userId;
	private boolean isValidUserLogin = false;
	private boolean isImsConnect = false;
    String attachments = null;
	private Document userDoc;
	private String endpoint = null;
	private String attachmentId = null;
	private Boolean success = true;
	Log apiLog = null;
    String reqId = null;

	@Override
    public void renderService(CustomService service, RestServiceEngine engine) throws ServiceException {
		
		Session session = null;
		ApiProvider apiProvider = null;
	    ArrayList<Integer> messageCode = new ArrayList<Integer>();
		HttpServletResponse response = engine.getHttpResponse();
        int httpResponse = 200;
        String responseJson = null;
    	try {
    		HttpServletRequest request = engine.getHttpRequest();
	        session = ExtLibUtil.getCurrentSessionAsSignerWithFullAccess();
			Database currentDb = session.getCurrentDatabase();
			ImsHomeDb ImsHomeDb = new ImsHomeDb (session);
			KeywordHandler kwHandlerIMSDB = new KeywordHandler(ImsHomeDb.getDatabase());
			//Check request headers
    		String method = request.getMethod(); //GET, POST, PUT, DELETE, PATCH
	        String contentType = request.getHeader("Content-Type");
	        //Split URL path
	        String[] arrPath = request.getPathInfo().split("/");
	        String reqServiceNoun = "";
	        String ipAddress = request.getRemoteAddr();
			JsonObject jsonObjRequest = new JsonObject();
			String jsonString = "";
	        if (Globals.indexInBound(arrPath, 1)) { //This is the noun part for the request //Can be bookings, users, checkin, checkout, checkin-status, notifications, patients
	        	reqServiceNoun = arrPath[1].trim();
	        } 
	        if (Globals.indexInBound(arrPath, 2)) { //This is the id coming after noun
	        	reqId = arrPath[2].trim();
	        }    
			apiLog = new Log(session);
			apiLog.createAPILog(method, reqServiceNoun);
			apiLog.updateLogField("IPAddress", ipAddress);
			apiLog.updateLogField("ID", reqId);
	        String consumerName = request.getHeader("Caller");
	        String clientName = request.getHeader("ClientName");
	        apiLog.updateLogField("Caller", consumerName);
	        Map<String, Object> reqParams = new HashMap<String, Object>();
	        Map<String, Object> params = new HashMap<String, Object>();
	        attachments = request.getHeader("attachments");	  
	        reqParams = request.getParameterMap();
        	params.put("userId", Globals.getRequestParameter(reqParams, "userId"));
        	params.put("limit", Globals.getRequestParameter(reqParams, "limit"));
        	params.put("futureBookings", Globals.getRequestParameter(reqParams, "futureBookings"));
        	params.put("modifiedSince", Globals.getRequestParameter(reqParams, "modifiedSince"));
        	params.put("action", Globals.getRequestParameter(reqParams, "action"));
        	params.put("deviceId", Globals.getRequestParameter(reqParams, "deviceId"));
        	params.put("appointmentStartDate", Globals.getRequestParameter(reqParams, "appointmentStartDate"));
        	params.put("appointmentStartDateGt", Globals.getRequestParameter(reqParams, "appointmentStartDateGt"));
        	params.put("appointmentStartDateLt", Globals.getRequestParameter(reqParams, "appointmentStartDateLt"));
        	params.put("appointmentStartDateGte", Globals.getRequestParameter(reqParams, "appointmentStartDateGte"));
        	params.put("appointmentStartDateLte", Globals.getRequestParameter(reqParams, "appointmentStartDateLte"));
        	//If files, incoming data type is form-data so need o retreive params in different way
	        if (attachments != null && attachments.equalsIgnoreCase("true")) {
	        	params = request.getParameterMap();
	        	Enumeration<String> paramNames = request.getParameterNames();
        		String paramNameAttachmentId = Globals.getRequestParameterNames(paramNames, "attachmentId");
        		String paramNameUser = Globals.getRequestParameterNames(paramNames, "userId");
	        	if (paramNameUser != null && request.getParameter(paramNameUser) != null) {
	        		params.put("userId", request.getParameter(paramNameUser).trim());
	        	}
	        	if (paramNameAttachmentId != null && request.getParameter(paramNameAttachmentId) != null) {
	        		attachmentId = request.getParameter(paramNameAttachmentId).trim();
	        		params.put("attachmentId", attachmentId);
	    			apiLog.updateLogField("Attachment_Id", attachmentId);
	        	}
	        }
   	        //For interpreter/user access, user is allowed to have access to their bookings
			String authorizationBasic = request.getHeader("AuthorizationBasic"); //Custom Header for user authorization and attachments
			if (authorizationBasic != null) {
				userDoc = ImsHomeDb.getValidUserDocByBase64(authorizationBasic);
				if (userDoc != null) {
					userId = userDoc.getItemValueString("DocKey"); //update userId to DocKey as user can login by id or email
					params.put("userId", userId);
					isValidUserLogin = true;
				}
			} else if (reqServiceNoun.equalsIgnoreCase(Constants.serviceNounUsers) && authorizationBasic == null) {
				isValidUserLogin = false;
			} else {
				if (params.get("userId") != null) {
					username = params.get("userId").toString().trim();
				}
				if (username != null && username != "") {
					userDoc = ImsHomeDb.getValidUserDocByUsername(username);
					if (userDoc != null) {
						userId = userDoc.getItemValueString("DocKey"); //update userId to DocKey as user can login by id or email
						isValidUserLogin = true;
					} 
				}
			}
			if (consumerName.equals("IMS Connect")) {
				isImsConnect = true;
			}
	        Globals.printCode("*************Starting IMS API V2.9F10 on " + request.getServerName() + " for clientName: " + clientName + " for consumer: " + consumerName + 
	        		" for: " + ((method.equalsIgnoreCase("POST") || method.equalsIgnoreCase("PATCH")) ? "Update ": " Get") + 
	        		(attachments != null? " attachment" + (attachmentId != null? " for attachmentId: " + attachmentId : "") + " for" : "") + 
	        		(reqId != null?" booking: " + reqId : "")  +  
	        		(userId != null?" user: " + userId : "") + "**" + reqServiceNoun + "************");
	        //For non-LSP access, LSP is allowed to have access to all bookings
			boolean isConsumerAllowedToAccessAll = kwHandlerIMSDB.isConsumerAllowedToAccessAll(consumerName);
	        String languageServiceProvider = Globals.getLSPByConsumerName(consumerName, session, kwHandlerIMSDB); //For non-LSP this might not exist in DB
	        apiLog.updateLogField("languageServiceProvider", languageServiceProvider);
			apiLog.updateLogField("User_Id", userId);
	        //Check if LSP is enabled to access API
	        boolean isLspEnabledToAccessApi = ImsHomeDb.isLspEnabledToAccessApi(consumerName);
	        String requestAction = null;
	        Boolean isInValid = false;
	        if (!isImsConnect && !isConsumerAllowedToAccessAll && (!isLspEnabledToAccessApi || consumerName == null || languageServiceProvider == null) && !isValidUserLogin) {
				isInValid = true;
				httpResponse = 401;
				responseJson = generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
			} else {
				if (attachments == null) {
			  		ServletInputStream inputStream = request.getInputStream();
			  		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			  		BufferedReader reader = new BufferedReader(inputStreamReader);
					StringBuilder buffer = new StringBuilder();
					String line = null;  
					while ((line = reader.readLine()) != null) {  
						buffer.append(line + "\n");  
				    }
					reader.close();
					if (!buffer.equals("")) {
						jsonString = buffer.toString();
					}
					if (!jsonString.equals("")) {
						jsonObjRequest = (JsonObject) Jsoner.deserialize(jsonString);
						apiLog.addRequestToLog(Jsoner.serialize(jsonObjRequest));
					}
		        }
				apiLog.updateLogField("API_REQUEST_BY", consumerName);
				requestAction = (String) request.getParameter("action");
		        if (Constants.allowedURLEndPoints.contains(reqServiceNoun)) {
		        	//Check HTTP method
					if (Constants.allowedHttpMethods.contains(method)) {
						responseJson = doAction(response, jsonObjRequest, method, requestAction, params, contentType, apiProvider, reqServiceNoun, 
								reqId, currentDb, apiLog, isInValid, messageCode, httpResponse, success, jsonString, session, languageServiceProvider, consumerName, kwHandlerIMSDB);							
						if (responseJson == null || responseJson.equals("")) {			
							isInValid = true;
							httpResponse = 503;
							apiLog.updateLogField("503", "allowedMethod " + method +"=false");
							responseJson = generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
						}
					} else {
						isInValid = true;
						httpResponse = 405;
						responseJson = generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
					}
		        } else {
			        isInValid = true;
					httpResponse = 400;
					messageCode.add(4000);
					responseJson = generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
		        }
			}
    		response.setHeader("Cache-Control", "no-cache");
    		response.setCharacterEncoding("utf-8");
	    	response.setStatus(httpResponse);
    		response.setContentType("application/json");
    		response.getWriter().write(responseJson);
			response.getWriter().close();
        	response.flushBuffer();
			apiLog.addResponseToLog(responseJson.toString());
			apiLog.saveAPILog();
    		Globals.printCode("*************Ending IMS API V2.9F10 on " + request.getServerName() + " for consumer: " + consumerName + " for endpoint: " + endpoint + "********************");
    	} catch (IOException e) {
    		Globals.printException(e);
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		} catch (Exception e) {
    		Globals.printException(e);
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
    	} finally {
			recycle(apiProvider);
	    	try {
				session.recycle();
			} catch (NotesException e) {
	    		Globals.printException(e);
				Log.writeLine ("Unable to recycle session - " + e.getMessage());
			}
    	}

    }
    
    /**
	 * Generate Response String
     * @param messageCode 
	 */
	public String generateResponse(HttpServletResponse response, Boolean isInValid, ArrayList<Integer> messageCode, int httpResponse, Session session, KeywordHandler kwHandlerIMSDB) {
		String responseJson = null;
		try {
			JsonObject responseJsonObj = new JsonObject();
			String requestId = Globals.requestID();
			JsonObject dataJson = new JsonObject();
			JsonArray messageArray = new JsonArray();
			if (messageCode.size() > 0) {
				for (int code : messageCode) {
					JsonObject ob = new JsonObject();
					ob.put(Integer.toString(code), Globals.getMessageFromCode(code, session, kwHandlerIMSDB));
					if (!messageArray.contains(ob)) {
						messageArray.add(ob);
					}
				}
			}
			String httpResponseMessage = Globals.getHTTPResponseMsg(httpResponse);
			if (isInValid) {
				success = false;
			}
			responseJsonObj.put("code", httpResponse);
			responseJsonObj.put("text", httpResponseMessage);
			responseJsonObj.put("success", success);
			responseJsonObj.put("requestId", requestId);
			if (messageArray.size() > 0) {
				dataJson.put(isInValid ? "errors" : "messages", messageArray);
			}
			if (!dataJson.isEmpty()) {
				responseJsonObj.put("data",  dataJson);
			}
			if (responseJsonObj instanceof Jsonable) {
				responseJson = (String) Jsoner.serialize(responseJsonObj);
			}
			if (isInValid) {
				apiLog.logAPIError(responseJson, reqId, httpResponseMessage);
			}
		} catch (Exception e) {
			Globals.printException(e);
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}
		return responseJson;
	}
    
    /**
     * 
     * @param response
     * @param jsonObjRequest
     * @param method
     * @param requestAction
     * @param params
     * @param contentType
     * @param apiProvider
     * @param reqServiceNoun
     * @param reqId
     * @param currentDb
     * @param apiLog
     * @param isInValid
     * @param messageCode
     * @param httpResponse
     * @param success
     * @param jsonString
     * @param session
     * @param languageServiceProvider
     * @param consumerName
     * @param kwHandlerIMSDB
     * @return
     * @throws Exception
     * @throws IOException
     * @throws NotesException
     */
    public String doAction(HttpServletResponse response, JsonObject jsonObjRequest, String method, String requestAction, Map<String, Object> params, 
    		String contentType, ApiProvider apiProvider, String reqServiceNoun, String reqId, Database currentDb, Log apiLog, 
    		Boolean isInValid, ArrayList<Integer> messageCode, int httpResponse, Boolean success, String jsonString, Session session, 
    		String languageServiceProvider, String consumerName, KeywordHandler kwHandlerIMSDB) throws Exception, IOException, NotesException {
		String responseJson = null;
		try {
			if (!isInValid) {
				//Check content type header, it should be application/json or multipart 
				//Below data needs to be validated only for end-points which require JSON data only such as for POST/PUT/PATCH method for Create/Update booking or add attachments
				if (method.equals(Constants.HTTP_POST) || method.equals(Constants.HTTP_PUT) || method.equals(Constants.HTTP_PATCH)) {
			        if (contentType == null || contentType.equals("") || (!contentType.equals(Constants.contentTypeJson) && !contentType.startsWith(Constants.contentTypeFormMultipart))) {
						isInValid = true;
						httpResponse = 415;
						messageCode.add(4003);
						return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
			        }
					if (contentType.equals(Constants.contentTypeJson) && jsonString.equals("")) {
						isInValid = true;
						httpResponse = 400;
						messageCode.add(4004);
						return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
					}
					if (contentType.equals(Constants.contentTypeJson) && !Globals.isValidJson(jsonString)) {
						isInValid = true;
						httpResponse = 400;
						messageCode.add(4005);		
						return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
					}
				}
				//Get bookings service
				if (reqServiceNoun != null && reqServiceNoun.equalsIgnoreCase(Constants.serviceNounBookings)) {						
					if (!Globals.isAPIEnabled("isBookingAPIEnabled", session, kwHandlerIMSDB)) {
						isInValid = true;
						httpResponse = 503;
						apiLog.updateLogField("503", "isBookingAPIEnabled=false");
						messageCode.add(1000);
						return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
					} else {
						apiProvider = new BookingController(currentDb, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
						if (apiProvider != null) {
					        if (attachments == null && method.equalsIgnoreCase(Constants.HTTP_GET)) {
								 //If Booking ID is passed, get booking details for given ID
								 if (reqId != null && !reqId.equals("")) {
									 endpoint = "Get Booking";
									 apiLog.updateLogField("ENDPOINT", "GETONE");
									 ((BookingController) apiProvider).getOne(reqId);
								 } else {
									 endpoint = "Get booking list";
									 apiLog.updateLogField("ENDPOINT", "GETALL");
									 ((BookingController) apiProvider).getAll();
								 }
							 } else if (method.equalsIgnoreCase(Constants.HTTP_GET) || method.equalsIgnoreCase(Constants.HTTP_POST)) { 
								 if (method.equalsIgnoreCase(Constants.HTTP_GET) && reqId != null && !reqId.equals("") && attachments != null && attachmentId != null && !attachmentId.equals("")) { //Get attachments for a booking
									 endpoint = "Get attachment";
									 apiLog.updateLogField("ENDPOINT", "ATTACHMENTS");
									 ((BookingController) apiProvider).getAttachmentEncodedDetails(reqId, attachmentId);
								 } else if (method.equalsIgnoreCase(Constants.HTTP_POST) && reqId != null && !reqId.equals("") && attachments.equals("true")) { //Add attachments for a booking
									 endpoint = "Add attachment";
									 apiLog.updateLogField("ENDPOINT", "ATTACHMENTS");
									 ((BookingController) apiProvider).addAttachments(reqId);										
								 } else if (method.equalsIgnoreCase(Constants.HTTP_POST)) {
									 endpoint = "Create booking";
									 apiLog.updateLogField("ENDPOINT", "CREATE"); //Create a booking
									 ((BookingController) apiProvider).getRequest(); //get JSON request
									 ((BookingController) apiProvider).createBooking();
								 }
							 } else if (method.equalsIgnoreCase(Constants.HTTP_PATCH) //Partial update
									 || 
									 (method.equalsIgnoreCase(Constants.HTTP_PUT) && Globals.isAPIEnabled("AllowPUT_APIUpdate", session, kwHandlerIMSDB))) { //Update Full booking	//Ashish - 25/03/2022 - For WH, allow booking updates through PUT request
								 if (requestAction != null && !requestAction.equals("")) {
									 endpoint = requestAction.toUpperCase();
									 apiLog.updateLogField("ENDPOINT", endpoint);
									 ((BookingController) apiProvider).bookingAction = requestAction;
									 ((BookingController) apiProvider).getRequest();
									 ((BookingController) apiProvider).updateBookingStatus();
								 } else {
									 endpoint = "UPDATE";
									 apiLog.updateLogField("ENDPOINT", endpoint);
									 ((BookingController) apiProvider).getRequest();
									 ((BookingController) apiProvider).updateBooking();
								 }
							 } else {
								 isInValid = true;
								 httpResponse = 405;
								 return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);								 
							 }
							return apiProvider.generateResponse();
						}
					}
				} else if (reqServiceNoun != null && reqServiceNoun.equalsIgnoreCase(Constants.serviceNounUsers)) {				
					if (!Globals.isAPIEnabled("isUserApiEnabled", session, kwHandlerIMSDB)) {
						isInValid = true;
						httpResponse = 503;
						apiLog.updateLogField("503", "isUserApiEnabled=false");
						messageCode.add(1000);
						return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
					} else if (!isValidUserLogin) {
						isInValid = true;
						httpResponse = 401;
						return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
					} else {
						apiProvider = new UserController(currentDb, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
						if (apiProvider != null) {
					        if (method.equalsIgnoreCase(Constants.HTTP_POST)) {
								 if (userId != null && !userId.equals("")) {
									 endpoint = "UPDATE";
									 apiLog.updateLogField("ENDPOINT", endpoint);
									 ((UserController) apiProvider).updateUser();
									 ((UserController) apiProvider).getUser();
								 }
							}
							return apiProvider.generateResponse();
						}
					}
				} else if (reqServiceNoun != null && reqServiceNoun.equalsIgnoreCase(Constants.serviceNounNotifications)) {						
					if (!Globals.isAPIEnabled("isNotificationApiEnabled", session, kwHandlerIMSDB)) {
						isInValid = true;
						httpResponse = 503;
						apiLog.updateLogField("503", "isNotificationApiEnabled=false");
						messageCode.add(1000);
						return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
					} else {
						apiProvider = new NotificationController(currentDb, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
						if (apiProvider != null) {
							if (method.equalsIgnoreCase(Constants.HTTP_GET)) {
								 endpoint = "NOTIFICATIONS";
								((NotificationController) apiProvider).getAll();
								 apiLog.updateLogField("ENDPOINT", endpoint);
							 }
							return apiProvider.generateResponse();
						}
					}
				} else if (reqServiceNoun != null && reqServiceNoun.equalsIgnoreCase(Constants.serviceNounLanguages)) {						
					if (!Globals.isAPIEnabled("isLanguageApiEnabled", session, kwHandlerIMSDB)) {
						isInValid = true;
						httpResponse = 503;
						apiLog.updateLogField("503", "isLanguageApiEnabled=false");
						messageCode.add(1000);
						return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
					} else {
						apiProvider = new LanguageController(currentDb, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
						if (apiProvider != null) {
							if (method.equalsIgnoreCase(Constants.HTTP_GET)) {
								 endpoint = "LANGUAGES";
								((LanguageController) apiProvider).getAll();
								 apiLog.updateLogField("ENDPOINT", endpoint);
							 }
							return apiProvider.generateResponse();
						}
					}
				} else if (reqServiceNoun != null && reqServiceNoun.equalsIgnoreCase(Constants.serviceNounClinics)) {						
					if (!Globals.isAPIEnabled("isClinicApiEnabled", session, kwHandlerIMSDB)) {
						isInValid = true;
						httpResponse = 503;
						apiLog.updateLogField("503", "isClinicApiEnabled=false");
						messageCode.add(1000);
						return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
					} else {
						apiProvider = new ClinicController(currentDb, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
						if (apiProvider != null) {
							if (method.equalsIgnoreCase(Constants.HTTP_GET)) {
								 endpoint = "CLINICS";
								((ClinicController) apiProvider).getAll();
								 apiLog.updateLogField("ENDPOINT", endpoint);
							 }
							return apiProvider.generateResponse();
						}
					}
				} else if (reqServiceNoun != null && reqServiceNoun.equalsIgnoreCase(Constants.serviceNounBookingLabels)) {						
					apiProvider = new BookingController(currentDb, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
					if (apiProvider != null) {
						if (method.equalsIgnoreCase(Constants.HTTP_GET)) {
							 endpoint = "LABELS";
							 apiLog.updateLogField("ENDPOINT", endpoint);
							((BookingController) apiProvider).getLabels();
						 }
						return apiProvider.generateResponse();
					}
				} else if (reqServiceNoun != null && reqServiceNoun.equalsIgnoreCase(Constants.serviceNounCheckin)) {						
					apiProvider = new UserController(currentDb, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
					if (apiProvider != null) {
						if (method.equalsIgnoreCase(Constants.HTTP_POST)) {
							if (userId != null && !userId.equals("")) {
								 endpoint = "CHECKIN";
								((UserController) apiProvider).checkin();
								 apiLog.updateLogField("ENDPOINT", endpoint);
							} else {
								isInValid = true;
								httpResponse = 400;
								return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
							}
						}
						return apiProvider.generateResponse();
					}
				} else if (reqServiceNoun != null && reqServiceNoun.equalsIgnoreCase(Constants.serviceNounCheckout)) {						
					apiProvider = new UserController(currentDb, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
					if (apiProvider != null) {
						if (method.equalsIgnoreCase(Constants.HTTP_GET)) {
							if (userId != null && !userId.equals("")) {
								 endpoint = "CHECKOUT";
								((UserController) apiProvider).checkout();
								 apiLog.updateLogField("ENDPOINT", endpoint);
							} else {
								isInValid = true;
								httpResponse = 400;
								return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
							}
						}
						return apiProvider.generateResponse();
					}
				} else if (reqServiceNoun != null && reqServiceNoun.equalsIgnoreCase(Constants.serviceNounCheckinStatus)) {						
					apiProvider = new UserController(currentDb, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
					if (apiProvider != null) {
						if (method.equalsIgnoreCase(Constants.HTTP_GET)) {
							if (userId != null && !userId.equals("")) {
								 endpoint = "CHECKIN-STATUS";
								 ((UserController) apiProvider).checkinStatus();
								 ((UserController) apiProvider).updateUser();
								 apiLog.updateLogField("ENDPOINT", endpoint);
							} else {
								isInValid = true;
								httpResponse = 400;
								return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
							}
						}
						return apiProvider.generateResponse();
					}
				} else if (reqServiceNoun != null && reqServiceNoun.equalsIgnoreCase(Constants.serviceNounPins)) {						
					apiProvider = new PinController(currentDb, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
					if (apiProvider != null) {
						if (method.equalsIgnoreCase(Constants.HTTP_POST)) {
							endpoint = "Validate PIN";
							apiLog.updateLogField("ENDPOINT", endpoint);
							((PinController) apiProvider).getRequest(); 
							if (((PinController) apiProvider).validatePin()) {
							//	((PinController) apiProvider).validatePin();
								//((PinController) apiProvider).createPin();
							} else {
								isInValid = true;
								httpResponse = 400;
								messageCode.add(1002);
								return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
							}
						}
						return apiProvider.generateResponse();
					}
				} else if (reqServiceNoun != null && reqServiceNoun.equalsIgnoreCase(Constants.serviceNounMeetings)) {						
					apiProvider = new MeetingController(currentDb, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
					if (method.equalsIgnoreCase(Constants.HTTP_POST)) {
						endpoint = "Get Meeting Details";
						apiLog.updateLogField("ENDPOINT", endpoint);
						((MeetingController) apiProvider).getRequest(); 
						//((MeetingController) apiProvider).createMeeting();
					}
					return apiProvider.generateResponse();
				}  else if (reqServiceNoun != null && reqServiceNoun.equalsIgnoreCase(Constants.serviceNounQuestions)) {		//placeholder for intake questions				
					apiProvider = new QuestionController(currentDb, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
					if (method.equalsIgnoreCase(Constants.HTTP_GET)) {
						endpoint = "Get Intake Questions";
						apiLog.updateLogField("ENDPOINT", endpoint);
						((QuestionController) apiProvider).getRequest(); 
						//((MeetingController) apiProvider).createMeeting();
					}
					return apiProvider.generateResponse();
				}   else if (reqServiceNoun != null && reqServiceNoun.equalsIgnoreCase(Constants.serviceNounCDR)) {//placeholder for CDR POST	
					Globals.printCode("reqServiceNoun="+reqServiceNoun);
					apiProvider = new CDRController(currentDb, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
					if (method.equalsIgnoreCase(Constants.HTTP_POST)) {
						endpoint = "Receive CDR";
						apiLog.updateLogField("ENDPOINT", endpoint);
						//((CDRController) apiProvider).getCDR(); 
						((CDRController) apiProvider).createCDR();
					}
					return apiProvider.generateResponse();
				}
			}
	    } catch (Exception e) {
			Globals.printException(e);
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
	    	//If still unable to proceed due to some issues in code, throw service unavailable error
	    	if (responseJson == null) {
				isInValid = true;
				httpResponse = 503;
				apiLog.updateLogField("503", "api error, responsejson=null");
				return generateResponse(response, isInValid, messageCode, httpResponse, session, kwHandlerIMSDB);
			}	
	    }
		return null;
    }

	/**
	 * call recycle on all of the notes objects or all of the 
	 * other objects we've created that have to recycle. 
	 * Pass it down the chain.
	 */
	public void recycle(ApiProvider apiProvider) {
		if (apiProvider != null) {
			apiProvider.recycle();
		}
	}
}
