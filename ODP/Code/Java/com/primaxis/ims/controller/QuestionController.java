package com.primaxis.ims.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsonable;
import org.json.simple.Jsoner;

import com.primaxis.ims.api.ApiProvider;
import com.primaxis.ims.model.Question;
import com.primaxis.ims.utils.Globals;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;

public class QuestionController  extends ApiProvider {

	private Question question;
	private String clientId;
	private String clientPin;
	public QuestionController(Database db, JsonObject jsonObjRequest, com.primaxis.ims.utils.Log apiLog,
			HttpServletResponse HttpServletResponse, Session session, String languageServiceProvider,
			String consumerName, Map<String, Object> params) throws NotesException, IOException {
		super(db, jsonObjRequest, apiLog, HttpServletResponse, session, languageServiceProvider, consumerName, params);
		try {
			question = new Question(this, session);
		} catch(Exception e) {
			Globals.printException(e);
		}
	}

	/**
	 * Generate response
	 */
	public String generateResponse() throws NotesException {
		responseJson = new JsonObject();
		requestId = Globals.requestID();
		messageArray = new JsonArray();
		
		if (messageCode.size() > 0) {
			for (int code : messageCode) {
				JsonObject ob = new JsonObject();
				ob.put(Integer.toString(code), Globals.getMessageFromCode(code, session, kwHandlerIMSDB));
				if (!messageArray.contains(ob)) {
					messageArray.add(ob);
				}
			}
		}
		if (isInValid) {
			success = false;
		} else {
			responseJson.put("questions", question.getQuestions(clientId));
		}
		if (messageArray.size() > 0) {
			dataJson.put(isInValid ? "errors" : "messages", messageArray);
		}
		if (!dataJson.isEmpty()) {
			responseJson.put("data",  dataJson);
		}
		response.setStatus(httpResponse);
		responseJson.put("code", httpResponse);
		responseJson.put("text", Globals.getHTTPResponseMsg(httpResponse));
		responseJson.put("success", success);
		responseJson.put("requestId", requestId);
		if (responseJson instanceof Jsonable) {
			responseJsonString = (String) Jsoner.serialize(responseJson);
		}
		if (isInValid) {
			Log.logAPIError(responseJsonString, clientId, Globals.getHTTPResponseMsg(httpResponse), interpreterDocKey);
		}
		Log.updateLogField("API_Success", String.valueOf(success));
		
		return responseJsonString;
	}
	
	/**
	 * Validate and assign values from requested JSON object to booking objects
	 * @return - void
	 * @throws NotesException 
	 * @throws URISyntaxException 
	 * @throws MalformedURLException 
	 */
	public void getRequest() {
		clientId = Globals.getIgnoreCase(requestJson, "client_id");
		Log.updateLogField("client_id", clientId);
	}

}

