package com.primaxis.ims.controller;

import com.primaxis.ims.api.ApiProvider;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.*;
import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;

import com.primaxis.ims.utils.ImsPatientDataDb;
import com.primaxis.ims.utils.Log;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.Constants;
	
/**
 * Patient class to manage patient related data
 * 
 * @author BKarwal
 * @since 23 April 2019
 */
public class PatientController extends ApiProvider {
	private Document patientDoc = null;

	private ImsPatientDataDb patientDataDB;
	private String patientID;
	private String facility;
	private String urNumber;
	private String lastName;
	private String firstName;
	private String secondName;
	private String title;
	private String DOB;
	private String gender;
	private String languageCode;
	private String language;
	private String priority;
	private String code;
	private JsonArray addresses;
	private JsonObject genderPreference;
	private JsonArray nextOfKin;
	private JsonArray phoneNumbers;				
	private boolean isInValidPh = false;			
	private boolean isInValidStateCode = false;
	private boolean isInValidPostCode = false;
     
	/**
	 * 
	 * @param db
	 * @param jsonObjRequest
	 * @param apiLog
	 * @throws NotesException
	 * @throws IOException
	 */
	public PatientController(Database db, JsonObject jsonObjRequest, Log apiLog, HttpServletResponse response, Session session, String languageServiceProvider, String consumerName, Map<String, Object> params) throws NotesException, IOException {
		super(db, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
		patientDataDB = new ImsPatientDataDb (session);
	}

	/**
	 * Generate Response String
	 */
	public String generateResponse() {
		try {
			responseJson = new JsonObject();
			requestId = Globals.requestID();
			responseJson.put("code", httpResponse);
			responseJson.put("text", Globals.getHTTPResponseMsg(httpResponse));
			responseJson.put("success", success);
			responseJson.put("requestId", requestId);
			dataJson = new JsonObject();
			messageArray = new JsonArray();
			if (messageCode.size() > 0) {
				for (int code : messageCode) {
					JsonObject ob = new JsonObject();
					ob.put(Integer.toString(code), Globals.getMessageFromCode(code, session, kwHandlerIMSDB));
					if (!messageArray.contains(ob)) {
						messageArray.add(ob);
					}
				}
			}
			if (isInValid) {
				success = false;
				httpResponse = 400;
			} else if (patientID != null) {
				dataJson.put("ID", patientID);
			}
			if (messageArray.size() > 0) {
				dataJson.put(isInValid ? "errors" : "messages", messageArray);
			}
			if (dataJson != null) {
				responseJson.put("data",  dataJson);
			}
			if (responseJson instanceof Jsonable) {
				responseJsonString = (String) Jsoner.serialize(responseJson);
			}
			Log.updateLogField("API_Success", String.valueOf(success));
		} catch (Exception e) {
			e.printStackTrace();
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}
		
		return responseJsonString;
	}
	
	/**
	 * Validate and assign values from requested JSON to patient fields
	 * Ex JSON :
	 * {
		    "id": "00009^D12312334",
		    "facility": "00009",
		    "urn": "D123123",
		    "lastname": "Citizen",
		    "firstname": "Jane",
		    "secondname": "Anne",
		    "title": "Ms.",
		    "dob": "19841201",
		    "sex": "f",
		    "languagecode": "1403",
		    "language": "",
		    "phonenumbers": [{"phonenumber": "111111"},{"phonenumber": "222222"}],
		    "addresses": [
		        {
		        	"address1": "p address1",
		            "address2": "p address2",
		            "suburb": "p suburb",
		            "postcode": "111111111111",
		            "statecode": "vic"
		        },
		        {
		        	"address1": "p 1 address1",
		            "address2": "p 1 address2",
		            "suburb": "p 1 suburb",
		            "postcode": "22222222222",
		            "statecode": "nsw"
		        }
		    ],
		    "nextofkin": [
		        {
		            "name": "John Citizen",
		            "relationship": "Father", 
		    		"phonenumbers": [{"phonenumber": "33333"},{"phonenumber": "444"}],
		            "addresses": [
				        {
				        	"address1": "nok address1",
				            "address2": "nok address2",
				            "suburb": "nok suburb",
				            "postcode": "33333333333333",
				            "statecode": "WA"
				        },
				        {
				        	"address1": "nok 1 address1",
				            "address2": "nok 1 address2",
				            "suburb": "nok 1 suburb",
				            "postcode": "44444444444444",
				            "statecode": "ACT"
				        }
				    ]},
				    {
		            "name": "Kim Citizen",
					"relationship": "Mother",
		    		"phonenumbers": [{"phonenumber": "55555"},{"phonenumber": "666666"}],
		            "addresses": [
				        {
				        	"address1": "level 1",
				            "address2": "488 bourke street",
				            "suburb": "Gold Cost",
				            "postcode": 3000
				        },
				        {
				            "address2": "address line 2",
				            "suburb": "South Brisbane",
				            "postcode": "4101"
				        }
				    ]
		        }
		    ],
		    "genderpreference": {
		        "priority": "Must be",
		        "gender": "M"
		    }
		} 
	 * @return boolean
	 * @throws NotesException 
	 */
	protected boolean setObjectProperties() throws NotesException {
		try {
			//Reset all fields;
			patientID = "";
			facility = "";
			urNumber = "";
			lastName = "";
			firstName = "";
			secondName = "";
			title = "";
			DOB = "";
			gender = "";
			languageCode = "";
			language = "";
			priority = "";
			code = "";
			addresses = new JsonArray();
			genderPreference= new JsonObject();
			nextOfKin = new JsonArray();
			phoneNumbers = new JsonArray();	
			//Validate ID
			if (requestJson.get("id") == null || requestJson.get("id").equals("")) {
				isInValid = true;
				messageCode.add(4006);
			}  else {
				String s = Globals.trimString(requestJson.get("id"));
				patientID = (s.length() > Globals.maxStringLength) ? s.substring(0,Globals.maxStringLength) : s;
			}
			
			//Validate first name
			if (requestJson.get("firstname") == null || requestJson.get("firstname").equals("") ) {
				isInValid = true;
				messageCode.add(4007);
			}  else {
				String s = Globals.trimString(requestJson.get("firstname"));
				firstName = (s.length() > Globals.maxStringLength) ? s.substring(0, Globals.maxStringLength) : s;
			}
			//Validate last name
			if (requestJson.get("lastname") != null) {
				String s = Globals.trimString(requestJson.get("lastname"));
				lastName = (s.length() > Globals.maxStringLength) ? s.substring(0, Globals.maxStringLength) : s;
			}
			//Validate second name
			if (requestJson.get("secondname") != null) {
				String s = Globals.trimString(requestJson.get("secondname"));
				secondName = (s.length() > Globals.maxStringLength) ? s.substring(0, Globals.maxStringLength) : s;
			}
			
			//Validate title
			if (requestJson.get("title") != null) {
				String s = Globals.trimString(requestJson.get("title"));
				title = (s.length() > Globals.maxStringLength) ? s.substring(0, Globals.maxStringLength) : s;
			}
			//Validate UR Number
			if (requestJson.get("urn") == null || requestJson.get("urn").equals("")) {
				isInValid = true;
				messageCode.add(4008);
			} else {
				if (Globals.isValidAlphaIntString(requestJson.get("urn"))) {
					String s = Globals.trimString(requestJson.get("urn"));
					urNumber = (s.length() > Globals.maxStringLength) ? s.substring(0, Globals.maxStringLength) : s;
				} else {
					isInValid = true;
					messageCode.add(4014);
				}
			}
	
			//Validate language code
			if (requestJson.get("languagecode") != null){
				if (Globals.isValidFloatString(requestJson.get("languagecode"))) {
					String s = Globals.trimString(requestJson.get("languagecode"));
					languageCode = (s.length() > Globals.maxStringLength) ? s.substring(0, Globals.maxStringLength) : s;	
				} else {
					isInValid = true;
					messageCode.add(4015);
				}
			}
	
			//Validate language
			if (requestJson.get("language") != null) {
				String s = Globals.trimString(requestJson.get("language"));
				language = (s.length() > Globals.maxStringLength) ? s.substring(0, Globals.maxStringLength) : s;	
			}
			
			//Validate facility
			if (requestJson.get("facility") != null) {
				if (Globals.isValidAlphaIntString(requestJson.get("facility"))) {
					String s = Globals.trimString(requestJson.get("facility"));
					facility = (s.length() > Globals.maxStringLength) ? s.substring(0, Globals.maxStringLength) : s;	
				} else {
					isInValid = true;
					messageCode.add(4016);
				}
			}
			
			//Validate DOB
			if (requestJson.get("dob") != null && !requestJson.get("dob").equals("")) {
				String dateDOB = Globals.trimString(requestJson.get("dob").toString());				
				String validDob = Globals.getJavaFormattedDate(dateDOB, "yyyyMMdd", kwHandlerIMSDB.getGlobalDateNotesFromFormat());
				if (validDob == null || dateDOB.length() != 8) {
					isInValid = true;
					messageCode.add(4010);
				} else {				
					DOB = validDob;
				}
			}	
			//Validate Gender
			gender = Globals.verifyGenderFormat(requestJson.get("sex").toString());
			if (requestJson.get("genderpreference") != null && !requestJson.get("genderpreference").equals("")) {
				genderPreference = Globals.validateJsonObject(requestJson.get("genderpreference"));
			}
			if (requestJson.get("addresses") != null && !requestJson.get("addresses").equals("")) {
				addresses = Globals.validateJsonArray(requestJson.get("addresses"));
			}
			if (requestJson.get("nextofkin") != null && !requestJson.get("nextofkin").equals("")) {
				nextOfKin = Globals.validateJsonArray(requestJson.get("nextofkin"));
			}
			if (requestJson.get("phonenumbers") != null && !requestJson.get("phonenumbers").equals("")) {
				phoneNumbers = Globals.validateJsonArray(requestJson.get("phonenumbers"));
			}
			return true;
		} catch (Exception e) {
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Process data for creating a patient record and add in database 
	 * 
	 */
	public void processResult() {
		boolean createDoc = true;
		try {
			if (setObjectProperties()) {
				if (!isInValid) {
					//Check if same ID exists, then update/overwrite patient details
					httpResponse = 200;
					if (patientID != null) {					   
						patientDoc = patientDataDB.getPatientDocumentByID(patientID);
					}
					if (patientDoc == null) {		
						patientDoc = patientDataDB.getDatabase().createDocument();	
						createDoc = true;	
						httpResponse = 201;
					} else {
						createDoc = false;
						removeFieldsBeforeUpdate();	
					}
					patientDoc.replaceItemValue("Form", Constants.formPatient);
					patientDoc.replaceItemValue("ID", patientID);
					patientDoc.replaceItemValue("Facility", facility);
					patientDoc.replaceItemValue("UrNumber", urNumber);
					patientDoc.replaceItemValue("LastName", lastName);
					patientDoc.replaceItemValue("FirstName", firstName);
					patientDoc.replaceItemValue("SecondName", secondName);
					patientDoc.replaceItemValue("Title", title);
					patientDoc.replaceItemValue("DateOfBirth", DOB);
					patientDoc.replaceItemValue("Gender", gender);
					String checkLanguageName = ImsHomeDb.checkLanguageName(language);
					
					/*
					 * If language code does not match, ignore language code and language
					 */
					if (checkLanguageName != null) {
						patientDoc.replaceItemValue("LanguageCode", languageCode);
						patientDoc.replaceItemValue("Language", checkLanguageName);
					} else {
						patientDoc.replaceItemValue("LanguageCode", "");
						patientDoc.replaceItemValue("Language", "");
					}
					
					addFieldOnPatient("CountAddress", "0"); //Reset counter
					setAddressFields();
					addFieldOnPatient("CountPhoneNumber", "0"); //Reset counter
					setPhoneNumberFields();
					setGenderPreference();
					addFieldOnPatient("CountNextOfKin", "0"); //Reset counter
					addNextOfKin();
					
				   if (isInValidPh) {
						isInValid = true;
						messageCode.add(4024);
				   }
				   if (isInValidStateCode) {
						isInValid = true;
						messageCode.add(4027);
				   }
				   if (isInValidPostCode) {
						isInValid = true;
						messageCode.add(4028);
				   }

				   addFieldOnPatient("GenderPreferencePriority", priority);
				   addFieldOnPatient("GenderPreferenceCode", code);
				   
				   if (!isInValid) {
					   String clientName = kwHandlerIMSDB.getKeywordValueString(Constants.keywordClientName);
					   if (createDoc) {
						   Globals.LogActionToDocument(nowDateTime, patientDoc, "Created document from IMS API - by " + clientName, true, session);
					   } else {
						   Globals.LogActionToDocument(nowDateTime, patientDoc, "Updated document from IMS API - by " + clientName, false, session);
					   }
					   patientDoc.computeWithForm(true, false);
					   patientDoc.save();
					  //patientDataDB.getDatabase().updateFTIndex(true);
					   error = false;
					   success = true;
				   } else {
					   error = true;
					   success = false;
					   httpResponse = 400;
				   }
				}
			} else {
				error = true;
				success = false;
				httpResponse = 400;
			}
		} catch (Exception e) {
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			e.printStackTrace();
		}
	}
	
	/**
	 * Checks if the field value is different and replaces it if it is
	 * returns true if we replaced it, false otherwise.
	 * @param fieldName
	 * @param value
	 * @return
	 */
	private boolean addFieldOnPatient (String fieldName, String fieldValue)
	{
		try {
			if (fieldName != null && fieldValue != null) {
				patientDoc.replaceItemValue(fieldName, fieldValue);
				return true;	
			}
		} catch (Exception e) {
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Remove patient field
	 * 
	 * @param fieldName
	 * @return
	 */
	private boolean removeFieldFromPatient (String fieldName)
	{
		try {
			if (fieldName != null) {
				patientDoc.removeItem(fieldName);
				return true;	
			}
		} catch (Exception e) {
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Set address for patient. Multiple addresses can be passed in JSON request as array
	 * Example JSON:
	 * "addresses": [
            {
                "address1": "488 Bourke Street",
                "address2": "Level 9",
                "suburb": "Melbourne",
                "postcode": "3000",
                "statecode": "VIC"
            },
            {
                "address1": "488 Bourke Street",
                "address2": "Level 9",
                "suburb": "Melbourne",
                "postcode": "3000",
                "statecode": "NSW"
            }
        ]
	 * @throws NotesException
	 */
	private void setAddressFields() throws NotesException 
	{
		try {			
			if (addresses != null && addresses.size() > 0) {
				//Save address count which will require while removing fields
				addFieldOnPatient("CountAddress", (new Integer(addresses.size()).toString()));
				//We can have multiple addresses
				for(int i=0;i<addresses.size();i++) {
				   JsonObject addressObj = (JsonObject) addresses.get(i);
				   String AddressLine1;
				   String AddressLine2;
				   String Suburb;
				   String PostCode;
				   String StateCode;
				   if (i == 0) {
					   AddressLine1 = "AddressLine1";
					   AddressLine2 = "AddressLine2";
					   Suburb = "Suburb";
					   PostCode = "PostCode";
					   StateCode = "StateCode";
				   } else {
					   AddressLine1 = "Address_" + i + "_AddressLine1";
					   AddressLine2 = "Address_" + i + "_AddressLine2";
					   Suburb = "Address_" + i + "_Suburb";
					   PostCode = "Address_" + i + "_PostCode";
					   StateCode = "Address_" + i + "_StateCode";
				   }
				   String line1 = Globals.validateString(addressObj.get("address1"));
				   String line2 = Globals.validateString(addressObj.get("address2"));
				   String suburb = Globals.validateString(addressObj.get("suburb"));
				   String postCode = null;
				   if (addressObj.get("postcode") != null) {
					   if (!Globals.isValidIntString(addressObj.get("postcode"))) {
							isInValidPostCode = true;
						} else {
							String s = Globals.trimString(addressObj.get("postcode"));
							postCode = (s.length() > Globals.maxStringLength) ? s.substring(0, Globals.maxStringLength) : s;	
						}
				   }
				   String existingStateCode = null;
				   String stateCode = Globals.validateString(addressObj.get("statecode"));
				   if (!stateCode.equals("")) {
						existingStateCode = kwHandlerIMSDB.getStateFromStates(stateCode);
						/*
						 * If state code does not match, throw error
						 */
						if (existingStateCode == null) {
							isInValidStateCode = true;
						}
						
				   }
				   if (line1 != null) {
					   addFieldOnPatient(AddressLine1 , line1);					
				   }
				   if (line2 != null) {
					   addFieldOnPatient(AddressLine2 , line2);					
				   }
				   if (suburb != null) {
					   addFieldOnPatient(Suburb , suburb);
				   }
				   if (postCode != null) {
					   addFieldOnPatient(PostCode , postCode);
				   }
				   if (existingStateCode != null) {
					   addFieldOnPatient(StateCode , existingStateCode);
				   }
				}
			}
		} catch (Exception e) {
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			e.printStackTrace();
		}
	}

	/**
	 * Adds patient phone numbers. Multiple phone numbers can be passed in a JSON request as array
	 * Ex: "phonenumbers": [{"phonenumber": "0473892479"},{"phonenumber": "0424442344"}],
	 * @throws NotesException
	 */
	private void setPhoneNumberFields() throws NotesException 
	{
		try {			
			if (phoneNumbers != null && phoneNumbers.size() > 0) {
				//Save phoneNumber count which will require while removing fields
				addFieldOnPatient("CountPhoneNumber", (new Integer(phoneNumbers.size()).toString()));
				//We can have multiple phone numbers
				for(int i=0;i<phoneNumbers.size();i++) {
				   JsonObject phoneNumberObj = (JsonObject) phoneNumbers.get(i);
				   String PhoneNumber;
				   String phoneNumber = null;
				   if (i == 0) {
					   PhoneNumber = "PhoneNumber";
				   } else {
					   PhoneNumber = "PhoneNumber_" + i;
				   }
				   if (phoneNumberObj.get("phonenumber")!=null && !Globals.isValidIntString(phoneNumberObj.get("phonenumber"))) {
						isInValidPh = true;
					} else {
						String s = Globals.trimString(phoneNumberObj.get("phonenumber"));
						phoneNumber = (s.length() > Globals.maxStringLength) ? s.substring(0, Globals.maxStringLength) : s;	
					}
					if (phoneNumber != null) {
						addFieldOnPatient(PhoneNumber , phoneNumber);
					}
				}
			}
		} catch (Exception e) {
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			e.printStackTrace();
		}
	}
	
	/**
	 * Add Next of Kin details 
	 * NOK is passed as JSON array of NOK
	 * Example JSON Request: 
	 * "nextofkin": [
        {
            "name": "John Citizen",
            "relationship": "Father",
            "phonenumbers": [
                {
                    "phonenumber": "3333333"
                },
                {
                    "phonenumber": "44444444"
                }
            ]
        },
        {
            "name": "Kim Citizen",
            "relationship": "Mother",
            "phonenumbers": [
                {
                    "phonenumber": "555555555"
                },
                {
                    "phonenumber": "666666666"
                }
            ],
            "addresses": [
                {
                    "address1": "488 Bourke Street",
                    "address2": "Level 9",
                    "suburb": "Melbourne",
                    "postcode": "3000",
                    "statecode": "VIC"
                },
                {
                    "address1": "488 Bourke Street",
                    "address2": "Level 9",
                    "suburb": "Melbourne",
                    "postcode": "3000",
                    "statecode": "NSW"
                }
            ]
        }
    ]
	 */
	private void addNextOfKin() {
		try {
			if (nextOfKin != null  && nextOfKin.size() > 0) {				   
				//Save nextOfKin count which will require while removing fields
				addFieldOnPatient("CountNextOfKin", (new Integer(nextOfKin.size()).toString()));
				//We can have multiple Next of Kin
				for(int i=0;i<nextOfKin.size();i++) {
				   JsonObject dataObj = (JsonObject) nextOfKin.get(i);
				   String name;
				   String relationship;
				   if (i == 0) {
					   name = Constants.prefixNok + "Name";
					   relationship = Constants.prefixNok + "Relationship";
				   } else {
					   name = Constants.prefixNok + "_" + i + "_Name";
					   relationship = Constants.prefixNok + "_" + i + "_Relationship";
				   }
				   
				   String Name = Globals.validateString(dataObj.get("name"));
				   String Relationship = Globals.validateString(dataObj.get("relationship"));
				   addFieldOnPatient(name, Name); 
				   addFieldOnPatient(relationship, Relationship);
				   JsonArray nokPhoneNumbers = new JsonArray();
				   if (dataObj.get("phonenumbers") != null) {
					   nokPhoneNumbers = (JsonArray) dataObj.get("phonenumbers");
				   }
				   //Save phoneNumber count which will require while removing fields
				   //Reset counters
				   if (i == 0) {
					   addFieldOnPatient("CountNOKPhoneNumbers", "0");
				   } else {
					   addFieldOnPatient("CountNOK_"+ i +"_PhoneNumbers", "0");
				   }
				   if (nokPhoneNumbers.size() > 0) {
					   if (i == 0) {
						   addFieldOnPatient("CountNOKPhoneNumbers", (new Integer(nokPhoneNumbers.size()).toString()));
					   } else {
						   addFieldOnPatient("CountNOK_" + i + "_PhoneNumbers", (new Integer(nokPhoneNumbers.size()).toString()));
					   }
						//We can have multiple phone numbers
						for(int p=0;p<nokPhoneNumbers.size();p++) {
						   JsonObject phoneNumberObj = (JsonObject) nokPhoneNumbers.get(p);
						   String phoneNumber;
						   String PhoneNumber = null;
						   if (i == 0 && p == 0) {
							   phoneNumber = Constants.prefixNok + "PhoneNumber";
						   }  else if (i == 0) {
							   phoneNumber = Constants.prefixNok + "PhoneNumber_" + p;
						   } else if (i != 0 && p == 0) {
							   phoneNumber = Constants.prefixNok + "_" + i + "_PhoneNumber";
						   } else {
							   phoneNumber = Constants.prefixNok + "_" + i + "_PhoneNumber_" + p;
						   }
						   if (phoneNumberObj.get("phonenumber")!=null && !Globals.isValidIntString(phoneNumberObj.get("phonenumber"))) {
							   isInValidPh = true;
						   } else {
							   String s = Globals.trimString(phoneNumberObj.get("phonenumber"));
							   PhoneNumber = (s.length() > Globals.maxStringLength) ? s.substring(0, Globals.maxStringLength) : s;	
						   }
						   if (phoneNumber != null) {
							   addFieldOnPatient(phoneNumber , PhoneNumber);
						   }
					   }
				   }
				   //NOK addresses
				   //Reset counters
				   if (i == 0) {
					   addFieldOnPatient("CountNOKAddresses", "0");
				   } else {
					   addFieldOnPatient("CountNOK_" + i + "_Addresses", "0");
				   }
				   JsonArray NOKAddresses = new JsonArray();
				   if (dataObj.get("addresses") != null) {
					   NOKAddresses =  Globals.validateJsonArray(dataObj.get("addresses"));	
				   }
				   if (NOKAddresses.size() > 0) {
					   //Save address count which will require while removing fields
					   
					   if (i == 0) {
						   addFieldOnPatient("CountNOKAddresses", (new Integer(NOKAddresses.size()).toString()));
					   } else {
						   addFieldOnPatient("CountNOK_" + i + "_Addresses", (new Integer(NOKAddresses.size()).toString()));
					   }
				   
					   //We can have multiple lines for a address of NOK
					   for (int a=0;a<NOKAddresses.size();a++) {
						   StringBuilder NOKAddressString = new StringBuilder(); 
						   JsonObject addressObj = (JsonObject) NOKAddresses.get(a);
						   String line1 = Globals.validateString(addressObj.get("address1"));
						   String line2 = Globals.validateString(addressObj.get("address2"));
						   String suburb = Globals.validateString(addressObj.get("suburb"));
						   String postCode = null;
						   String stateCode = Globals.validateString(addressObj.get("statecode"));
						   if (addressObj.get("postcode") != null) {
							   if (!Globals.isValidIntString(addressObj.get("postcode"))) {
									isInValidPostCode = true;
								} else {
									String s = Globals.trimString(addressObj.get("postcode"));
									postCode = (s.length() > Globals.maxStringLength) ? s.substring(0, Globals.maxStringLength) : s;	
								}
						   }
						   
						   String existingStateCode = null;
						   if (!stateCode.equals("")) {
								existingStateCode = kwHandlerIMSDB.getStateFromStates(stateCode);
								/*
								 * If state code does not match, throw error
								 */
								if (existingStateCode == null) {
									isInValidStateCode = true;
								}
								
						   }

						   if (!line1.equals("") && line1 != null) {
							   NOKAddressString.append(line1);					
						   }
						   if (!line2.equals("") && line2 != null) {
							   if (!NOKAddressString.equals("")) {
								   NOKAddressString.append("\n");
							   }
							   NOKAddressString.append(line2);	
						   }
						   if (!suburb.equals("") && suburb != null) {
							   if (!NOKAddressString.equals("")) {
								   NOKAddressString.append("\n");
							   }
							   NOKAddressString.append(suburb);	
						   }
						   if (postCode != null) {
							   if (!NOKAddressString.equals("")) {
								   NOKAddressString.append("\n");
							   }
							   NOKAddressString.append(postCode);	
						   }
						   if (existingStateCode != null) {
							   if (!NOKAddressString.equals("")) {
								   NOKAddressString.append("\n");
							   }
							   NOKAddressString.append(existingStateCode);	
						   }
						   
						   String NOKAddressLines;
						   if (i == 0 && a == 0) {
							   NOKAddressLines = Constants.prefixNok + "Address";
						   } else if (i == 0) {
							   NOKAddressLines = Constants.prefixNok + "Address_" + a;
						   } else if (i != 0 && a == 0) { 
							   NOKAddressLines = Constants.prefixNok + "_" + i + "_Address";
						   } else {
							   NOKAddressLines = Constants.prefixNok  + "_" + i + "_Address_" + a;
						   }
						   addFieldOnPatient(NOKAddressLines , NOKAddressString.toString().trim()); 
					   }
				   }
				}
			}
		} catch (Exception e) {
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			e.printStackTrace();
			return;
		}
	}
	
	/**
	 * Set gender preference
	 */
	private void setGenderPreference() {
		try {
			if (genderPreference != null && genderPreference.size() > 0) {
				priority = Globals.validateString(genderPreference.get("priority"));
				code = Globals.validateString(genderPreference.get("gender"));
				//Validate Gender priority
				if (!priority.equals("")) {
					if (!Globals.isValidPriority(priority)) {
						isInValid = true;
						messageCode.add(4026);
					}
				}
				//Validate Gender code
				if (!code.equals("")) {
					if (!Globals.isValidPriority(code)) {
						isInValid = true;
						messageCode.add(4025);
					} else {
						code = Globals.GenderFormat(code);
					}
				}
			}
		} catch (Exception e) {
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			e.printStackTrace();
		}
	}

	/**
	 * Remove dynamic fields in the same way as they were added to document before updating
	 * 
	 */
	private void removeFieldsBeforeUpdate() {
		try {
			//Remove address fields
			int a = getCounterOfField("CountAddress");
			for (int f=0;f<a;f++) {
			   String AddressLine1;
			   String AddressLine2;
			   String Suburb;
			   String PostCode;
			   String StateCode;
			   if (f == 0) {
				   AddressLine1 = "AddressLine1";
				   AddressLine2 = "AddressLine2";
				   Suburb = "Suburb";
				   PostCode = "PostCode";
				   StateCode = "StateCode";
			   } else {
				   AddressLine1 = "Address_" + f + "_AddressLine1";
				   AddressLine2 = "Address_" + f + "_AddressLine2";
				   Suburb = "Address_" + f + "_Suburb";
				   PostCode = "Address_" + f + "_PostCode";
				   StateCode = "Address_" + f + "_StateCode";
			   }
				removeFieldFromPatient(AddressLine1);					
				removeFieldFromPatient(AddressLine2);					
				removeFieldFromPatient(Suburb);
				removeFieldFromPatient(PostCode);
				removeFieldFromPatient(StateCode);
			}
			removeFieldFromPatient("CountAddress");
			//Remove Next of kin fields
			int n = getCounterOfField("CountNextOfKin");
			for (int nok=0;nok<n;nok++) {
				String name;
				String relationship;
			   if (nok == 0) {
				   name = Constants.prefixNok + "Name";
				   relationship = Constants.prefixNok + "Relationship";
			   } else {
				   name = Constants.prefixNok + "_" + nok + "_Name";
				   relationship = Constants.prefixNok + "_" + nok + "_Relationship";
			   }
			   removeFieldFromPatient(name); 
			   removeFieldFromPatient(relationship);
			   int np = 0;
			   int na = 0;
			   if (nok == 0) {
				   np = getCounterOfField("CountNOKPhoneNumbers");
				   na = getCounterOfField("CountNOKAddresses");
			   } else {
				   np = getCounterOfField("CountNOK_" + nok + "_PhoneNumbers");
				   na = getCounterOfField("CountNOK_" + nok + "_Addresses");
			   }
				
			   //Remove NOK addresses
			   for (int nai=0;nai<na;nai++) {
				   String NOKAddressLines;
				   if (nok == 0 && nai == 0) {
					   NOKAddressLines = Constants.prefixNok + "Address";
				   } else if (nok == 0) {
					   NOKAddressLines = Constants.prefixNok + "Address_" + nai;
				   } else if (nok != 0 && nai == 0) { 
					   NOKAddressLines = Constants.prefixNok + "_" + nok + "_Address";
				   } else {
					   NOKAddressLines = Constants.prefixNok  + "_" + nok + "_Address_" + nai;
				   }
				   removeFieldFromPatient(NOKAddressLines);
			   }
			   //remove NOK phone numbers
			   for (int nap=0;nap<np;nap++) {
				   String PhoneNumber;
				   if (nok == 0 && nap == 0) {
					   PhoneNumber = Constants.prefixNok + "PhoneNumber";
				   }  else if (nok == 0) {
					   PhoneNumber = Constants.prefixNok + "PhoneNumber_" + nap;
				   } else if (nok != 0 && nap == 0) {
					   PhoneNumber = Constants.prefixNok + "_" + nok + "_PhoneNumber";
				   } else {
					   PhoneNumber = Constants.prefixNok + "_" + nok + "_PhoneNumber_" + nap;
				   }
				   removeFieldFromPatient(PhoneNumber);
			   }
			}
			removeFieldFromPatient("CountNextOfKin");
			removeFieldFromPatient("CountNOKAddresses");
			removeFieldFromPatient("CountNOKPhoneNumbers");
			//Remove phone number fields
			int p = getCounterOfField("CountPhoneNumber");
			for (int f=0;f<p;f++) {
				String PhoneNumber;
				if (f == 0) {
				   PhoneNumber = "PhoneNumber";
				} else {
				   PhoneNumber = "PhoneNumber_" + f;
				}
				removeFieldFromPatient(PhoneNumber);
			}
			removeFieldFromPatient("CountPhoneNumber");
		} catch (Exception e) {
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			e.printStackTrace();
		}
	}
	
	/**
	 * Get counter fields
	 * These get used to know how many dynamic fields need to be removed while updating document
	 * 
	 * @param fieldName
	 * @return
	 */
	private int getCounterOfField(String fieldName) {
		int counter = 0;
		try {
			String field = patientDoc.getItemValueString(fieldName);
			if (!field.equals("") && field != null) {
				counter = new Integer (field);
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return counter;
	}
}