package com.primaxis.ims.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsonable;
import org.json.simple.Jsoner;

import com.primaxis.ims.api.ApiProvider;
import com.primaxis.ims.model.Notification;
import com.primaxis.ims.utils.Constants;
import com.primaxis.ims.utils.Globals;

import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.Session;

public class NotificationController extends ApiProvider {
	private Notification notification;
	private JsonArray notificationsJson;
	private int count;
	private int totalResults;
	private int limit = Constants.defaultNumberOfNotificationsToBeReturned; //default number of notifications to be returned
	private int limitAll = Constants.maxNumberOfNotificationsToBeReturned; //Max number of notifications to be returned
	
	public NotificationController(Database db, JsonObject jsonObjRequest, com.primaxis.ims.utils.Log apiLog,
			HttpServletResponse HttpServletResponse, Session session, String languageServiceProvider,
			String consumerName, Map<String, Object> params) throws NotesException, IOException {
		super(db, jsonObjRequest, apiLog, HttpServletResponse, session, languageServiceProvider, consumerName, params);
		notification = new Notification(this, session, interpreterDocKey);
		setMobileApp(true);
	}

	@Override
	public String generateResponse() throws NotesException {
		responseJson = new JsonObject();
		requestId = Globals.requestID();
		messageArray = new JsonArray();
		if (messageCode.size() > 0) {
			for (int code : messageCode) {
				JsonObject ob = new JsonObject();
				ob.put(Integer.toString(code), Globals.getMessageFromCode(code, session, kwHandlerIMSDB));
				if (!messageArray.contains(ob)) {
					messageArray.add(ob);
				}
			}
		}
		if (isInValid) {
			success = false;
		}
		if (messageArray.size() > 0) {
			dataJson.put(isInValid ? "errors" : "messages", messageArray);
		}
		if (notificationsJson != null && !notificationsJson.isEmpty()) {
			dataJson.put("notifications", notificationsJson);
		}
		dataJson.put("limit", limit);
		dataJson.put("count", count);
		dataJson.put("total", totalResults);
		if (!dataJson.isEmpty()) {
			responseJson.put("data",  dataJson);
		}
		response.setStatus(httpResponse);
		responseJson.put("code", httpResponse);
		responseJson.put("text", Globals.getHTTPResponseMsg(httpResponse));
		responseJson.put("success", success);
		responseJson.put("requestId", requestId);
		if (responseJson instanceof Jsonable) {
			responseJsonString = (String) Jsoner.serialize(responseJson);
		}
		if (isInValid) {
			Log.logAPIError(responseJsonString, interpreterDocKey, Globals.getHTTPResponseMsg(httpResponse));
		}
		Log.updateLogField("API_Success", String.valueOf(success));
		
		return responseJsonString;
	}
	
	/**
	 * Retrieve all notifications for a user for a mobile application
	 * 
	 * @throws NotesException
	 * @throws ParseException 
	 */
	public void getAll() throws NotesException, ParseException {	
		String searchBy = null;
		String searchByValue = null;
		int limitReq = 0;
		if (params.size() > 0) {
			if (params.get("userId") != null) {
				searchBy = "InterpreterDocKey";
				searchByValue = interpreterDocKey;
			}
			if (params.get("limit") != null) {
				limitReq = new Integer((String) params.get("limit"));
				if (Globals.validateIntValue(limitReq) > 0 
						&& Globals.validateIntValue(limitReq) <= limitAll)  {
					limit = Globals.validateIntValue(limitReq);
				}
			} else {
				limit = limitAll;
			}
		}
		if (isMobileApp() && searchBy != null && !searchByValue.equals("")) {
			JsonArray searchedNotifications = new JsonArray();
			searchedNotifications  = notification.searchAllNotifications(searchBy, searchByValue);
			if (searchedNotifications != null && searchedNotifications.size() > 0 && Globals.isValidJsonArray(searchedNotifications)) { //Get JSON from view column and process to make sure it is a valid JSON format
				totalResults = searchedNotifications.size();
				//Sort array by creation date descending
				JsonArray sortedNotifications = Globals.getSortedList(searchedNotifications, "creationDateTime", false);
				if (sortedNotifications.size() > 0) {
					JsonArray arrList = new JsonArray();
					Iterator<?> it = sortedNotifications.iterator();
					int i = 0;
					while(it.hasNext()) {
						JsonObject retNotification = new JsonObject();
						JsonObject thisNotification = (JsonObject) it.next();
						if (thisNotification != null) {
							String id = (String) thisNotification.get("id");
							String bookingStatus = (String) thisNotification.get("bookingStatus");
							String bookingId = (String) thisNotification.get("bookingId");
							String title = (String) thisNotification.get("title");
							String text = (String) thisNotification.get("text");
							String creationDateTime = (String) thisNotification.get("creationDateTime");
							retNotification.put("id", id);
							retNotification.put("bookingStatus", bookingStatus);
							retNotification.put("bookingId", bookingId);
							retNotification.put("title", title);
							retNotification.put("text", text);
							retNotification.put("creationDateTime", creationDateTime);
						}
						if (retNotification.size()>0) {
							arrList.add(retNotification);
						}
						i++;
						if (i>=limit) {
							break;
						}
					}
					notificationsJson = arrList;
					count = i;
				}
			}
		}
	}

}
