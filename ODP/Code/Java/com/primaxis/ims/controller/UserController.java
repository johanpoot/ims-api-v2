package com.primaxis.ims.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.DeserializationException;
import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsonable;
import org.json.simple.Jsoner;

import com.primaxis.ims.api.ApiProvider;
import com.primaxis.ims.model.User;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.Constants;

import lotus.domino.Agent;
import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewEntryCollection;

public class UserController extends ApiProvider {
	private User user;
	private StringBuilder userLog;
	private boolean isCheckinFeatureEnabled = false;
	private String checkinStatus = null;
	private boolean isCompareServiceEnabled = false;
	
	public UserController(Database db, JsonObject jsonObjRequest, com.primaxis.ims.utils.Log apiLog,
			HttpServletResponse HttpServletResponse, Session session, String languageServiceProvider,
			String consumerName, Map<String, Object> params) throws NotesException, IOException {
		super(db, jsonObjRequest, apiLog, HttpServletResponse, session, languageServiceProvider, consumerName, params);
		user = new User(session, interpreterDocKey);
		user.setNowDateTime(nowDateTime);
		user.setNowDateTimeFormatted(nowDateTimeFormatted);
	}

	@Override
	public String generateResponse() throws NotesException {
		responseJson = new JsonObject();
		requestId = Globals.requestID();
		messageArray = new JsonArray();
		if (messageCode.size() > 0) {
			for (int code : messageCode) {
				JsonObject ob = new JsonObject();
				ob.put(Integer.toString(code), Globals.getMessageFromCode(code, session, kwHandlerIMSDB));
				if (!messageArray.contains(ob)) {
					messageArray.add(ob);
				}
			}
		}
		if (isInValid) {
			success = false;
		} else {
			if (user.getDocKey() != null) {
				Log.updateLogField("ID", user.getDocKey());
				dataJson.put("userId", user.getDocKey());
				dataJson.put("checkinStatus", this.checkinStatus);
			}
		}
		if (messageArray.size() > 0) {
			dataJson.put(isInValid ? "errors" : "messages", messageArray);
		}
		if (!dataJson.isEmpty()) {
			responseJson.put("data",  dataJson);
		}
		response.setStatus(httpResponse);
		responseJson.put("code", httpResponse);
		responseJson.put("text", Globals.getHTTPResponseMsg(httpResponse));
		responseJson.put("success", success);
		responseJson.put("requestId", requestId);
		if (responseJson instanceof Jsonable) {
			responseJsonString = (String) Jsoner.serialize(responseJson);
		}
		if (isInValid) {
			Log.logAPIError(responseJsonString, interpreterDocKey, Globals.getHTTPResponseMsg(httpResponse));
		}
		Log.updateLogField("API_Success", String.valueOf(success));
		
		return responseJsonString;
	}
	
	/**
	 * Get user details
	 * @return
	 */
	public void getUser() {
		if (user.getDoc() != null) {
			if (user.getDocKey() != null) { dataJson.put("userId", user.getDocKey()); }
			if (user.getFirstName() != null) { dataJson.put("firstName", user.getFirstName()); }
			if (user.getLastName() != null) { dataJson.put("lastName", user.getLastName()); }
			if (user.getGender() != null) { dataJson.put("gender", user.getGender()); }
			if (user.getEmail() != null) { dataJson.put("email", user.getEmail()); }
		} else {
			httpResponse = 404;
			messageCode.add(5001);
			isInValid = true;
		}
	}

	/**
	 * Update user
	 * Add more as required
	 */
	public void updateUser() {
		try {
			if (requestJson.size() > 0 || params.get("deviceId") != null) {
				userLog = new StringBuilder();
				Globals.printCode("Current user.getDeviceId(): " +user.getDeviceId());
				if (user.getDeviceId() == null) {
					user.setDeviceId("");
				}
				if (requestJson.get("deviceId") != null && !user.getDeviceId().equals(requestJson.get("deviceId").toString())) {
					Globals.printCode("New requestJson: " +requestJson.get("deviceId"));
					user.setDeviceId(requestJson.get("deviceId").toString());
					userLog.append("Device Id updated to \"" + requestJson.get("deviceId").toString() + "\". ");
				} else if (params.get("deviceId") != null && !user.getDeviceId().equals(params.get("deviceId").toString().trim())) {
					Globals.printCode("New user param deviceid: " + params.get("deviceId").toString().trim());
					user.setDeviceId(params.get("deviceId").toString().trim());
					userLog.append("Device Id updated to \"" + params.get("deviceId").toString() + "\". ");
				}
				if (requestJson.get("deviceOs") != null
						&& user.getDeviceOs() != null
						&& !user.getDeviceOs().equals(requestJson.get("deviceOs").toString())) {
					user.setDeviceOs(requestJson.get("deviceOs").toString());
					userLog.append("Device operating system updated to \"" + requestJson.get("deviceOs").toString() + "\". ");
				}
				if (userLog.length() > 0) {
					userLog.append("- by user: " + user.getDocKey() + " and API consumer: " + consumerName);
					user.setUserLog(userLog);
				}
				user.update();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	/**
	 * @return the isCheckinFeatureEnabled
	 */
	/*public boolean isCheckinFeatureEnabled() {
		String configJson = kwHandlerIMSDB.getKeywordValueString(Constants.KW_ONDEMAND_CONFIG);
		if (!configJson.isEmpty()) {
			JsonArray config = Jsoner.deserialize(configJson, new JsonArray());
			JsonObject onDemandConfig = (JsonObject) config.get(0);
			if (onDemandConfig.size() > 0 && onDemandConfig.get("featureEnabled") != null && onDemandConfig.get("featureEnabled").toString().equalsIgnoreCase("yes")) {
				isCheckinFeatureEnabled = true;
			}
		}
		return isCheckinFeatureEnabled;
	}*/

	/**
	 * @return the isCheckinFeatureEnabled
	 */
	/*public boolean isCompareServiceEnabled() {
		String configJson = kwHandlerIMSDB.getKeywordValueString(Constants.KW_ONDEMAND_CONFIG);
		if (!configJson.isEmpty()) {
			JsonArray config = Jsoner.deserialize(configJson, new JsonArray());
			JsonObject onDemandConfig = (JsonObject) config.get(0);
			if (onDemandConfig.size() > 0 && onDemandConfig.get("compareService") != null && onDemandConfig.get("compareService").toString().equalsIgnoreCase("yes")) {
				isCompareServiceEnabled  = true;
			}
		}
		return isCompareServiceEnabled;
	}*/
	
	/**
	 * Check-in interpreter user
	 */
	public void checkin() {		
		try {
			double currentLat = Globals.validateStringToDouble(requestJson.get("latitude"));
			double currentLong = Globals.validateStringToDouble(requestJson.get("longitude"));
			View vwCheck = ImsHomeDb.getView("CHKIN_ActiveByUserDockey");
			Document docCheckin = vwCheck.getDocumentByKey(user.getDocKey(),true);
			if (docCheckin == null) {
				docCheckin = ImsHomeDb.createDocument();
			}
			boolean useTwoLanguages = false;
			if (kwHandlerIMSDB.getKeywordValueString("Use_Two_Languages").equalsIgnoreCase("Yes")) {
				useTwoLanguages = true;
			}
			this.checkinStatus = "checked-in";
			docCheckin.replaceItemValue("Form" , "CHKIN");
			docCheckin.replaceItemValue("CHKIN_Status" , "Checked-in");	
			docCheckin.replaceItemValue("Dockey_User" , user.getDocKey());
			docCheckin.replaceItemValue("CHKIN_Location" , "");
			
			//Languages from User Profile
			if(useTwoLanguages) {
				docCheckin.replaceItemValue("CHKIN_Language_From", user.getLanguageFrom());
			}
			docCheckin.replaceItemValue("CHKIN_Languages", user.getLanguages());
			docCheckin.replaceItemValue("CHKIN_UserFullName", user.getFullName());
			docCheckin.replaceItemValue("CHKIN_UserPhone", user.getPhone());
			docCheckin.replaceItemValue("CHKIN_UserMobile", user.getMobile());
			docCheckin.replaceItemValue("CHKIN_UserEmail", user.getEmail());
			docCheckin.replaceItemValue("UNID_User", user.getUniversalId());
			
			String strDetails = user.getDocKey() + "~" + user.getFullName() + "~" + user.getMobile() + "~" + user.getEmail();
			docCheckin.replaceItemValue("CHKIN_UserDetails" , strDetails);
			long ndtFormattedNumberFrom = Long.parseLong(Globals.getFormattedNotesDateTimeCurrent(nowDateTime, "yyyyMMddHHmm"));
			docCheckin.replaceItemValue("CHKIN_From_Number" , ndtFormattedNumberFrom);
			String dateTimeToFormat = globalDateFormat + " " + globalTimeFormat;
			String ndtFormattedFrom  = Globals.getFormattedNotesDateTimeCurrent(nowDateTime, dateTimeToFormat);
			DateTime ndtFrom = session.createDateTime(ndtFormattedFrom);
			docCheckin.replaceItemValue("CHKIN_From_Date" , ndtFrom);	
			if (requestJson.get("latitude") != null) {
				docCheckin.replaceItemValue("CHKIN_Latitude", currentLat);	
			}
			if (requestJson.get("longitude") != null) {
				docCheckin.replaceItemValue("CHKIN_Longitude", currentLong);	
			}
			if (requestJson.get("address") != null) {
				docCheckin.replaceItemValue("CHKIN_Mobile_Location_Address", requestJson.get("address"));	
			}
			if (requestJson.get("suburb") != null) {
				docCheckin.replaceItemValue("CHKIN_Mobile_Location_Suburb", requestJson.get("suburb"));	
			}
			if (requestJson.get("state") != null) {
				docCheckin.replaceItemValue("CHKIN_Mobile_Location_State", requestJson.get("state"));	
			}
			if (requestJson.get("postCode") != null) {
				docCheckin.replaceItemValue("CHKIN_Mobile_Location_Postcode", requestJson.get("postCode"));	
			}
			if (requestJson.get("country") != null) {
				docCheckin.replaceItemValue("CHKIN_Mobile_Location_Country", requestJson.get("country"));	
			}
			//find clinic near checked in location
			Document docCln = findNearByClinic(currentLat, currentLong);
			if (docCln != null) {
				/*if(isCompareServiceEnabled()) {
					docCheckin.replaceItemValue("CHKIN_Location" , docCln.getItemValueString("CLN_Location_Name"));
					docCheckin.replaceItemValue("CHKIN_Site" , docCln.getItemValueString("CLN_Site_Name"));	
				} else {*/
					docCheckin.replaceItemValue("CHKIN_Site" , docCln.getItemValueString("CLN_Site_Name"));
					docCheckin.replaceItemValue("CHKIN_ClinicCode" , docCln.getItemValueString("CLN_Code"));
				//}
			}
			docCheckin.computeWithForm(false,false);
			docCheckin.save();
			vwCheck.refresh();
			updateScheduler();
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Scheduler update to indicate whether user is checked in or checked out
	 */
	private void updateScheduler() {
		Agent agtJson;
		try {
			agtJson = ImsHomeDb.getDatabase().getAgent(Constants.AGENT_RESOURCE_CALENDAR_JSON);
			Document docParam = ImsHomeDb.getDatabase().createDocument();
			docParam.replaceItemValue("UserKey",user.getDocKey());
			docParam.save();
			agtJson.run(docParam.getNoteID());
			docParam.replaceItemValue("Form" , "Param_Delete");
		    docParam.save();
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Find nearby clinic for a current latitude and longitude
	 * @param currentLat
	 * @param currentLong
	 * @return
	 */
	private Document findNearByClinic(double currentLat, double currentLong) {
		Document clinic = null;
		View vwCheck = ImsHomeDb.getView("lkpClnByLat&Long");
		double smallestDistance = -1.0;
		try {
			if (vwCheck != null) {
				ViewEntryCollection allEntries = vwCheck.getAllEntries();
				if (allEntries.getCount() > 0) {
					ViewEntry entry = allEntries.getFirstEntry();
					while (entry != null) {
						double clinicLat = Double.parseDouble(entry.getColumnValues().elementAt(0).toString());
						double clinicLong = Double.parseDouble(entry.getColumnValues().elementAt(1).toString());
						ViewEntry tmpentry = allEntries.getNextEntry(entry);
						double distance = Globals.getDistance(currentLat, currentLong, clinicLat, clinicLong);
						if(smallestDistance == -1.0 || distance < smallestDistance){
					        smallestDistance = distance;
					        clinic = entry.getDocument();
					    }
						entry.recycle();
						entry = tmpentry;
					}
					allEntries.recycle();
				}
				vwCheck.recycle();
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return clinic;
	}
	
	/**
	 * Check-out interpreter user
	 */
	public void checkout() {
		try {
			View vwCheck = ImsHomeDb.getView("CHKIN_ActiveByUserDockey");
			Document docCheckin = vwCheck.getDocumentByKey(user.getDocKey(),true);
			if (docCheckin != null){
				docCheckin.replaceItemValue("CHKIN_Status", "Checked-out");
				String dateTimeToFormat = globalDateFormat + " " + globalTimeFormat;
				String ndtFormattedFrom  = Globals.getFormattedNotesDateTimeCurrent(nowDateTime, dateTimeToFormat);
				long ndtFormattedNumberFrom = Long.parseLong(Globals.getFormattedNotesDateTimeCurrent(nowDateTime, "yyyyMMddHHmm"));
				DateTime ndtFrom = session.createDateTime(ndtFormattedFrom);
				docCheckin.replaceItemValue("CHKIN_Till_Date" , ndtFrom);	
				docCheckin.replaceItemValue("CHKIN_Till_Number" , ndtFormattedNumberFrom);		
				docCheckin.save();
				vwCheck.refresh();
				updateScheduler();
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Check user checkin status
	 * @return
	 */
	public void checkinStatus() {
		Document docCheckin;
		try {
			View vwCheck = ImsHomeDb.getView("CHKIN_ActiveByUserDockey");
			docCheckin = vwCheck.getDocumentByKey(user.getDocKey(), true);
			if (docCheckin != null) {
				checkinStatus = docCheckin.getItemValueString("CHKIN_Status").toLowerCase();
			} else {
				checkinStatus = null;
			}
		}  catch (NotesException e) {
			e.printStackTrace();
		}
	}
}
