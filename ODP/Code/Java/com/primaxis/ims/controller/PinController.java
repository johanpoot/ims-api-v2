package com.primaxis.ims.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsonable;
import org.json.simple.Jsoner;

import com.primaxis.ims.api.ApiProvider;
import com.primaxis.ims.model.Pin;
import com.primaxis.ims.utils.Constants;
import com.primaxis.ims.utils.Globals;

import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.Document;

/**
 * //TODO - Remove PIN Form as we have new fields on clinic, facility and service and validating against a view only.
 * @author Baljit Kaur
 *
 */
public class PinController extends ApiProvider {

	private Pin pin;
	private String clientId;
	private String clientPin;
	public PinController(Database db, JsonObject jsonObjRequest, com.primaxis.ims.utils.Log apiLog,
			HttpServletResponse HttpServletResponse, Session session, String languageServiceProvider,
			String consumerName, Map<String, Object> params) throws NotesException, IOException {
		super(db, jsonObjRequest, apiLog, HttpServletResponse, session, languageServiceProvider, consumerName, params);
		try {
			pin = new Pin(this, session);
		} catch(Exception e) {
			Globals.printException(e);
		}
	}

	/**
	 * Generate response
	 */
	public String generateResponse() throws NotesException {
		responseJson = new JsonObject();
		requestId = Globals.requestID();
		messageArray = new JsonArray();
		if (!validatePin()) {
			isInValid = true;
			httpResponse = 404;
			messageCode.add(1007);
		}
		if (messageCode.size() > 0) {
			for (int code : messageCode) {
				JsonObject ob = new JsonObject();
				ob.put(Integer.toString(code), Globals.getMessageFromCode(code, session, kwHandlerIMSDB));
				if (!messageArray.contains(ob)) {
					messageArray.add(ob);
				}
			}
		}
		if (isInValid) {
			success = false;
		} else {
			if (clientId != null) {
				responseJson.put("client_id", clientId);
			}
			if (clientPin != null) {
				responseJson.put("client_pin", clientPin);
			}
		}
		if (messageArray.size() > 0) {
			dataJson.put(isInValid ? "errors" : "messages", messageArray);
		}
		if (!dataJson.isEmpty()) {
			responseJson.put("data",  dataJson);
		}
		response.setStatus(httpResponse);
		responseJson.put("code", httpResponse);
		responseJson.put("text", Globals.getHTTPResponseMsg(httpResponse));
		responseJson.put("success", success);
		responseJson.put("requestId", requestId);
		if (responseJson instanceof Jsonable) {
			responseJsonString = (String) Jsoner.serialize(responseJson);
		}
		if (isInValid) {
			Log.logAPIError(responseJsonString, pin.getId(), Globals.getHTTPResponseMsg(httpResponse), interpreterDocKey);
		}
		Log.updateLogField("API_Success", String.valueOf(success));
		
		return responseJsonString;
	}
	
	/**
	 * Validate and assign values from requested JSON object to booking objects
	 * @return - void
	 * @throws NotesException 
	 * @throws URISyntaxException 
	 * @throws MalformedURLException 
	 */
	public void getRequest() {
		clientId = Globals.getIgnoreCase(requestJson, "client_id");
		clientPin = Globals.getIgnoreCase(requestJson, "client_pin");
		//pin.getPin(clientPin);
		Globals.printCode("record client/pin details = " + clientId + "/" + clientPin);
		Log.updateLogField("client_id", clientId);
		Log.updateLogField("client_pin", clientPin);
	}
	
	/**
	 * Validate client id. //May be clinic code - unique//facility
	 * //TODO - Update function to validate from facility, clinic or service 
	 * @return
	 */
	public boolean validateClientId() {
		return true;
		/*if (clientId != null) {
			if (clientId.equals(kwHandlerIMSDB.getKeywordValueString(Constants.KW_IMS_CONNECT_CLIENT_ID))) {
				return true;
			}
		}
		return false;*/
	}

	/**
	 * Validate pin if it exists to a booking
	 * @return
	 */
	public boolean validatePin() {
		return pin.verifyPin(clientId, clientPin);
		/*if (pin.getDoc() != null) {
			return true;
		}
		return false;*/
	}
	
	public void createPin() {
		Document document = ImsHomeDb.createDocument();
		try {
			document.replaceItemValue("Form", pin.getForm());
			document.replaceItemValue(pin.getFormPrefix() + "Id", clientPin);
			document.replaceItemValue(pin.getFormPrefix() + "DocKey", "BK-24003001");//TODO change to dynamic
			document.save();
		} catch (NotesException e) {
			Globals.printException(e);
		}
	}

}
