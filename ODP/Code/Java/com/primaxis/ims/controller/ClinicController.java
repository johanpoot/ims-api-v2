package com.primaxis.ims.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsonable;
import org.json.simple.Jsoner;

import com.primaxis.ims.api.ApiProvider;
import com.primaxis.ims.model.Clinic;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.Log;

import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.Session;

public class ClinicController extends ApiProvider {
	private Clinic clinic;
	private JsonArray clinicsJson;
	private int count = 0;
	
	public ClinicController(Database db, JsonObject jsonObjRequest, Log apiLog, HttpServletResponse response, Session session,String languageServiceProvider, String consumerName, Map<String, Object> params) throws NotesException, IOException, ParseException  {
		super(db, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
		clinic = new Clinic(this, session);
	}

	@Override
	public String generateResponse() throws NotesException {
		responseJson = new JsonObject();
		requestId = Globals.requestID();
		messageArray = new JsonArray();
		if (messageCode.size() > 0) {
			for (int code : messageCode) {
				JsonObject ob = new JsonObject();
				ob.put(Integer.toString(code), Globals.getMessageFromCode(code, session, kwHandlerIMSDB));
				if (!messageArray.contains(ob)) {
					messageArray.add(ob);
				}
			}
		}
		if (isInValid) {
			success = false;
		}
		if (messageArray.size() > 0) {
			dataJson.put(isInValid ? "errors" : "messages", messageArray);
		}
		if (clinicsJson != null && !clinicsJson.isEmpty()) {
			dataJson.put("clinics", clinicsJson);
		}
		dataJson.put("count", count);
		if (!dataJson.isEmpty()) {
			responseJson.put("data",  dataJson);
		}
		response.setStatus(httpResponse);
		responseJson.put("code", httpResponse);
		responseJson.put("text", Globals.getHTTPResponseMsg(httpResponse));
		responseJson.put("success", success);
		responseJson.put("requestId", requestId);
		if (responseJson instanceof Jsonable) {
			responseJsonString = (String) Jsoner.serialize(responseJson);
		}
		if (isInValid) {
			Log.logAPIError(responseJsonString, interpreterDocKey, Globals.getHTTPResponseMsg(httpResponse));
		}
		Log.updateLogField("API_Success", String.valueOf(success));
		
		return responseJsonString;
	}
	
	/**
	 * Retrieve all clinics
	 * @return 
	 * 
	 * @throws NotesException
	 * @throws ParseException 
	 */
	public JsonArray getAll() throws NotesException, ParseException {
			clinicsJson  = clinic.getAll();
			count = clinicsJson.size();
			return clinicsJson;
	}
}
