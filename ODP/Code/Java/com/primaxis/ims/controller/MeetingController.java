package com.primaxis.ims.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsonable;
import org.json.simple.Jsoner;

import com.primaxis.ims.api.ApiProvider;
import com.primaxis.ims.model.Meeting;
import com.primaxis.ims.utils.Globals;

import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.Document;

public class MeetingController extends ApiProvider {

	private Meeting meeting;
	private String meetingId;
	private String language;
	private String languageCode;
	
	public MeetingController(Database db, JsonObject jsonObjRequest, com.primaxis.ims.utils.Log apiLog,
			HttpServletResponse HttpServletResponse, Session session, String languageServiceProvider,
			String consumerName, Map<String, Object> params) throws NotesException, IOException {
		super(db, jsonObjRequest, apiLog, HttpServletResponse, session, languageServiceProvider, consumerName, params);
		try {
			meeting = new Meeting(this, session);
		} catch(Exception e) {
			Globals.printException(e);
		}
	}

	/**
	 * Generate response
	 */
	public String generateResponse() throws NotesException {
		responseJson = new JsonObject();
		requestId = Globals.requestID();
		messageArray = new JsonArray();
		if (messageCode.size() > 0) {
			for (int code : messageCode) {
				JsonObject ob = new JsonObject();
				ob.put(Integer.toString(code), Globals.getMessageFromCode(code, session, kwHandlerIMSDB));
				if (!messageArray.contains(ob)) {
					messageArray.add(ob);
				}
			}
		}
		if (isInValid || meeting.getDoc() == null) {
			success = false;
			httpResponse = 404;
		} else {
			if (meeting.getId() != null) {
				responseJson.put("meeting_id", meeting.getId());
			}
			if (meeting.getLanguage() != null) {
				responseJson.put("languageName", meeting.getLanguage());
			}
			if (meeting.getLanguageCode() != null) {
				responseJson.put("languageCode", meeting.getLanguageCode());
			}
		}
		if (messageArray.size() > 0) {
			dataJson.put(isInValid ? "errors" : "messages", messageArray);
		}
		if (!dataJson.isEmpty()) {
			responseJson.put("data",  dataJson);
		}
		response.setStatus(httpResponse);
		responseJson.put("code", httpResponse);
		responseJson.put("text", Globals.getHTTPResponseMsg(httpResponse));
		responseJson.put("success", success);
		responseJson.put("requestId", requestId);
		if (responseJson instanceof Jsonable) {
			responseJsonString = (String) Jsoner.serialize(responseJson);
		}
		if (isInValid) {
			Log.logAPIError(responseJsonString, meeting.getId(), Globals.getHTTPResponseMsg(httpResponse), interpreterDocKey);
		}
		Log.updateLogField("API_Success", String.valueOf(success));
		
		return responseJsonString;
	}
	
	/**
	 * Validate and assign values from requested JSON object to booking objects
	 * @return - void
	 * @throws NotesException 
	 * @throws URISyntaxException 
	 * @throws MalformedURLException 
	 */
	public void getRequest() {
		meetingId = Globals.getIgnoreCase(requestJson, "meeting_id");
		meeting.getMeeting(meetingId);
		Log.updateLogField("ID", meetingId);
	}
	
	public void createMeeting() {
		Document document = ImsHomeDb.createDocument();
		try {
			document.replaceItemValue("Form", meeting.getForm());
			document.replaceItemValue(meeting.getFormPrefix()+"Id", meetingId);
			document.replaceItemValue(meeting.getFormPrefix() + "DocKey", "BK-24003001");
			document.replaceItemValue(meeting.getFormPrefix()+"Language", "Punjabi");
			document.replaceItemValue(meeting.getFormPrefix()+"LanguageCode", "5207");
			document.save();
		} catch (NotesException e) {
			Globals.printException(e);
		}
	}

}
