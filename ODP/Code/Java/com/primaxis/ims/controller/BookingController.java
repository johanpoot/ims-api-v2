package com.primaxis.ims.controller;

import com.primaxis.ims.utils.AttachmentHelper;
import com.ibm.xsp.http.UploadedFile;
import com.primaxis.ims.api.ApiProvider;
import com.primaxis.ims.model.Attachment;
import com.primaxis.ims.model.Booking;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletResponse;
import org.json.simple.*;

import lotus.domino.Agent;
import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.DocumentCollection;
import com.primaxis.ims.utils.Log;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.Constants;
	
/**
 * Booking class controller to manage bookings related data
 * 
 * @author BKarwal
 * @since November 2020
 */
public class BookingController extends ApiProvider {
	
	JsonArray jsonArray;
	private Booking booking;
	private JsonArray bookingsJson;
	private JsonArray labelsJson;
	private int count;
	private int totalResults;
	private int limit = Constants.defaultNumberOfBookingsToBeReturned; //default number of bookings to be returned in each request
	private int limitAll = Constants.maxNumberOfBookingsToBeReturned; //Allow max
	private String modifiedSince;
	private String todayDate;
	private String agencyLabel;
	private JsonObject dataJson = new JsonObject();
	public String bookingAction = null;	
	//JSON Variables;
	//Ashish - Feb 2022 - Add external System ID as an additional DocKey which is external
	private String pmsId = null;
	private String id = null;
	private String clinicCode = null;
	private String costCentre = null;
	private String appointmentDate = null;
	private String appointmentTime = null;
	private int duration = 0;
	private String languageCode = null;
	private String externalReference = null;
	private String externalLink = null;
	private String patientType = null;
	private String deliveryMethod = null;
	private String communicationDetails = null;
	private String videoConferencePlatform = null;
	private String notesForLSP = null;
	private String notesForInterpreter = null;
	private String cancellationReason = null;
	private String rejectionReason = null;
	private String rejectedBy = null;
	private String cancelledBy = null;
	private String cancellationRequestedBy = null;
	private JsonObject ethnicityPreference = new JsonObject();
	private JsonObject genderPreference = new JsonObject();
	private JsonArray professionalDetails = new JsonArray();
	private JsonArray patientDetails = new JsonArray();
	private JsonArray interpreterDetails = new JsonArray();
	private JsonArray contactDetails = new JsonArray();
	private JsonObject patientAddress = new JsonObject();
	private Boolean isCompleted = null;
	private Boolean isServiceDelivered = null;
	private String actualAppointmentStartTime = null;
	private int actualDuration = 0;
	private double waitingDuration = 0;
	private double numCalls = 0;
	private String completionNotes = null;
	private String nonCompletionReason = null;
	private double distanceTravelled = 0;
	private double travelTime = 0;
	private double otherCharges = 0;
	private JsonArray attachments = new JsonArray();
	private String createdDate;
	private boolean isMultiBookingsAllowed = false;
	private String isFlexible = null;
	private String flexibleStartTime = null;
	private String flexibleEndTime = null;
	private String modality = null;
	private String systemComments = null;
	
	/**
	 * 
	 * @param db
	 * @param jsonObjRequest
	 * @param apiLog
	 * @param response 
	 * @throws NotesException
	 * @throws IOException
	 * @throws ParseException 
	 */
	public BookingController(Database db, JsonObject jsonObjRequest, Log apiLog, HttpServletResponse response, Session session,String languageServiceProvider, String consumerName, Map<String, Object> params) throws NotesException, IOException, ParseException  {
		super(db, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
		try {
			apiLog.saveAPILog();
			agencyLabel = kwHandlerIMSDB.getKeywordValueString("agency_label");
			todayDate = Globals.formatDate(nowDateTime, "yyyyMMdd");
			modifiedSince = Globals.getNextOrPrevYearDate(false, "yyyyMMdd", todayDate);
			booking = new Booking(this, session);
			booking.setNowDateTime(nowDateTime);
			booking.setNowDateTimeFormatted(nowDateTimeFormatted);
			if (getKeywordValue(Constants.KW_ALLOW_MULTI_BOOKINGS) != null && getKeywordValue(Constants.KW_ALLOW_MULTI_BOOKINGS).equalsIgnoreCase(Constants.YES)) {
				isMultiBookingsAllowed  = true;
			}
		} catch(Exception e) {
			Globals.printException(e);
		}
	}
	
	/**
	 * Generate response
	 */
	public String generateResponse() throws NotesException {
		responseJson = new JsonObject();
		requestId = Globals.requestID();
		messageArray = new JsonArray();
		if (messageCode.size() > 0) {
			for (int code : messageCode) {
				JsonObject ob = new JsonObject();
				ob.put(Integer.toString(code), Globals.getMessageFromCode(code, session, kwHandlerIMSDB));
				if (!messageArray.contains(ob)) {
					messageArray.add(ob);
				}
			}
		}
		if (isInValid) {
			success = false;
		} else {
			if (booking != null && booking.getId() != null) {
				Log.updateLogField("ID", booking.getId());
				dataJson.put("imsId", booking.getId());
				
				//Ashish - Support PMS ID from WH
				if(booking.getBookingDoc().hasItem(booking.getFormPrefix() + "PMSID")){
					dataJson.put("pmsId", booking.getBookingDoc().getItemValueString(booking.getFormPrefix() + "PMSID"));
				}
			} else {
				if (bookingsJson != null && !bookingsJson.isEmpty()) {
					dataJson.put("bookings", bookingsJson);
				}
				if (labelsJson != null && !labelsJson.isEmpty()) {
					dataJson.put("labels", labelsJson);
				}
				if(!isMobileApp() && !isLspAllowedToAccessAll) {
					dataJson.put("limit", limit);
					dataJson.put("count", count);
				}
				dataJson.put("total", totalResults);
				try {
					if(!isMobileApp() && !isLspAllowedToAccessAll) {
						modifiedSince = Globals.getJavaFormattedDate(modifiedSince, "yyyyMMdd", "yyyy-MM-dd");
						dataJson.put("modifiedSince", modifiedSince);
					}
				} catch (ParseException e) {
					Globals.printException(e);
				}
			}
		}
		if (messageArray.size() > 0) {
			dataJson.put(isInValid ? "errors" : "messages", messageArray);
		}
		if (!dataJson.isEmpty()) {
			responseJson.put("data",  dataJson);
		}
		response.setStatus(httpResponse);
		responseJson.put("code", httpResponse);
		responseJson.put("text", Globals.getHTTPResponseMsg(httpResponse));
		responseJson.put("success", success);
		responseJson.put("requestId", requestId);
		if (responseJson instanceof Jsonable) {
			responseJsonString = (String) Jsoner.serialize(responseJson);
		}
		if (isInValid) {
			Log.logAPIError(responseJsonString, booking.getId(), Globals.getHTTPResponseMsg(httpResponse), interpreterDocKey);
		}
		Log.updateLogField("API_Success", String.valueOf(success));
		
		return responseJsonString;
	}
	
	/**
	 * Validate and assign values from requested JSON object to booking objects
	 * @return - void
	 * @throws NotesException 
	 * @throws URISyntaxException 
	 * @throws MalformedURLException 
	 */
	public void getRequest() throws NotesException, MalformedURLException, URISyntaxException {
		try {
			id = Globals.getIgnoreCase(requestJson, "imsId");
			Log.updateLogField("ID", id);
			//Ashish - Feb 2022 - Add pmsID which is External DocKey
			if (requestJson.get("pmsId") != null) {
				pmsId = Globals.getIgnoreCase(requestJson, "pmsId");
			}else{
				pmsId = Globals.getIgnoreCase(requestJson, "imsId");
			}
			
			clinicCode = Globals.getIgnoreCase(requestJson, "clinicCode");
			costCentre = Globals.getIgnoreCase(requestJson, "costCentre");
			appointmentDate = Globals.getIgnoreCase(requestJson, "appointmentDate");
			appointmentTime = Globals.getIgnoreCase(requestJson, "appointmentTime");
			// TODO - Update below to implement ignore case
			if (requestJson.get("duration") != null && Globals.isValidIntString(requestJson.get("duration"))) {
				duration = Globals.validateIntValue(requestJson.get("duration"));
			}
			if (requestJson.get("languageCode") != null 
					&& Globals.isValidAlphaIntString(requestJson.get("languageCode"))) {
				languageCode = Globals.validateString(requestJson.get("languageCode"));
			}
			if (requestJson.get("externalReference") != null) {
				externalReference = Globals.validateString(requestJson.get("externalReference"));
			}
			if (requestJson.get("isFlexible") != null) {
				isFlexible = Globals.validateString(requestJson.get("isFlexible"));
			}
			if (requestJson.get("flexibleStartTime") != null) {
				flexibleStartTime = Globals.validateString(requestJson.get("flexibleStartTime"));
			}
			if (requestJson.get("flexibleEndTime") != null) {
				flexibleEndTime = Globals.validateString(requestJson.get("flexibleEndTime"));
			}
			if (requestJson.get("cancellationReason") != null && Globals.isValidString(requestJson.get("cancellationReason"))) {
				cancellationReason = Globals.validateString(requestJson.get("cancellationReason"));
			}
			if (requestJson.get("rejectionReason") != null && Globals.isValidString(requestJson.get("rejectionReason"))) {
				rejectionReason = Globals.validateString(requestJson.get("rejectionReason"));
			}
			if (requestJson.get("cancellationRequestedBy") != null && Globals.isValidString(requestJson.get("cancellationRequestedBy"))) {
				cancellationRequestedBy = Globals.validateString(requestJson.get("cancellationRequestedBy"));
			}
			if (requestJson.get("cancelledBy") != null && Globals.isValidString(requestJson.get("cancelledBy"))) {
				cancelledBy = Globals.validateString(requestJson.get("cancelledBy"));
			}
			if (requestJson.get("rejectedBy") != null && Globals.isValidString(requestJson.get("rejectedBy"))) {
				rejectedBy = Globals.validateString(requestJson.get("rejectedBy"));
			}
			if (requestJson.get("patientType") != null && Globals.isValidString(requestJson.get("patientType"))) {
				patientType = Globals.verifyPatientType(requestJson.get("patientType").toString());
			} 
			if (requestJson.get("patientAddress") != null) {
				if (Globals.isValidJsonObject(requestJson.get("patientAddress"))) {
					patientAddress = Globals.validateJsonObject(requestJson.get("patientAddress"));
				} else {
					invalidDataString.put(3025, requestJson.get("patientAddress").toString());
					isInValid = true;
					messageCode.add(3025);
				}
			}
			//Home Visit is allowed in Updates
			if (requestJson.get("patientType") != null 
					&& requestJson.get("patientType").toString().equalsIgnoreCase(Constants.homeVisit) 
					&& patientAddress == null) {
				invalidDataString.put(3025, requestJson.get("patientAddress").toString());				
				isInValid = true;
				messageCode.add(3025);
			}
			
			if (requestJson.get("deliveryMethod") != null && Globals.isValidString(requestJson.get("deliveryMethod"))) {
				deliveryMethod = Globals.verifyDeliveryMethod(requestJson.get("deliveryMethod").toString());
			}
			if (requestJson.get("modality") != null && Globals.isValidString(requestJson.get("modality"))) {
				modality = Globals.validateString(requestJson.get("modality").toString());
			}
			if (requestJson.get("communicationDetails") != null) {
				communicationDetails = Globals.validateString(requestJson.get("communicationDetails").toString());
			}
			if (requestJson.get("videoConferencePlatform") != null) {
				videoConferencePlatform = Globals.validateString(requestJson.get("videoConferencePlatform").toString());
			}
			if (requestJson.get("externalLink") != null) {
				if (Globals.isValidURL((String) requestJson.get("externalLink"))) {
					externalLink = Globals.validateString(requestJson.get("externalLink"));
				} else {
					invalidDataString.put(3002, requestJson.get("externalLink").toString());
					isInValid = true;
					messageCode.add(3002);
				}
			}
			if (requestJson.get("notesForLSP") != null && Globals.isValidString(requestJson.get("notesForLSP"))) {
				notesForLSP = Globals.validateTextString(requestJson.get("notesForLSP"));
			}
			if (requestJson.get("systemComments") != null && Globals.isValidString(requestJson.get("systemComments"))) {
				systemComments = Globals.validateTextString(requestJson.get("systemComments"));
			}
			if (requestJson.get("notesForInterpreter") != null && Globals.isValidString(requestJson.get("notesForInterpreter"))) {
				notesForInterpreter = Globals.validateTextString(requestJson.get("notesForInterpreter"));
			}
			if (requestJson.get("isServiceDelivered") != null) {
				if (requestJson.get("isServiceDelivered").equals(Constants.TRUE) || requestJson.get("isServiceDelivered").equals(Constants.FALSE)) {
					isServiceDelivered = (Boolean) requestJson.get("isServiceDelivered");
				} else {
					invalidDataString.put(4005, requestJson.get("isServiceDelivered").toString());
					isInValid = true;
					messageCode.add(4005);
				}
			}
			if (requestJson.get("actualAppointmentStartTime") != null && Globals.isValidString(requestJson.get("actualAppointmentStartTime"))) {
				actualAppointmentStartTime = Globals.validateTextString(requestJson.get("actualAppointmentStartTime"));
			}
			if (requestJson.get("actualDuration") != null) {
				if (Globals.validateStringToInt(requestJson.get("actualDuration")) != 0) {
					actualDuration = Globals.validateStringToInt(requestJson.get("actualDuration"));
				} else {
					invalidDataString.put(3029, requestJson.get("actualDuration").toString());
					isInValid = true;
					messageCode.add(3029);
				}
			}
			if (requestJson.get("waitingDuration") != null) {
				if (Globals.validateStringToDouble(requestJson.get("waitingDuration")) != 0) {
					numCalls = Math.round(Globals.validateStringToDouble(requestJson.get("waitingDuration"))*100)/100.00;
				}
				
			}
			if (requestJson.get("travelTime") != null) {
				if (Globals.validateStringToDouble(requestJson.get("travelTime")) != 0) {
					travelTime = Math.round(Globals.validateStringToDouble(requestJson.get("travelTime"))*100)/100.00;

				}
			}
			if (requestJson.get("distanceTravelled") != null) {
				if (Globals.validateStringToDouble(requestJson.get("distanceTravelled")) != 0) {
					distanceTravelled = Math.round(Globals.validateStringToDouble(requestJson.get("distanceTravelled"))*100)/100.00;
				}
			}
			if (requestJson.get("otherCharges") != null) {
				if (Globals.validateStringToDouble(requestJson.get("otherCharges")) != 0) {
					otherCharges = Math.round(Globals.validateStringToDouble(requestJson.get("otherCharges"))*100)/100.00;
				}
			}	
			if (requestJson.get("completionNotes") != null && Globals.isValidString(requestJson.get("completionNotes"))) {
				completionNotes = Globals.validateTextString(requestJson.get("completionNotes"));
			}
			if (requestJson.get("nonCompletionReason") != null && Globals.isValidString(requestJson.get("nonCompletionReason"))) {
				nonCompletionReason = Globals.validateTextString(requestJson.get("nonCompletionReason"));
			}
			/*if (requestJson.get("isManagedByIms") != null && Globals.isValidString(requestJson.get("isManagedByIms"))) { //TODO - TBC
				booking.setManagedByIMS(Globals.validateValue(requestJson.get("isManagedByIms"), true));
			}*/
			if (requestJson.get("ethnicityPreference") != null) {
				if (Globals.isValidJsonObject(requestJson.get("ethnicityPreference"))) {
					ethnicityPreference = Globals.validateJsonObject(requestJson.get("ethnicityPreference"));
				} else {
					invalidDataString.put(4005, requestJson.get("ethnicityPreference").toString());
					isInValid = true;
					messageCode.add(4005);		
				}
			}
			if (requestJson.get("genderPreference") != null) {
				if (Globals.isValidJsonObject(requestJson.get("genderPreference"))) {
					genderPreference = Globals.validateJsonObject(requestJson.get("genderPreference"));
				} else {
					invalidDataString.put(4005, requestJson.get("genderPreference").toString());
					isInValid = true;
					messageCode.add(4005);		
				}
			}
			if (requestJson.get("professionalDetails") != null) {
				if (Globals.isValidJsonArray(requestJson.get("professionalDetails"))) {
					professionalDetails = Globals.validateJsonArray(requestJson.get("professionalDetails"));
				} else {
					invalidDataString.put(4005, requestJson.get("professionalDetails").toString());
					isInValid = true;
					messageCode.add(4005);		
				}
			}
			if (requestJson.get("patientDetails") != null) { 
				if (Globals.isValidJsonArray(requestJson.get("patientDetails"))) {
					patientDetails = Globals.validateJsonArray(requestJson.get("patientDetails"));
				} else {
					invalidDataString.put(4005, requestJson.get("patientDetails").toString());
					isInValid = true;
					messageCode.add(4005);		
				}
			}
			if (requestJson.get("interpreterDetails") != null) {
				if (Globals.isValidJsonArray(requestJson.get("interpreterDetails"))) {
					interpreterDetails = Globals.validateJsonArray(requestJson.get("interpreterDetails"));
				} else {
					invalidDataString.put(4005, requestJson.get("interpreterDetails").toString());
					isInValid = true;
					messageCode.add(4005);		
				}
			}
			if (requestJson.get("contactDetails") != null) {
				if (Globals.isValidJsonArray(requestJson.get("contactDetails"))) {
					contactDetails = Globals.validateJsonArray(requestJson.get("contactDetails"));
				} else {
					invalidDataString.put(4005, requestJson.get("contactDetails").toString());
					isInValid = true;
					messageCode.add(4005);		
				}
			}
		} catch (URISyntaxException e) {
			Globals.printException(e);
			isInValid = true; 
			httpResponse = 500;
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
		}
	} 
	
	/**
	 * Create a new booking
	 * Updates the booking objects with the provided values
	 */
	public void createBooking() throws NotesException {
		try {
			if (!isInValid || isLSPAllowedToCreateIgnoreValid) { //If a booking is valid or LSP allowed to create invalid booking
				if (!isLspAllowedToAccessAll && !isLSPAllowedToCreateIgnoreValid) {
					if (interpreterFullName != null && !interpreterFullName.equals("") && interpreterDocKey != null) { //set agency details and interpreter type to agency
						booking.setInterpreterDocKey(interpreterDocKey);
						booking.setInterpreterFullName(interpreterFullName);
						booking.setInterpreterType(Constants.InterpreterTypeAgency);
						booking.setStatus(Constants.bookingStatusSubmitted); //set submitted status for assigned booking
					} else {
						isInValid = true;
						httpResponse = 401;
					}
				}
				booking.setCreatedDateTime(nowDateTime);
				createdDate = Globals.formatDate(nowDateTime, globalDateFormat);
				booking.setCreatedDate(createdDate);
				booking.setId(Globals.validateString(requestJson.get("imsId")));
				booking.setForm(Constants.formBook);
				booking.setFormPrefix();
				//ASHISH - Feb 2022 - Save the External DocKey reference to the booking				
				if (requestJson.get("pmsId") != null && (isLspAllowedToAccessAll || isLSPAllowedToCreateIgnoreValid)) {				
					if (!booking.isPMSIDExists(Globals.validateString(requestJson.get("pmsId")) , Globals.validateString(requestJson.get("imsId")))){
						booking.setPmsId(Globals.validateString(requestJson.get("pmsId")));
						booking.setSource(Constants.sourcePMS);
						booking.setSourceDisplay(Constants.sourcePMSDisplay);
					} else {
						invalidDataString.put(5002, requestJson.get("pmsId").toString());
						isInValid = true;
						messageCode.add(5002);
						ArrayList<String> existingIMSIds = booking.getAllExistingIMSIdsForPMSId(requestJson.get("pmsId").toString());
						String imsIDs = String.join(", ", existingIMSIds);
						Globals.printCode("imsIDs: " 
								+imsIDs);
						invalidDataString.put(5003, imsIDs);
						messageCode.add(5003);
					}
				}				
				if (clinicCode != null && !clinicCode.equals("")) {
					booking.setClinicCode(clinicCode);
				} else {
					isInValid = true;
					messageCode.add(3005);
				}
				if (costCentre != null && !costCentre.isEmpty()) {
					booking.setCostCentre(costCentre);
				}
				if (duration == 0) {
					duration = new Integer(kwHandlerIMSDB.getKeywordValueString("BOOK_Default_Duration"));
				}
				if (duration != 0) {
					booking.setDuration(duration);
				} else {
					isInValid = true;
					messageCode.add(3029);
				}
				if (appointmentDate != null && appointmentTime != null && !appointmentDate.isEmpty() && !appointmentTime.isEmpty()) {
					DateTime validAppointmentDateTime = Globals.getDateTime(appointmentDate, appointmentTime, session);
					int hoursInPast = new Integer(kwHandlerIMSDB.getKeywordValueString("hoursAllowedToCreateBookingInPast"));
					DateTime nowDateTimeToAdjust = kwHandlerIMSDB.dateOffset(session.createDateTime("Today"));
					nowDateTimeToAdjust.adjustHour(-hoursInPast);
					if (validAppointmentDateTime == null) {
						invalidDataString.put(3003, appointmentDate + " " + appointmentTime);
						isInValid = true;
						messageCode.add(3003);
					} else {
						Date validJavaAppointmentDateTime = validAppointmentDateTime.toJavaDate();
						Date allowedJavaCreateDate = nowDateTimeToAdjust.toJavaDate();
						if (validJavaAppointmentDateTime.before(allowedJavaCreateDate)) { 
							invalidDataString.put(3004, appointmentDate + " " + appointmentTime);
							isInValid = true;
							messageCode.add(3004);
						}
						booking.setAppointmentStartDateTime(validAppointmentDateTime);
						if (Globals.formatDate(validAppointmentDateTime, globalDateFormat) != null) {
							booking.setAppointmentStartDateText (Globals.formatDate(validAppointmentDateTime, globalDateFormat));
						}
						if (Globals.formatTime(validAppointmentDateTime, globalTimeFormat) != null) {
							booking.setAppointmentStartTimeText(Globals.formatTime(validAppointmentDateTime, globalTimeFormat));
						}
						DateTime endDateTime = Globals.getEndDateTime(validAppointmentDateTime, booking.getDuration());
						String endTimeText = Globals.getEndTime(validAppointmentDateTime, booking.getDuration(), session);
						booking.setAppointmentEndDateTime(endDateTime);
						booking.setAppointmentEndTimeText(endTimeText);
					}
				} else {
					invalidDataString.put(3003, appointmentDate + " " + appointmentTime);
					isInValid = true;
					messageCode.add(3003);
				}
				if (languageCode != null) {
					booking.setLanguageCode(languageCode);
				} else {
					invalidDataString.put(3007, languageCode);
					isInValid = true;
					messageCode.add(3007);
				}
				booking.setExternalLink(externalLink);
				if (patientType == null) {
					patientType = kwHandlerIMSDB.getKeywordValueString("BOOK_DefaultPatientType");
				}
				booking.setPatientType(patientType);
				if (deliveryMethod == null) {
					deliveryMethod = kwHandlerIMSDB.getKeywordValueString("BOOK_DefaultDeliveryMethod");
				}
				booking.setDeliveryMethod(deliveryMethod);
				booking.setModality(modality);
				booking.setCommunicationDetails(communicationDetails);
				booking.setVideoConferencePlatform(videoConferencePlatform);
				if (isFlexible != null && isFlexible.equals("true")) {
					booking.setFlexible(true);
				}
				//Set flexible time start and end to required format to save
				if (flexibleStartTime != null) {
					flexibleStartTime = Globals.getJavaFormattedDate(flexibleStartTime, Constants.timeFormat24Hour, globalTimeFormat);
				}
				if (flexibleEndTime != null) {
					flexibleEndTime = Globals.getJavaFormattedDate(flexibleEndTime, Constants.timeFormat24Hour, globalTimeFormat);
				}
				booking.setFlexibleTimeStart(flexibleStartTime);
				booking.setFlexibleTimeEnd(flexibleEndTime);
				booking.setAgencyNotes(notesForLSP);
				booking.setSystemComments(systemComments);
				booking.setInterpreterNotes(notesForInterpreter);
				booking.setEthnicityPreference(ethnicityPreference);
				booking.setGenderPreference(genderPreference);
				if (!isLspAllowedToAccessAll) {
					booking.setStatus(Constants.bookingStatusDefault);				
				} else {
					booking.setStatus(Constants.bookingStatusNew);		
				}
				booking.setKpiSubmittedAgencyBy(interpreterFullName);
				booking.setKpiSubmittedAgencyDate(nowDateTime);
				booking.setKpiSubmittedAgencyDocKey(interpreterDocKey);
		        
				if (externalReference != null && !booking.isExternalIdExist(externalReference, booking.getId())) {
					booking.setExternalReference(externalReference);
				} else {
					
					if(pmsId == null){						
						//Ashish - Feb 2022 - Validate External Reference only if PMS ID is null in request JSON
						invalidDataString.put(3035, pmsId);
						messageCode.add(3035);
						isInValid = true;
					}
				}
				if (professionalDetails.size() > 0) {
					booking.setProfessionals(professionalDetails);
				}
				if (patientDetails.size() > 0) {
					booking.setPatients(patientDetails);
				}
				if (contactDetails.size() > 0) {
					booking.setContacts(contactDetails);
				}
				if (interpreterDetails.size() > 0) {
					booking.setInterpreters(interpreterDetails);
					booking.setKpiAllocatedFirstBy(interpreterFullName);
					booking.setKpiAllocatedFirstDate(nowDateTime);
					booking.setKpiAllocatedFirstDocKey(interpreterDocKey);
					booking.setKpiAllocatedLastBy(interpreterFullName);
					booking.setKpiAllocatedLastDate(nowDateTime);
					booking.setKpiAllocatedLastDocKey(interpreterDocKey);
				}
				
				//Ashish - Feb 2022 - Validate External Reference only when PMS IS is null or blank in the JSON request
				if (clinicCode == null
						|| (booking.getExternalReference() == null && (pmsId == null || pmsId == ""))
						|| booking.getAppointmentStartDateTime() == null 
						|| booking.getLanguageCode() == null 
						|| booking.getDuration() == 0
						|| booking.getContacts() == null 
						|| booking.getContacts().size() == 0
						|| booking.getPatients() == null 
						|| booking.getPatients().size() == 0) {
					isInValid = true;
					messageCode.add(3027);
				}
				//Validate clinic code from the IMS Clinic list and get clinic details
				if (clinicCode != null) {
					JsonObject clinicDetails = kwHandlerIMSDB.getClinicDetails(clinicCode);
					if (clinicDetails != null && clinicDetails.size() > 0) {
						if (!clinicDetails.get("clinicCode").toString().isEmpty()) {
							booking.setClinicCode(clinicDetails.get("clinicCode").toString());
						}
						booking.setSite(clinicDetails.get("siteCode") != null ? (String) clinicDetails.get("siteCode") : "");
						booking.setSiteName(kwHandlerIMSDB.getSiteName(booking.getForm(), clinicDetails.get("siteCode") != null ? (String) clinicDetails.get("siteCode") : ""));
						booking.setLocation(clinicDetails.get("locationName") != null ? (String) clinicDetails.get("locationName") : "");
						booking.setClinic(clinicDetails.get("clinicName") != null ? (String) clinicDetails.get("clinicName") : "");	
						booking.setClinicAddress(clinicDetails.get("clinicAddress") != null ? (String) clinicDetails.get("clinicAddress") : "");
						booking.setClinicSuburb(clinicDetails.get("clinicSuburb") != null ? (String) clinicDetails.get("clinicSuburb") : "");
						booking.setClinicPostCode(clinicDetails.get("clinicPostCode") != null ? (String) clinicDetails.get("clinicPostCode") : "");
						booking.setClinicState(clinicDetails.get("clinicState") != null ? (String) clinicDetails.get("clinicState") : "");
					}else{
						
						//Validate the existance of clinic profile in IMS
						messageCode.add(3005);
						isInValid = true;
						invalidDataString.put(3005, clinicCode);
					}
				}				
				//Validate Cost Centre from the list if not empty
				if (booking.getCostCentre() != null && !booking.getCostCentre().isEmpty()) {
					boolean isValidCostCentre = kwHandlerIMSDB.isValidCostCentre(booking.getSiteName(), booking.getLocation(), booking.getClinic(), booking.getCostCentre());
					if (!isValidCostCentre) {
						invalidDataString.put(3006, booking.getCostCentre());
						messageCode.add(3006);
						isInValid = true;
					}
				}
				String langCode = ImsHomeDb.getLanguageFromCode(booking.getLanguageCode(), false);
				if (langCode != null) {
					booking.setLanguage(langCode);
				} else {
					messageCode.add(3007);
					isInValid = true;
					invalidDataString.put(3007, booking.getLanguageCode());
				}
				if (!isInValid || isLSPAllowedToCreateIgnoreValid) {
					booking.setAgencyLabel(agencyLabel);
					booking.createBooking();
					booking.save();
					httpResponse = 201;
					if (isLSPAllowedToCreateIgnoreValid) {
						isInValid = false; //Validate to return success response for LSPs allowed to create invalid booking
					}
				} else {
					isInValid = true;
					httpResponse = 400;
				}
			} else {
				isInValid = true;
				httpResponse = 400;
			}
		} catch (NotesException | ParseException e) {
			isInValid = true;
			httpResponse = 500;
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}
	
	/**
	 * 
	 * @param bookingStatus
	 * @return
	 */
	public String getKWBookingStatus(String bookingStatus) {
		return kwHandlerIMSDB.getKeywordValueString(Constants.formBook + "_Status_" + bookingStatus);
	}
	
	/**
	 * 
	 * @param accreditationLevel
	 * @return
	 */
	public boolean validateKWAccreditationLevel(String accreditationLevel) {
		return kwHandlerIMSDB.validateAccreditationLevel(accreditationLevel);
	}
	
	/**
	 * 
	 * @param code
	 * @return
	 * @throws NotesException
	 */
	public String getKWEthnicityFromCode(String code) throws NotesException {
		return kwHandlerIMSDB.getEthnicityFromCode(code);
	}
	
	/**
	 * 
	 * @param States
	 * @return
	 * @throws NotesException
	 */
	public String[] getKWStateList(String States) throws NotesException {
		return kwHandlerIMSDB.getKeywordValueArray(States);
	}
	
	/**
	 * Retrieve all agency bookings
	 * 
	 * @throws NotesException
	 * @throws ParseException 
	 */
	public void getAll() throws NotesException, ParseException {		
		try {
			int limitReq = 0;
			String searchBy = null;  
			String operator = "=";  
			String modifiedSinceReq = null;
			String searchByValue = null;
			if (params.size() > 0) {
				if (params.get("limit") != null) {
					limitReq = new Integer((String) params.get("limit"));
					if (Globals.validateIntValue(limitReq) > 0 
							&& Globals.validateIntValue(limitReq) <= Constants.maxNumberOfBookingsToBeReturned)  {
						limit = Globals.validateIntValue(limitReq);
					}
				}	
				if (params.get("modifiedSince") != null) {
					modifiedSinceReq = (String) params.get("modifiedSince");
					if (Globals.isValidDate(modifiedSinceReq, "yyyyMMdd")) {
						modifiedSince = Globals.validatedDate(modifiedSinceReq, "yyyyMMdd"); //yyyyMMdd
						searchBy = "ModifiedDate";
						operator = ">=";
						searchByValue = (String) params.get("modifiedSince");
					} else {
						isInValid = true;
						messageCode.add(3015);
					}
				} else if (params.get("appointmentStartDate") != null) {
					searchBy = "AppointmentStartDate";
					searchByValue = (String) params.get("appointmentStartDate");
				} else if (params.get("appointmentStartDateGt") != null) {
					searchBy = "AppointmentStartDate";
					operator = ">";
					searchByValue = (String) params.get("appointmentStartDateGt");
				} else if (params.get("appointmentStartDateLt") != null) {
					searchBy = "AppointmentStartDate";
					operator = "<";
					searchByValue = (String) params.get("appointmentStartDateLt");
				} else if (params.get("appointmentStartDateGte") != null) {
					searchBy = "AppointmentStartDate";
					operator = ">=";
					searchByValue = (String) params.get("appointmentStartDateGte");
				} else if (params.get("appointmentStartDateLte") != null) {
					searchBy = "AppointmentStartDate";
					operator = "<=";
					searchByValue = (String) params.get("appointmentStartDateLte");
				} else if (params.get("userId") != null) {
					searchBy = "Interpreter";
					operator = "=";
					searchByValue = interpreterDocKey;
				}
				//If not a mobile app, searchByValue is not a date type
				if (!isMobileApp()) {
					if (Globals.isValidDate(searchByValue, "yyyyMMdd")) {
						searchByValue = Globals.validatedDate(searchByValue, "yyyyMMdd"); //yyyyMMdd
					} else {
						isInValid = true;
						messageCode.add(3015);
					}
				}
			}
			JsonArray searchedBookings = new JsonArray();
			if (isLspAllowedToAccessAll) {
				limit = limitAll;
				searchedBookings  = booking.searchAllBookings(searchBy, searchByValue, operator, todayDate, true, false);
			} else if (isMobileApp()) {
				boolean isFutureOnly = false;
				if (params.get("futureBookings") != null) {
					isFutureOnly = Boolean.valueOf((String) params.get("futureBookings"));
				}
				limit = limitAll;
				searchedBookings  = booking.searchAllBookings(searchBy, searchByValue, operator, todayDate, false, isFutureOnly);
			} else {
				if (Globals.validateIntValue(limitReq) > 0 
						&& Globals.validateIntValue(limitReq) <= Constants.maxNumberOfBookingsToBeReturned)  {
					limit = Globals.validateIntValue(limitReq);
				}
				if (modifiedSinceReq != null) {
					if (Globals.isValidDate(modifiedSinceReq, "yyyyMMdd")) {
						modifiedSince = Globals.validatedDate(modifiedSinceReq, "yyyyMMdd"); //yyyyMMdd
					} else {
						isInValid = true;
						messageCode.add(3015);
					}
				}
				searchedBookings  = booking.searchBookings(interpreterDocKey, modifiedSince, todayDate);
			}
			if (searchedBookings != null && Globals.isValidJsonArray(searchedBookings)) { //Get JSON from view column and process to make sure it is a valid JSON format
				totalResults = searchedBookings.size();
				if (searchedBookings.size() > 0) {
					JsonArray arrList = new JsonArray();
					Iterator<?> it = searchedBookings.iterator();
					int i = 0;
					while(it.hasNext()) {
						JsonObject retBooking = new JsonObject();
						String jsonData = it.next().toString(); //convert to string to parse further
						JsonObject thisBooking = Jsoner.deserialize(jsonData, new JsonObject());
						if (thisBooking != null) {
							//Below partial code might be helpful later when child bookings are allowed based on keyword configuration AllowMultiBookings
							//Currently allow all child bookings to be displayed and get updated for mobile app
							/*Boolean isBookingAllowed = false;
							String parentId = (String) thisBooking.get("parentId");
							if (parentId.equals("") || (isMultiBookingsAllowed && !parentId.equals(""))) {
								isBookingAllowed = true;
							}*/
							Integer durationInMinutes = 0;
							String duration = null;
							String id = (String) thisBooking.get("DocKey");
							String status = (String) thisBooking.get("Status");
							if (thisBooking.containsKey("duration") && !thisBooking.get("duration").equals("")) {
								retBooking.put("duration","0");
								durationInMinutes = new Integer((String) thisBooking.get("duration"));
								duration = minutesToHours(new Integer((String) thisBooking.get("duration")));
							}
							String language = (String) thisBooking.get("language");
							String languageCode = (String) thisBooking.get("languageCode");
							String facility = (String) thisBooking.get("facility");
							String service = (String) thisBooking.get("service");
							String patientType = (String) thisBooking.get("patientType");
							String deliveryMethod = (String) thisBooking.get("deliveryMethod");
							JsonObject serviceAddress = (JsonObject) thisBooking.get("serviceAddress");
							String statusDisplay = (String) thisBooking.get("Status_Display");
							String modifiedDateTime = (String) thisBooking.get("LastModifiedDateTime");
							String appointmentStart = (String) thisBooking.get("Appt_Start");
							boolean futureBooking = false;
							if (appointmentStart != null) {
								DateTime appointmentStartDateTime = Globals.getDateTimeFromString(appointmentStart, session);
								if (appointmentStartDateTime != null) {
									String apptStartDate = Globals.formatDate(appointmentStartDateTime, "yyyy-MM-dd");
									String appointmentStartTime = Globals.formatDate(appointmentStartDateTime, Constants.timeFormat24Hour);
									retBooking.put("appointmentStartDate", apptStartDate);
									retBooking.put("appointmentStartTime", appointmentStartTime);									
									if (Globals.isFutureDate(nowDateTime, appointmentStartDateTime, session)) {
										futureBooking = true;
									}									
								}
							}
							//If Booking is not completed due to any reason, return cancelled 
							if (thisBooking.get("Completed") != null && !thisBooking.get("Completed").equals("")) {
								isCompleted = thisBooking.get("Completed").toString().equalsIgnoreCase("Yes") ? true : false;
								if (!isCompleted) {
									String cancellationDateTimeString = (String) thisBooking.get("Cancellation_Time");
									if (!cancellationDateTimeString.equals("") && cancellationDateTimeString != null) {
										DateTime cancellationDateTime = Globals.getDateTimeFromString(cancellationDateTimeString, session);
										String cancellationDT = Globals.formatDate(cancellationDateTime, "yyyy-MM-dd HH:mm");
										retBooking.put("cancellationDateTime", cancellationDT);
									}
									if (!isMobileApp() && !isLspAllowedToAccessAll && !status.equalsIgnoreCase(Constants.bookingStatusRejected)) {
										statusDisplay = Constants.bookingStatusCancelledDisplay;
									}
								}
							}
						
							if (!isLspAllowedToAccessAll && (thisBooking.get("Status").toString().equalsIgnoreCase(Constants.bookingStatusAmended) 
									|| thisBooking.get("Status").toString().equalsIgnoreCase(Constants.bookingStatusCancellationRequested))) {
								statusDisplay = Constants.bookingStatusRescheduling;
							}
							//If mobile app, return status for reallocated with specific display status using KW, if amended, show booked
							if (isMobileApp()) {
								if (status.equalsIgnoreCase(Constants.bookingStatusReallocated)) {
									statusDisplay = getKeywordValue(Constants.KW_STATUS_REALLOCATED_INTERPRETER);
								} else if (status.equalsIgnoreCase(Constants.bookingStatusAmended)) {
									statusDisplay = Constants.bookingStatusBookedDisplay;
								}
								//Allow status override
								String overrideStatus = kwHandlerIMSDB.getKeywordValueString(Constants.KW_MOBILE_BOOKEDSTATUSOVERRIDE);
								if (statusDisplay.equalsIgnoreCase(Constants.bookingStatusBooked) && overrideStatus != null && !overrideStatus.equals("-")) {
									statusDisplay = overrideStatus;
								}
							}
							retBooking.put("imsId", id);
							
							//Ashish - Feb 2022 - Add pmsID if exists in the booking 
							//If the booking has pmsID, then add it to booking
							if(thisBooking.containsKey("pmsId")){
								if(thisBooking.get("pmsId") !=null){
									retBooking.put("pmsId", thisBooking.get("pmsId"));
								}
							}
							
							retBooking.put("status", statusDisplay);
							
							if(isLspAllowedToAccessAll){
								retBooking.put("languageCode", languageCode);
							}
							
							if (isUserAllowed) {
								retBooking.put("languageCode", languageCode);
								retBooking.put("durationInMinutes", durationInMinutes);
								retBooking.put("duration", duration);
								retBooking.put("language", language);
								retBooking.put("serviceAddress", serviceAddress);
								retBooking.put("patientType", patientType);
								retBooking.put("deliveryMethod", deliveryMethod);
								retBooking.put("facility", facility);
								retBooking.put("service", service);
								retBooking.put("futureBooking", futureBooking);
								boolean isInComplete = false;
								if (thisBooking.get("Completed") == null || thisBooking.get("Completed").toString().isEmpty()) {
									isInComplete = true;
								}
								retBooking.put("isInComplete", isInComplete);
							}
							if(!isMobileApp() && !isLspAllowedToAccessAll && !isUserAllowed) {
								retBooking.put("modifiedDateTime", modifiedDateTime);
							}
						}
						if (retBooking.size()>0) {
							arrList.add(retBooking);
						}
						i++;
						if (i>=limit) {
							break;
						}
					}
					bookingsJson = arrList;
					count = i;
				}
			}
		} catch (NotesException e) {
			Globals.printException(e);
			isInValid = true; 
			httpResponse = 500;
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
		}
		
	}
	
	/**
	 * Get booking details from given id
	 * 
	 * @param id - Unique Booking ID
	 * @throws NotesException
	 * @throws NotesException
	 */
	public void getOne(String id) throws NotesException {
		try {
			if (id != null) {
				booking = new Booking(this, id, session);
				if (booking.getBookingDoc() != null) {
					if ((isUserAllowed && interpreterDocKey != null && interpreterDocKey.equalsIgnoreCase(booking.getInterpreterDocKey())) 
							|| isLspAllowedToAccessAll 
							|| (interpreterFullName!=null && interpreterFullName.equalsIgnoreCase(booking.getInterpreterFullName()) && interpreterDocKey.equalsIgnoreCase(booking.getInterpreterDocKey()))) {
						//If booking status is allowed or completed with YES or completed with NO and not cancelled OR give full access to allowed user or LSP
						if((isUserAllowed && interpreterDocKey != null && interpreterDocKey.equalsIgnoreCase(booking.getInterpreterDocKey())) || isLspAllowedToAccessAll || (Constants.allowedStatusForLspToGetBooking.contains(booking.getStatus()) 
								|| (booking.getCompleted() != null && (booking.getCompleted().equalsIgnoreCase(Constants.YES) || (booking.getCompleted().equalsIgnoreCase(Constants.NO)))))) {
							if (booking.isCombinedBooking()) {
								JsonArray bookings = new JsonArray();
								dataJson = booking.getBookingDetails();
								//Remove unwanted objects for combined booking
								dataJson.remove("patientDetails"); 
								dataJson.remove("professionalDetails"); 
								DocumentCollection childBookings = booking.getChildBookings();
								Document childDoc = childBookings.getFirstDocument();
								while (childDoc != null) {
									Booking childBooking = new Booking(this, childDoc.getItemValueString("DocKey"), session);
									JsonObject childBookingDetails = (JsonObject) childBooking.getBookingDetails();
									childBookingDetails.remove("interpreterDetails");
									childBookingDetails.remove("externalLink");
									childBookingDetails.remove("externalReference");
									childBookingDetails.remove("status");
									bookings.add(childBookingDetails);
									childDoc = childBookings.getNextDocument(childDoc);
								}
								dataJson.put("bookings", bookings);
							} else {
								dataJson = booking.getBookingDetails();								
								if (isMobileApp()) {
									dataJson.put("nonCompletionReasons", getNonCompletionReasons());
								}
							}
						} else if (booking.getStatus().equalsIgnoreCase(Constants.bookingStatusRejected)) { //If booking is rejected
							messageCode.add(3034);
						} else {
							httpResponse = 403;
							messageCode.add(3034);
							isInValid = true;
						}
					} else {
						httpResponse = 403;
						messageCode.add(3034);
						isInValid = true;
					}
				} else {
					httpResponse = 404;
					messageCode.add(3001);
					isInValid = true;
				}
			} else {
				httpResponse = 404;
				messageCode.add(3001);
				isInValid = true;
			}
		} catch (NotesException e) {
			isInValid = true; 
			httpResponse = 500;
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}
	
	/**
	 * Get non completion reasons list for bookings
	 * @return - JsonArray
	 */
	private JsonArray getNonCompletionReasons() {
		JsonArray reasons = new JsonArray();
		JsonObject reasonsObj = new JsonObject();
		String[] coreReasons = kwHandlerIMSDB.getKeywordValueArray(Constants.KW_IMS_INTERPRETER_NC_REASONS);
		if (coreReasons != null) {
			for (String reason : coreReasons) {
				String[] arrReason = reason.split("\\|");
				if (Globals.indexInBound(arrReason, 0) && Globals.indexInBound(arrReason, 1)) {
					reasonsObj.put(arrReason[1].trim(), arrReason[0].trim());
				}
				if (!Globals.indexInBound(arrReason, 1)) { 
					reasonsObj.put(arrReason[0].trim(), arrReason[0].trim()); 
				}
			}
		}
		reasons.add(reasonsObj);
		return reasons;
	}

	/**
	 * Updates a booking based on action sent by request
	 * This covers received, allocated, unallocated, cancelled, rejected, completed statuses
	 * @param id - Booking ID
	 * @throws ParseException 
	 * @throws IOException 
	 */
	public void updateBookingStatus() throws ParseException, IOException {
		try {
			Boolean bSendWebhook = false;
			booking.setPendingSendWebhook(Constants.NO);
			if (id != null) {
				booking = new Booking(this, id, session); //get booking for id coming from JSON request
				booking.setNowDateTime(nowDateTime);
				booking.setNowDateTimeFormatted(nowDateTimeFormatted);
			}
			if (booking.getBookingDoc() != null) {				
				booking.setAgencyLabel(agencyLabel);
				if (bookingAction != null) {
					if (isLspAllowedToAccessAll) {
						//Only few actions are allowed
						if (!Constants.ALLOWED_ACTION_TO_ACCESS_ALL.contains(bookingAction)) {
							httpResponse = 403;
							isInValid = true;
							messageCode.add(3046);
							return;
						}
					} else if (isUserAllowed && interpreterDocKey.equalsIgnoreCase(booking.getInterpreterDocKey())) {
						//Only few actions are allowed
						if (!Constants.ALLOWED_ACTION_TO_USER_ACCESS.contains(bookingAction)) {
							httpResponse = 403;
							isInValid = true;
							messageCode.add(3047);
							return;
						}
					} else {
						//Verify if the booking is still associated with the agency
						if (interpreterFullName == null || interpreterDocKey == null || (!interpreterFullName.equalsIgnoreCase(booking.getInterpreterFullName()) && !interpreterDocKey.equalsIgnoreCase(booking.getInterpreterDocKey()))) {
							httpResponse = 403;
							messageCode.add(3034);
							isInValid = true;
							return;
						}
						//Only few actions are allowed
						if (!Constants.allowedActions.contains(bookingAction)) {
							httpResponse = 400;
							isInValid = true;
							messageCode.add(3017);
							return;
						}
					}				
					//Do not allow if booking is locked
					if (booking.isLocked()) {
						httpResponse = 409;
						isInValid = true;
						messageCode.add(3037);
						return;
					}
					DateTime actAppointmentStartDateTime = booking.getAppointmentStartDateTime();
					//Actions are not allowed <x> number of hours after the start date time of the booking
					if (!Globals.isAllowedToAction(nowDateTime, actAppointmentStartDateTime, session, kwHandlerIMSDB) && !bookingAction.equalsIgnoreCase(Constants.ACTION_COMPLETED)) {
						httpResponse = 400;
						isInValid = true;
						messageCode.add(3045);
						return;
					}
					//Completed action is not allowed on future bookings
					if (Globals.isFutureDate(nowDateTime, booking.getAppointmentStartDateTime(), session) && bookingAction.equalsIgnoreCase(Constants.ACTION_COMPLETED)) {
						httpResponse = 400;
						isInValid = true;
						messageCode.add(3042);
						return;
					}
					//Do not allow action on child bookings directly for LSP API
					if (!isMobileApp() && booking.isChildBooking()) {
						httpResponse = 400;
						isInValid = true;
						messageCode.add(3001);
						return;
					}
					/**
					 * Check allowed actions and action each separately
					 * Some actions might requires update in fields
					 * Some actions might require update in fields but return error/message to response
					 */
					String currentStatus = booking.getStatus();
					booking.setOldStatus(currentStatus);
					booking.setOldDisplayStatus(booking.getDisplayStatus());
					//If this booking is a combined (parent) booking, get child bookings which also need to be updated
					DocumentCollection childBookings = null;
					if (booking.isCombinedBooking()) {
						childBookings = booking.getChildBookings();
					}
					if (bookingAction.equalsIgnoreCase(Constants.ACTION_ACCEPTED)) {
						if (currentStatus.equalsIgnoreCase(Constants.bookingStatusBooked)) {
							actionAccepted(childBookings);
						}  else {
							httpResponse = 400;
							isInValid = true;
							messageCode.add(3048);
						}					
					} else if (bookingAction.equalsIgnoreCase(Constants.ACTION_RECEIVED)) {
						//If status is waiting for acknowledgement change status to submitted/booked
						//If booking is not completed (cancelled) in IMS, allow acknowledgement by LSP
						if (currentStatus.equalsIgnoreCase(Constants.bookingStatusWaitingAcknowledgment)) {
							actionAcknowledgeReceipt(childBookings);
						} else if (currentStatus.equalsIgnoreCase(Constants.bookingStatusCancelled) || booking.getCompleted().equalsIgnoreCase(Constants.NO)) {
							actionAcknowledgeReceiptCancellation(childBookings);
						} else {
							httpResponse = 400;
							isInValid = true;
							messageCode.add(3009);
						}					
					} else if (bookingAction.equalsIgnoreCase(Constants.ACTION_ALLOCATED)) { 
						//Allow Submitted/Booked to Booked and add interpreter details
						if (currentStatus.equalsIgnoreCase(Constants.bookingStatusSubmitted) 
								|| currentStatus.equalsIgnoreCase(Constants.bookingStatusBooked)) {
							if (interpreterDetails.size() > 0) {
								actionAllocation(childBookings);
							} else {
								httpResponse = 400;
								isInValid = true;
								messageCode.add(3016);
							}
						} else {
							httpResponse = 400;
							isInValid = true;
							messageCode.add(3010);
						}
					} else if (bookingAction.equalsIgnoreCase(Constants.ACTION_UNALLOCATED)) {
						//Check status for Booked/Submitted
						if (currentStatus.equalsIgnoreCase(Constants.bookingStatusSubmitted)
								|| currentStatus.equalsIgnoreCase(Constants.bookingStatusBooked)) {
							actionUnAllocate(childBookings);
						} else {
							httpResponse = 400;
							isInValid = true;
							messageCode.add(3011);//Allow Submitted/Booked to Booked and add interpreter details
						}
					}  else if (bookingAction.equalsIgnoreCase(Constants.ACTION_REJECTED)) {
						//Allow rejection on Waiting, Submitted, booked
						if ((isUserAllowed && currentStatus.equalsIgnoreCase(Constants.bookingStatusBooked)) 
								|| (!isUserAllowed && (currentStatus.equalsIgnoreCase(Constants.bookingStatusWaitingAcknowledgment) 
								|| currentStatus.equalsIgnoreCase(Constants.bookingStatusSubmitted)
								|| currentStatus.equalsIgnoreCase(Constants.bookingStatusBooked)))) {
							actionRejection(childBookings);
						} else { //default invalid, can't update booking
							httpResponse = 400;
							isInValid = true;
							if (isUserAllowed) {
								messageCode.add(3048);
							} else {
								messageCode.add(3013);
							}
						}
					} else if (bookingAction.equalsIgnoreCase(Constants.ACTION_CANCELLED)) {
						// Do not allow cancellation on CBN bookings as there is no known use case for this yet
						if (booking.isCombinedBooking()) {
							httpResponse = 400;
							isInValid = true;
							messageCode.add(3033);
						} else {
							
							
							//Ashish - March 2022 - Allow cancellation only on all the bookings (Keyword driven) except Book_completed = Yes
							boolean bAllwed = false;	
							
							
							if (isLspAllowedToAccessAll) {
								String[] arrAllowedStatus = kwHandlerIMSDB.getKeywordValueArray(Constants.KW_ALLOWED_UPDATE_STATUS);
								if (arrAllowedStatus != null) {								
									for (String st : arrAllowedStatus) {
										if(st.equalsIgnoreCase(currentStatus)){
											bAllwed = true;
											break;
										}
									}
								} else {								
									if (currentStatus.equalsIgnoreCase(Constants.bookingStatusNew) 
											|| currentStatus.equalsIgnoreCase(Constants.bookingStatusAmended)
											|| currentStatus.equalsIgnoreCase(Constants.bookingStatusSubmitted)
											|| currentStatus.equalsIgnoreCase(Constants.bookingStatusBooked)
											|| currentStatus.equalsIgnoreCase(Constants.bookingStatusUnmet)
											|| currentStatus.equalsIgnoreCase(Constants.bookingStatusRejected)
											|| currentStatus.equalsIgnoreCase(Constants.bookingStatusWaitingAcknowledgment)
											|| currentStatus.equalsIgnoreCase(Constants.bookingStatusDefault)
											|| currentStatus.equalsIgnoreCase(Constants.bookingStatusReallocated)
											|| currentStatus.equalsIgnoreCase(Constants.bookingStatusRescheduling)){
										bAllwed = true;
									}
								}
							}
							
							
							
							//Allow cancellation on Waiting, Submitted, booked individual bookings only. OR for non-LSP if bAllwed
							if (currentStatus.equalsIgnoreCase(Constants.bookingStatusWaitingAcknowledgment) 
									|| currentStatus.equalsIgnoreCase(Constants.bookingStatusSubmitted)
									|| currentStatus.equalsIgnoreCase(Constants.bookingStatusBooked)
									|| (bAllwed && booking.getCompleted().equalsIgnoreCase(""))) {						
								if (cancelledBy != null && cancellationRequestedBy != null && cancellationReason != null) {
									isCompleted = false;
									String newStatus = Constants.bookingStatusCancelled;
									//ASHISH - March 2022 - Set the booking in cancellation requested status
									String cancellationDateTimeFormat = Globals.formatDate(nowDateTime, globalDateFormat + " " + globalTimeFormat);
									String cancellationDateTextFormat = Globals.formatDate(nowDateTime, globalDateFormat);
									String cancellationTimeTextFormat = Globals.formatDate(nowDateTime, globalTimeFormat);
									booking.setCancelledBy(cancelledBy);
									booking.setCancellationRequestedBy(cancellationRequestedBy);
									booking.setCancellationReason(cancellationReason);
									booking.setCancellationTime(cancellationDateTimeFormat);
									booking.setCancellationDateText(cancellationDateTextFormat);
									booking.setCancellationTimeText(cancellationTimeTextFormat);	
									if (isLspAllowedToAccessAll 
												&& !currentStatus.equalsIgnoreCase(Constants.bookingStatusNew) 
												&& !currentStatus.equalsIgnoreCase(Constants.bookingStatusAmended)) {
										if (kwHandlerIMSDB.isSkipCancelRequested() && kwHandlerIMSDB.isAgencyAPIEnabled(booking.getInterpreterFullName())){ //if inhouse we will get false
											//set trigger to send webhook
											bSendWebhook = true;
											booking.setPendingSendWebhook(Constants.YES);
										}
										if (!kwHandlerIMSDB.isSkipCancelRequested()) {
											newStatus = Constants.bookingStatusCancellationRequested;
										} else {
											booking.setCompleted(Constants.NO);
										}
									}
									else{
										booking.setAgencyFlowComplete(Constants.YES);
										booking.setCompleted(Constants.NO);
										booking.setAgencyFlowCompleteTime(Globals.formatDate(nowDateTime, globalDateFormat + " " + globalTimeFormat));
									}
									booking.setStatus(newStatus);
									
								} else {
									isInValid = true;
									messageCode.add(3027);
								}
							} else {
								httpResponse = 400;
								isInValid = true;
								messageCode.add(3038);
							}
						}
					} else if (bookingAction.equalsIgnoreCase(Constants.ACTION_COMPLETED)) {
						if (!Globals.isAPIEnabled("isBookingCompletionAPIEnabled", session, kwHandlerIMSDB)) {
							isInValid = true;
							httpResponse = 503;
							messageCode.add(1000);
						} else {
							//Allow Service Delivered on booked and service delivered bookings only OR if mobile app and interpreter is allowed to complete booking and not invoiced
							if ((!isMobileApp() && (currentStatus.equalsIgnoreCase(Constants.bookingStatusBooked) || currentStatus.equalsIgnoreCase(Constants.bookingStatusServiceDelivered)))
									|| (isMobileApp() && booking.isCompletionAllowed())) {
								//Clear out completed fields if value added previously
								booking.setActualDuration(0);
								booking.setNumCalls(0);
								booking.setActualAppointmentStartTimeText(null);
								booking.setActualAppointmentEndTimeText(null);
								booking.setNonCompletionReason(null);
								booking.setCancellationReason(null);
								booking.setDistanceTravelled(0);
								booking.setTravelTime(0);
								booking.setOtherCharges(0);
								if (isServiceDelivered == null) {
									isInValid = true;
									httpResponse = 400;
									messageCode.add(3027);
								} else if (isServiceDelivered) { //If service is delivered
									booking.setActualDuration(actualDuration);
									DateTime actualAppointmentStartDateTime = booking.getAppointmentStartDateTime();
									if (actualAppointmentStartTime != null) {
										if (!actualAppointmentStartTime.matches(Constants.regex24Hour)) {
											isInValid = true;
											messageCode.add(3043);
									    } else {
											DateTime validActualAppointmentDateTime = Globals.getDateTime(Globals.formatDate(actualAppointmentStartDateTime, "yyyyMMdd"), actualAppointmentStartTime, session);
											actualAppointmentStartTime = Globals.formatTime(validActualAppointmentDateTime, globalTimeFormat);
											booking.setActualAppointmentStartTimeText(actualAppointmentStartTime);
											booking.setActualAppointmentEndTimeText(Globals.getEndTime(validActualAppointmentDateTime, actualDuration, session));
									    }
									}
									booking.setCompleted(Constants.YES);
								} else if (!isServiceDelivered) { //If service is not delivered
									if (nonCompletionReason != null) {
										if (kwHandlerIMSDB.validateNonCompletionReason(nonCompletionReason, isMobileApp())) {
											booking.setNonCompletionReason(nonCompletionReason);
										} else {
											isInValid = true;
											messageCode.add(3044);
										}
									}
									booking.setCompleted(Constants.NO);
								}
								String newStatus = Constants.bookingStatusServiceDelivered;
								if (isMobileApp()) {
									if (!kwHandlerIMSDB.getKeywordValueString(Constants.KW_ALLOW_INTERPRETER_SERVICE_DELIVERED).equals("1")) {
										newStatus = Constants.bookingStatusCompleted;
										booking.saveCompleted(true);
									}
								}
								booking.setStatus(newStatus);
								booking.setDistanceTravelled(distanceTravelled);
								booking.setTravelTime(travelTime);
								booking.setOtherCharges(otherCharges);
								if (numCalls != 0) {
									if (!Globals.isValidFloatString(numCalls)) {
										isInValid = true;
										httpResponse = 400;
										messageCode.add(3049);
										numCalls = 0;
									} else {
										booking.setNumCalls(numCalls);
									}
								}
								if (completionNotes != null) {
									booking.setCancellationReason(completionNotes);
								}
							} else { //default invalid, can't update booking
								httpResponse = 400;
								isInValid = true;
								messageCode.add(3041);
							}
						}
					} else { //Default - in case action is allowed for request but not defined here
						httpResponse = 403;
						isInValid = true;
					}
					
					if (!isInValid) {
						booking.partialUpdate(bookingAction);
						if (bSendWebhook){
							sendWebhook();
						}
						httpResponse = 200;
						//If booking is already amended or in cancellation request return rescheduling non-lsp and non-mobile app API
						if (!isMobileApp() && !isLspAllowedToAccessAll && (booking.getStatus().equalsIgnoreCase(Constants.bookingStatusAmended) 
								|| booking.getStatus().equalsIgnoreCase(Constants.bookingStatusCancellationRequested))) {
								dataJson.put("status", Constants.bookingStatusRescheduling);
						} else {
							dataJson.put("status", booking.getDisplayStatus());
						}
						
						if(isLspAllowedToAccessAll){
							dataJson.put("languageCode", booking.getLanguageCode());
						}
						
					}
				}
			} else {
				
				isInValid = true;
				httpResponse = 404; //Not found
			}
		} catch (NotesException e) {
			isInValid = true; 
			httpResponse = 500;
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
	}

	/**
	 * Update child bookings for field updates
	 * @param bookings - Document Collection of bookings to update
	 * @param fieldsToUpdate - JSON Object of fields to update on documents
	 */
	public void updateBookings(DocumentCollection bookings, JsonObject fieldsToUpdate) {
		try {
			if (bookings != null && bookings.getCount() > 0) {
				Document childDoc = bookings.getFirstDocument();
				while (childDoc != null) {
					Booking childBooking = new Booking(this, childDoc.getItemValueString("DocKey"), session);
					childBooking.setAgencyLabel(agencyLabel);
					childBooking.setNowDateTime(nowDateTime);
					childBooking.setNowDateTimeFormatted(nowDateTimeFormatted);
					if (fieldsToUpdate.size() > 0) {
						Iterator<String> iter = fieldsToUpdate.keySet().iterator();
					    while (iter.hasNext()) {
					        String fieldName = iter.next();
					        String fieldValue = "";
					        if (fieldName.equalsIgnoreCase("interpreterDetails")) {
								JsonArray interpreters = (JsonArray) fieldsToUpdate.get(fieldName);
								childBooking.setInterpreters(interpreters);
							} else {
								fieldValue = fieldsToUpdate.get(fieldName) !=null ? fieldsToUpdate.get(fieldName).toString() : "";
							}
					        if (fieldName.equalsIgnoreCase("externalLink")) {
					        	childBooking.setExternalLink(fieldValue);
					        }
					        if (fieldName.equalsIgnoreCase("externalReference")) {
					        	childBooking.setExternalReference(fieldValue);
					        }
					        if (fieldName.equalsIgnoreCase("status")) {
					        	childBooking.setStatus(fieldValue);
					        }
					        if (fieldName.equalsIgnoreCase("rejectionReason")) {
					        	childBooking.setRejectionReason(fieldValue);
					        }
					        if (fieldName.equalsIgnoreCase("rejectedBy")) {
					        	childBooking.setRejectedBy(fieldValue);
					        }
					        if (fieldName.equalsIgnoreCase("kpiAllocatedFirstBy")) {
					        	childBooking.setKpiAllocatedFirstBy(fieldValue);
					        }
					        if (fieldName.equalsIgnoreCase("kpiAllocatedFirstDate")) {
					        	childBooking.setKpiAllocatedFirstDate(nowDateTime);
					        }
					        if (fieldName.equalsIgnoreCase("kpiAllocatedFirstDocKey")) {
					        	childBooking.setKpiAllocatedFirstDocKey(fieldValue);
					        }
					        if (fieldName.equalsIgnoreCase("kpiAllocatedLastBy")) {
					        	childBooking.setKpiAllocatedLastBy(fieldValue);
					        }
					        if (fieldName.equalsIgnoreCase("kpiAllocatedLastDate")) {
					        	childBooking.setKpiAllocatedLastDate(nowDateTime);
					        }
					        if (fieldName.equalsIgnoreCase("kpiAllocatedLastDocKey")) {
					        	childBooking.setKpiAllocatedLastDocKey(fieldValue);
					        }
					        if (fieldName.equalsIgnoreCase("kpiSubmittedAgencyBy")) {
					        	childBooking.setKpiSubmittedAgencyBy(fieldValue);
					        }
					        if (fieldName.equalsIgnoreCase("kpiSubmittedAgencyDate")) {
					        	childBooking.setKpiSubmittedAgencyDate(nowDateTime);
					        }
					        if (fieldName.equalsIgnoreCase("kpiSubmittedAgencyDocKey")) {
					        	childBooking.setKpiSubmittedAgencyDocKey(fieldValue);
					        }
					    }
						childBooking.partialUpdate(bookingAction);
						childDoc = bookings.getNextDocument(childDoc);
					}
				}
			}
		} catch (Exception e) {
			Globals.printException(e);
		}
	}
	
	/**
	 * Action Accepted on bookings
	 * No status update, add booking log entry only currently. Require update later.
	 * @param childBookings - Document collection of child bookings is current booking is combined booking
	 */
	private void actionAccepted(DocumentCollection childBookings) {
		//Also update child bookings if it is a combined booking
		if (booking.isCombinedBooking()) {
			JsonObject fieldsToUpdate = new JsonObject();
			updateBookings(childBookings, fieldsToUpdate);
		}
	}
	
	/**
	 * Action acknowledge receipt of bookings. Also add fields required to update to JSON Object fields to action for child bookings.
	 * @param childBookings - Document collection of child bookings is current booking is combined booking
	 */
	private void actionAcknowledgeReceipt(DocumentCollection childBookings) {
		JsonObject fieldsToUpdate = new JsonObject();
		if (externalReference != null && !booking.isExternalIdExist(externalReference, booking.getId())) {
			booking.setExternalReference(externalReference);
		} else {			
			//Ashish - Feb 2022 - Validate External Reference only if PMS ID is null in request JSON
			if(pmsId == null){
				messageCode.add(3035);
				isInValid = true;
				return;
			}
		}
		if (externalLink != null) {
			booking.setExternalLink(externalLink);
		}
		String newStatus;
		fieldsToUpdate.put("externalReference", externalReference);
		fieldsToUpdate.put("externalLink", externalLink);
		//Check if there are interpreter details, Change status to Booked
		if (booking.getInterpreterDetails().size() > 0 && booking.isValidInterpreterExists) { 
			newStatus = Constants.bookingStatusBooked;
			booking.setStatus(newStatus);
			if (booking.getKpiAllocatedLastBy() != null && !booking.getKpiAllocatedLastBy().equals("")) {
				booking.setKpiAllocatedLastBy(interpreterFullName);
				booking.setKpiAllocatedLastDate(nowDateTime);
				booking.setKpiAllocatedLastDocKey(interpreterDocKey);
				fieldsToUpdate.put("kpiAllocatedLastBy", interpreterFullName);
				fieldsToUpdate.put("kpiAllocatedLastDate", nowDateTime);
				fieldsToUpdate.put("kpiAllocatedLastDocKey", interpreterDocKey);
			} else {
				booking.setKpiAllocatedFirstBy(interpreterFullName);
				booking.setKpiAllocatedFirstDate(nowDateTime);
				booking.setKpiAllocatedFirstDocKey(interpreterDocKey);
				booking.setKpiAllocatedLastBy(interpreterFullName);
				booking.setKpiAllocatedLastDate(nowDateTime);
				booking.setKpiAllocatedLastDocKey(interpreterDocKey);
				fieldsToUpdate.put("kpiAllocatedFirstBy", interpreterFullName);
				fieldsToUpdate.put("kpiAllocatedFirstDate", nowDateTime);
				fieldsToUpdate.put("kpiAllocatedFirstDocKey", interpreterDocKey);
				fieldsToUpdate.put("kpiAllocatedLastBy", interpreterFullName);
				fieldsToUpdate.put("kpiAllocatedLastDate", nowDateTime);
				fieldsToUpdate.put("kpiAllocatedLastDocKey", interpreterDocKey);
			}
		} else {
			booking.removeInterpreterFields(); //Clear previous interpreter fields if existing while changing booking back to submitted
			newStatus = Constants.bookingStatusSubmitted;
			booking.setStatus(newStatus);
			booking.setKpiSubmittedAgencyBy(interpreterFullName);
			booking.setKpiSubmittedAgencyDate(nowDateTime);
			booking.setKpiSubmittedAgencyDocKey(interpreterDocKey);
			fieldsToUpdate.put("kpiSubmittedAgencyBy", interpreterFullName);
			fieldsToUpdate.put("kpiSubmittedAgencyDate", nowDateTime);
			fieldsToUpdate.put("kpiSubmittedAgencyDocKey", interpreterDocKey);
		}
		//Also update child bookings if it is a combined booking
		if (booking.isCombinedBooking()) {
			fieldsToUpdate.put("status", newStatus);
			updateBookings(childBookings, fieldsToUpdate);
		}
	}
	
	/**
	 * 
	 * @param childBookings
	 */
	private void actionAcknowledgeReceiptCancellation(DocumentCollection childBookings) {
		JsonObject fieldsToUpdate = new JsonObject();
		//Also update child bookings if it is a combined booking
		if (booking.isCombinedBooking()) {
			updateBookings(childBookings, fieldsToUpdate);
		}
	}

	/**
	 * Action Allocation on bookings
	 * @param childBookings - Document collection of child bookings is current booking is combined booking
	 */
	private void actionAllocation(DocumentCollection childBookings) {
		String newStatus = Constants.bookingStatusBooked;
		booking.setStatus(newStatus);
		booking.setInterpreters(interpreterDetails);
		JsonObject fieldsToUpdate = new JsonObject();
		if (booking.getKpiAllocatedLastBy() != null && !booking.getKpiAllocatedLastBy().equals("")) {
			booking.setKpiAllocatedLastBy(interpreterFullName);
			booking.setKpiAllocatedLastDate(nowDateTime);
			booking.setKpiAllocatedLastDocKey(interpreterDocKey);
			fieldsToUpdate.put("kpiAllocatedLastBy", interpreterFullName);
			fieldsToUpdate.put("kpiAllocatedLastDate", nowDateTime);
			fieldsToUpdate.put("kpiAllocatedLastDocKey", interpreterDocKey);
		} else {
			booking.setKpiAllocatedFirstBy(interpreterFullName);
			booking.setKpiAllocatedFirstDate(nowDateTime);
			booking.setKpiAllocatedFirstDocKey(interpreterDocKey);
			booking.setKpiAllocatedLastBy(interpreterFullName);
			booking.setKpiAllocatedLastDate(nowDateTime);
			booking.setKpiAllocatedLastDocKey(interpreterDocKey);
			fieldsToUpdate.put("kpiAllocatedFirstBy", interpreterFullName);
			fieldsToUpdate.put("kpiAllocatedFirstDate", nowDateTime);
			fieldsToUpdate.put("kpiAllocatedFirstDocKey", interpreterDocKey);
			fieldsToUpdate.put("kpiAllocatedLastBy", interpreterFullName);
			fieldsToUpdate.put("kpiAllocatedLastDate", nowDateTime);
			fieldsToUpdate.put("kpiAllocatedLastDocKey", interpreterDocKey);
		}
		//Also update child bookings if it is a combined booking
		if (booking.isCombinedBooking()) {
			fieldsToUpdate.put("status", newStatus);
			fieldsToUpdate.put("interpreterDetails", interpreterDetails);
			updateBookings(childBookings, fieldsToUpdate);
		}
	}

	/**
	 * Action Deallocation on bookings
	 * @param childBookings - Document collection of child bookings is current booking is combined booking
	 */
	private void actionUnAllocate(DocumentCollection childBookings) {
		String newStatus = Constants.bookingStatusNew;
		if (booking.getInterpreterType().equalsIgnoreCase("Agency")) {
			newStatus = Constants.bookingStatusSubmitted;
		}
		booking.setStatus(newStatus);
		//Also update child bookings if it is a combined booking
		if (booking.isCombinedBooking()) {
			JsonObject fieldsToUpdate = new JsonObject();
			fieldsToUpdate.put("status", newStatus);
			updateBookings(childBookings, fieldsToUpdate);
		}
	}

	/**
	 * Action Rejection on bookings
	 * @param childBookings - Document collection of child bookings is current booking is combined booking
	 */
	private void actionRejection(DocumentCollection childBookings) {
		String newStatus = Constants.bookingStatusNew;
		if (booking.getInterpreterType().equalsIgnoreCase("Agency")) {
			newStatus = Constants.bookingStatusRejected;
			booking.setRejectionReason(rejectionReason);
			booking.setRejectedBy(rejectedBy);
		}
		booking.setStatus(newStatus);
		if (booking.isCombinedBooking()) {
			JsonObject fieldsToUpdate = new JsonObject();
			fieldsToUpdate.put("status", newStatus);
			if (booking.getInterpreterType().equalsIgnoreCase("Agency")) {
				fieldsToUpdate.put("rejectionReason", rejectionReason);
				fieldsToUpdate.put("rejectedBy", rejectedBy);
			}
			updateBookings(childBookings, fieldsToUpdate);
		}
	}


	
	/**
	 * Update a booking with allowed fields only.
	 * Below fields are allowed to be updated:
	 * 	Cost Centre, Duration, Appointment Date, Appointment Time, Gender Preference, 
	 *  External reference, External Link, Delivery Method, Communication Details, 
	 *  Patient Address, Is Home Visit
	 * @throws SecurityException 
	 * @throws NoSuchFieldException 
	 * @throws NotesException 
	 */
	public void updateBooking() throws NoSuchFieldException, SecurityException {
		try {	
			if (id != null && !id.equals("")) {
				booking = new Booking(this, id, session); //get booking for id coming from JSON request
				booking.setNowDateTime(nowDateTime);
				booking.setNowDateTimeFormatted(nowDateTimeFormatted);
				//Ashish - Feb 2022 - Assumption is Request JSON can contain a PMS ID or actual IMS ID in 'imsId' property
				//if id exists, assign id to PMS ID
				booking.setPmsId(id);
				
			}
			if (booking.getBookingDoc() != null) {
				booking.setAgencyLabel(agencyLabel);
				//Verify if the booking is still associated with the agency
				if(interpreterFullName != null && interpreterDocKey != null){
					if (!interpreterFullName.equalsIgnoreCase(booking.getInterpreterFullName()) && !interpreterDocKey.equalsIgnoreCase(booking.getInterpreterDocKey())) {
						httpResponse = 403;
						messageCode.add(3034);
						isInValid = true;
						return;
					}
				}
				//Do not allow if booking is locked/being updated by IMS user
				if (booking.isLocked()) {
					httpResponse = 409;
					isInValid = true;
					messageCode.add(3037);
					return;
				}
				
				//Ashish - 06/10/2022 - Allow edits on past bookings (till certain time)
				/*
				//Updates are not allowed on past bookings
				if (!Globals.isFutureDate(booking.getAppointmentStartDateTime(), session)) {
					httpResponse = 400;
					isInValid = true;
					messageCode.add(3014);
					return;
				}
				*/
				
				//Do not allow updates on child bookings directly
				if (booking.isChildBooking()) {
					httpResponse = 400;
					isInValid = true;
					messageCode.add(3001);
					return;
				}
				//Do not allow updates on CBN bookings as there is no known use case for this yet
				if (booking.isCombinedBooking()) {
					httpResponse = 400;
					isInValid = true;
					messageCode.add(3040);
					return;
				}
				//Keep old values to update Log
				String currentStatus = booking.getStatus();
				booking.setOldStatus(currentStatus);
				JsonObject values = new JsonObject();
				JsonObject statusObj = new JsonObject();
				JsonObject costcentreObj = new JsonObject();
				JsonObject clinicObj = new JsonObject();
				JsonObject apptDateObj = new JsonObject();
				JsonObject apptTimeObj = new JsonObject();
				JsonObject durationObj = new JsonObject();
				JsonObject patientAddressObj = new JsonObject();
				JsonObject externalReferenceObj = new JsonObject();
				JsonObject externalLinkObj = new JsonObject();
				JsonObject patientTypeObj = new JsonObject();
				JsonObject deliveryMethodObj = new JsonObject();
				JsonObject communicationDetailsObj = new JsonObject();
				JsonObject videoConferencePlatformObj = new JsonObject();
				JsonObject genderPreferenceCodeObj = new JsonObject();
				JsonObject genderPreferencePriorityObj = new JsonObject();
				JsonObject notesToLSPObj = new JsonObject();
				JsonObject languageObj = new JsonObject();
				JsonObject systemCommentsObj = new JsonObject();
				
				statusObj.put("Old", booking.getDisplayStatus());
				clinicObj.put("Old", booking.getClinic());
				costcentreObj.put("Old", booking.getCostCentre());
				apptDateObj.put("Old", booking.getAppointmentStartDateText());
				apptTimeObj.put("Old", booking.getAppointmentStartTimeText());
				durationObj.put("Old", booking.getDuration());

				String oldAddress = "";//TODO - Multi-patient, currently getting first patient only
				if (booking.getPatientAddressFields(0).get("address") != null) {
					oldAddress += booking.getPatientAddressFields(0).get("address");
				}
				if (booking.getPatientAddressFields(0).get("suburb") != null) {
					oldAddress += oldAddress != "" ? ", " + booking.getPatientAddressFields(0).get("suburb") : booking.getPatientAddressFields(0).get("suburb") ;
				}
				if (booking.getPatientAddressFields(0).get("state") != null) {
					oldAddress += oldAddress != "" ? ", " + booking.getPatientAddressFields(0).get("state") : booking.getPatientAddressFields(0).get("state");
				}
				if (booking.getPatientAddressFields(0).get("postCode") != null) {
					oldAddress += oldAddress != "" ? ", " + booking.getPatientAddressFields(0).get("postCode") : booking.getPatientAddressFields(0).get("postCode");
				}
				
				patientAddressObj.put("Old", oldAddress); 
				externalReferenceObj.put("Old", booking.getExternalReference());
				externalLinkObj.put("Old", booking.getExternalLink());
				patientTypeObj.put("Old", booking.getPatientType());
				deliveryMethodObj.put("Old", booking.getDeliveryMethod());
				communicationDetailsObj.put("Old", booking.getCommunicationDetails());
				videoConferencePlatformObj.put("Old", booking.getVideoConferencePlatform());
				genderPreferenceCodeObj.put("Old", booking.getGenderPreferenceCode());
				genderPreferencePriorityObj.put("Old", booking.getGenderPreferencePriority());
				notesToLSPObj.put("Old", booking.getAgencyNotes());
				languageObj.put("Old", booking.getLanguage());
				systemCommentsObj.put("Old", booking.getSystemComments());
											
				boolean bUpdateStatus = false;
				boolean bAllwed = false;
				boolean bUpdateCancelledBooking = false;
				boolean bSendWebhook = false;
				booking.setPendingSendWebhook(Constants.NO);
				if (isLspAllowedToAccessAll) {
					String[] arrAllowedStatus = kwHandlerIMSDB.getKeywordValueArray(Constants.KW_ALLOWED_UPDATE_STATUS); //not sure this is required - Johan
					String allowUpdateOnCancelledBooking = kwHandlerIMSDB.getKeywordValueString(Constants.KW_ALLOWED_UPDATE_CANCEL_BOOKING);
					if(allowUpdateOnCancelledBooking.equalsIgnoreCase("Yes")){
						bUpdateCancelledBooking = true;						
					} else {
						if (arrAllowedStatus != null) {								
							for (String st : arrAllowedStatus) {
								if(st.equalsIgnoreCase(currentStatus)){
									bAllwed = true;
									break;
								}
							}
						} else {								
							if (currentStatus.equalsIgnoreCase(Constants.bookingStatusNew) 
									|| currentStatus.equalsIgnoreCase(Constants.bookingStatusAmended)
									|| currentStatus.equalsIgnoreCase(Constants.bookingStatusSubmitted)
									|| currentStatus.equalsIgnoreCase(Constants.bookingStatusBooked)
									|| currentStatus.equalsIgnoreCase(Constants.bookingStatusUnmet)
									|| currentStatus.equalsIgnoreCase(Constants.bookingStatusRejected)
									|| currentStatus.equalsIgnoreCase(Constants.bookingStatusWaitingAcknowledgment)
									|| currentStatus.equalsIgnoreCase(Constants.bookingStatusDefault)
									|| currentStatus.equalsIgnoreCase(Constants.bookingStatusReallocated)
									|| currentStatus.equalsIgnoreCase(Constants.bookingStatusRescheduling)){
								bAllwed = true;
							}
						}
					}
				}
				//Allow updates on Waiting, Submitted, booked
				if (currentStatus.equalsIgnoreCase(Constants.bookingStatusWaitingAcknowledgment) 
						|| currentStatus.equalsIgnoreCase(Constants.bookingStatusSubmitted)
						|| currentStatus.equalsIgnoreCase(Constants.bookingStatusBooked)
						|| (bAllwed && booking.getCompleted().equalsIgnoreCase(""))
						|| bUpdateCancelledBooking ) {
					//WH - If update on a cancelled booking is allowed, set the booking in the New status and clear post booking details:
					//Remove assigned interpreter, Clear Cancellation fields, Set status to new
					if(bUpdateCancelledBooking && (booking.getStatus().equalsIgnoreCase(Constants.bookingStatusCancelled)
							|| booking.getCompleted().equalsIgnoreCase("No"))){
							booking.removeInterpreterFields();
							booking.removeCancellationDetails();
							booking.setStatus(Constants.bookingStatusNew);
							bUpdateStatus = true;					
					} else {
						if (currentStatus.equalsIgnoreCase(Constants.bookingStatusWaitingAcknowledgment) 
								|| currentStatus.equalsIgnoreCase(Constants.bookingStatusSubmitted)
								|| currentStatus.equalsIgnoreCase(Constants.bookingStatusBooked)
								|| currentStatus.equalsIgnoreCase(Constants.bookingStatusCancellationRequested)) {
							booking.setStatus(Constants.bookingStatusAmended); //set status to amended but this might be overridden later
							//check is agency and api enabled and attempt to send web hook
							if (kwHandlerIMSDB.isAgencyAPIEnabled(booking.getInterpreterFullName()) 
									&& kwHandlerIMSDB.getKeywordValueString(Constants.KW_API_SKIP_AMEND_STATUS).equalsIgnoreCase("1")){ //if in-house we will get false
								//set trigger to send webhook
								if (isLspAllowedToAccessAll){
									bSendWebhook = true;
									booking.setPendingSendWebhook(Constants.YES);
								} //only send webhook for PMS 
								if (currentStatus.equalsIgnoreCase(Constants.bookingStatusBooked) 
										|| currentStatus.equalsIgnoreCase(Constants.bookingStatusSubmitted)
										|| currentStatus.equalsIgnoreCase(Constants.bookingStatusCancellationRequested)){
									booking.setStatus(Constants.bookingStatusSubmitted);
									booking.removeInterpreterFields();
									booking.removeCancellationDetails(); //JUST IN CASE CANCEL REQ
									//set last modified
								}
								//if waitingacknowledge resend webhook?
							}
							else
							{
								//normal amend workflow for manual agency or inhouse interpreter
								//do we check if email enabled and maybe skip amended? Emails get triggered when the booking record is saved
							}
							
							bUpdateStatus = true;
						}
						if (currentStatus.equalsIgnoreCase(Constants.bookingStatusRejected)){
							booking.setStatus(Constants.bookingStatusNew);
							bUpdateStatus = true;
						}
	
					}
					
					//WH - 15/12/2022 - Update the clinic details if different from the booking
					if(clinicCode != null){					
						if(!booking.getClinicCode().equals(clinicCode)){
							JsonObject clinicDetails = kwHandlerIMSDB.getClinicDetails(clinicCode);
							if (clinicDetails != null && clinicDetails.size() > 0) {
								if (!clinicDetails.get("clinicCode").toString().isEmpty()) {
									booking.setClinicCode(clinicDetails.get("clinicCode").toString());
								}
								booking.setSite(clinicDetails.get("siteCode") != null ? (String) clinicDetails.get("siteCode") : "");
								booking.setSiteName(kwHandlerIMSDB.getSiteName(booking.getForm(), clinicDetails.get("siteCode") != null ? (String) clinicDetails.get("siteCode") : ""));
								booking.setLocation(clinicDetails.get("locationName") != null ? (String) clinicDetails.get("locationName") : "");
								booking.setClinic(clinicDetails.get("clinicName") != null ? (String) clinicDetails.get("clinicName") : "");	
								booking.setClinicAddress(clinicDetails.get("clinicAddress") != null ? (String) clinicDetails.get("clinicAddress") : "");
								booking.setClinicSuburb(clinicDetails.get("clinicSuburb") != null ? (String) clinicDetails.get("clinicSuburb") : "");
								booking.setClinicPostCode(clinicDetails.get("clinicPostCode") != null ? (String) clinicDetails.get("clinicPostCode") : "");
								booking.setClinicState(clinicDetails.get("clinicState") != null ? (String) clinicDetails.get("clinicState") : "");
							}else{
								isInValid = true;
								messageCode.add(3005);
							}
						}
					}
					if (costCentre!=null && !costCentre.isEmpty()) {
						boolean isValidCostCentre = kwHandlerIMSDB.isValidCostCentre(booking.getSiteName(), booking.getLocation(), booking.getClinic(), costCentre);
						if (isValidCostCentre) {
							booking.setCostCentre(costCentre);
						} else {
							isInValid = true;
							messageCode.add(3006);
						}
					}
					
					if (duration != 0) {
						booking.setDuration(duration);
					}					
					//Booking JSON may not have appointment date. Get it from booking document
					if (appointmentDate == null){
						appointmentDate = Globals.formatDate(booking.getAppointmentStartDateTime(), "yyyyMMdd");
					}
					if (appointmentTime == null){
						appointmentTime = Globals.formatDate(booking.getAppointmentStartDateTime(), Constants.timeFormat24Hour);
					}
					if (appointmentDate != null && appointmentTime !=null) {
						DateTime validAppointmentDateTime = Globals.getDateTime(appointmentDate, appointmentTime, session);
						int hoursInPast = new Integer(kwHandlerIMSDB.getKeywordValueString("hoursAllowedToUpdateBookingInPast"));
						DateTime nowDateTimeToAdjust = kwHandlerIMSDB.dateOffset(session.createDateTime("Today"));
						nowDateTimeToAdjust.adjustHour(-hoursInPast);
						if (validAppointmentDateTime == null) {
							isInValid = true;
							messageCode.add(3003);
						} else {
							Date validJavaAppointmentDateTime = validAppointmentDateTime.toJavaDate();
							Date allowedJavaCreateDate = nowDateTimeToAdjust.toJavaDate();
							if (validJavaAppointmentDateTime.before(allowedJavaCreateDate) || validJavaAppointmentDateTime.equals(allowedJavaCreateDate)) { 
								isInValid = true;								
								messageCode.add(3050);
							}
							booking.setAppointmentStartDateTime(validAppointmentDateTime);
							if (Globals.formatDate(validAppointmentDateTime, globalDateFormat) != null) {
								booking.setAppointmentStartDateText (Globals.formatDate(validAppointmentDateTime, globalDateFormat));
							}
							if (Globals.formatTime(validAppointmentDateTime, globalTimeFormat) != null) {
								booking.setAppointmentStartTimeText(Globals.formatTime(validAppointmentDateTime, globalTimeFormat));
							}
							booking.setAppointmentEndTimeText(Globals.getEndTime(validAppointmentDateTime, booking.getDuration(), session));
						}
					}
					if (externalReference != null && !booking.isExternalIdExist(externalReference, booking.getId())) {
						booking.setExternalReference(externalReference);
					} else {						
						if(pmsId == null){
							messageCode.add(3035);
							isInValid = true;
						}
					}
					if (externalLink != null) {
						booking.setExternalLink(externalLink);
					}
					if (patientType != null && !patientType.equals("")) {
						booking.setPatientType(patientType);
					}					
					if (patientAddress != null) {
						booking.setAddress(patientAddress);
						booking.setPatientAddress(0); //TODO - Implement multi-patient later
					}
					if (deliveryMethod != null && !deliveryMethod.equals("")) {
						booking.setDeliveryMethod(deliveryMethod);
					}					
					if (communicationDetails != null) {
						booking.setCommunicationDetails(communicationDetails);
					}					
					if (videoConferencePlatform != null) {
						booking.setVideoConferencePlatform(videoConferencePlatform);	
					}
					if (genderPreference != null) {
						booking.setGenderPreference(genderPreference);
					}
					if (genderPreference.get("gender") != null) {
						booking.setGenderPreferenceCode(genderPreference.get("gender").toString());
					}
					if (genderPreference.get("priority") != null) {
						booking.setGenderPreferencePriority(genderPreference.get("priority").toString());
					}					
					if (notesForLSP != null) {
						booking.setAgencyNotes(notesForLSP);
					}
					if (systemComments != null) {
						booking.setSystemComments(systemComments);
					}
					if(languageCode != null){
						if(!booking.getLanguageCode().equals(languageCode)){
							if(currentStatus.equalsIgnoreCase(Constants.bookingStatusNew) || currentStatus.equalsIgnoreCase(Constants.bookingStatusRejected)
								|| currentStatus.equalsIgnoreCase(Constants.bookingStatusBooked) || currentStatus.equalsIgnoreCase(Constants.bookingStatusSubmitted)
								|| currentStatus.equalsIgnoreCase(Constants.bookingStatusPending) || currentStatus.equalsIgnoreCase(Constants.bookingStatusWaitingAcknowledgment)){
							
								booking.setLanguageCode(languageCode);
								String langCode = ImsHomeDb.getLanguageFromCode(booking.getLanguageCode(), false);
								if (langCode != null) {
									booking.setLanguage(langCode);
								} else {
									messageCode.add(3007);
									isInValid = true;
								}
								
								//WH Change - If the booking is in Booked, Submitted, Pending or Waiting Acknowledgement status, then change the language on the booking AND set the status of the booking to Amended
								if(currentStatus.equalsIgnoreCase(Constants.bookingStatusBooked) || currentStatus.equalsIgnoreCase(Constants.bookingStatusSubmitted)
									|| currentStatus.equalsIgnoreCase(Constants.bookingStatusPending) || currentStatus.equalsIgnoreCase(Constants.bookingStatusWaitingAcknowledgment)){									
									booking.setStatus(Constants.bookingStatusAmended); 
									bUpdateStatus = true;
								}
								
							}else{
								isInValid = true;
								messageCode.add(3051);
							}
						}
					}
					
					
					
				} else {
					httpResponse = 400;
					isInValid = true;
					messageCode.add(3039);
				}
				if (!isInValid) {
					statusObj.put("New", booking.getDisplayStatus());
					clinicObj.put("New", booking.getClinic());
					costcentreObj.put("New", booking.getCostCentre());
					apptDateObj.put("New", booking.getAppointmentStartDateText());
					apptTimeObj.put("New", booking.getAppointmentStartTimeText());
					durationObj.put("New", booking.getDuration());
					String newAddress = "";//TODO - Multi-patient, currently getting first patient only
					if (booking.getPatientAddressFields(0).get("line1") != null) {
						newAddress += booking.getPatientAddressFields(0).get("line1") != "" ? booking.getPatientAddressFields(0).get("line1") : "";
					}
					if (booking.getPatientAddressFields(0).get("line2") != null) {
						newAddress += newAddress != "" ? ", " + booking.getPatientAddressFields(0).get("line2") : booking.getPatientAddressFields(0).get("line2");
					}
					if (booking.getPatientAddressFields(0).get("address") != null) {
						newAddress += newAddress != "" ? ", " + booking.getPatientAddressFields(0).get("suburb") : booking.getPatientAddressFields(0).get("address");
					}
					if (booking.getPatientAddressFields(0).get("suburb") != null) {
						newAddress += newAddress != "" ? ", " + booking.getPatientAddressFields(0).get("suburb") : booking.getPatientAddressFields(0).get("suburb");
					}
					if (booking.getPatientAddressFields(0).get("state") != null) {
						newAddress += newAddress != "" ? ", " + booking.getPatientAddressFields(0).get("state") : booking.getPatientAddressFields(0).get("state");
					}
					if (booking.getPatientAddressFields(0).get("postCode") != null) {
						newAddress += newAddress != "" ? ", " + booking.getPatientAddressFields(0).get("postCode") : booking.getPatientAddressFields(0).get("postCode");
					}
					Globals.printCode("call patientAddressObj.put");
					patientAddressObj.put("New", newAddress); 
					externalReferenceObj.put("New", booking.getExternalReference());
					externalLinkObj.put("New", booking.getExternalLink());
					patientTypeObj.put("New", booking.getPatientType());
					deliveryMethodObj.put("New", booking.getDeliveryMethod());
					communicationDetailsObj.put("New", booking.getCommunicationDetails());
					videoConferencePlatformObj.put("New", booking.getVideoConferencePlatform());
					genderPreferenceCodeObj.put("New", booking.getGenderPreferenceCode());
					genderPreferencePriorityObj.put("New", booking.getGenderPreferencePriority());
					notesToLSPObj.put("New", booking.getAgencyNotes());
					languageObj.put("New", booking.getLanguage());
					systemCommentsObj.put("New", booking.getSystemComments());
					
					values.put("Status", statusObj);
					values.put("Clinic", clinicObj);
					values.put("Cost Centre", costcentreObj);
					values.put("Appointment Date", apptDateObj);
					values.put("Appointment Time", apptTimeObj);
					values.put("Duration", durationObj);
					values.put("Patient Address", patientAddressObj);
					values.put("External Reference", externalReferenceObj);
					values.put("External Link", externalLinkObj);
					if (isLspAllowedToAccessAll) {
						values.put("Patient Type", patientTypeObj);
					}
					values.put("Delivery Method", deliveryMethodObj);
					values.put("Communication Details", communicationDetailsObj);
					values.put("Video Conference Platform", videoConferencePlatformObj);
					values.put("Gender Preference Code", genderPreferenceCodeObj);
					values.put("Gender Preference Priority", genderPreferencePriorityObj);
					values.put("Notes to Agency", notesToLSPObj);
					values.put("Language", languageObj);
					values.put("System Comments", systemCommentsObj);
					booking.update(values,bUpdateStatus);
					//send webhook now
					if (bSendWebhook){
						if (booking.getNoteId()!=null){
							sendWebhook();
							Globals.printCode("End webhook for noteid="+booking.getNoteId());
							
						}
						
					}
					
					httpResponse = 200;
				}	
				
				//If booking is already amended or in cancellation request return rescheduling
				if (!isMobileApp() && !isLspAllowedToAccessAll && (booking.getStatus().equalsIgnoreCase(Constants.bookingStatusAmended) 
						|| booking.getStatus().equalsIgnoreCase(Constants.bookingStatusCancellationRequested))) {
					dataJson.put("status", Constants.bookingStatusRescheduling);
				} else {
					dataJson.put("status", booking.getDisplayStatus());
				}
				if(isLspAllowedToAccessAll){
					dataJson.put("languageCode", booking.getLanguageCode());
				}
			} else {
				
				isInValid = true;
				httpResponse = 404; //Not found
			}
		} catch (NotesException e) {
			isInValid = true; 
			httpResponse = 500;
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			Globals.printException(e);
		}
		
	}
	
	public String getClinicCode(String siteName, String location, String clinic) throws NotesException {
		String clinicCode = null;
		try {
			clinicCode = kwHandlerIMSDB.getClinicCode(siteName, location, clinic);
		} catch (Exception e) {
			
		}
		return clinicCode;
	}

	/**
	 * Get Global contact details
	 * @return
	 */
	public JsonObject getGlobalContactDetails() {
		JsonObject globalContact = new JsonObject();
		Boolean isGlobalContactAllowed = false;
		if (kwHandlerIMSDB.getKeywordValueString(Constants.KW_ALLOW_GLOBAL_CONTACT) != null && kwHandlerIMSDB.getKeywordValueString(Constants.KW_ALLOW_GLOBAL_CONTACT).equalsIgnoreCase("yes")) {
			isGlobalContactAllowed = true;
		}
		if (isGlobalContactAllowed) {
			if (kwHandlerIMSDB.getKeywordValueString(Constants.KW_GLOBAL_CONTACT_DOCKEY) != null && !kwHandlerIMSDB.getKeywordValueString(Constants.KW_GLOBAL_CONTACT_DOCKEY).isEmpty()) {
				String globalContactDocKey = kwHandlerIMSDB.getKeywordValueString(Constants.KW_GLOBAL_CONTACT_DOCKEY);
				try {
					globalContact = kwHandlerIMSDB.getUserDetails(globalContactDocKey);
				} catch (NotesException e) {
					e.printStackTrace();
					Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
				}
			}
		}
		return globalContact;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isLspAllowedToAccessAll() {
		return isLspAllowedToAccessAll;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getClientName() {
		return languageServiceProvider;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public String getConsumerName() {		
		return consumerName;
	}

	/**
	 * @return the attachments
	 */
	public JsonArray getAttachments() {
		return attachments;
	}

	/**
	 * @param attachments the attachments to set
	 */
	public void setAttachments(JsonArray attachments) {
		this.attachments = attachments;
	}
	
	/**
	 * Convert booking minutes to hours and minutes based on configuration
	 * @param minutes
	 * @return
	 */
	public String minutesToHours(int minutes) {
		if (kwHandlerIMSDB.getKeywordValueString("Use_HoursMin").equalsIgnoreCase("Yes")) {
			int retHours = minutes / 60;
			int retMinutes = minutes % 60;
			String hoursLabel = "hour";
			if (retHours == 0) {
				return minutes + " minutes";
			}
			if (retHours > 1) {
				hoursLabel = "hours";
			}
			return retHours + " " + hoursLabel + ", " + retMinutes + " minutes";
		} else {
			return minutes + " minutes";
		}
	}

	/**
	 * Get Keyword value string
	 * @param keyword
	 * @return
	 */
	public String getKeywordValue(String keyword) {
		String value = null;
		if (keyword != null) {
			value = kwHandlerIMSDB.getKeywordValueString(keyword);
		}
		return value;
	}

	/**
	 * Get booking labels from configured keyword
	 */
	public void getLabels() {
		try {
			String labels = getKeywordValue(Constants.KW_DYNAMIC_LABELS);
			labelsJson = Jsoner.deserialize(labels, new JsonArray());
			totalResults = labelsJson.size();
		} catch(Exception e) {
			Globals.printException(e);
		}
	}
	
	/**
	 * Get global configured date format
	 * @return
	 */
	public String getGlobalDateFormat() {
		return globalDateFormat;
	}
	
	/**
	 * Get global configured time format
	 * @return
	 */
	public String getGlobalTimeFormat() {
		return globalTimeFormat;
	}

	/**
	 * Add attachments to a booking
	 * @param id
	 */
	public void addAttachments(String bookingId) {
		try {
			if (bookingId != null) {
				booking = new Booking(this, bookingId, session);
				if (booking.getBookingDoc() != null) {
					if ((isUserAllowed && interpreterDocKey != null && interpreterDocKey.equalsIgnoreCase(booking.getInterpreterDocKey())) || isLspAllowedToAccessAll || (interpreterFullName!=null && interpreterFullName.equalsIgnoreCase(booking.getInterpreterFullName()) 
						&& interpreterDocKey.equalsIgnoreCase(booking.getInterpreterDocKey()))) {
						if (booking.getStatus().equals(Constants.bookingStatusBooked) || booking.getStatus().equals(Constants.bookingStatusServiceDelivered) || booking.getStatus().equals(Constants.bookingStatusCompleted)) {
							booking.setNowDateTime(nowDateTime);
							booking.setNowDateTimeFormatted(nowDateTimeFormatted);
							StringBuilder bookingLog = new StringBuilder();
							Document bookingDoc = booking.getBookingDoc();
							if (bookingDoc != null) {
								AttachmentHelper attachmentHelper = new AttachmentHelper(session);
								StringBuilder strLog = new StringBuilder();
								int fileCount = 0;
								for (Entry<String, Object> tmpEntry : params.entrySet()) {
								    if (tmpEntry.getValue() instanceof UploadedFile) {
								    	String filenameUnique = "File_" + Globals.createUniqueName();
	                                    if (fileCount > 0) {
	                                        filenameUnique = filenameUnique  + "_" + fileCount;
	                                    }
										File file = attachmentHelper.createFile(tmpEntry.getValue(), filenameUnique);
										Attachment attachment = new Attachment(file);
										booking.addPbdAttachment(attachment);
										if (strLog.length() == 0) {
											strLog.append(file.getName());
										} else {
											strLog.append(", " + file.getName());
										}
										attachmentHelper.deleteTempFile(file);
										fileCount++;
								    }
								}
								try {
									if (strLog != null) {
										bookingLog.append("Booking updated with these details: Attachments: " + strLog.toString());
									}
									if (booking.getInterpreterType().equalsIgnoreCase(Constants.InterpreterTypeInHouse)) {
										bookingLog.append(" - by Interpreter: " + interpreterFullName);
									} else {
										bookingLog.append(" - by " + agencyLabel + ": " + getClientName());
									}	
									Globals.LogActionToDocument(nowDateTime, bookingDoc, bookingLog.toString(), false, session);
									bookingDoc.computeWithForm(true, false);
									bookingDoc.save();
								} catch (NotesException e) {
									e.printStackTrace();
								}
							}
						} else {
							httpResponse = 400;
							messageCode.add(3041);
							isInValid = true;
						}
					} else {
						httpResponse = 403;
						messageCode.add(3034);
						isInValid = true;
					}
				} else {
					httpResponse = 404;
					messageCode.add(3001);
					isInValid = true;
				}
			}
		} catch (NotesException | IOException e) {
			e.printStackTrace();
		}
	}

	public String getLanguageServiceProvider() {
		return languageServiceProvider;
	}

	/**
	 * Get attachment in base64 encoded format in response JSON
	 * @param reqId
	 * @param attachmentId
	 */
	public void getAttachmentEncodedDetails(String id, String attachmentId) {
		JsonObject fileDetails = null;
		try {
			if (id != null) {
				booking = new Booking(this, id, session);
				if (booking.getBookingDoc() != null) {
					if ((isUserAllowed && interpreterDocKey != null && interpreterDocKey.equalsIgnoreCase(booking.getInterpreterDocKey())) 
							|| isLspAllowedToAccessAll 
							|| (interpreterFullName!=null && interpreterFullName.equalsIgnoreCase(booking.getInterpreterFullName())	&& interpreterDocKey.equalsIgnoreCase(booking.getInterpreterDocKey()))) {
						//If booking status is allowed or completed with YES or completed with NO and not cancelled OR give full access to allowed user or LSP
						if (booking.getStatus().equals(Constants.bookingStatusBooked) || booking.getStatus().equals(Constants.bookingStatusServiceDelivered) || booking.getStatus().equals(Constants.bookingStatusCompleted)) {
							fileDetails = booking.getAttachmentEncodedDetails(attachmentId);
							if (fileDetails != null && fileDetails.size() > 0) {
								dataJson.put("base64encoded", fileDetails.get("encoded"));
								dataJson.put("name", fileDetails.get("name"));
								dataJson.put("mimeType", fileDetails.get("mimeType"));
							} else {
								isInValid = true;
								httpResponse = 404;
							}
						}
					}
				}
			}
		} catch (IOException | NotesException e) {
			Log.writeError ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());	
			e.printStackTrace();
		}
	}

	//Clean up all our Notes Objects
	public void recycle () {
		if (booking != null){
			booking.recycle();
		}
	}

	public boolean isLSPAllowedToCreateIgnoreValid() {
		return isLSPAllowedToCreateIgnoreValid;
	}

	public boolean isInValid() {
		return isInValid;
	}

	
	/**
	 * Send webhook by calling agent
	 * @param docBook
	 */
	public void sendWebhook() {
		try {
			boolean bWebhookEnabled = false;
			String strWebhookConfig = "";
			JsonArray jArrWebhook = new JsonArray();
			JsonObject jsonWebhook = new JsonObject();
			if (db != null) {
				strWebhookConfig = kwHandlerIMSDB.getKeywordValueString(Constants.KW_WEBHOOKS_CONFIG);
				if(!strWebhookConfig.isEmpty()) {
					jArrWebhook = Jsoner.deserialize(strWebhookConfig, new JsonArray());
					if(jArrWebhook != null){
						if(jArrWebhook.size() > 0){
							jsonWebhook = (JsonObject) jArrWebhook.get(0);
							if(jsonWebhook != null){
								if(jsonWebhook.get("enabled").toString().equalsIgnoreCase(Constants.YES)){
									bWebhookEnabled = true;
								}							
							}
						}					
					}
				}
				
				if (bWebhookEnabled && !booking.getStatus().equalsIgnoreCase(Constants.bookingStatusNew)) {
					
					Agent agtJson;
					try {
						agtJson = ImsHomeDb.getDatabase().getAgent(Constants.AGENT_SEND_WEBHOOK);
						Document docParam = ImsHomeDb.getDatabase().createDocument();
						docParam.replaceItemValue ("DocKey", booking.getId());
						docParam.replaceItemValue ("provider", booking.getInterpreterFullName());
						docParam.save();
						agtJson.run(docParam.getNoteID());
						docParam.replaceItemValue("Form" , "Param_Delete");
					    docParam.save();
					} catch (NotesException e) {
						e.printStackTrace();
					}
					/*Document docTemp; 
					String strID;
					docTemp = db.getProfileDocument("", booking.getBookingDoc().getUniversalID());
					docTemp.replaceItemValue ("DocKey", booking.getId());
					docTemp.replaceItemValue ("provider", booking.getInterpreterFullName());
					docTemp.save(true, false);
					strID = docTemp.getNoteID();
					docTemp.recycle();
					db.getAgent("agt_Send_Webhook_Data").run(strID);
					docTemp = db.getDocumentByID(strID);
					if(docTemp == null) {
						Globals.printCode("!!!!!!! CLASS: Globals.sendWebhookData() *** ERROR: Cannot Locate '(Agent - Send Webhook Data)' Agent document");
					}
					docTemp.remove(true);*/			
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
	