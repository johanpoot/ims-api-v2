package com.primaxis.ims.controller;

import com.primaxis.ims.api.ApiProvider;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.*;
import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;

import com.primaxis.ims.utils.ImsCDRDb;
import com.primaxis.ims.utils.Log;
import com.primaxis.ims.utils.Globals;
import com.primaxis.ims.utils.Constants;

public class CDRController extends ApiProvider {
	private Document cdrDoc = null;

	private ImsCDRDb cdrDB;
	private String phone_number;
	private String call_type;
	private String did_id;
	private String trunk_id;
	private String rule_id;
	private String acd_id; //queue
	private String agt_id; //agent
	private String camp_id;
	private String ext_number;
	private String call_start_time;
	private String call_queue_time;
	private String call_agent_time;
	private String call_hangup_time;
	private String call_status;
	private String hangup_reason;
	private String hangup_by;
	private String queue_record_name;
	
	private String call_id;
	private String incoming_number;
	private String outgoing_number;
	private String clientid;
	private String pin;
	private String authcode;
	private String call_method; //audio or video
	private String failover_info;
	
	
	
	private JsonObject intake_questions;
	private JsonArray nextOfKin;
	private JsonArray phoneNumbers;				
	private boolean isInValidPh = false;			
	private boolean isInValidStateCode = false;
	private boolean isInValidPostCode = false;
     
	/**
	 * 
	 * @param db
	 * @param jsonObjRequest
	 * @param apiLog
	 * @throws NotesException
	 * @throws IOException
	 */
	public CDRController(Database db, JsonObject jsonObjRequest, Log apiLog, HttpServletResponse response, Session session, String languageServiceProvider, String consumerName, Map<String, Object> params) throws NotesException, IOException {
		super(db, jsonObjRequest, apiLog, response, session, languageServiceProvider, consumerName, params);
		cdrDB = new ImsCDRDb (session);
	}

	/**
	 * Generate Response String
	 */
	public String generateResponse() {
		try {
			responseJson = new JsonObject();
			requestId = Globals.requestID();
			responseJson.put("code", httpResponse);
			responseJson.put("text", Globals.getHTTPResponseMsg(httpResponse));
			responseJson.put("success", success);
			responseJson.put("requestId", requestId);
			dataJson = new JsonObject();
			messageArray = new JsonArray();
			if (messageCode.size() > 0) {
				for (int code : messageCode) {
					JsonObject ob = new JsonObject();
					ob.put(Integer.toString(code), Globals.getMessageFromCode(code, session, kwHandlerIMSDB));
					if (!messageArray.contains(ob)) {
						messageArray.add(ob);
					}
				}
			}
			if (isInValid) {
				success = false;
				httpResponse = 400;
			} else if (call_id != null) {
				dataJson.put("call_id", call_id);
			}
			if (messageArray.size() > 0) {
				dataJson.put(isInValid ? "errors" : "messages", messageArray);
			}
			if (dataJson != null) {
				responseJson.put("data",  dataJson);
			}
			if (responseJson instanceof Jsonable) {
				responseJsonString = (String) Jsoner.serialize(responseJson);
			}
			Log.updateLogField("API_Success", String.valueOf(success));
		} catch (Exception e) {
			e.printStackTrace();
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());
		}
		
		return responseJsonString;
	}
	
	/**
	 * Validate and assign values from requested JSON to patient fields
	 * Ex JSON :

	 * @return boolean
	 * @throws NotesException 
	 */
	protected boolean getCDR() throws NotesException {
		try {
			//Reset all fields;
			phone_number = "";
			call_type = "";
			did_id = "";
			trunk_id = "";
			rule_id = "";
			acd_id = ""; //queue
			agt_id = ""; //agent
			camp_id = "";
			ext_number = "";
			call_start_time = "";
			call_queue_time = "";
			call_agent_time = "";
			call_hangup_time = "";
			call_status = "";
			hangup_reason = "";
			hangup_by = "";
			queue_record_name = "";
			
			call_id = "";
			incoming_number = "";
			outgoing_number = "";
			clientid = "";
			pin = "";
			authcode = "";
			call_method=""; //audio or video
			failover_info="";
			
			//nextOfKin = new JsonArray();
			//phoneNumbers = new JsonArray();	
			//Validate ID
			if (requestJson.get("call_type") == null || requestJson.get("call_type").equals("")) {
				isInValid = true;
				messageCode.add(4006);
			}  else {
				call_type = Globals.trimString(requestJson.get("call_type"));
			//	patientID = (s.length() > Globals.maxStringLength) ? s.substring(0,Globals.maxStringLength) : s;
			}			
			if (requestJson.get("trunk_id") != null) {
				trunk_id = Globals.validateString(requestJson.get("trunk_id"));}			
			if (requestJson.get("did_id") != null) {
				did_id = Globals.validateString(requestJson.get("did_id"));}			
			if (requestJson.get("rule_id") != null) {
				rule_id = Globals.validateString(requestJson.get("rule_id"));}			
			if (requestJson.get("acd_id") != null) {
				acd_id = Globals.validateString(requestJson.get("acd_id"));}			
			if (requestJson.get("agt_id") != null) {
				agt_id = Globals.validateString(requestJson.get("agt_id"));}			
			if (requestJson.get("camp_id") != null) {
				camp_id = Globals.validateString(requestJson.get("camp_id"));}			
			if (requestJson.get("ext_number") != null) {
				ext_number = Globals.validateString(requestJson.get("ext_number"));}			
			if (requestJson.get("call_start_time") != null) {
				call_start_time = Globals.validateString(requestJson.get("call_start_time"));}
			if (requestJson.get("call_queue_time") != null) {
				call_queue_time = Globals.validateString(requestJson.get("call_queue_time"));}			
			if (requestJson.get("call_agent_time") != null) {
				call_agent_time = Globals.validateString(requestJson.get("call_agent_time"));}			
			if (requestJson.get("call_hangup_time") != null) {
				call_hangup_time = Globals.validateString(requestJson.get("call_hangup_time"));}			
			if (requestJson.get("call_status") != null) {
				call_status = Globals.validateString(requestJson.get("call_status"));}			
			if (requestJson.get("hangup_reason") != null) {
				hangup_reason = Globals.validateString(requestJson.get("hangup_reason"));}			
			if (requestJson.get("hangup_by") != null) {
				hangup_by = Globals.validateString(requestJson.get("hangup_by"));}			
			if (requestJson.get("ext_number") != null) {
				ext_number = Globals.validateString(requestJson.get("ext_number"));}			
			if (requestJson.get("queue_record_name") != null) {
				queue_record_name = Globals.validateString(requestJson.get("queue_record_name"));}
			
			if (requestJson.get("call_id") != null) {
				call_id = Globals.validateString(requestJson.get("call_id"));}
			if (requestJson.get("incoming_number") != null) {
				incoming_number = Globals.validateString(requestJson.get("incoming_number"));}			
			if (requestJson.get("outgoing_number") != null) {
				outgoing_number = Globals.validateString(requestJson.get("outgoing_number"));}			
			if (requestJson.get("clientid") != null) {
				clientid = Globals.validateString(requestJson.get("clientid"));}			
			if (requestJson.get("pin") != null) {
				pin = Globals.validateString(requestJson.get("pin"));}			
			if (requestJson.get("authcode") != null) {
				authcode = Globals.validateString(requestJson.get("authcode"));}			
			if (requestJson.get("call_method") != null) {
				call_method = Globals.validateString(requestJson.get("call_method"));}			
			if (requestJson.get("failover_info") != null) {
				failover_info = Globals.validateString(requestJson.get("failover_info"));}			
				
			
			
			
			//Validate Gender
			/*
			gender = Globals.verifyGenderFormat(requestJson.get("sex").toString());
			if (requestJson.get("genderpreference") != null && !requestJson.get("genderpreference").equals("")) {
				genderPreference = Globals.validateJsonObject(requestJson.get("genderpreference"));
			}
			if (requestJson.get("addresses") != null && !requestJson.get("addresses").equals("")) {
				addresses = Globals.validateJsonArray(requestJson.get("addresses"));
			}
			if (requestJson.get("nextofkin") != null && !requestJson.get("nextofkin").equals("")) {
				nextOfKin = Globals.validateJsonArray(requestJson.get("nextofkin"));
			}
			if (requestJson.get("phonenumbers") != null && !requestJson.get("phonenumbers").equals("")) {
				phoneNumbers = Globals.validateJsonArray(requestJson.get("phonenumbers"));
			}*/
			return true;
		} catch (Exception e) {
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Process data for creating a patient record and add in database 
	 * 
	 */
	public void createCDR() {
		boolean createDoc = true;
		try {
			if (getCDR()) {
				Globals.printCode("isInValid="+isInValid);
					//Check if same ID exists, then update/overwrite patient details
					httpResponse = 200;
				/*	if (patientID != null) {					   
						patientDoc = cdrDB.getCDRDocumentByID(patientID);
					}
					*/
					if (cdrDoc == null) {		
						cdrDoc = cdrDB.getDatabase().createDocument();	
						createDoc = true;	
						httpResponse = 201;
					} else {
						createDoc = false;
						//removeFieldsBeforeUpdate();	
					}
					cdrDoc.replaceItemValue("Form", Constants.formCDR);
					cdrDoc.replaceItemValue("phone_number", phone_number);
					cdrDoc.replaceItemValue("call_type", call_type);
					cdrDoc.replaceItemValue("did_id", did_id);
					cdrDoc.replaceItemValue("trunk_id", trunk_id);
					cdrDoc.replaceItemValue("rule_id", rule_id);
					cdrDoc.replaceItemValue("acd_id", acd_id);
					cdrDoc.replaceItemValue("agt_id", agt_id);
					cdrDoc.replaceItemValue("camp_id", camp_id);
					cdrDoc.replaceItemValue("ext_number", ext_number);
					cdrDoc.replaceItemValue("call_start_time", call_start_time);
					cdrDoc.replaceItemValue("call_queue_time", call_queue_time);
					cdrDoc.replaceItemValue("call_hangup_time", call_hangup_time);
					cdrDoc.replaceItemValue("call_status", call_status);
					cdrDoc.replaceItemValue("hangup_reason", hangup_reason);
					cdrDoc.replaceItemValue("hangup_by", hangup_by);
					cdrDoc.replaceItemValue("queue_record_name", queue_record_name);
					
					cdrDoc.replaceItemValue("call_id", call_id);
					cdrDoc.replaceItemValue("incoming_number", incoming_number);					
					cdrDoc.replaceItemValue("outgoing_number", outgoing_number);
					cdrDoc.replaceItemValue("clientid", clientid);
					cdrDoc.replaceItemValue("pin", pin);
					cdrDoc.replaceItemValue("authcode", authcode);
					cdrDoc.replaceItemValue("call_method", call_method);
					cdrDoc.replaceItemValue("failover_info", failover_info);						
				   Globals.printCode("ready to save");
				   if (!isInValid) {
					   String clientName = kwHandlerIMSDB.getKeywordValueString(Constants.keywordClientName);
					   if (createDoc) {
						   Globals.LogActionToDocument(nowDateTime, cdrDoc, "Created document from IMS API - by " + clientName, true, session);
					   } else {
						   Globals.LogActionToDocument(nowDateTime, cdrDoc, "Updated document from IMS API - by " + clientName, false, session);
					   }
					   //cdrDoc.computeWithForm(true, false);
					   cdrDoc.save();
					  //patientDataDB.getDatabase().updateFTIndex(true);
					   error = false;
					   success = true;
				   } else {
					   error = true;
					   success = false;
					   httpResponse = 400;
				   }
			} else {
				error = true;
				success = false;
				httpResponse = 400;
			}
		} catch (Exception e) {
			Log.writeLine ("CLASS: " + this.getClass().getSimpleName() + " *** " + e.getMessage());		
			e.printStackTrace();
		}
	}
	
}
	
	
	